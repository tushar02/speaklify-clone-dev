// Import Firestore database
import {db} from '../firebase/Firebase-config';
import { useState  } from 'react';
import {collection, getDocs ,doc,getDoc} from 'firebase/firestore';
import Select from "react-select";
import { useEffect } from 'react';
const Hlo = () => {
    const [users , setusers] = useState([]);
    const [users1 , setusers1] = useState([]);
    const [users2 , setusers2] = useState([]);
    const [users3 , setusers3] = useState([]);
    const [users4 , setusers4] = useState([]);
    const [users5 , setusers5] = useState([]);
    const [rootvalue  , setrootvalue] = useState('');
    const [rootName  , setrootName] = useState([]);
    const [lessonName , setlessonName] = useState([]);
    const [topicName , settopicName] = useState([]);
    const [quizName , setquizName] = useState([]);
    const [poolName , setpoolName] = useState([]);
    const [lessonName1 , setlessonName1] = useState([]);
    const [topicName1 , settopicName1] = useState([]);
    const [quizName1 , setquizName1] = useState([]);
    const [poolName1 , setpoolName1] = useState([]);
    const [userrootname , setuserrootname] = useState('');
    const [userlessonname , setuserlessonname] = useState('');
    const [usertopicname , setusertopicname] = useState('');
    const [userquizname , setuserquizname] = useState('');
    const [quizvalue , setquizvalue] = useState('');
    const [quizvaluecolumn , setquizvaluecolumn] = useState('');
    const [userLessonType , setuserLessonType] = useState('');
    const [userTopicType , setuserTopicType] = useState('');
    const [userquestionPool , setuserquestionPool] = useState('');
    var root = [
        {
          value : 'Courses',
          label : "General Course"
        },
        {
            value : 'Grammar',
            label : 'Grammar'
        },
        {
         value : 'Practice',
         label : "Practice"
       },
       {
         value : 'video',
         label : "Video"
       },
      {
       value : 'Test',
       label : "Test"
      },
      {
       value : 'Story',
       label : "Story"
      }
      ];
      
      var[ lesson ,setlesson] = useState([
        {
          value : 'Courses',
          label : "Lesson",
         isDisabled : false
        },
        {
         value : "Objective",
         label : "Objective",
         isDisabled : false
       },
       {
         value : "Arcade",
         label : "Arcade",
         isDisabled : false
       }
      ]);
      var [ topic, settopic]= useState([
        {
          value : 'Lessons',
          label : "Topic",
          isDisabled : false
        },
        {
         value : 'Round',
         label : "Round",
         isDisabled : false
       }
      ]);
      const customStyles = {
        control: base => ({
          ...base,
          height: 35,
          minHeight: 35
        })
      };
        
        const getlessonType = async () => {
          const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            setusers1(docSnap.data());
            console.log("Document data:", docSnap.data());         
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
     }
          const getTopicType = async () => {
            const data =   doc(db,   `Lessons/${userlessonname}`);
            const docSnap = await getDoc(data);
  
            if (docSnap.exists()) {
              console.log("Document data:", docSnap.data());
              setusers2(docSnap.data());
            } else {
              console.log("No such document!");
            }
            }
         const getquespool = async () => {
                    const data =   doc(db,   `Quizs/${userquizname}`);
                    const docSnap = await getDoc(data);
          
                    if (docSnap.exists()) {
                      console.log("Document data:", docSnap.data());
                      setusers4(docSnap.data());
                      const data1 =   doc(db,   `QuestionPool/${users4.quizQuestionPool}`);
                    const docSnap1 = await getDoc(data1);
                    if(docSnap1.exists()){
                        console.log("Document data:", docSnap1.data());
                        setusers5(docSnap1.data());
                    }  else {
                        // doc.data() will be undefined in this case
                        console.log("No such document! user 5");
                      }
                    } else {
                      // doc.data() will be undefined in this case
                      console.log("No such document! user 4");
                    }
                    }
        const confirm = ()=>{
            setrootName([]);
            console.log(users.map((user) => { 
                const query = {value : user.courseId, label:user.courseName}
             setrootName(rootName => rootName.concat(query))}));
             
        };
        useEffect(()=>{
            if(rootvalue=='Courses' ||rootvalue=='Grammar'){
                setlesson((prev)=>{
                    return prev.filter((curvalue , idx)=>{
                        if(idx==1 ||idx==2){
                            curvalue.isDisabled=true;
                        }else{
                            curvalue.isDisabled=false;
                           }
                        return curvalue;
                    })
                }) 
                settopic((prev)=>{
                   return prev.filter((curvalue , idx)=>{
                       if(idx==1){
                           curvalue.isDisabled=true;
                       }else{
                        curvalue.isDisabled=false;
                       }
                       return curvalue;
                   })
               })
           }else if(rootvalue=='Practice'){
            setlesson((prev)=>{
                return prev.filter((curvalue , idx)=>{
                    if(idx==0){
                        curvalue.isDisabled=true;
                    }else{
                        curvalue.isDisabled=false;
                       }
                    return curvalue;
                })
            }) 
            settopic((prev)=>{
               return prev.filter((curvalue , idx)=>{
                   if(idx==0){
                       curvalue.isDisabled=true;
                   }else{
                    curvalue.isDisabled=false;
                   }
                   return curvalue;
               })
           })
           }else if(rootvalue=='Test'||rootvalue=='video'){
            setlesson((prev)=>{
                return prev.filter((curvalue , idx)=>{
                    if(idx==0||idx==1||idx==2){
                        curvalue.isDisabled=true;
                    }else{
                        curvalue.isDisabled=false;
                       }
                    return curvalue;
                })
            }) 
            settopic((prev)=>{
               return prev.filter((curvalue , idx)=>{
                   if(idx==0||idx==1){
                       curvalue.isDisabled=true;
                   }else{
                    curvalue.isDisabled=false;
                   }
                   return curvalue;
               })
           })
           }
        },[rootvalue])
        useEffect(() => {
            confirm();
          },[users]);
            useEffect(() => {
           getlessonType();
          },[userLessonType]);
          useEffect(() => {
            getlessonName();
           },[users1]);
           useEffect(() => {
            getTopicType();
           },[userTopicType]);
           useEffect(() => {
             getTopicName();
            },[users2]);
            useEffect(() => {
                quizsoln();
               },[users3]);
               useEffect(() => {
                getquespool();
                quespool();
            
               },[userquizname]);
            function getquestionPool(){
                getquespool();
               }
        const getlessonName = ()=>{
            setlessonName(users1.courseLessons);
            if(lessonName!=undefined){
            setlessonName1([]);
             console.log(lessonName.map((user) => { 
                const query = {value : user, label:user}
             setlessonName1(lessonName1 => lessonName1.concat(query))}));
            }
        };
        const getTopicName = ()=>{
            settopicName(users2.lessonTopics);
            if(topicName!=undefined){
               settopicName1([]);
               console.log(topicName.map((user) => { 
                  const query = {value : user, label:user}
               settopicName1(topicName1 => topicName1.concat(query))}));
          }
        }
          const getrootName = async() =>{
            const usercollectionRef = collection( db , rootvalue);
            const data = await getDocs(usercollectionRef);
            console.log(data);
            setusers(data.docs.map((doc) => ({...doc.data(),id: doc.id})));
        };
      const rootvaluefromuser = (e)=>{
         setrootvalue(e.value);
         setuserrootname();
         setuserLessonType('');
         setuserlessonname('');
         setusertopicname('');
         setuserquizname('');
         setuserLessonType('');
         setuserTopicType('');
         setuserquestionPool('');   
        
    }
      const rootnamefromuser = (e)=>{
        setuserrootname(e.value);
        setuserLessonType('');
        setuserlessonname('');
        setusertopicname('');
        setuserquizname('');
        setuserLessonType('');
        setuserTopicType('');
        setuserquestionPool('');

     };
     const setuserlesson = (e)=>{
        setuserlessonname(e.value);
        setusertopicname('');
        setuserquizname('');
        setuserTopicType('');
        setuserquestionPool('');
     };
     const setTopicValue = (e)=>{
         setusertopicname(e.value);
         setuserquestionPool('');
         setuserquizname('');

     }
     const setquizValue = (e)=>{
        setuserquizname(e.value);
        setuserquestionPool('');
        getquestionPool();
        
    }
     const getQuizdata= async ()=>{
        if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test' ||rootvalue=='Practice'){
            if(usertopicname!=''||rootvalue=='Practice'){
                const data =   doc(db,   `Topics/${usertopicname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
      
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }
            }else if(usertopicname==''&& userlessonname==''){
                const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }       
             }else if(usertopicname=='' && userlessonname!=''){
                const data =   doc(db,   `Lessons/${userlessonname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }
             }
     }
     }
     const quizsoln = ()=>{
         setquizName1([])
         if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test'||rootvalue=='Practice'){
            if(usertopicname!=''||rootvalue=='Practice'){
                setquizName(users3.topicQuiz);
                console.log(quizName.map((user) => { 
                   const query = {value : user, label:user}
                setquizName1(quizName1 => quizName1.concat(query))}));
            }else if(usertopicname=='' && userlessonname!=''){
                   setquizName(users3.lessonQuiz);
                   console.log(quizName.map((user) => { 
                      const query = {value : user, label:user}
                   setquizName1(quizName1 => quizName1.concat(query))})); 
            }else if(usertopicname==''&& userlessonname==''){
                setquizName(users3.courseFinalQuiz);
                   const query = {value : quizName, label:quizName}
                setquizName1(quizName1 => quizName1.concat(query));        
     }
}
     }
     const quespool = ()=>{
         setpoolName1([]);
        setpoolName(users5.questionPoolId);
           const query = {value : poolName, label:poolName}
        setpoolName1(poolName1 => poolName1.concat(query));
     };
     const setlessonType = (e)=>{
         setuserLessonType(e.value);
         setuserlessonname('');
        setusertopicname('');
        setuserquizname('');
        setuserTopicType('');
        setuserquestionPool('');
     }
     const setTopicType = (e)=>{
    setuserTopicType(e.value);
    setusertopicname('');
    setuserquizname('');
    setuserquestionPool('');
     }
     const setQuestionPool =(e)=>{
        setuserquestionPool(e.value);
     }
    return (<>
     <label> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <button onClick={ getrootName }> Get Data</button>
     <br /><br />
     <label> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
    <label> Choose the lesson Type : </label>
    <Select value={lesson.filter(function(option) {return option.value === userLessonType;})}  options={lesson} onChange={setlessonType} ></Select>
    <button onClick={getlessonName}> Get Data</button>
    <br /><br />
    <label> Choose the Lesson Name : </label>
    <Select value={lessonName1.filter(function(option) {return option.value === userlessonname;})} options={lessonName1} onChange={setuserlesson} ></Select>
    <br /><br />
    <label> Choose the Topic Type : </label>
    <Select value={topic.filter(function(option) {return option.value === userTopicType;})} options={topic} onChange={setTopicType} ></Select>
    <button onClick={ getTopicName }> Get Data</button>
    <br /><br />
    <label> Choose the Topic Name : </label>
    <Select  value={topicName1.filter(function(option) {return option.value === usertopicname;})} options={topicName1} onChange ={setTopicValue} ></Select>

    <br /><br /><br /><hr /><br />
    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button onClick={getQuizdata}>Confirm</button> </div>
    <br />
    <label> Choose the Quiz : </label>
    <Select value={quizName1.filter(function(option) {return option.value === userquizname;})} options={quizName1} onChange={setquizValue}></Select>
    <button onClick={quizsoln}>Show Data</button>
    <button onClick={ getquestionPool}>confirm</button>
    <br /><br />
    <label> Choose the Question Pool : </label>
    <Select value={poolName1.filter(function(option) {return option.value === userquestionPool;})} options={poolName1} onChange={setQuestionPool} ></Select>
    <button onClick={ quespool}>show data</button>
    <br /><br /><br /><br />
     {`          ${JSON.stringify(lesson)}   ${lesson[0].isDisabled} `}
        </>
    );
}
  
export default Hlo;
     /* {users.map((user) => { 
            return( 
            <div> 
              <h2 key={1}>Name : {user.courseId}  &emsp; Age :{user.courseName} </h2> 
            </div>
            );
            })}
            {lessonName.map((user1) => { 
            return( 
            <div> 
              <h2 key={1}>Name : {user1}</h2> 
            </div>
            );
            })} */
  /*const getUsers = async() =>{
            const usercollectionRef = collection( db , rootvalue);
            const data = await getDocs(usercollectionRef);
            console.log(data);
            setusers (data.docs.map((doc) => ({...doc.data(),id: doc.id})));           
        }; 
  
  
  const setrootname= ()=>{
            const getrootName = async() =>{
                const usercollectionRef = collection( db , rootvalue);
                const data = await getDocs(usercollectionRef);
                console.log(data);
                setusers (data.docs.map((doc) => ({...doc.data(),id: doc.id})));           
            };
            if(rootvalue=='Courses' ||rootvalue=='Grammar'){
                getrootName();
            }
          } */
/*  const getUsers1 = async() =>{
            const usercollectionRef = collection( db , rootvalue,'Courses/extraInfo');
            const data = await getDocs(usercollectionRef);
            console.log(data);
            setusers1 (data.docs.map((doc) => ({...doc.data(),id: doc.id})));           
        }; */
   /*  const usercollectionRef = collection( db , 'Courses'); */
           /*  const data = await getDocs(usercollectionRef).collection;
            console.log(data);
            db.collection('Courses').doc('Sample Course 1').collection('extraInfo') */
     /* const museums = query(collectionGroup(db, 'extraInfo'), where('courseDependencyId', '==', 'hi'));
const querySnapshot = await getDocs(museums); */
/* console.log(querySnapshot);
setusers1(querySnapshot.docs.map((doc) => ({...doc.data(),id: doc.id}))); */
/* querySnapshot.forEach((doc) => {
    console.log(doc.id, ' => ', doc.data());
}); */
       /*      const sfRef = db.collection('Courses').doc('Sample Course 1');
     const collections = await sfRef.listCollections();
         collections.forEach(collection => {
    console.log('Found subcollection with id:', collection.id);
     }); */
         /*     db.collection('Courses').doc('Sample Course 1').collection('extraInfo').get()
              .then(response => {
                const fetchedMovies = [];
                response.forEach(document => {
                  const fetchedMovie = {
                    id: document.id,
                    ...document.data()
                  };
                  fetchedMovies.push(fetchedMovie);
                });
                setMovies(fetchedMovies);
              })
              .catch(error => {
                console.log(error);
              }); */