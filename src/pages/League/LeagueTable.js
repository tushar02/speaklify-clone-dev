import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc,Timestamp, doc, deleteDoc, updateDoc , arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable} from '@firebase/storage';
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
import './league.css'
import { Button, Dialog, DialogActions } from '@mui/material';
import { DialogContent, DialogTitle } from '@material-ui/core';
import { Delete, Upload } from '@mui/icons-material';
function LeagueTable() {
    const [Open ,setOpen] = useState(false);
    const [Details, setDetails] = useState([]);
    const [Name , setName] = useState('');  
    const [isActiveFormUSer , setisActiveFormUSer] = useState(false);  
    const [image1, setImage1] = useState(null);
    const [file1 , setfile1] = useState(null);
    const[url1 , seturl1] = useState("");
    const [progress1 , setprogress1] = useState(0);
    const [StartDate , setStartDate] = useState('')
    const [EndDate , setEndDate] = useState('')
    const [Id , setId] = useState('');
    const [Reset ,setReset ] = useState(false);
  useEffect(() => { 
    const getUsers = async () => {
        const usercollectionRef = collection(db, "League")
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Open,Reset]); 
  
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async(i) => {
      if(Details[i].startDate.toDate()<=new Date()){
          alert("Date is over you can not edit")
      }else{
    setOpen(true);
    setId(Details[i].id);
    seturl1(Details[i].imageUrl);
        // Convert the backend date string to a Date object
        const backendDate = new Date(Details[i].endDate.toDate());

        // Adjust the date to the UTC timezone
        const utcDate = new Date(backendDate.getTime() - backendDate.getTimezoneOffset() * 60000);

        // Convert the adjusted date to a string format compatible with datetime-local input
        const formattedDate = utcDate.toISOString().slice(0, 16);
        setEndDate(formattedDate);
        const backendDateStart = new Date(Details[i].startDate.toDate());

        // Adjust the date to the UTC timezone
        const utcDateStart = new Date(backendDateStart.getTime() - backendDateStart.getTimezoneOffset() * 60000);

        // Convert the adjusted date to a string format compatible with datetime-local input
        const formattedDateStart = utcDateStart.toISOString().slice(0, 16);
        setStartDate(formattedDateStart);
 setisActiveFormUSer(Details[i].isActive);
}
  };
  const deleteImage1 = (file) =>{
    const desertRef = ref(storage, file);
   deleteObject(desertRef).then(() => {
     seturl1("");
     alert("deleted succefully")
   }).catch((error) => {
     console.log(error);
   }); 
   };
   const uploadFiles1 = (file)=>{
    if(!file) return;
    let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
      const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const  uploadTask = uploadBytesResumable(storageref,file);
    uploadTask.on("state_changed",(snapshot)=>{
       const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
       setprogress1(prog);
    },(err)=>console.log(err),()=>{
       getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
    });
 };
 const onImageChange1 = (event) => {
  if (event.target.files && event.target.files[0]) {
    setImage1(URL.createObjectURL(event.target.files[0]));
  }
   setfile1( event.target.files[0]);
 }
  function submit(e) {
    e.preventDefault();
  }
  const updateDataOnFirebase=async()=>{
    const Ref = doc(db, "League", Id);
    await updateDoc(Ref, {
        imageUrl:url1,
        isActive:isActiveFormUSer,
        startDate:Timestamp.fromDate(new Date(StartDate)),
        endDate:Timestamp.fromDate(new Date(EndDate)),
    });
      alert("data updated succefully");
      setOpen(false);
  }
  async function deleteDetails(i){
    if (window.confirm("Are you sure")) {    
        await deleteDoc(doc(db, `League/${Details[i].id}`))
    setReset(!Reset);
    alert("League Deleted Successfully");   
    }
  }
  return (
    <div className="App">
      <h2>League Data</h2>
      <table className='content-table'>
        <thead>
          <tr >
            <th style={{width: "10%"}}>S No.</th>
            <th style={{width: "50%"}}>Name</th>
            <th style={{width: "50%"}}>Edit/Delete</th>
          </tr>
        </thead>
        <tbody>
          {Details.map((details, i) => {
            return (
              <tr key={i} >
                <td  >{i + 1}</td>
                <td className='idName'>{details.id}
                  <br /><br />
               </td>
            
               <td>
               <button className='edit'  onClick={()=>handleOpen(i)}>Edit</button>
               <button className='delete' onClick={()=>deleteDetails(i)}>Delete</button>
               </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog onClose={handleClose} open={Open}>
        <DialogTitle>Update Data</DialogTitle>
        <DialogContent>
    <div>
      <form  onSubmit={submit} style={{fontFamily:"sans-serif"}} id="League-form" >
        <label style={{fontSize:"16px",fontWeight:"bold"}}>Name : {Name} </label>
              <label style={{ fontSize: "16px", fontWeight: "bold" }}>Start Date  :  </label> 
              <input defaultValue={StartDate} type="datetime-local" onChange={(event) => { setStartDate(event.target.value); }} />
        <br /><br />
        <label style={{fontSize:"16px",fontWeight:"bold"}}>End Date  :  </label> 
              <input defaultValue={EndDate} type="datetime-local" onChange={(event) => { setEndDate(event.target.value); }}  />
        <br /><br />
        <label  style={{fontSize:"16px",fontWeight:"bold"}}>isActive  : </label>
    <input value={isActiveFormUSer} type="checkbox" defaultChecked={isActiveFormUSer} onChange={(event) => { setisActiveFormUSer(!isActiveFormUSer) }} />
    <br /><br />
    <label style={{fontSize:"16px",fontWeight:"bold"}} >Image : </label>
    <input  type="file"  onChange={onImageChange1} />
    <span style={{fontSize:"16px",fontWeight:"bold"}} >uploaded {progress1}%</span><br/><br/>
    <img src={url1} alt="" width="193" height="130" /><br/>
        <Button startIcon={<Upload/>} onClick={()=> uploadFiles1(file1)}>upload</Button>
        <Button startIcon={<Delete/>}onClick={()=> deleteImage1(url1)}>Delete</Button>
        </form>
        </div>
        </DialogContent>
        <DialogActions>
        <Button type="submit" onClick={updateDataOnFirebase} >Submit</Button>
            <Button sx={{color:"red"}} onClick={handleClose}  >Cancel</Button>
        </DialogActions>
        </Dialog>
    </div>
  );
}
export default LeagueTable;