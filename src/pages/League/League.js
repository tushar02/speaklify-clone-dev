import React, { useState } from 'react'
import LeagueTable from './LeagueTable';
import LeagueForm from './LeagueForm';
import './league.css'
function League() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new League");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("back to League");
    }else{
     
      setvalue("Add new League");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<LeagueTable/>:<LeagueForm/>}
      </>
  )
}

export default League;