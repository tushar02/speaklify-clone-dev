import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, Timestamp, doc, deleteDoc, updateDoc, arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable } from '@firebase/storage';
import './league.css'
import { Accordion, AccordionDetails, AccordionSummary, Button, Dialog, DialogActions, Typography } from '@mui/material';
import { DialogContent, DialogTitle } from '@material-ui/core';
import { Delete, ExpandMoreSharp, Upload } from '@mui/icons-material';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { MdDelete } from "react-icons/md";
import { MdEdit } from "react-icons/md";
function DynamicLinkTable() {
  const [list, setList] = useState([]);
  const [update, setUpdate] = useState(false);
  const [Open, setOpen] = useState(false);
  const [Details, setDetails] = useState([]);
  const [Name, setName] = useState('');
  const [isActiveFormUSer, setisActiveFormUSer] = useState(false);
  const [index, setIndex] = useState(-1);
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [Id, setId] = useState('');
  const [Reset, setReset] = useState(false);
  const [urlString, setUrlString] = useState('');
  useEffect(() => {
    const getUsers = async () => {
      const usercollectionRef = collection(db, "dynamicLink")
      const data = await getDocs(usercollectionRef);
      console.log(data.docs[0].data().data);
      setList(data.docs[0].data().data);
    };
    getUsers();
  }, [Open, Reset, update]);

  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async (i) => {
    setOpen(true);
    setId(Details[i].id);
    setName(Details[i].name);
    seturl1(Details[i].imageUrl);
    setUrlString(Details[i].url);
  };
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef).then(() => {
      seturl1("");
      alert("deleted succefully")
    }).catch((error) => {
      console.log(error);
    });
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = (date.toString().replaceAll(" ", "")).substring(0, 20).replace(/[^a-z0-9 -]/gi, '');
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on("state_changed", (snapshot) => {
      const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes * 100));
      setprogress1(prog);
    }, (err) => console.log(err), () => {
      getDownloadURL(uploadTask.snapshot.ref).then(url => console.log(seturl1(url)));
    });
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  }
  function submit(e) {
    e.preventDefault();
  }
  const updateDataOnFirebase = async () => {
    const Ref = doc(db, "dynamicLink", Id);
    await updateDoc(Ref, {
      imageUrl: url1,
      url: urlString,
    });
    alert("data updated succefully");
    setOpen(false);
  }
  async function deleteDetails(i) {
    if (window.confirm("Are you sure")) {
      await deleteDoc(doc(db, `dynamicLink/${Details[i].id}`))
      setReset(!Reset);
      alert("Dynamic Link Deleted Successfully");
    }
  }
  function handleOnDragEnd(result) {
    console.log("🚀 ~ file: DynamicLinkTable.js:92 ~ handleOnDragEnd ~ result:", result)
    if (!result.destination) return;

    const items = Array.from(list);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    setList(items);
    saveOrder(items);

  }
  const saveOrder = async (items) => {
    const Ref = doc(db, "dynamicLink", "carouselDoc");
    await updateDoc(Ref, {
      data: items
    });
    setUpdate(update => !update);
  }
  const del = async (index) => {
    let tempList = list
    tempList.splice(index, 1);
    saveOrder(tempList);
  }
  const edit = async (index) => {

  }
  return (
    <div>

      <DragDropContext onDragEnd={handleOnDragEnd}>
        <Droppable droppableId="list">
          {(provided) => (
            <ul
              id="itemList"
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                display: "block",
                overflow: "hidden",
                width: "100%",
                listStyleType: "none",
                margin: "0px",
                padding: "0px",
              }}
            >
              {list.map((item, index) => (
                <Draggable key={String(index)} draggableId={String(index)} index={index}>
                  {(provided) => (
                    <li
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      key={index}
                    >
                      <Accordion sx={{ backgroundColor: "#308efe", margin: "10px" }} >
                        <AccordionSummary
                          expandIcon={<ExpandMoreSharp />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <Typography>Course Type : {item?.isCourse ? "Course" : "League"}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          {item?.isCourse ?
                            <>
                              <Typography> Course Id : {item?.courseId}</Typography>
                              <Typography> Course Type : {item?.courseType}</Typography>
                            </> :
                            <>
                              <Typography> League Id : {item?.leagueId}</Typography>
                            </>}
                          <img src={item?.bannerImageLink} alt="" />
                          <MdDelete
                            style={{ float: "right", margin: "20px" }}
                            size={28}
                            onClick={() => del(index)}
                          />
                          <MdEdit
                            style={{ float: "right", margin: "20px" }}
                            size={28}
                            onClick={() => edit(item)}
                          />

                        </AccordionDetails>
                      </Accordion>
                    </li>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </ul>
          )}
        </Droppable>
      </DragDropContext>
      <Dialog onClose={handleClose} open={Open}>
        <DialogTitle>Update Data</DialogTitle>
        <DialogContent>
          <div>
            <form onSubmit={submit} style={{ fontFamily: "sans-serif" }} id="League-form" >
              <label style={{ fontSize: "16px", fontWeight: "bold" }}>Name : {Name} </label>
              <br /><br />
              <label>Dynamic URl : </label>
              <input type="text" placeholder='Url' value={urlString} onChange={(e) => { setUrlString(e.target.value) }} />
              <br /><br />
              <label style={{ fontSize: "16px", fontWeight: "bold" }} >Image : </label>
              <input type="file" onChange={onImageChange1} />
              <span style={{ fontSize: "16px", fontWeight: "bold" }} >uploaded {progress1}%</span><br /><br />
              <img src={url1} alt="" width="193" height="130" /><br />
              <Button startIcon={<Upload />} onClick={() => uploadFiles1(file1)}>upload</Button>
              <Button startIcon={<Delete />} onClick={() => deleteImage1(url1)}>Delete</Button>
            </form>
          </div>
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={updateDataOnFirebase} >Submit</Button>
          <Button sx={{ color: "red" }} onClick={handleClose}  >Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
    // <div className="App">
    //   <h2> Dynamic Link Data</h2>
    //   <table className='content-table'>
    //     <thead>
    //       <tr >
    //         <th style={{width: "10%"}}>S No.</th>
    //         <th style={{width: "50%"}}>Name</th>
    //         <th style={{width: "50%"}}>Edit/Delete</th>
    //       </tr>
    //     </thead>
    //     <tbody>
    //       {Details.map((details, i) => {
    //         return (
    //           <tr key={i} >
    //             <td  >{i + 1}</td>
    //             <td className='idName'>{details.id}
    //               <br /><br />
    //            </td>

    //            <td>
    //            <button className='edit'  onClick={()=>handleOpen(i)}>Edit</button>
    //            <button className='delete' onClick={()=>deleteDetails(i)}>Delete</button>
    //            </td>
    //           </tr>
    //         );
    //       })}
    //     </tbody>
    //   </table>
    // </div>
  );
}
export default DynamicLinkTable;