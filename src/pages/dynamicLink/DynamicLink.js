import React, { useState } from 'react'
import DynamicLinkTable from './DynamicLinkTable';
import DynamicLinkForm from './DynamicLinkForm.js';
import './league.css'
function DynamicLink() {
  const [Reset, setReset] = useState(false);
  const [value, setvalue] = useState("Add new Dynamic Link");
  const change = () => {
    setReset(!Reset);
    if (Reset == false) {
      setvalue("back to Dynamic Link");
    } else {

      setvalue("Add new Dynamic Link");
    }
  }
  return (
    <>
      <button className='addButton' onClick={change}>{value}</button>
      {Reset == false ? <DynamicLinkTable /> : <DynamicLinkForm />}
    </>
  )
}

export default DynamicLink;