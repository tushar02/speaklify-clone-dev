import React, { useState } from 'react'
import { db, storage } from '../../firebase/Firebase-config';
import { getDownloadURL, uploadBytesResumable } from '@firebase/storage';
import { ref, deleteObject } from "firebase/storage";
import { arrayUnion, doc, setDoc, Timestamp, updateDoc } from "firebase/firestore";
import './league.css'
import { Select } from '@mui/material';
function DynamicLinkForm() {
  const [courseId, setCourseId] = useState('');
  const [leagueId, setLeagueId] = useState('');
  const [isCourse, setIsCourse] = useState(false);
  const [couseType , setCourseType] = useState("")
  const [urlString, setUrlString] = useState("")
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef).then(() => {
      seturl1("");
      alert("deleted succefully")
    }).catch((error) => {
      console.log(error);
    });
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = (date.toString().replaceAll(" ", "")).substring(0, 20).replace(/[^a-z0-9 -]/gi, '');
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on("state_changed", (snapshot) => {
      const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes * 100));
      setprogress1(prog);
    }, (err) => console.log(err), () => {
      getDownloadURL(uploadTask.snapshot.ref).then(url => console.log(seturl1(url)));
    });
  };

  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  }
  function submit(e) {
    e.preventDefault();
  }
  const submitDataOnFirebase = async () => {
    if (isCourse) {
      if (courseId == "") {
        alert("Please Enter Course Name");
        return;
      }
      if (couseType == "") {
        alert("Please Select Course Type");
        return;
      }
    } else {
      if (leagueId == "") {
        alert("Please enter League Id");
        return;
      }
    }
    const docData = {
      courseId: courseId,
      courseType: couseType,
      leagueId: leagueId,
      isCourse: isCourse,
      bannerImageLink : url1
    };
    const Ref = doc(db, "dynamicLink", "carouselDoc");
    await updateDoc(Ref, {
      data: arrayUnion(docData)
    });
    document.getElementById("League-form").reset();
    seturl1("");
    setCourseId("");
    setCourseType("");
    setLeagueId("");
    alert("Dynamic Link Created succefully");
  }
  return (
    <>
      <h1 className='heading'>Dynamic Link Form</h1>
      <form id="League-form" onSubmit={submit} >
        <label  >Banner Image : </label>
        <input type="file" onChange={onImageChange1} />
        <span >uploaded {progress1}%</span>
        <button onClick={() => uploadFiles1(file1)}>upload</button>
        <button onClick={() => deleteImage1(url1)}>Delete</button>
        <img src={url1} alt="" width="193" height="130" />
        <br /><br />
        <label>isCourse : </label>
        <input type="Checkbox" onChange={(e) => { setIsCourse(isCourse=>!isCourse) }} />
        {isCourse ? <>
        <br /><br />
          <label>Course Id : </label>
          <input type="text" placeholder=' Name' onChange={(e) => { setCourseId(e.target.value) }} />
          <br /><br />
          <label htmlFor="">Course Type : </label>
          <select onChange={(e) => {
            console.log("🚀 ~ file: DynamicLinkForm.js:89 ~ DynamicLinkForm ~ e:", e.target.value)
          
            setCourseType(e.target.value);
          }} >
            <option value="">Select Course Type</option>
            <option value="Course">Course</option>
            <option value="Grammar">Grammar</option>
            <option value="League">League</option>
            <option value="Test">Test</option>
            <option value="Audio">Audio</option>
            <option value="Video">Video</option>
            <option value="Story">Story</option>
          </select>
        </> :
          <>
            <br /><br />
            <label>League Id : </label>
            <input type="text" placeholder=' Name' onChange={(e) => { setLeagueId(e.target.value) }} />
          </>}
        <br /><br />

        <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button onClick={submitDataOnFirebase}>Submit</button> </div>
      </form>
    </>
  )
}

export default DynamicLinkForm