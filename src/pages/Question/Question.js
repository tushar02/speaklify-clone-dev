import React, { useState } from 'react'
import QuestionForm from './QuestionForm';
import QuestionTable from './QuestionTable';
import './question.css'

function Question() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Question");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Question");
    }else{
     
      setvalue("Add new Question");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<QuestionTable/>:<QuestionForm/>}
      </>
  )
}

export default Question;