import React, { useEffect, useState } from "react";
import {
  getDownloadURL,
  ref,
  uploadBytesResumable,
  deleteObject,
} from "@firebase/storage";
import {
  collection,
  getDocs,
  doc,
  getDoc,
  setDoc,
  updateDoc,
  arrayUnion,
  arrayRemove,
  increment,
} from "firebase/firestore";
import { storage, db } from "../../firebase/Firebase-config";
import Select from "react-select";
function QuestionForm() {
  const [value, setValue] = useState("");
  const [QuestionOption, setQuestionOption] = useState([{ option: "" }]);
  const [correctSequence, setCorrectSequence] = useState([{ option: "" }]);
  const [qusName, setqusName] = useState("");
  const [qusMode, setqusMode] = useState("");
  const [qusPointscore, setqusPointscoreMode] = useState(0);
  const [quscategoryDisable, setquscategoryDisable] = useState(false);
  const [hideTopic, sethideTopic] = useState("");
  const [hideLessonTopic, sethideLessonTopic] = useState(false);
  const [qusCorrectAnswer, setqusCorrectAnswer] = useState("");
  const [quswrongAnswer, setquswrongAnswer] = useState("");
  const [qusnooflosthearts, setqusnooflosthearts] = useState(0);
  const [qushint, sethint] = useState("");
  const [quscategory, setquscategory] = useState("");
  const [quscategorypoint, setquscategorypoint] = useState(0);
  const [qustype, setqustype] = useState("");
  const [qus, setqus] = useState("");
  const [qusInstruction, setqusInstruction] = useState("");
  const [PointorScore, setPointorScore] = useState("");
  const [RewardAd, setRewardAd] = useState(false);
  const [NoOfRewardAd, setNoOfRewardAd] = useState(0);
  const [Dictionarybubble, setDictionarybubble] = useState(false);
  const [qus4, setqus4] = useState({ tts: " ", answer: "" });
  const [qus10, setqus10] = useState({
    textForTTS: "",
    textForDisplay: "",
    passingScore: 0,
    noOfTries: 0,
  });
  const [qus1, setqus1] = useState({ options: "", answer: "" });
  const [qus11, setqus11] = useState({ text: "", answer: "" });
  const [qus8, setqus8] = useState({ text: "", answer: "" });
  const [qus5_3_7, setqus5_3_7] = useState([{ option: "", iscorrect: "" }]);
  const [qus5, setqus5] = useState({
    option1: "",
    option2: "",
    option3: "",
    option4: "",
  });
  const [qus3, setqus3] = useState({
    option1: "",
    option2: "",
    option3: "",
    option4: "",
  });
  const [qus0, setqus0] = useState({ option1: "", option2: "", option3: "" });
  const [qus5Answer, setqus5Answer] = useState("Option 1");
  const [qus3Answer, setqus3Answer] = useState({
    Listentext: "",
    answer: "Option 1",
  });
  const [qus7lhs, setqus7lhs] = useState({
    option1: "",
    option2: "",
    option3: "",
  });
  const [qus7rhs, setqus7rhs] = useState({
    option1: "",
    option2: "",
    option3: "",
  });
  const [qus6, setqus6] = useState({ textForTts: "", minScore: 0, noOfTries: 0 });
  const [qus2, setqus2] = useState({ word: "", correctImage: 0 });
  const [progress1, setprogress1] = useState(0);
  const [progress2, setprogress2] = useState(0);
  const [progress3, setprogress3] = useState(0);
  const [progress4, setprogress4] = useState(0);
  const [url1, seturl1] = useState("");
  const [url2, seturl2] = useState("");
  const [url3, seturl3] = useState("");
  const [url4, seturl4] = useState("");
  const [image1, setImage1] = useState(null);
  const [image2, setImage2] = useState(null);
  const [image3, setImage3] = useState(null);
  const [image4, setImage4] = useState(null);
  const [file1, setfile1] = useState(null);
  const [file2, setfile2] = useState(null);
  const [file3, setfile3] = useState(null);
  const [file4, setfile4] = useState(null);
  const [users, setusers] = useState([]);
  const [users1, setusers1] = useState([]);
  const [users2, setusers2] = useState([]);
  const [users3, setusers3] = useState([]);
  const [users4, setusers4] = useState([]);
  const [users5, setusers5] = useState([]);
  const [rootvalue, setrootvalue] = useState("");
  const [rootName, setrootName] = useState([]);
  const [lessonName, setlessonName] = useState([]);
  const [topicName, settopicName] = useState([]);
  const [quizName, setquizName] = useState([]);
  const [poolName, setpoolName] = useState([]);
  const [lessonName1, setlessonName1] = useState([]);
  const [topicName1, settopicName1] = useState([]);
  const [quizName1, setquizName1] = useState([]);
  const [poolName1, setpoolName1] = useState([]);
  const [userrootname, setuserrootname] = useState("");
  const [userlessonname, setuserlessonname] = useState("");
  const [usertopicname, setusertopicname] = useState("");
  const [userquizname, setuserquizname] = useState("");
  const [quizvalue, setquizvalue] = useState("");
  const [quizvaluecolumn, setquizvaluecolumn] = useState("");
  const [userLessonType, setuserLessonType] = useState("");
  const [userTopicType, setuserTopicType] = useState("");
  const [userquestionPool, setuserquestionPool] = useState("");
  const [dictionarybubbledisabled, setdictionarybubbledisabled] =
    useState(false);
  const addForm = () => {
    const query = { option: "" };
    setQuestionOption((QuestionOption) => QuestionOption.concat(query));
    setCorrectSequence((correctSequence) => correctSequence.concat(query));
  };
  const deleteForm = () => {
    setQuestionOption(QuestionOption.splice(0, QuestionOption.length - 1, 1));
    setCorrectSequence(
      correctSequence.splice(0, correctSequence.length - 1, 1)
    );
  };
  var root = [
    {
      value: "Courses",
      label: "General Course",
    },
    {
      value: "Grammar",
      label: "Grammar",
    },
    {
      value: "Practice",
      label: "Practice",
    },
    {
      value: "Test",
      label: "Test",
    },
    {
      value: "Story",
      label: "Story",
    },
  ];

  var [lesson, setlesson] = useState([
    {
      value: "Courses",
      label: "Lesson",
      isDisabled: false,
    },
    {
      value: "Objective",
      label: "Objective",
      isDisabled: false,
    },
    {
      value: "Arcade",
      label: "Arcade",
      isDisabled: false,
    },
  ]);
  var [topic, settopic] = useState([
    {
      value: "Lessons",
      label: "Topic",
      isDisabled: false,
    },
    {
      value: "Round",
      label: "Round",
      isDisabled: false,
    },
  ]);

  const getlessonType = async () => {
    const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setusers1(docSnap.data());
      console.log("Document data:", docSnap.data());
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
    }
  };
  const getTopicType = async () => {
    const data = doc(db, `Lessons/${userlessonname}`);
    const docSnap = await getDoc(data);

    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      setusers2(docSnap.data());
    } else {
      console.log("No such document!");
    }
  };
  const getquespool = async () => {
    const data = doc(db, `Quizs/${userquizname}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      setusers4(docSnap.data());
      const data1 = doc(db, `QuestionPool/${docSnap.data().quizQuestionPool}`);
      const docSnap1 = await getDoc(data1);
      if (docSnap1.exists()) {
        console.log("Document data:", docSnap1.data());
        setusers5(docSnap1.data());
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document! user 5");
      }
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document! user 4");
    }
  };
  const confirm = () => {
    setrootName([]);
    console.log(
      users.map((user) => {
        const query = { value: user.courseId, label: user.courseId };
        setrootName((rootName) => rootName.concat(query));
      })
    );
  };
  useEffect(() => {
    if (rootvalue == "Courses" || rootvalue == "Grammar") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setqusMode("QuizMode");
      setPointorScore("Score");
      setdictionarybubbledisabled(false);
    } else if (rootvalue == "Practice") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else if (rootvalue == "Test") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setqusMode("QuizMode");
      setPointorScore("Score");
      setdictionarybubbledisabled(false);
    } else if (rootvalue == "video") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    }
    else if(rootvalue=='Story'){
      setlesson((prev)=>{
          return prev.filter((curvalue , idx)=>{
              if(idx==2||idx==1){
                  curvalue.isDisabled=true;
              }else{
                  curvalue.isDisabled=false;
                 }
              return curvalue;
          })
      })
     }
  }, [rootvalue]);
  useEffect(() => {
    confirm();
  }, [users]);
  useEffect(() => {
    getlessonType();
    if (userLessonType == "Objective") {
      setqusMode("ObjectiveMode");
      setPointorScore("Point");
      setdictionarybubbledisabled(false);
    } else if (userLessonType == "Arcade") {
      setqusMode("ArcadeMode");
      setPointorScore("Score");
      setdictionarybubbledisabled(true);
    }
  }, [userLessonType]);
  useEffect(() => {
    getlessonName();
  }, [users1]);
  useEffect(() => {
    getTopicType();
  }, [userTopicType]);
  useEffect(() => {
    getTopicName();
  }, [users2]);
  useEffect(() => {
    quizsoln();
  }, [users3]);
  useEffect(() => {
    getquespool();
    quespool();
  }, [userquizname]);
  function getquestionPool() {
    getquespool();
  }
  const getlessonName = () => {
    if (userLessonType == "Arcade") {
      if (users1.courseFinalArcade != undefined) {
        setlessonName1([]);
        setlessonName1([
          { value: users1.courseFinalArcade, label: users1.courseFinalArcade },
        ]);
      }
    } else {
      setlessonName(users1.courseLessons);
      if (lessonName != undefined) {
        setlessonName1([]);
        console.log(
          lessonName.map((user) => {
            const query = { value: user, label: user };
            setlessonName1((lessonName1) => lessonName1.concat(query));
          })
        );
      }
    }
  };
  const getTopicName = () => {
    settopicName(users2.lessonTopics);
    if (topicName != undefined) {
      settopicName1([]);
      console.log(
        topicName.map((user) => {
          const query = { value: user, label: user };
          settopicName1((topicName1) => topicName1.concat(query));
        })
      );
    }
  };
  const getrootName = async () => {
    const usercollectionRef = collection(db, rootvalue);
    const data = await getDocs(usercollectionRef);
    console.log(data);
    setusers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    setuserrootname("");
    setuserLessonType("");
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserLessonType("");
    setuserTopicType("");
    setuserquestionPool("");
    if (e.value == "Test") {
      sethideLessonTopic("none");
      sethideTopic("none");
    } else if (e.value == "Story") {
      sethideLessonTopic("none");
      sethideTopic("none");
    } else {
      sethideLessonTopic(false);
      sethideTopic(false);
    }
  };
  const rootnamefromuser = (e) => {
    setuserrootname(e.value);
    setuserLessonType("");
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserLessonType("");
    setuserTopicType("");
    setuserquestionPool("");
  };
  const setuserlesson = (e) => {
    setuserlessonname(e.value);
    setusertopicname("");
    setuserquizname("");
    setuserTopicType("");
    setuserquestionPool("");
  };
  const setTopicValue = (e) => {
    setusertopicname(e.value);
    setuserquestionPool("");
    setuserquizname("");
  };
  const setquizValue = (e) => {
    setuserquizname(e.value);
    setusers5([]);
    setpoolName1([]);
    setuserquestionPool("");
    getquestionPool();
  };
  const getQuizdata = async () => {
    if (
      rootvalue == "Courses" ||
      rootvalue == "Grammar" ||
      rootvalue == "Story" ||
      rootvalue == "Test" ||
      rootvalue == "Practice"
    ) {
      if (usertopicname != "" || rootvalue == "Practice") {
        const data = doc(db, `Topics/${usertopicname}/extraInfo/infoDoc`);
        const docSnap = await getDoc(data);
        if (docSnap.exists()) {
          console.log("Document data:", docSnap.data());
          setusers3(docSnap.data());
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      } else if (usertopicname == "" && userlessonname == "") {
        const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
        const docSnap = await getDoc(data);
        if (docSnap.exists()) {
          console.log("Document data:", docSnap.data());
          setusers3(docSnap.data());
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      } else if (usertopicname == "" && userlessonname != "") {
        const data = doc(db, `Lessons/${userlessonname}/extraInfo/infoDoc`);
        const docSnap = await getDoc(data);
        if (docSnap.exists()) {
          console.log("Document data:", docSnap.data());
          setusers3(docSnap.data());
          console.log(docSnap.data());
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }
    }
  };

  const quizsoln = () => {
    setquizName1([]);
    if (
      rootvalue == "Courses" ||
      rootvalue == "Grammar" ||
      rootvalue == "Story" ||
      rootvalue == "Test" ||
      rootvalue == "Practice"
    ) {
      if (usertopicname != "" || rootvalue == "Practice") {
        setquizName(users3.topicQuiz);
        console.log(
          quizName.map((user) => {
            const query = { value: user, label: user };
            setquizName1((quizName1) => quizName1.concat(query));
          })
        );
      } else if (usertopicname == "" && userlessonname != "") {
        setquizName(users3.lessonQuiz);
        console.log(
          quizName.map((user) => {
            const query = { value: user, label: user };
            setquizName1((quizName1) => quizName1.concat(query));
          })
        );
      } else if (usertopicname == "" && userlessonname == "") {
        setquizName(users3.courseFinalQuiz);
        const query = { value: quizName, label: quizName };
        setquizName1((quizName1) => quizName1.concat(query));
      }
    }
  };
  const quespool = () => {
    setpoolName1([]);
    // setpoolName(users5.questionPoolId);
    const query = {
      value: users5.questionPoolId,
      label: users5.questionPoolId,
    };
    setpoolName1((poolName1) => poolName1.concat(query));
  };
  const setlessonType = (e) => {
    setuserLessonType(e.value);
    if (e.value == "Arcade") {
      setquscategoryDisable(false);
    } else {
      setquscategoryDisable(true);
      setquscategory("No");
      setquscategorypoint(0);
    }
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserTopicType("");
    setuserquestionPool("");
  };
  const setTopicType = (e) => {
    setuserTopicType(e.value);
    setusertopicname("");
    setuserquizname("");
    setuserquestionPool("");
  };
  const setQuestionPool = (e) => {
    setuserquestionPool(e.value);
  };
  var Imagetype = [
    {
      value: 1,
      label: "Image 1",
    },
    {
      value: 2,
      label: "Image 2",
    },
    {
      value: 3,
      label: "Image 3",
    },
  ];
  var optiontype = [
    {
      value: 1,
      label: "option 1",
    },
    {
      value: 2,
      label: "Option 2",
    },
    {
      value: 3,
      label: "Option 3",
    },
  ];
  const [qus0Image, setqus0Image] = useState({});
  const [qus0option, setqus0option] = useState({});
  const added = (e) => {
    setqus0Image(Array.isArray(e) ? e.map((x) => x.label) : []);
  };
  const added1 = (e) => {
    setqus0option(Array.isArray(e) ? e.map((x) => x.label) : []);
  };
  const [pair1, setpair1] = useState("");
  const [pair2, setpair2] = useState("");
  const [pair3, setpair3] = useState("");

  const createPair = () => {
    setpair1(qus0Image[0] + ";" + qus0option[0]);
    setpair2(qus0Image[1] + ";" + qus0option[1]);
    setpair3(qus0Image[2] + ";" + qus0option[2]);
  };
  const clearPair = () => {
    setpair1("");
    setpair2("");
    setpair3("");
  };
  var lhstype = [
    {
      value: 1,
      label: "LHS 1",
    },
    {
      value: 2,
      label: "LHS 2",
    },
    {
      value: 3,
      label: "LHS 3",
    },
  ];
  var rhstype = [
    {
      value: 1,
      label: "RHS 1",
    },
    {
      value: 2,
      label: "RHS 2",
    },
    {
      value: 3,
      label: "RHS 3",
    },
  ];
  const [qus7lhstype, setqus7lhstype] = useState({});
  const [qus7rhstype, setqus7rhstype] = useState({});
  const addedlhs = (e) => {
    setqus7lhstype(Array.isArray(e) ? e.map((x) => x.label) : []);
  };
  const added1rhs = (e) => {
    setqus7rhstype(Array.isArray(e) ? e.map((x) => x.label) : []);
  };
  const createPairLhsRhs = () => {
    setpair1(qus7lhstype[0] + ";" + qus7rhstype[0]);
    setpair2(qus7lhstype[1] + ";" + qus7rhstype[1]);
    setpair3(qus7lhstype[2] + ";" + qus7rhstype[2]);
  };
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...qus5_3_7];
    list[index][name] = value;
    setqus5_3_7(list);
  };

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    const list = [...qus5_3_7];
    list.splice(index, 1);
    setqus5_3_7(list);
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  };
  const onImageChange2 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage2(URL.createObjectURL(event.target.files[0]));
    }
    setfile2(event.target.files[0]);
  };
  const onImageChange3 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage3(URL.createObjectURL(event.target.files[0]));
    }
    setfile3(event.target.files[0]);
  };
  const onImageChange4 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage4(URL.createObjectURL(event.target.files[0]));
    }
    setfile4(event.target.files[0]);
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    if (!file) return;
    let flag = true;
    var reader = new FileReader();

    //Read the contents of Image File.
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;

      //Validate the File Height and Width.
      image.onload = function () {
        var height = this.height;
        var width = this.width;
        if (height != 512 && width != 512) {
          alert("Height and Width must not equal 512*512.");
          flag = false;
          return false;
        } else {
          let date = new Date();
          let ModifiedDate = date
            .toString()
            .replaceAll(" ", "")
            .substring(0, 20)
            .replace(/[^a-z0-9 -]/gi, "");
          const storageref = ref(
            storage,
            `/files/${file.name}_${ModifiedDate}`
          );
          const uploadTask = uploadBytesResumable(storageref, file);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setprogress1(prog);
            },
            (err) => console.log(err),
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((url) =>
                console.log(seturl1(url))
              );
            }
          );
        }
      };
    };
  };
  const uploadFiles2 = (file) => {
    if (!file) return;
    if (!file) return;
    let flag = true;
    var reader = new FileReader();

    //Read the contents of Image File.
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;

      //Validate the File Height and Width.
      image.onload = function () {
        var height = this.height;
        var width = this.width;
        if (height != 512 && width != 512) {
          alert("Height and Width must not equal 512*512.");
          flag = false;
          return false;
        } else {
          let date = new Date();
          let ModifiedDate = date
            .toString()
            .replaceAll(" ", "")
            .substring(0, 20)
            .replace(/[^a-z0-9 -]/gi, "");
          const storageref = ref(
            storage,
            `/files/${file.name}_${ModifiedDate}`
          );
          const uploadTask = uploadBytesResumable(storageref, file);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setprogress2(prog);
            },
            (err) => console.log(err),
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((url) =>
                console.log(seturl2(url))
              );
            }
          );
        }
      };
    };
  };
  const uploadFiles3 = (file) => {
    if (!file) return;
    if (!file) return;
    let flag = true;
    var reader = new FileReader();

    //Read the contents of Image File.
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;

      //Validate the File Height and Width.
      image.onload = function () {
        var height = this.height;
        var width = this.width;
        if (height != 512 && width != 512) {
          alert("Height and Width must not equal 512*512.");
          flag = false;
          return false;
        } else {
          let date = new Date();
          let ModifiedDate = date
            .toString()
            .replaceAll(" ", "")
            .substring(0, 20)
            .replace(/[^a-z0-9 -]/gi, "");
          const storageref = ref(
            storage,
            `/files/${file.name}_${ModifiedDate}`
          );
          const uploadTask = uploadBytesResumable(storageref, file);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setprogress3(prog);
            },
            (err) => console.log(err),
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((url) =>
                console.log(seturl3(url))
              );
            }
          );
        }
      };
    };
  };
  const uploadFiles4 = (file) => {
    if (!file) return;
    if (!file) return;
    let flag = true;
    var reader = new FileReader();

    //Read the contents of Image File.
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;

      //Validate the File Height and Width.
      image.onload = function () {
        var height = this.height;
        var width = this.width;
        if (height != 512 && width != 512) {
          alert("Height and Width must not equal 512*512.");
          flag = false;
          return false;
        } else {
          let date = new Date();
          let ModifiedDate = date
            .toString()
            .replaceAll(" ", "")
            .substring(0, 20)
            .replace(/[^a-z0-9 -]/gi, "");
          const storageref = ref(
            storage,
            `/files/${file.name}_${ModifiedDate}`
          );
          const uploadTask = uploadBytesResumable(storageref, file);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setprogress4(prog);
            },
            (err) => console.log(err),
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((url) =>
                console.log(seturl4(url))
              );
            }
          );
        }
      };
    };
  };
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl1("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const deleteImage2 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl2("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const deleteImage3 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl3("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const deleteImage4 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl4("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleScramble = () => {
    setQuestionOption((prev) => {
      let res = scrambleArr(prev);
      console.log(res);
      return res;
    });
  };
  const scrambleArr = (arr) => {
    console.log(arr[0]);
    let res = Array.from(arr);
    return res.sort((a, b) => 0.5 - Math.random());
  };
  function submit(e) {
    e.preventDefault();
  }
  let questionCategoryTypeEnumfromuser;
  let questionTypeEnum;
  let questionCategoryPointfromuser=0;
  let managequestionData;
  let mode =0;
  let quscategorypointAsInt = parseInt(quscategorypoint, 10);
  let qusnooflostheartsAsInt = parseInt(qusnooflosthearts, 10);
  let NoOfRewardAdAsInt = parseInt(NoOfRewardAd, 10);
  let qusPointscoreAsInt = parseInt(qusPointscore, 10);

  // let date = new Date();
  // let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20);
  // let questionIdfromuser = (qusName+"_"+ModifiedDate);

  let date = new Date();
  let ModifiedDate = date
    .toString()
    .replaceAll(" ", "")
    .substring(0, 20)
    .replace(/[^a-z0-9 -]/gi, "")
    .toLowerCase();

  let final = qusName.toLowerCase().replace(/[^a-z0-9 -]/gi, "");
  let newfinaldata = final.replace(/\s+/, "");
  //console.log(newfinaldata+ModifiedDate)
  let questionIdfromuser = newfinaldata + ModifiedDate;

  const storedDataOnFirebase = async () => {
    if (qusMode == "ObjectiveMode") {
      mode = 0;
    } else if (qusMode == "ArcadeMode") {
      mode = 1;
    } else if (qusMode == "QuizMode") {
      mode = 2;
    }

    if (PointorScore == "Point") {
      questionCategoryPointfromuser = 0;
    } else if (PointorScore == "Score") {
      questionCategoryPointfromuser = 1;
    }
    if (quscategory == "No") {
      questionCategoryTypeEnumfromuser = 0;
    } else if (quscategory == "Spelling") {
      questionCategoryTypeEnumfromuser = 1;
    } else if (quscategory == "Grammer") {
      questionCategoryTypeEnumfromuser = 2;
    } else if (quscategory == "Pronunciation") {
      questionCategoryTypeEnumfromuser = 3;
    }

    if (qustype == "Type what you hear") {
      questionTypeEnum = 4;
      let questionDatafromUser = {
        answer: qus4.tts,
        question: qus,
        questionInstruction: qusInstruction,
        tts: qus4.answer,
      };
      managequestionData = questionDatafromUser;
    } else if (qustype == "Choose the correct option") {
      let setoptionFromuser;
      if (qus5Answer == "Option 1") {
        let optionsFromUser = [
          { iscorrect: true, option: qus5.option1 },
          { iscorrect: false, option: qus5.option2 },
          { iscorrect: false, option: qus5.option3 },
          { iscorrect: false, option: qus5.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus5Answer == "Option 2") {
        let optionsFromUser = [
          { iscorrect: false, option: qus5.option1 },
          { iscorrect: true, option: qus5.option2 },
          { iscorrect: false, option: qus5.option3 },
          { iscorrect: false, option: qus5.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus5Answer == "Option 3") {
        let optionsFromUser = [
          { iscorrect: false, option: qus5.option1 },
          { iscorrect: false, option: qus5.option2 },
          { iscorrect: true, option: qus5.option3 },
          { iscorrect: false, option: qus5.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus5Answer == "Option 4") {
        let optionsFromUser = [
          { iscorrect: false, option: qus5.option1 },
          { iscorrect: false, option: qus5.option2 },
          { iscorrect: false, option: qus5.option3 },
          { iscorrect: true, option: qus5.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      }
      let questionDatafromUser = {
        options: setoptionFromuser,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;

      questionTypeEnum = 5;
    } else if (qustype == "Read Listen Speak") {
      let qusNoOfTriesAsInt = parseInt(qus10.noOfTries, 10);
      let qusPassingScoreAsInt = parseInt(qus10.passingScore, 10);
      let questionDatafromUser = {
        noOfTries: qusNoOfTriesAsInt,
        passingScore: qusPassingScoreAsInt,
        question: qus,
        questionInstruction: qusInstruction,
        textForDisplay: qus10.textForDisplay,
        textForTTS: qus10.textForTTS,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 10;
    } else if (qustype == "Fill in the sequence") {
      const myArray = qus1.options.split(";");
      let answer = "";
      let option = [];
      for (var i = 0; i < QuestionOption.length; i++) {
        option.push(QuestionOption[i].option);
        answer += correctSequence[i].option + " ";
      }
      answer = answer.trim();
      let questionDatafromUser = {
        answer: answer,
        options: option,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 1;
    } else if (qustype == "Identify the image") {
      let qusMinScoreAsInt = parseInt(qus6.minScore, 10);
      let qusNoOfTriesAsInt = parseInt(qus6.noOfTries, 10);
      let questionDatafromUser = {
        imageLink: url1,
        minScore: qusMinScoreAsInt,
        noOfTries: qusNoOfTriesAsInt,
        question: qus,
        questionInstruction: qusInstruction,
        textForTts: qus6.textForTts,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 6;
    } else if (qustype == "Choose the correct image of the given word") {
      let qusCorrectImageAsInt = parseInt(qus2.correctImage, 10);
      let imageOptionFromUser = [url1, url2, url3, url4];
      let questionDatafromUser = {
        correctImage: qusCorrectImageAsInt,
        imageOptions: imageOptionFromUser,
        question: qus,
        questionInstruction: qusInstruction,
        word: qus2.word,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 2;
    } else if (qustype == "Fill in the blank (type)") {
      const myAnswer = [];
      const myoption = qus8.answer;
      for (var i = 0; i < QuestionOption.length; i++) {
        myAnswer.push(QuestionOption[i].option);
      }
      let questionDatafromUser = {
        answer: myAnswer,
        text: myoption,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 8;
    } else if (qustype == "Fill in the blank (voice)") {
      const myAnswer = qus11.text.split(";");
      const myoption = qus.answer.split(";");
      let questionDatafromUser = {
        answer: myAnswer,
        text: myoption,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 11;
    } else if (qustype == "Pairing (words to words)") {
      var res1 = qus7lhstype[0].charAt(qus7lhstype[0].length - 1);
      var lhs1 = parseInt(res1, 10) - 1;
      var res2 = qus7rhstype[0].charAt(qus7rhstype[0].length - 1);
      var rhs1 = parseInt(res2, 10) - 1;
      var res3 = qus7lhstype[1].charAt(qus7lhstype[1].length - 1);
      var lhs2 = parseInt(res3, 10) - 1;
      var res4 = qus7rhstype[1].charAt(qus7rhstype[1].length - 1);
      var rhs2 = parseInt(res4, 10) - 1;
      var res5 = qus7lhstype[2].charAt(qus7lhstype[2].length - 1);
      var lhs3 = parseInt(res5, 10) - 1;
      var res6 = qus7rhstype[2].charAt(qus7rhstype[2].length - 1);
      var rhs3 = parseInt(res6, 10) - 1;
      let pairFromUser = [
        { lhsId: lhs1, rhsId: rhs1 },
        { lhsId: lhs2, rhsId: rhs2 },
        { lhsId: lhs3, rhsId: rhs3 },
      ];
      let lhsFromUser = [qus7lhs.option1, qus7lhs.option2, qus7lhs.option3];
      let rhsFromUser = [qus7rhs.option1, qus7rhs.option2, qus7rhs.option3];
      let questionDatafromUser = {
        LHS: lhsFromUser,
        RHS: rhsFromUser,
        pair: pairFromUser,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 7;
    } else if (qustype == "Pairing (word to images)") {
      let ans1;
      let ans2;
      let ans3;
      if (qus0Image[0] == "Image 1") {
        var res = qus0option[0].charAt(qus0option[0].length - 1);
        ans1 = parseInt(res, 10) - 1;
      } else if (qus0Image[1] == "Image 1") {
        var res = qus0option[1].charAt(qus0option[1].length - 1);
        ans1 = parseInt(res, 10) - 1;
      } else if (qus0Image[2] == "Image 1") {
        var res = qus0option[2].charAt(qus0option[2].length - 1);
        ans1 = parseInt(res, 10) - 1;
      }
      if (qus0Image[0] == "Image 2") {
        var res = qus0option[0].charAt(qus0option[0].length - 1);
        ans2 = parseInt(res, 10) - 1;
      } else if (qus0Image[1] == "Image 2") {
        var res = qus0option[1].charAt(qus0option[1].length - 1);
        ans2 = parseInt(res, 10) - 1;
      } else if (qus0Image[2] == "Image 2") {
        var res = qus0option[2].charAt(qus0option[2].length - 1);
        ans2 = parseInt(res, 10) - 1;
      }
      if (qus0Image[0] == "Image 3") {
        var res = qus0option[0].charAt(qus0option[0].length - 1);
        ans3 = parseInt(res, 10) - 1;
      } else if (qus0Image[1] == "Image 3") {
        var res = qus0option[1].charAt(qus0option[1].length - 1);
        ans3 = parseInt(res, 10) - 1;
      } else if (qus0Image[2] == "Image 3") {
        var res = qus0option[2].charAt(qus0option[2].length - 1);
        ans3 = parseInt(res, 10) - 1;
      }
      let imageOptionFromUser = [
        { answer: ans1, imageLink: url1 },
        { answer: ans2, imageLink: url2 },
        { answer: ans3, imageLink: url3 },
      ];
      let testOptionFromUser = [
        { option: qus0.option1 },
        { option: qus0.option2 },
        { option: qus0.option3 },
      ];
      let questionDatafromUser = {
        imageoptions: imageOptionFromUser,
        textoptions: testOptionFromUser,
        question: qus,
        questionInstruction: qusInstruction,
      };
      managequestionData = questionDatafromUser;
      questionTypeEnum = 0;
    } else if (qustype == "Tap what you hear") {
      let setoptionFromuser;
      if (qus3Answer.answer == "Option 1") {
        let optionsFromUser = [
          { iscorrect: true, option: qus3.option1 },
          { iscorrect: false, option: qus3.option2 },
          { iscorrect: false, option: qus3.option3 },
          { iscorrect: false, option: qus3.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus3Answer.answer == "Option 2") {
        let optionsFromUser = [
          { iscorrect: false, option: qus3.option1 },
          { iscorrect: true, option: qus3.option2 },
          { iscorrect: false, option: qus3.option3 },
          { iscorrect: false, option: qus3.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus3Answer.answer == "Option 3") {
        let optionsFromUser = [
          { iscorrect: false, option: qus3.option1 },
          { iscorrect: false, option: qus3.option2 },
          { iscorrect: true, option: qus3.option3 },
          { iscorrect: false, option: qus3.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      } else if (qus3Answer.answer == "Option 4") {
        let optionsFromUser = [
          { iscorrect: false, option: qus3.option1 },
          { iscorrect: false, option: qus3.option2 },
          { iscorrect: false, option: qus3.option3 },
          { iscorrect: true, option: qus3.option4 },
        ];
        setoptionFromuser = optionsFromUser;
      }
      let questionDatafromUser = {
        listenText: qus3Answer.Listentext,
        options: setoptionFromuser,
        question: qus,
        questionInstruction: qusInstruction,
      };
      console.log(JSON.stringify(qus3Answer.answer));
      managequestionData = questionDatafromUser;
      questionTypeEnum = 3;
    }
    const docData = {
      assoLessonId: userlessonname,
      assoParentId: userrootname,
      assoQuizId: userquizname,
      assoTopicId: usertopicname,
      associatedRootType: rootvalue,
      associatedLessonType: userLessonType,
      associatedTopicType: userTopicType,
      mode: mode,
      questionCategoryPoint: quscategorypointAsInt,
      questionCategoryTypeEnum: questionCategoryTypeEnumfromuser,
      questionCorrectMessage: qusCorrectAnswer,
      questionData: managequestionData,
      questionDictionaryBubble: Dictionarybubble,
      questionHasRewardAd: true,
      questionHeartFromReward: 3,
      questionHeartTxn: qusnooflostheartsAsInt,
      questionHint: qushint,
      questionId: questionIdfromuser,
      questionPointOrScore: questionCategoryPointfromuser,
      questionPoints: qusPointscoreAsInt,
      questionPoolId: userquestionPool,
      questionType: questionTypeEnum,
      questionWrongMessage: quswrongAnswer,
    };
    console.log(docData);
    await setDoc(doc(db, "Questions", questionIdfromuser), docData);
    const washingtonRef = doc(db, "QuestionPool", userquestionPool);
    if (qustype == "Type what you hear") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForTypeWhatYouHear: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Choose the correct option") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForMcq: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Read Listen Speak") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForReadListenSpeak: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Fill in the sequence") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForFillTheSequence: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Identify the image") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForIdentifyTheImage: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Choose the correct image of the given word") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForCorrectImageFromWord: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Fill in the blank (type)") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForFillTheBlanksType: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Fill in the blank (voice)") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForFillTheBlanksVoice: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Pairing (words to words)") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForWordsToWords: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Pairing (word to images)") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForWordsToImage: increment(1),
        availQuestionsInPool: increment(1),
      });
    } else if (qustype == "Tap what you hear") {
      await updateDoc(washingtonRef, {
        questionsId: arrayUnion(questionIdfromuser),
        availQuestionsForTapWhatYouHear: increment(1),
        availQuestionsInPool: increment(1),
      });
    }
    alert("data saved successfully");
    document.getElementById("question-form").reset();
    setDictionarybubble(false);
    setRewardAd(false);
    setrootName([]);
    setCorrectSequence([]);
    setQuestionOption([]);
    setlessonName1([]);
    settopicName1([]);
    setpoolName1([]);
    setquizName1([]);
    seturl1("");
    seturl2("");
    seturl3("");
    seturl4("");
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Question Form</h1>

      <label> Choose the Root : </label>
      <Select
        value={root.filter(function (option) {
          return option.value === rootvalue;
        })}
        options={root}
        onChange={rootvaluefromuser}
      ></Select>
      <button onClick={getrootName}> Get Data</button>
      <br />
      <br />
      <label> Choose the Root Name : </label>
      <Select
        value={rootName.filter(function (option) {
          return option.value === userrootname;
        })}
        options={rootName}
        onChange={rootnamefromuser}
      ></Select>
      <br />
      <br />
      <div style={{ display: hideTopic }}>
        <label> Choose the lesson Type : </label>
        <Select
          value={lesson.filter(function (option) {
            return option.value === userLessonType;
          })}
          options={lesson}
          onChange={setlessonType}
        ></Select>
        <button onClick={getlessonName}> Get Data</button>
        <br />
        <br />
        <label> Choose the Lesson Name : </label>
        <Select
          value={lessonName1.filter(function (option) {
            return option.value === userlessonname;
          })}
          options={lessonName1}
          onChange={setuserlesson}
        ></Select>
        <br />
        <br />
      </div>

      <div style={{ display: hideLessonTopic }}>
        <label> Choose the Topic Type : </label>
        <Select
          value={topic.filter(function (option) {
            return option.value === userTopicType;
          })}
          options={topic}
          onChange={setTopicType}
        ></Select>
        <button onClick={getTopicName}> Get Data</button>
        <br />
        <br />
        <label> Choose the Topic Name : </label>
        <Select
          value={topicName1.filter(function (option) {
            return option.value === usertopicname;
          })}
          options={topicName1}
          onChange={setTopicValue}
        ></Select>
      </div>

      <br />
      <br />
      <br />
      <hr />
      <br />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {" "}
        <button onClick={getQuizdata}>Confirm</button>{" "}
      </div>
      <br />
      <label> Choose the Quiz : </label>
      <Select
        value={quizName1.filter(function (option) {
          return option.value === userquizname;
        })}
        options={quizName1}
        onChange={setquizValue}
      ></Select>
      <button onClick={quizsoln}>Show Data</button>
      <br />
      <br />
      <label> Choose the Question Pool : </label>
      <Select
        value={poolName1.filter(function (option) {
          return option.value === userquestionPool;
        })}
        options={poolName1}
        onChange={setQuestionPool}
      ></Select>
      <button onClick={quespool}>show data</button>
      <br />
      <br />
      <form id="question-form" onSubmit={submit}>
        <div>
          <label>Question Name : </label>
          <input
            required
            type="text"
            placeholder="Question Name"
            onChange={(event) => {
              setqusName(event.target.value);
            }}
          />
          <br />
          <br />
          <label>Associated Question Pool : </label>{" "}
          <span>{`${userquestionPool}`}</span>
          <br />
          <br />
          <label>Question Mode : </label>
          <span>{`${qusMode}`}</span>
          <br /> <br />
          <label>Point or Score : </label>
          <span>{`${PointorScore}`}</span>
          <br />
          <br />
          <label>Point/Score for the Question : </label>
          <input
            type="number"
            required
            style={{ width: "50px" }}
            min="0"
            placeholder="Point/Score for the Question"
            onChange={(event) => {
              setqusPointscoreMode(event.target.value);
            }}
          />
          {/* <br />
          <br />
          <label>Will there be any Reward Ad ? : </label>
          <input
            type="checkbox"
            defaultChecked={RewardAd}
            onChange={(event) => {
              setRewardAd(!RewardAd);
            }}
          />
          <br />
          <br />
          <label>No of Hearts from Reward : </label>
          <input
            type="number"
            required
            style={{ width: "50px" }}
            placeholder="No of Hearts from Reward"
            onChange={(event) => {
              setNoOfRewardAd(event.target.value);
            }}
          /> */}
          <br />
          <br />
          <label>Dictionary Bubble : </label>
          <input
            type="checkbox"
            disabled={dictionarybubbledisabled}
            defaultChecked={Dictionarybubble}
            onChange={(event) => {
              setDictionarybubble(!Dictionarybubble);
            }}
          />
          <br />
          <br />
          <label>Type of question : </label>
          <select
            required
            value={qustype}
            onChange={(event) => {
              setqustype(event.target.value);
            }}
          >
            <option value="">--------------</option>
            <option value="Type what you hear">Type what you hear</option>
            <option value="Choose the correct option">
              Choose the correct option
            </option>
            <option value="Read Listen Speak">Read Listen Speak</option>
            <option value="Fill in the sequence">Fill in the sequence</option>
            <option value="Identify the image">Identify the image</option>
            <option value="Choose the correct image of the given word">
              Choose the correct image of the given word
            </option>
            <option value="Fill in the blank (type)">
              Fill in the blank (type)
            </option>
            {/*   <option value="Fill in the blank (voice)">Fill in the blank (voice)</option> */}
            <option value="Pairing (words to words)">
              Pairing (words to words)
            </option>
            <option value="Pairing (word to images)">
              Pairing (word to images)
            </option>
            <option value="Tap what you hear">Tap what you hear</option>
          </select>
          <br />
          <br />
          <label>Instruction : </label>
          <textarea
            required
            cols="30"
            rows="10"
            placeholder="Instruction"
            onChange={(event) => {
              setqusInstruction(event.target.value);
            }}
          ></textarea>
          <br />
          <br />
          {qustype == "Type what you hear" ? (
            <>
              <label>Text for TTS : </label>
              <textarea
                required
                cols="30"
                rows="10"
                placeholder="Text For TTS"
                onChange={(e) => setqus4({ ...qus4, tts: e.target.value })}
              ></textarea>
              <br />
              <br />
              <label>Answer : </label>
              <textarea
                required
                cols="30"
                rows="10"
                placeholder="Answer"
                onChange={(event) => {
                  setqus4({ ...qus4, answer: event.target.value });
                }}
              ></textarea>
            </>
          ) : (
            " "
          )}
          {qustype == "Read Listen Speak" ? (
            <>
              <label>Text for TTS : </label>
              <textarea
                required
                cols="30"
                rows="10"
                type="text"
                placeholder="Text For TTS"
                onChange={(event) => {
                  setqus10({ ...qus10, textForTTS: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Text For Display : </label>
              <textarea
                required
                cols="30"
                rows="10"
                type="text"
                placeholder="Text For Display"
                onChange={(event) => {
                  setqus10({ ...qus10, textForDisplay: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Passing Score : </label>
              <input
                required
                type="text"
                placeholder="Passing Score"
                onChange={(event) => {
                  setqus10({ ...qus10, passingScore: event.target.value });
                }}
              />
              <br />
              <br />
              <label>No Of Tries : </label>
              <input
                required
                type="text"
                placeholder="No Of Tries "
                onChange={(event) => {
                  setqus10({ ...qus10, noOfTries: event.target.value });
                }}
              />
            </>
          ) : (
            " "
          )}
          {qustype == "Fill in the sequence" ? (
            <>
              {/* <label>Optios (Enter the otion seperated by semicolion) : </label>
          <textarea cols="30" rows="10" type="text" placeholder='Ex - you;where;going;are' onChange={(event) => {setqus1({ ...qus1, options: event.target.value})}} /> */}
              <br />
              <br />
              {QuestionOption.map((data, i) => {
                return (
                  <>
                    <br />
                    <label>Option : {i + 1} </label>
                    <input
                      required
                      maxlength="45"
                      type="text"
                      value={data.option}
                      placeholder={"Option"}
                      onChange={(event) => {
                        /*  data.pointOfContactName = event.target.value  */
                        setQuestionOption((prev) => {
                          return prev.filter((curvalue, idx) => {
                            if (idx == i) {
                              curvalue.option = event.target.value;
                            }
                            return curvalue;
                          });
                        });
                        setCorrectSequence((prev) => {
                          return prev.filter((curvalue, idx) => {
                            if (idx == i) {
                              curvalue.option = event.target.value;
                            }
                            return curvalue;
                          });
                        });
                      }}
                    />
                    <button type="button" onClick={deleteForm}>
                      {" "}
                      Delete
                    </button>
                    <br />
                  </>
                );
              })}{" "}
              <br /> <br />
              <button type="button" onClick={addForm}>
                Add Option
              </button>
              <br />
              <br />
              <button type="button" onClick={handleScramble}>
                Scramble
              </button>{" "}
              <br />
              <br />
              <label>Correct Sequence : </label>
              {correctSequence.map((data, i) => {
                return (
                  <>
                    <label>{data.option} </label>
                  </>
                );
              })}
              {/*   <textarea cols="30" rows="10" type="text" placeholder='Correct Sequence' onChange={(event) => {setqus1({ ...qus1,answer : event.target.value})}} /> */}
            </>
          ) : (
            " "
          )}
          {/*    {qustype== 'Fill in the blank (voice)'?   <>
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <textarea cols="30" rows="10" type="text" placeholder='Ex - you;where;going;are' onChange={(event) => {setqus11({ ...qus11,text : event.target.value})}} />
          <br /><br />
          <label>Text  : </label> 
          <textarea cols="30" rows="10" type="text" placeholder='Text For Display' onChange={(event) => {setqus11({ ...qus11,answer : event.target.value})}} />
      </>: " "} */}
          {qustype == "Fill in the blank (type)" ? (
            <>
              {/*  <label>Optios (Enter the otion seperated by semicolion) : </label>
          <textarea cols="30" rows="10" type="text" placeholder='Ex - you;where;going;are' onChange={(event) => {setqus8({ ...qus8,text : event.target.value})}} /> */}
              <br />
              <br />
              {QuestionOption.map((data, i) => {
                return (
                  <>
                    <br />
                    <label>Option : {i + 1} </label>
                    <input
                      required
                      maxlength="45"
                      type="text"
                      value={data.option}
                      placeholder={"Option"}
                      onChange={(event) => {
                        /*  data.pointOfContactName = event.target.value  */
                        setQuestionOption((prev) => {
                          return prev.filter((curvalue, idx) => {
                            if (idx == i) {
                              curvalue.option = event.target.value;
                            }
                            return curvalue;
                          });
                        });
                        setCorrectSequence((prev) => {
                          return prev.filter((curvalue, idx) => {
                            if (idx == i) {
                              curvalue.option = event.target.value;
                            }
                            return curvalue;
                          });
                        });
                      }}
                    />
                    <button type="button" onClick={deleteForm}>
                      {" "}
                      Delete
                    </button>
                    <br />
                  </>
                );
              })}{" "}
              <br /> <br />
              <button type="button" onClick={addForm}>
                Add Option
              </button>
              <br />
              <br />
              <label>Text : </label>
              <textarea
                required
                cols="30"
                rows="10"
                type="text"
                placeholder="where_you going"
                onChange={(event) => {
                  setqus8({ ...qus8, answer: event.target.value });
                }}
              />
            </>
          ) : (
            " "
          )}
          {qustype == "Choose the correct option" ? (
            <>
              <label>Option 1 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus5({ ...qus5, option1: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 2 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus5({ ...qus5, option2: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 3 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus5({ ...qus5, option3: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 4 : </label>
              <input
                required
                type="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus5({ ...qus5, option4: event.target.value });
                }}
              />
              <br />
              <br />
              <label> Choose the correct Option : </label>
              <select
                required
                value={qus5Answer}
                onChange={(event) => {
                  setqus5Answer(event.target.value);
                }}
              >
                <option value="">---------</option>
                <option value="Option 1">Option 1</option>
                <option value="Option 2">Option 2</option>
                <option value="Option 3">Option 3</option>
                <option value="Option 4">Option 4</option>
              </select>
            </>
          ) : (
            " "
          )}
          {qustype == "Tap what you hear" ? (
            <>
              <label>listen Text : </label>
              <textarea
                required
                cols="30"
                rows="10"
                type="text"
                placeholder="Listen Text"
                onChange={(event) => {
                  setqus3Answer({
                    ...qus3Answer,
                    Listentext: event.target.value,
                  });
                }}
              />
              <br /> <br />
              <label>Option 1 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus3({ ...qus3, option1: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 2 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus3({ ...qus3, option2: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 3 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus3({ ...qus3, option3: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Option 4 : </label>
              <input
                required
                type="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus3({ ...qus3, option4: event.target.value });
                }}
              />
              <br />
              <br />
              <label> Choose the correct Option : </label>
              <select
                required
                value={qus3Answer.answer}
                onChange={(event) => {
                  setqus3Answer({ ...qus3Answer, answer: event.target.value });
                }}
              >
                <option value="">------------</option>
                <option value="Option 1">Option 1</option>
                <option value="Option 2">Option 2</option>
                <option value="Option 3">Option 3</option>
                <option value="Option 4">Option 4</option>
              </select>
            </>
          ) : (
            " "
          )}
          {qustype === "Pairing (words to words)" ? (
            <>
              <label>LHS 1 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7lhs({ ...qus7lhs, option1: event.target.value });
                }}
              />
              &nbsp;
              <label>RHS 1 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7rhs({ ...qus7rhs, option1: event.target.value });
                }}
              />
              <br />
              <br />
              <label>LHS 2 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7lhs({ ...qus7lhs, option2: event.target.value });
                }}
              />
              &nbsp;
              <label>RHS 2 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7rhs({ ...qus7rhs, option2: event.target.value });
                }}
              />
              <br />
              <br />
              <label>LHS 3 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7lhs({ ...qus7lhs, option3: event.target.value });
                }}
              />
              &nbsp;
              <label>RHS 3 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus7rhs({ ...qus7rhs, option3: event.target.value });
                }}
              />
              <br />
              <br />
              <label> Choose the correct Pair : </label>
              <Select isMulti options={lhstype} onChange={addedlhs}></Select>
              <Select isMulti options={rhstype} onChange={added1rhs}></Select>
              <button type="button" onClick={createPairLhsRhs}>
                Add Pair
              </button>
              <button type="button" onClick={clearPair}>
                Reset Pair
              </button>
              <h3>Pair 1 - {` ${pair1}`} </h3>
              <h3>Pair 2 - {` ${pair2}`}</h3>
              <h3> Pair 3 - {` ${pair3} `}</h3>
            </>
          ) : (
            " "
          )}
          {qustype == "Pairing (word to images)" ? (
            <>
              <label>Image 1 :</label>
              <input required type="file" onChange={onImageChange1} />
              <button type="button" onClick={() => uploadFiles1(file1)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage1(url1)}>
                Delete
              </button>
              <img src={url1} alt="" width="193" height="130" />
              <label> Option 1 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus0({ ...qus0, option1: event.target.value });
                }}
              />
              <h3>uploaded {progress1}%</h3>
              <br />
              <br />
              <label>Image 2 :</label>
              <input required type="file" onChange={onImageChange2} />
              <button type="button" onClick={() => uploadFiles2(file2)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage2(url2)}>
                Delete
              </button>
              <img src={url2} alt="" width="193" height="130" />
              <label> Option 2 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus0({ ...qus0, option2: event.target.value });
                }}
              />
              <h3>uploaded {progress2}%</h3>
              <br />
              <br />
              <label>Image 3 :</label>
              <input required type="file" onChange={onImageChange3} />
              <button type="button" onClick={() => uploadFiles3(file3)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage3(url3)}>
                Delete
              </button>
              <img src={url3} alt="" width="193" height="130" />
              <label> Option 3 : </label>
              <input
                required
                name="text"
                placeholder="Enter Option"
                onChange={(event) => {
                  setqus0({ ...qus0, option3: event.target.value });
                }}
              />
              <h3>uploaded {progress3}%</h3>
              <label> Choose the correct Pair : </label>
              <Select isMulti options={Imagetype} onChange={added}></Select>
              <Select isMulti options={optiontype} onChange={added1}></Select>
              <button type="button" onClick={createPair}>
                Add Pair
              </button>
              <button type="button" onClick={clearPair}>
                Reset Pair
              </button>
              <h3>Pair 1 - {` ${pair1}`} </h3>
              <h3>Pair 2 - {` ${pair2}`}</h3>
              <h3> Pair 3 - {` ${pair3} `}</h3>
            </>
          ) : (
            " "
          )}
          {qustype == "Identify the image" ? (
            <>
              <label>Text for tts : </label>
              <input
                required
                type="text"
                placeholder="Ex - you;where;going;are"
                onChange={(event) => {
                  setqus6({ ...qus6, textForTts: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Minmium Score : </label>
              <input
                required
                type="number"
                style={{ width: "50px" }}
                min="0"
                onChange={(event) => {
                  setqus6({ ...qus6, minScore: event.target.value });
                }}
              />
              <br />
              <br />
              <label>No Of Tries : </label>
              <input
                required
                type="number"
                placeholder="No Of Tries "
                onChange={(event) => {
                  setqus6({ ...qus6, noOfTries: event.target.value });
                }}
              />
              <br />
              <br />
              <label>Image 1 :</label>
              <input required type="file" onChange={onImageChange1} />
              <button type="button" onClick={() => uploadFiles1(file1)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage1(url1)}>
                Delete
              </button>
              <img src={url1} alt="" width="193" height="130" />
              <h3>uploaded {progress1}%</h3>
            </>
          ) : (
            " "
          )}
          {qustype == "Choose the correct image of the given word" ? (
            <>
              <label>Words: </label>
              <textarea
                required
                cols="30"
                rows="10"
                type="text"
                placeholder="Ex - you;where;going;are"
                onChange={(event) => {
                  setqus2({ ...qus2, word: event.target.value });
                }}
              />
              <br />
              <br />
              <label> Choose the correct Image : </label>
              <select
                required
                value={qus5Answer.answer}
                onChange={(event) => {
                  setqus2({ ...qus2, correctImage: event.target.value });
                }}
              >
                <option value="0">Image1</option>
                <option value="1">Image2</option>
                <option value="2">Image3</option>
                <option value="3">Image4</option>
              </select>
              <br />
              <br />
              <label>Image 1 :</label>
              <input required type="file" onChange={onImageChange1} />
              <button type="button" onClick={() => uploadFiles1(file1)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage1(url1)}>
                Delete
              </button>
              <img src={url1} alt="" width="193" height="130" />
              <h3>uploaded {progress1}%</h3>
              <br />
              <br />
              <label>Image 2 :</label>
              <input required type="file" onChange={onImageChange2} />
              <button type="button" onClick={() => uploadFiles2(file2)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage2(url2)}>
                Delete
              </button>
              <img src={url2} alt="" width="193" height="130" />
              <h3>uploaded {progress2}%</h3>
              <br />
              <br />
              <label>Image 3 :</label>
              <input required type="file" onChange={onImageChange3} />
              <button type="button" onClick={() => uploadFiles3(file3)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage3(url3)}>
                Delete
              </button>
              <img src={url3} alt="" width="193" height="130" />
              <h3>uploaded {progress3}%</h3>
              <label>Image 4 :</label>
              <br />
              <br />
              <h3>uploaded {progress4}%</h3>
              <input required type="file" onChange={onImageChange4} />
              <button type="button" onClick={() => uploadFiles4(file4)}>
                upload
              </button>
              <button type="button" onClick={() => deleteImage4(url4)}>
                Delete
              </button>
              <img src={url4} alt="" width="193" height="130" />
            </>
          ) : (
            " "
          )}
          <br /> <br />
          <label>Question : </label>
          <textarea
            required
            cols="30"
            rows="10"
            placeholder="Question"
            onChange={(event) => {
              setqus(event.target.value);
            }}
          ></textarea>
          <br />
          <br />
          <label>Hint Message (maximum 95 characters) : </label>
          <textarea
            maxLength="95"
            required
            cols="30"
            rows="10"
            placeholder="Hints"
            onChange={(event) => {
              sethint(event.target.value);
            }}
          ></textarea>
          <br />
          <br />
          <label>System defined category for question : </label>
          <select
            required
            disabled={quscategoryDisable}
            value={quscategory}
            onChange={(event) => {
              setquscategory(event.target.value);
            }}
          >
            <option value="">--------</option>
            <option value="No">No</option>
            <option value="Spelling">Spelling</option>
            <option value="Grammer">Grammer</option>
            <option value="Pronunciation">Pronunciation</option>
          </select>
          <br />
          <br />
          <label>Category point : </label>
          <input
            required
            disabled={quscategoryDisable}
            value={quscategorypoint}
            type="number"
            style={{ width: "50px" }}
            min="0"
            placeholder="Category point"
            onChange={(event) => {
              setquscategorypoint(event.target.value);
            }}
          />
          <br />
          <br />
          <label>Message for Correct Answer (maximum 80 characters) : </label>
          <textarea
            maxLength="80"
            required
            cols="20"
            rows="5"
            placeholder="Message for correct answer"
            onChange={(event) => {
              setqusCorrectAnswer(event.target.value);
            }}
          ></textarea>
          <br />
          <br />
          <label>Message for Wrong Answer (maximum 80 characters) : </label>
          <textarea
            maxLength="80"
            required
            cols="20"
            rows="5"
            placeholder="Message for wrong answer"
            onChange={(event) => {
              setquswrongAnswer(event.target.value);
            }}
          ></textarea>
          <br />
          <br />
          <label>No of hearts lost after wrong answer : </label>
          <input
            type="number"
            required
            style={{ width: "50px" }}
            min="0"
            placeholder="No of hearts lost after wrong answer"
            onChange={(event) => {
              setqusnooflosthearts(event.target.value);
            }}
          />
          <br />
          <br />
          <button type="submit" onClick={storedDataOnFirebase}>
            {" "}
            Submit{" "}
          </button>
        </div>
      </form>

      {/*  <p>{`You selected ${Dictionarybubble} ${RewardAd} ${url2} ${url3}  ${url4} ${qus2.correctImage} ${qus2.word}  ${qusCorrectAnswer} ${qus5_3_7.length} ${qusnooflosthearts} ${qushint} ${qusPointscore} ${quscategory} ${quscategorypoint} ${qustype} ${qus} ${qusInstruction}`}</p>
       */}
      {/*    {
         `${qus4.tts}`
       } */}
      {/*  {`${JSON.stringify(qus3Answer.answer)}`} */}
    </>
  );
}

export default QuestionForm;
/*   function PointScoreCheckbox() {
    return (
      <>
        <div onChange={(event) => { setPointorScore(event.target.value); }}>
          <label>Points or Score : </label>
          <input type="radio" value="points" name="setpointscore" /> Points
          <input type="radio" value="score" name="setpointscore" />  Score
        </div>
      </>
    );
  }
  function RewardAdCheckbox() {
    return (
      <>
        <div onChange={(event) => { setRewardAd(event.target.value); }}>
          <label>If Reward Ad if heart 0 ? : </label>
          <input type="radio" value="point" name="rewardAd" /> Points
          <input type="radio" value="score" name="rewardAd" />  Score
        </div>
      </>
    );
  }
  function DictionaryCheckbox() {

    return (
      <>
        <div onChange={(event) => { setDictionarybubble(event.target.value); }}>
          <label>Dictionary Bubble : </label>
          <input type="radio" value="point" name="dictionaryb" /> Points
          <input type="radio" value="score" name="dictionaryb" />  Score
        </div>
      </>
    );
  }
  function transfer() {

    setj(qustype);
  }
  function QusType() {
     if(qustype=="Type what you hear"){
       console.log("rahy")
      switch (j) {
        case "Type what you hear":
    return (
      <>
        <label>Text for TTS : </label>
        <input type="text" value={qus4.tts} placeholder='Text For TTS' onChange={e => setqus4({ ...qus4, tts: e.target.value })} />
        <br /><br />
        <label>Answer  : </label>
        <input type="text" placeholder='Answer' onChange={(event) => { setqus4({ ...qus4, answer: event.target.value }) }} />
      </>
    );
      break;
     case "showHideLName":
      this.setState({ showHideLName: !this.state.showHideLName });
      break;
       default: 
    return(
   <>
   <h1>hello world</h1>
  </>
  );

  }

      }
      else if(qustype=="Read Listen Speak"){
        return(
          <>
          <label>Text for TTS : </label>
          <input type="text" placeholder='Text For TTS' onChange={(event) => {settexFortts_4_10(event.target.value); }} />
          <br /><br />
          <label>Text For Display  : </label> 
          <input type="text" placeholder='Text For Display' onChange={(event) => { settextFordisplay10(event.target.value); }} />
          <br /><br />
          <label>Passing Score  : </label> 
          <input type="text" placeholder='Passing Score' onChange={(event) => { setpassingScore10(event.target.value); }} />
          <br /><br />
          <label>No Of Tries  : </label> 
          <input type="text" placeholder='No Of Tries ' onChange={(event) => { setnoOftries10(event.target.value); }} />
      </>
        );
      }
      else if(qustype=="Fill in the sequence"){
        return(
          <>
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <input type="text" placeholder='Ex - you;where;going;are' onChange={(event) => {setoption1_8(event.target.value); }} />
          <br /><br />
          <label>Correct Sequence  : </label> 
          <input type="text" placeholder='Text For Display' onChange={(event) => { setAnswerForTTs4_1(event.target.value); }} />
      </>
        );
      }else if(qustype=="Fill in the blank (type)"){
        return(
          <>
          
          <label>Text  : </label> 
          <input type="text" placeholder='Text For Display' onChange={(event) => { setAnswerForTTs4_1(event.target.value); }} />
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <input type="text" placeholder='Ex - you;where;going;are' onChange={(event) => {setoption1_8(event.target.value); }} />
          <br /><br />
      </>
        );
      }
      else{
        return(
        <>
        <h1>hello world</h1>
        </>
        );
      }
  function updateData() {
    return (
      alert("data saved")
    )
  } */
