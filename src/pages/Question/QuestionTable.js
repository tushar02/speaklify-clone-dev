import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, doc, deleteDoc,getDoc, updateDoc, arrayRemove, increment } from 'firebase/firestore';
import Modal from '@material-ui/core/Modal';
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable} from '@firebase/storage';
import Select from 'react-select';
import './question.css'
import { Button, Dialog, TextField } from '@mui/material';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Delete, Upload } from '@mui/icons-material';
function QuestionTable() {
  const [QuestionDetails, setQuestionDetails] = useState([]);
  const [QuestionDetailsCopy, setQuestionDetailsCopy] = useState([]);
  const [rootvalue , setrootvalue] = useState('');
  const [rootName  , setrootName] = useState([]);
  const [lesson , setLesson]= useState([]);
  const [topic , setTopic]= useState([]);
  const [quiz , setQuiz]= useState([]);
  const [userrootname , setuserrootname] = useState('');
  const [lessonUser , setLessonUser] = useState('');
  const [quizUser , setQuizUser] = useState('');
  const [topicUser , setTopicUser] = useState('');
  const [Reset, setReset] = useState(false);
  const [open, setOpen] = useState(false);
  const [questionId, setquestionId] = useState('');
  const [questionCorrectMessageFromUser, setquestionCorrectMessageFromUser] = useState('');
  const [questionWorongMessageFromUser, setquestionWorongMessageFromUser] = useState('');
  const [questionHintsFromUser, setquestionHintsFromUser] = useState('');
  const [questionPointsFromUser, setquestionPointsFromUser] = useState('');
  const [questionHeartTxnFromUser, setquestionHeartTxnFromUser] = useState('');
  const [questionHeartFromRewardFromUser, setquestionHeartFromRewardFromUser] = useState('');
  const [questionCategoryPointsFromUser, setquestionCategoryPointsFromUser] = useState('');
  const [RewardAd, setRewardAd] = useState(false);
  const [quscategory, setquscategory] = useState('');
  const [QuestionType , setQuestionType] = useState('');
  const [qusInstruction , setqusInstruction] = useState('');
  const [qus , setqus] = useState('');
  const [qus4tts, setqus4tts] = useState('');
  const [qus4answer, setqus4answer] = useState('');
  const [qus10textForTTS, setqus10textForTTS] = useState('');
  const [qus10textForDisplay, setqus10textForDisplay] = useState('');
  const [qus10passingScore, setqus10passingScore] = useState(0);
  const [qus10noOfTries, setqus10noOfTries] = useState(0);
  const [qus1options , setqus1options] = useState('');
  const [qus1answer , setqus1answer] = useState('');
  const [qus11answer , setqus11answer] = useState('');
  const [qus11options , setqus11options] = useState('');
  const [qus8options , setqus8options] = useState('');
  const [qus8answer , setqus8answer] = useState('');
  const [qus5option1 ,setqus5option1] = useState('');
  const [qus5option2 ,setqus5option2] = useState('');
  const [qus5option3 ,setqus5option3] = useState('');
  const [qus5option4 ,setqus5option4] = useState('');
  const [qus5Answer ,setqus5Answer] = useState('');
  const [qus3option1 ,setqus3option1] = useState('');
  const [qus3option2 ,setqus3option2] = useState('');
  const [qus3option3 ,setqus3option3] = useState('');
  const [qus3option4 ,setqus3option4] = useState('');
  const [qus3Answer ,setqus3Answer] = useState('');
  const [qus3Listentext ,setqus3Listentext] = useState('');
  const [qus7lhsOption1, setqus7lhsOption1] = useState('');
  const [qus7lhsOption2, setqus7lhsOption2] = useState('');
  const [qus7lhsOption3, setqus7lhsOption3] = useState('');
  const [qus7rhsOption3, setqus7rhsOption3] = useState('');
  const [qus7rhsOption2, setqus7rhsOption2] = useState('');
  const [qus7rhsOption1, setqus7rhsOption1] = useState('');
  const [pair1, setpair1] = useState("");
  const [pair2, setpair2] = useState("");   
const [pair3, setpair3] = useState("");
const [progress1 , setprogress1] = useState(0);
const [progress2 , setprogress2] = useState(0);
const [progress3 , setprogress3] = useState(0);
const [progress4 , setprogress4] = useState(0);
 const[url1 , seturl1] = useState("");
 const[url2, seturl2] = useState("");
 const[url3 , seturl3] = useState("");
 const[url4 , seturl4] = useState("");
 const [image1, setImage1] = useState(null);
 const [image2, setImage2] = useState(null);
 const [image3, setImage3] = useState(null);
 const [image4, setImage4] = useState(null);
 const [file1 , setfile1] = useState(null);
 const [file2 , setfile2] = useState(null);
 const [file3 , setfile3] = useState(null);
 const [file4 , setfile4] = useState(null);
 const [qus0option1 ,setqus0option1] = useState('');
 const [qus0option2 ,setqus0option2] = useState('');
 const [qus0option3 ,setqus0option3] = useState('');
 const [qus6textForTts , setqus6textForTts] = useState('');
 const [qus6minScore ,setqus6minScore] = useState('');
 const [qus6minScoreNoOfTries ,setqus6minScoreNoOfTries] = useState('');
 const [qus2correctImage ,setqus2correctImage] = useState('');
 const [qus2word ,setqus2word] = useState('');
 var root = [
  {
    value : 'Courses',
    label : "General Course"
  },
  {
      value : 'Grammar',
      label : 'Grammar'
  },
  {
   value : 'Practice',
   label : "Practice"
 },
{
 value : 'Test',
 label : "Test"
},
{
 value : 'Story',
 label : "Story"
}
];
  var lhstype = [
    {
      value : 1,
      label : "LHS 1"
    },
    {
     value : 2,
     label : "LHS 2"
   },
   {
     value : 3,
     label : "LHS 3"
   }
  ];
  var rhstype = [
  {
   value : 1,
   label : "RHS 1"
  },
  {
  value : 2,
  label : "RHS 2"
  },
  {
  value : 3,
  label : "RHS 3"
  }
  ];
const [qus7lhstype, setqus7lhstype] = useState({});
const [qus7rhstype, setqus7rhstype] = useState({});
const addedlhs = (e)=>{
 setqus7lhstype(Array.isArray(e)?e.map(x=>x.label):[])
}
const added1rhs = (e)=>{
 setqus7rhstype(Array.isArray(e)?e.map(x=>x.label):[])
}
const createPairLhsRhs =()=>{
setpair1(qus7lhstype[0]+";"+qus7rhstype[0]);
setpair2(qus7lhstype[1]+";"+qus7rhstype[1]);
setpair3(qus7lhstype[2]+";"+qus7rhstype[2]);
}
const clearPair =()=>{
  setpair1("");
  setpair2("");
  setpair3("");
  }  
  var Imagetype = [
    {
      value : 1,
      label : "Image 1"
    },
    {
     value : 2,
     label : "Image 2"
   },
   {
     value : 3,
     label : "Image 3"
   }
];
var optiontype = [
 {
   value : 1,
   label : "option 1"
 },
 {
  value : 2,
  label : "Option 2"
},
{
  value : 3,
  label : "Option 3"
}
];
const [qus0Image, setqus0Image] = useState({});
const [qus0option, setqus0option] = useState({});
const added = (e)=>{
   setqus0Image(Array.isArray(e)?e.map(x=>x.label):[])
}
const added1 = (e)=>{
   setqus0option(Array.isArray(e)?e.map(x=>x.label):[])
}

const createPair =()=>{
setpair1(qus0Image[0]+";"+qus0option[0]);
setpair2(qus0Image[1]+";"+qus0option[1]);
setpair3(qus0Image[2]+";"+qus0option[2]);
}
  const usercollectionRef = collection(db, "Questions")
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setQuestionDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setQuestionDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset, open]);
  const handleClose = () => {
    setOpen(false);
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
     setfile1( event.target.files[0]);
   }
   const onImageChange2 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage2(URL.createObjectURL(event.target.files[0]));
    }
     setfile2( event.target.files[0]);
   }
   const onImageChange3 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage3(URL.createObjectURL(event.target.files[0]));
    }
     setfile3( event.target.files[0]);
   }
   const onImageChange4 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage4(URL.createObjectURL(event.target.files[0]));
    }
     setfile4( event.target.files[0]);
   }
    const uploadFiles1 = (file)=>{
       if(!file) return;
       if(!file) return;
      let flag = true;
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(file);
      reader.onload = function (e) {
      
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        //Validate the File Height and Width.
        image.onload = function () {
          var height = this.height;
          var width = this.width;
          if (height !=512 && width != 512) {
            alert("Height and Width must not equal 512*512.");
            flag = false;
            return false;
          }else{
       let date = new Date();
       let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
         const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
       const  uploadTask = uploadBytesResumable(storageref,file);
       uploadTask.on("state_changed",(snapshot)=>{
          const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
          setprogress1(prog);
       },(err)=>console.log(err),()=>{
          getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
       });
      }
    }
  }
    };
    const uploadFiles2 = (file)=>{
      if(!file) return;
      if(!file) return;
      let flag = true;
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(file);
      reader.onload = function (e) {
      
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        //Validate the File Height and Width.
        image.onload = function () {
          var height = this.height;
          var width = this.width;
          if (height !=512 && width != 512) {
            alert("Height and Width must not equal 512*512.");
            flag = false;
            return false;
          }else{
      let date = new Date();
      let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
        const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
      const  uploadTask = uploadBytesResumable(storageref,file);
      uploadTask.on("state_changed",(snapshot)=>{
         const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
         setprogress2(prog);
        },(err)=>console.log(err),()=>{
         getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl2(url)));
      });
    }
  }
}
   };
   const uploadFiles3 = (file)=>{
    if(!file) return;
    if(!file) return;
      let flag = true;
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(file);
      reader.onload = function (e) {
      
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        //Validate the File Height and Width.
        image.onload = function () {
          var height = this.height;
          var width = this.width;
          if (height !=512 && width != 512) {
            alert("Height and Width must not equal 512*512.");
            flag = false;
            return false;
          }else{
    let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
      const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const  uploadTask = uploadBytesResumable(storageref,file);
    uploadTask.on("state_changed",(snapshot)=>{
       const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
       setprogress3(prog);
      },(err)=>console.log(err),()=>{
       getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl3(url)));
    });
  }
}
      }
 };
 const uploadFiles4 = (file)=>{
  if(!file) return;
  if(!file) return;
      let flag = true;
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(file);
      reader.onload = function (e) {
      
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        //Validate the File Height and Width.
        image.onload = function () {
          var height = this.height;
          var width = this.width;
          if (height !=512 && width != 512) {
            alert("Height and Width must not equal 512*512.");
            flag = false;
            return false;
          }else{
  let date = new Date();
  let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
  const  uploadTask = uploadBytesResumable(storageref,file);
  uploadTask.on("state_changed",(snapshot)=>{
     const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
     setprogress4(prog);
    },(err)=>console.log(err),()=>{
     getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl4(url)));
  });
}
        }
      }
};
    const deleteImage1 = (file) =>{
 const desertRef = ref(storage, file);
deleteObject(desertRef).then(() => {
  seturl1("");
  alert("deleted succefully")
}).catch((error) => {
  console.log(error);
}); 
};
const deleteImage2 = (file) =>{
  const desertRef = ref(storage, file);
 deleteObject(desertRef).then(() => {
  seturl2(""); 
  alert("deleted succefully")
 }).catch((error) => {
   console.log(error);
 }); 
 };
 const deleteImage3 = (file) =>{
  const desertRef = ref(storage, file);
 deleteObject(desertRef).then(() => {
  seturl3(""); 
  alert("deleted succefully")
 }).catch((error) => {
   console.log(error);
 }); 
 };
 const deleteImage4 = (file) =>{
  const desertRef = ref(storage, file);
 deleteObject(desertRef).then(() => {
   seturl4("");
   alert("deleted succefully")
 }).catch((error) => {
   console.log(error);
 }); 
 };

  const handleOpen = (i) => {
    setOpen(true);
    console.log(QuestionDetails[i]);
    setquestionId(QuestionDetails[i].questionId);
    setquestionCorrectMessageFromUser(QuestionDetails[i].questionCorrectMessage);
    setquestionWorongMessageFromUser(QuestionDetails[i].questionWrongMessage);
    setquestionHintsFromUser(QuestionDetails[i].questionHint);
    setquestionPointsFromUser(QuestionDetails[i].questionPoints);
    setquestionHeartTxnFromUser(QuestionDetails[i].questionHeartTxn);
    setquestionHeartFromRewardFromUser(QuestionDetails[i].questionHeartFromReward);
    setquestionCategoryPointsFromUser(QuestionDetails[i].questionCategoryPoint);
    setRewardAd(QuestionDetails[i].questionHasRewardAd);
    if(QuestionDetails[i].questionCategoryTypeEnum ==0){ 
      setquscategory("No");
     }else if(QuestionDetails[i].questionCategoryTypeEnum==1){
      setquscategory("Spelling");
      }else if(QuestionDetails[i].questionCategoryTypeEnum==2){
       setquscategory("Grammer")
      }else if(QuestionDetails[i].questionCategoryTypeEnum==3){
      setquscategory("Pronunciation")
      }
      setQuestionType(QuestionDetails[i].questionType);
      setqus(QuestionDetails[i].questionData.question);
      setqusInstruction(QuestionDetails[i].questionData.questionInstruction);
      if(QuestionDetails[i].questionType==4){
        setqus4answer(QuestionDetails[i].questionData.answer)
        setqus4tts(QuestionDetails[i].questionData.tts);
      } else if(QuestionDetails[i].questionType==10){
        setqus10textForTTS(QuestionDetails[i].questionData.textForTTS);
        setqus10textForDisplay (QuestionDetails[i].questionData.textForDisplay);
        setqus10passingScore(QuestionDetails[i].questionData.passingScore);
        setqus10noOfTries(QuestionDetails[i].questionData.noOfTries);
      } else if(QuestionDetails[i].questionType==1){
        let questionArray = QuestionDetails[i].questionData.options;
        let QuestionOption = '';
        for(let i =0;i<questionArray.length;i++){
           QuestionOption+= questionArray[i]+';';
        }       
        setqus1answer( QuestionDetails[i].questionData.answer);
        setqus1options(QuestionOption);
      }  else if(QuestionDetails[i].questionType==11){
        let questionArray = QuestionDetails[i].questionData.answer;
        let QuestionOption = '';
        for(let i =0;i<questionArray.length;i++){
           if(i==0){
            QuestionOption+= questionArray[i];  
          }else{
            QuestionOption+= ';'+questionArray[i];
          }
        }       
        let textArray = QuestionDetails[i].questionData.text;
        let Questiontext ='';
        for(let i =0;i<textArray.length;i++){
           if(i==0){
            Questiontext+= textArray[i];
          }else{
            Questiontext+= ';'+textArray[i];
          }
        }       
        setqus11answer(Questiontext);
        setqus11options(QuestionOption);
      } 
        else if(QuestionDetails[i].questionType==8){
        let questionArray = QuestionDetails[i].questionData.answer;
        let QuestionOption = '';
        for(let i =0;i<questionArray.length;i++){
          if(i==0){
            QuestionOption+= questionArray[i];   
          }else{
            QuestionOption+= ";"+questionArray[i];
          }
        }       
        let textArray = QuestionDetails[i].questionData.text;
        // let Questiontext ='';
        // for(let i =0;i<textArray.length;i++){
         
        //    if(i==0){
        //     Questiontext+= textArray[i];  
        //   }else{
        //     Questiontext+= ';'+textArray[i];
        //   }
        // }       
        setqus8answer(textArray);
        setqus8options(QuestionOption);
      } 
        else if(QuestionDetails[i].questionType==5){
        setqus5option1(QuestionDetails[i].questionData.options[0].option);
        setqus5option2(QuestionDetails[i].questionData.options[1].option);
        setqus5option3(QuestionDetails[i].questionData.options[2].option);
        setqus5option4(QuestionDetails[i].questionData.options[3].option);
         if(QuestionDetails[i].questionData.options[0].iscorrect==true){
            setqus5Answer('Option 1');
         }else if(QuestionDetails[i].questionData.options[1].iscorrect==true){
          setqus5Answer('Option 2');
         }else if(QuestionDetails[i].questionData.options[2].iscorrect==true){
          setqus5Answer('Option 3');
         }else if(QuestionDetails[i].questionData.options[3].iscorrect==true){
          setqus5Answer('Option 4');
         }
      } 
        else if(QuestionDetails[i].questionType==3){
        setqus3option1(QuestionDetails[i].questionData.options[0].option);
        setqus3option2(QuestionDetails[i].questionData.options[1].option);
        setqus3option3(QuestionDetails[i].questionData.options[2].option);
        setqus3option4(QuestionDetails[i].questionData.options[3].option);
         if(QuestionDetails[i].questionData.options[0].iscorrect==true){
            setqus3Answer('Option 1');
         }else if(QuestionDetails[i].questionData.options[1].iscorrect==true){
          setqus3Answer('Option 2');
         }else if(QuestionDetails[i].questionData.options[2].iscorrect==true){
          setqus3Answer('Option 3');
         }else if(QuestionDetails[i].questionData.options[3].iscorrect==true){
          setqus3Answer('Option 4');
         }
         setqus3Listentext(QuestionDetails[i].questionData.listenText);
      } 
        else if(QuestionDetails[i].questionType==7){
            setqus7lhsOption1(QuestionDetails[i].questionData.LHS[0])
            setqus7lhsOption2(QuestionDetails[i].questionData.LHS[1])
            setqus7lhsOption3(QuestionDetails[i].questionData.LHS[2])
            setqus7rhsOption1(QuestionDetails[i].questionData.RHS[0])
            setqus7rhsOption2(QuestionDetails[i].questionData.RHS[1])
            setqus7rhsOption3(QuestionDetails[i].questionData.RHS[2])
            setpair1("LHS "+(QuestionDetails[i].questionData.pair[0].lhsId+1)+";"+"RHS "+(QuestionDetails[i].questionData.pair[0].rhsId+1));
           setpair2("LHS "+(QuestionDetails[i].questionData.pair[1].lhsId+1)+";"+"RHS "+(QuestionDetails[i].questionData.pair[1].rhsId+1));
             setpair3("LHS "+(QuestionDetails[i].questionData.pair[2].lhsId+1)+";"+"RHS "+(QuestionDetails[i].questionData.pair[2].rhsId+1));
      } 
        else if(QuestionDetails[i].questionType==0){
            seturl1(QuestionDetails[i].questionData.imageoptions[0].imageLink);
            seturl2(QuestionDetails[i].questionData.imageoptions[1].imageLink);
            seturl3(QuestionDetails[i].questionData.imageoptions[2].imageLink);
            setqus0option1(QuestionDetails[i].questionData.textoptions[0].option);
            setqus0option2(QuestionDetails[i].questionData.textoptions[1].option);
            setqus0option3(QuestionDetails[i].questionData.textoptions[2].option);
            setpair1("Image 1;"+"Option "+(QuestionDetails[i].questionData.imageoptions[0].answer+1));
            setpair2("Image 2;"+"Option "+(QuestionDetails[i].questionData.imageoptions[1].answer+1));
            setpair3("Image 3;"+"Option "+(QuestionDetails[i].questionData.imageoptions[2].answer+1));
      } 
        else if(QuestionDetails[i].questionType==6){
            seturl1(QuestionDetails[i].questionData.imageLink);
            setqus6minScore(QuestionDetails[i].questionData.minScore);
            setqus6minScoreNoOfTries(QuestionDetails[i].questionData.noOfTries)
            setqus6textForTts(QuestionDetails[i].questionData.textForTts);
      } 
        else if(QuestionDetails[i].questionType==2){
            seturl1(QuestionDetails[i].questionData.imageOptions[0]);
            seturl2(QuestionDetails[i].questionData.imageOptions[1]);
            seturl3(QuestionDetails[i].questionData.imageOptions[2]);
            seturl4(QuestionDetails[i].questionData.imageOptions[3]);
            setqus2correctImage(QuestionDetails[i].questionData.correctImage);
            setqus2word(QuestionDetails[i].questionData.word);
      } 
  };
  function submit(e) {
    e.preventDefault();
  }
  async function DeleteQuestion(i) {
    if (window.confirm("Are you sure")) {    
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    let questionType2 = '';
    let QuesitonPoolId =  QuestionDetails[i].questionPoolId;
    let QuestionId = QuestionDetails[i].questionId;
    questionType2 = QuestionDetails[i].questionType;
    if (questionType2 == 6) {
      imageUrl1 = QuestionDetails[i].questionData.imageLink;
      await deleteDoc(doc(db, "Questions", QuestionDetails[i].questionId));
      const QuestionPoolRef = doc(db, "QuestionPool", QuesitonPoolId);
      await updateDoc(QuestionPoolRef, { questionsId: arrayRemove(QuestionId) });
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (questionType2 == 2) {
      imageUrl1 = QuestionDetails[i].questionData.imageOptions[0];
      imageUrl2 = QuestionDetails[i].questionData.imageOptions[1];
      imageUrl3 = QuestionDetails[i].questionData.imageOptions[2];
      imageUrl4 = QuestionDetails[i].questionData.imageOptions[3];
      await deleteDoc(doc(db, "Questions", QuestionDetails[i].questionId));
      const QuestionPoolRef = doc(db, "QuestionPool", QuesitonPoolId);
      await updateDoc(QuestionPoolRef, { questionsId: arrayRemove(QuestionId) });
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (questionType2==0) {
      imageUrl1 = QuestionDetails[i].questionData.imageoptions[0].imageLink;
      imageUrl2 = QuestionDetails[i].questionData.imageoptions[1].imageLink;
      imageUrl3 = QuestionDetails[i].questionData.imageoptions[2].imageLink;
      await deleteDoc(doc(db, "Questions", QuestionDetails[i].questionId));
      const QuestionPoolRef = doc(db, "QuestionPool", QuesitonPoolId);
      await updateDoc(QuestionPoolRef, { questionsId: arrayRemove(QuestionId) });
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });  
    }
    await deleteDoc(doc(db, "Questions", QuestionDetails[i].questionId));

    const QuestionPoolRef = doc(db, "QuestionPool", QuesitonPoolId);
    if(questionType2==4){ 
      await updateDoc(QuestionPoolRef, {
        questionsId: arrayRemove(QuestionId),
        availQuestionsForTypeWhatYouHear: increment(-1),
        availQuestionsInPool:increment(-1)
    });
     }else if(questionType2==5){
      await updateDoc(QuestionPoolRef, {
        questionsId: arrayRemove(QuestionId),
        availQuestionsForMcq: increment(-1),
        availQuestionsInPool:increment(-1)
    });
      }else if(questionType2==10){
        await updateDoc(QuestionPoolRef, {
          questionsId: arrayRemove(QuestionId),
          availQuestionsForReadListenSpeak: increment(-1),
          availQuestionsInPool:increment(-1)
      });
      }else if(questionType2==1){
        await updateDoc(QuestionPoolRef, {
          questionsId: arrayRemove(QuestionId),
          availQuestionsForFillTheSequence: increment(-1),
          availQuestionsInPool:increment(-1)
      });
      }else if(questionType2==6){
        await updateDoc(QuestionPoolRef, {
          questionsId: arrayRemove(QuestionId),
          availQuestionsForIdentifyTheImage: increment(-1),
          availQuestionsInPool:increment(-1)
      });
       }else if(questionType2==2){
        await updateDoc(QuestionPoolRef, {
          questionsId: arrayRemove(QuestionId),
          availQuestionsForCorrectImageFromWord: increment(-1),
          availQuestionsInPool:increment(-1)
      });
        }else if(questionType2==8){
          await updateDoc(QuestionPoolRef, {
            questionsId: arrayRemove(QuestionId),
            availQuestionsForFillTheBlanksType: increment(-1),
            availQuestionsInPool:increment(-1)
        });
        }else if(questionType2==11){
          await updateDoc(QuestionPoolRef, {
            questionsId: arrayRemove(QuestionId),
            availQuestionsForFillTheBlanksVoice: increment(-1),
            availQuestionsInPool:increment(-1)
        });
        }else if(questionType2==7){
          await updateDoc(QuestionPoolRef, {
            questionsId: arrayRemove(QuestionId),
            availQuestionsForWordsToWords: increment(-1),
            availQuestionsInPool:increment(-1)
        });
        }else if(questionType2==0){
          await updateDoc(QuestionPoolRef, {
            questionsId: arrayRemove(QuestionId),
            availQuestionsForWordsToImage: increment(-1),
            availQuestionsInPool:increment(-1)
        });
        }else if(questionType2==3){
          await updateDoc(QuestionPoolRef, {
            questionsId: arrayRemove(QuestionId),
            availQuestionsForTapWhatYouHear: increment(-1),
            availQuestionsInPool:increment(-1)
        });
      }
    await updateDoc(QuestionPoolRef, { questionsId: arrayRemove(QuestionId) });
    setReset(!Reset);
    alert("Deleted data successfully");
  }
}
  const updateDataOnFirebase = async () => {
    const questionRef = doc(db, "Questions", questionId);
    let questionPointsFromUserAsInt = parseInt(questionPointsFromUser, 10);
    let questionHeartTxnFromUserAsInt = parseInt(questionHeartTxnFromUser, 10);
    let questionHeartFromRewardFromUserAsInt = parseInt(questionHeartFromRewardFromUser, 10);
    let questionCategoryPointsFromUserAsInt = parseInt(questionCategoryPointsFromUser, 10);
    let  questionCategoryTypeEnumfromuser =0;
    let QuestionData ;
    if(QuestionType== 4){ 
    let  questionDatafromUser = {
      answer:qus4tts,
      question:qus,
      questionInstruction:qusInstruction,
      tts:qus4answer
      }
      QuestionData = questionDatafromUser
     }else if(QuestionType==5){
      let setoptionFromuser ;
      if(qus5Answer=="Option 1"){
      let optionsFromUser =[{iscorrect:true,option:qus5option1},{iscorrect:false,option:qus5option2},{iscorrect:false,option:qus5option3},{iscorrect:false,option:qus5option4}];
         setoptionFromuser = optionsFromUser; 
    }else if(qus5Answer=="Option 2"){
        let optionsFromUser =[{iscorrect:false,option:qus5option1},{iscorrect:true,option:qus5option2},{iscorrect:false,option:qus5option3},{iscorrect:false,option:qus5option4}];
        setoptionFromuser = optionsFromUser;
      }else if(qus5Answer=="Option 3"){
          let optionsFromUser =[{iscorrect:false,option:qus5option1},{iscorrect:false,option:qus5option2},{iscorrect:true,option:qus5option3},{iscorrect:false,option:qus5option4}];
          setoptionFromuser = optionsFromUser;  
        }else if(qus5Answer=="Option 4"){
            let optionsFromUser =[{iscorrect:false,option:qus5option1},{iscorrect:false,option:qus5option2},{iscorrect:false,option:qus5option3},{iscorrect:true,option:qus5option4}];
            setoptionFromuser = optionsFromUser; 
          }
      let  questionDatafromUser = {
        options: setoptionFromuser,
        question:qus,
        questionInstruction:qusInstruction,
        }
        QuestionData = questionDatafromUser
      }else if(QuestionType ==10){
        let  qusNoOfTriesAsInt = parseInt(qus10noOfTries, 10);  
        let  qusPassingScoreAsInt = parseInt(qus10passingScore, 10);
        let  questionDatafromUser = {
          noOfTries:qusNoOfTriesAsInt,
          passingScore: qusPassingScoreAsInt,
          question:qus,
          questionInstruction:qusInstruction,
          textForDisplay:qus10textForDisplay,
          textForTTS:qus10textForTTS,
          }
          QuestionData = questionDatafromUser
      } 
      else if(QuestionType ==1){
          
        const myArray = qus1options.split(";");
        let  questionDatafromUser = {
          answer:qus1answer,
          options:myArray,
          question:qus,
          questionInstruction:qusInstruction,
          }
          QuestionData = questionDatafromUser
      }else if(QuestionType ==6){
        let  qusMinScoreAsInt = parseInt(qus6minScore, 10);
        let  qusNoOfTriesAsInt = parseInt(qus6minScoreNoOfTries, 10);
        let  questionDatafromUser = {
          imageLink: url1,
          minScore:qusMinScoreAsInt,
          noOfTries: qusNoOfTriesAsInt,
          question:qus,
          questionInstruction:qusInstruction,
          textForTts:qus6textForTts,
          }
          QuestionData = questionDatafromUser 
       }else if(QuestionType ==2){
        let  qusCorrectImageAsInt = parseInt(qus2correctImage, 10);
        let imageOptionFromUser =[url1,url2,url3,url4] 
        let  questionDatafromUser = {
          correctImage: qusCorrectImageAsInt,
          imageOptions:imageOptionFromUser,
          question:qus,
          questionInstruction:qusInstruction,
          word:qus2word,
          }
          QuestionData = questionDatafromUser 
        }else if(QuestionType ==8){
          const myAnswer = qus8options.split(";");
          const myoption = qus8answer.split(";");
          alert(qus8answer+" "+qus8options)
          let  questionDatafromUser = {
            answer: myAnswer,
            text:myoption,
            question:qus,
            questionInstruction:qusInstruction,
            }
            QuestionData = questionDatafromUser;
        }else if(QuestionType == 11){
          const myAnswer = qus11options.split(";");
          const myoption = qus11answer.split(";");
          let  questionDatafromUser = {
            answer: myAnswer,
            text:myoption,
            question:qus,
            questionInstruction:qusInstruction,
            }
            QuestionData = questionDatafromUser 
        }else if(QuestionType==7){
          var res1 = qus7lhstype[0].charAt(qus7lhstype[0].length-1);
          var lhs1 = parseInt(res1, 10)-1;
          var res2 = qus7rhstype[0].charAt(qus7rhstype[0].length-1);
         var rhs1 = parseInt(res2, 10)-1;
          var res3 = qus7lhstype[1].charAt(qus7lhstype[1].length-1);
         var  lhs2 = parseInt(res3, 10)-1;
          var res4 = qus7rhstype[1].charAt(qus7rhstype[1].length-1);
        var     rhs2 = parseInt(res4, 10)-1;
          var res5 = qus7lhstype[2].charAt(qus7lhstype[2].length-1);
         var lhs3 = parseInt(res5, 10)-1;
          var res6 = qus7rhstype[2].charAt(qus7rhstype[2].length-1);
        var  rhs3 = parseInt(res6, 10)-1;
          let pairFromUser = [{lhsId:lhs1,rhsId:rhs1},{lhsId:lhs2,rhsId:rhs2},{lhsId:lhs3, rhsId:rhs3}];
             let lhsFromUser = [qus7lhsOption1,qus7lhsOption2,qus7lhsOption3];
             let rhsFromUser =   [qus7rhsOption1,qus7rhsOption2,qus7rhsOption3];
          let  questionDatafromUser = {
            LHS: lhsFromUser,
            RHS: rhsFromUser,
            pair:pairFromUser,
            question:qus,
            questionInstruction:qusInstruction,
            }
           QuestionData = questionDatafromUser;
        }else if(QuestionType ==0){
          let ans1 ;
          let ans2 ;
          let ans3 ;
          if(qus0Image[0]=="Image 1"){
            var res = qus0option[0].charAt(qus0option[0].length-1);
            ans1 = parseInt(res, 10)-1;
          }else if(qus0Image[1]=="Image 1"){
            var res = qus0option[1].charAt(qus0option[1].length-1);
            ans1 = parseInt(res, 10)-1;
          }else if(qus0Image[2]=="Image 1"){
            var res = qus0option[2].charAt(qus0option[2].length-1);
            ans1 = parseInt(res, 10)-1;
          }
          if(qus0Image[0]=="Image 2"){
            var res = qus0option[0].charAt(qus0option[0].length-1);
            ans2 = parseInt(res, 10)-1;
          }else if(qus0Image[1]=="Image 2"){
            var res = qus0option[1].charAt(qus0option[1].length-1);
            ans2 = parseInt(res, 10)-1;
          }else if(qus0Image[2]=="Image 2"){
            var res = qus0option[2].charAt(qus0option[2].length-1);
            ans2 = parseInt(res, 10)-1;
          }
          if(qus0Image[0]=="Image 3"){
            var res = qus0option[0].charAt(qus0option[0].length-1);
            ans3 = parseInt(res, 10)-1;
          }else if(qus0Image[1]=="Image 3"){
            var res = qus0option[1].charAt(qus0option[1].length-1);
            ans3 = parseInt(res, 10)-1;
          }else if(qus0Image[2]=="Image 3"){
            var res = qus0option[2].charAt(qus0option[2].length-1);
            ans3 = parseInt(res, 10)-1;
          }
             let imageOptionFromUser = [{answer:ans1,imageLink:url1},{answer:ans2,imageLink:url2},{answer:ans3,imageLink:url3}];
             let testOptionFromUser =   [{option:qus0option1},{option:qus0option2},{option:qus0option3}];
          let  questionDatafromUser = {
            imageoptions: imageOptionFromUser,
            textoptions: testOptionFromUser,
            question:qus,
            questionInstruction:qusInstruction,
            }
            QuestionData = questionDatafromUser;
          }else if(QuestionType ==3){
            let setoptionFromuser ;
            if(qus3Answer=="Option 1"){
            let optionsFromUser =[{iscorrect:true,option:qus3option1},{iscorrect:false,option:qus3option2},{iscorrect:false,option:qus3option3},{iscorrect:false,option:qus3option4}];
               setoptionFromuser = optionsFromUser; 
          }else if(qus3Answer=="Option 2"){
              let optionsFromUser =[{iscorrect:false,option:qus3option1},{iscorrect:true,option:qus3option2},{iscorrect:false,option:qus3option3},{iscorrect:false,option:qus3option4}];
              setoptionFromuser = optionsFromUser;
            }else if(qus3Answer=="Option 3"){
                let optionsFromUser =[{iscorrect:false,option:qus3option1},{iscorrect:false,option:qus3option2},{iscorrect:true,option:qus3option3},{iscorrect:false,option:qus3option4}];
                setoptionFromuser = optionsFromUser;  
              }else if(qus3Answer=="Option 4"){
                  let optionsFromUser =[{iscorrect:false,option:qus3option1},{iscorrect:false,option:qus3option2},{iscorrect:false,option:qus3option3},{iscorrect:true,option:qus3option4}];
                  setoptionFromuser = optionsFromUser; 
                }
            let  questionDatafromUser = {
              listenText: qus3Listentext,
              options: setoptionFromuser,
              question:qus,
              questionInstruction:qusInstruction,
              }
              QuestionData = questionDatafromUser
        } 

    if(quscategory=="No"){ 
      questionCategoryTypeEnumfromuser  =0;
     }else if(quscategory=="Spelling"){
        questionCategoryTypeEnumfromuser  =1;
      }else if(quscategory=="Grammer"){
      questionCategoryTypeEnumfromuser  =2;
      }else if(quscategory=="Pronunciation"){
        questionCategoryTypeEnumfromuser  =3
      }
    await updateDoc(questionRef, {
      questionCorrectMessage: questionCorrectMessageFromUser,
      questionWrongMessage: questionWorongMessageFromUser,
      questionHint: questionHintsFromUser,
      questionPoints: questionPointsFromUserAsInt,
      questionHeartTxn: questionHeartTxnFromUserAsInt,
      questionHeartFromReward: questionHeartFromRewardFromUserAsInt,
      questionCategoryPoint: questionCategoryPointsFromUserAsInt,
      questionCategoryTypeEnum:questionCategoryTypeEnumfromuser,
      questionHasRewardAd:RewardAd,
      questionData:QuestionData,
    }).then(() => {
      alert("data updated succefully");
      setOpen(false);
    })
  }



  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setQuestionDetails([]);
      for(var i =0;i<QuestionDetailsCopy.length;i++){
          if (QuestionDetailsCopy[i]["associatedRootType"]=='Courses') {
            const query = QuestionDetailsCopy[i];
            setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
      if (QuestionDetailsCopy[i]["associatedRootType"]=='Practice'  ) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) => QuestionDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
      if (QuestionDetailsCopy[i]["associatedRootType"]=='Grammar' ) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
      if (QuestionDetailsCopy[i]["associatedRootType"]=='video') {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
      if (QuestionDetailsCopy[i]["associatedRootType"]=="Audio") {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
    if (QuestionDetailsCopy[i]["associatedRootType"]=='Story') {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const getlessonType = async (value) => {
  setLesson([]);  
  const data =   doc(db,   `${rootvalue}/${value}/extraInfo/infoDoc`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
   if(docSnap.data().courseFinalArcade!=undefined){
    if(docSnap.data().courseFinalArcade!=""){
    setLesson([{value : docSnap.data().courseFinalArcade, label:docSnap.data().courseFinalArcade}]);
    }
  }
     console.log(docSnap.data().courseLessons.map((user) => { 
        const query = {value : user, label:user}
     setLesson(lesson=> lesson.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const getTopicType = async (value) => {
  setTopic([]);
  const data = doc(db, `Lessons/${value}`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
     console.log(docSnap.data().lessonTopics.map((user) => { 
        const query = {value : user, label:user}
     setTopic(topic=> topic.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){
    if (QuestionDetailsCopy[i]["assoParentId"]==value) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
  getlessonType(e.value);
};
const lessonnamefromuser =(e)=>{
  setLessonUser(e.value);
  let lessonValue = e.value;
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){   
    if (QuestionDetailsCopy[i]["assoLessonId"]==lessonValue) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
  getTopicType(lessonValue);
}
const topicnamefromuser =(e)=>{
  setTopicUser(e.value);
  let lessonValue = e.value;
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){   
    if (QuestionDetailsCopy[i]["assoTopicId"]==lessonValue) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
}
const quiznamefromuser =(e)=>{
  setQuizUser(e.value);
  let quizValue = e.value;
  setQuestionDetails([]);
  for(var i =0;i<QuestionDetailsCopy.length;i++){   
    if (QuestionDetailsCopy[i]["assoQuizId"]==quizValue) {
        const query = QuestionDetailsCopy[i];
        setQuestionDetails((QuestionDetails) =>  QuestionDetails.concat(query));
    }
  }
}
const change=()=>{
     setQuestionDetails(QuestionDetailsCopy);
     setrootvalue('');
     setuserrootname('');
     setLessonUser('');
     setTopicUser('');
}
const getQuizdata= async ()=>{
  setQuiz([]);
  if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test' ||rootvalue=='Practice'){
      if(topicUser!=''||rootvalue=='Practice'){
          const data =   doc(db,   `Topics/${topicUser}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);

          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().topicQuiz);
            console.log(docSnap.data().topicQuiz.map((user) => { 
              const query = {value : user, label:user}
           setQuiz(quiz => quiz.concat(query))}));
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
      }else if(topicUser==''&& lessonUser==''){
          const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().courseFinalQuiz);
         //   setquizName(users3.lessonQuiz);
         //setquizName(users3.courseFinalQuiz);
         const query = {value : docSnap.data().courseFinalQuiz, label:docSnap.data().courseFinalQuiz}
      setQuiz(quiz => quiz.concat(query));   
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }       
       }else if(topicUser=='' && lessonUser!=''){
          const data =   doc(db,   `Lessons/${lessonUser}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().lessonQuiz);
           // setquizName(users3.lessonQuiz);
           console.log(docSnap.data().lessonQuiz.map((user) => { 
              const query = {value : user, label:user}
           setQuiz(quiz => quiz.concat(query))})); 
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
       }
}
}
  return (
    <div className="App">
     {/*  {`${JSON.stringify(QuestionDetails)}`} */}
        <label className='label'> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <br /><br />
     <label className='label'> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Lesson Name : </label>
    <Select value={lesson.filter(function(option) {return option.value === lessonUser;})} options={lesson} onChange={lessonnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Topic Name : </label>
    <Select value={topic.filter(function(option) {return option.value === topicUser;})} options={topic} onChange={topicnamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={getQuizdata}>Confirm</button>
    <br />
     <label className='label'> Choose the Quiz Name : </label>
    <Select value={quiz.filter(function(option) {return option.value === quizUser;})} options={quiz} onChange={quiznamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={change}>Reset Filter</button>
      <h2>Question Data</h2>
      <table className='content-table' style={{width:"100%"}}>
        <thead>
          <tr >
            <th style={{width: "10%"}}>S No.</th>
            <th style={{width: "20%"}}>Question Name</th>
            <th style={{width: "20%"}}>Question Type</th>
            <th style={{width: "20%"}}>Associated Parent</th>
            <th style={{width: "20%"}}>Associated Lesson objective Arcade</th>
            <th style={{width: "20%"}}>Associated Topic Round</th>
            <th style={{width: "20%"}}>Associated Question Pool</th>
          </tr>
        </thead>
        <tbody>
        {QuestionDetails.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {QuestionDetails.map((Question, i) => {
            if(Question.questionType!=undefined){
             if (Question.questionType== 0) {
              Question.Type ="Pairing(words to image)";
            } else if (Question.questionType == 1) {
              Question.Type= "FillTheSequence";
            } else if (Question.questionType == 2) {
              Question.Type= "ChooseCorrectImageFromWord";
            } else if (Question.questionType == 3) {
              Question.Type= "TapWhatYouHear";
            } else if (Question.questionType == 4) {
              Question.Type= "TypeWhatYouHear";
            } else if (Question.questionType == 5) {
              Question.Type= "Mcq";
            } else if (Question.questionType == 6) {
              Question.Type= "IdentifyTheImage";
            } else if (Question.questionType == 7) {
              Question.Type= "Pairing(words to words)";
            } else if (Question.questionType == 8) {
              Question.Type= "FillTheBlanks(type)";
            } else if (Question.questionType == 9) {
              Question.Type= "FillWithBlanks(Options)";
            } else if (Question.questionType == 10) {
              Question.Type= "ReadListenSpeak";
            } else if (Question.questionType == 11) {
              Question.Type= " FillTheBlanks(voice)";
            }
          }
            return (
              <tr key={i} >
                <td  >{i + 1}</td>
                <td  style={{width:"30%", borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}}>{Question.questionId}
                  <br /><br />
                  <button className='edit' onClick={() => handleOpen(i)}>Edit</button>&nbsp;&nbsp;
                  <button className='delete' onClick={() => DeleteQuestion(i)}>Delete</button>
                </td>
                <td  style={{ borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}}>{Question.Type}
                </td>
                <td  style={{ borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}}>{Question.assoParentId}
                </td>
                <td  style={{ borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}}>{Question.assoLessonId}
                </td>
                <td style={{ borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}} >{Question.assoTopicId}
                </td>
                <td style={{ borderCollapse: "collapse", textAlign: "center", padding: "8px" ,wordWrap:"break-word",maxWidth:"200px"}} >{Question.questionPoolId}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog open={open} onClose={handleClose} sx={{width:"100%"}} PaperProps={{style:{padding:"10px",borderRadius:"20px",width:"100%"}}}>
        <DialogTitle><text style={{color:"#308efe"}}>Update Question Form</text></DialogTitle>
        <DialogContent>
        <div>
          <form id="questionEdit-form"  onSubmit={submit} style={{fontFamily:"sans-serif"}}>
            <span>Question Id -   {questionId}</span><br /><br />
            <label>Question correct message  (maximum 80 characters) : : </label><br/><br/>
            <textarea  maxLength="80"
       
            cols="30"
            rows="3" value={questionCorrectMessageFromUser} type="text" placeholder={"Question correct message"} onChange={(event) => { setquestionCorrectMessageFromUser(event.target.value); }} />
            <br /><br />
            <label>Question wrong message  (maximum 80 characters) : </label><br/><br/>
           <textarea  maxLength="80"
       
            cols="30"
            rows="3" value={questionWorongMessageFromUser} type="text" placeholder={"Question wrong message"} onChange={(event) => { setquestionWorongMessageFromUser(event.target.value); }} />
            <br /><br />
            <label>Question hints  (maximum 95 characters) : : </label><br/><br/>
            <textarea  maxLength="95"
        
            cols="30"
            rows="3" value={questionHintsFromUser} type="text" placeholder={"Question Hints"} onChange={(event) => { setquestionHintsFromUser(event.target.value); }} />
            <br /><br />
            <label>Point/Score for the question : </label>
            <input value={questionPointsFromUser} type="number" style={{width:'50px'}} min="0" placeholder={"Point/Score for the question"} onChange={(event) => { setquestionPointsFromUser(event.target.value); }} />
            <br /><br />
            <label>No of hearts lost after wrong answer ? : </label>
            <input value={questionHeartTxnFromUser} type="number" style={{width:'50px'}}  min="0" placeholder={"No of hearts lost after wrong answer ?"} onChange={(event) => { setquestionHeartTxnFromUser(event.target.value); }} />
            <br /><br />
            {/* <label>No of Hearts from Reward : </label>
            <input value={questionHeartFromRewardFromUser} type="number" style={{width:'50px'}}  min="0" placeholder={"No of Hearts from Reward"} onChange={(event) => { setquestionHeartFromRewardFromUser(event.target.value); }} />
            <br /><br />
        <label>if Reward Ad if heart 0 ? : </label>
        <input type="checkbox" defaultChecked={RewardAd} onChange={(event) => { setRewardAd(!RewardAd) }} />
            <br /><br /> */}
            <label>System defined category for question : </label>
        <select value={quscategory} onChange={(event) => { setquscategory(event.target.value); }}>
          <option value="No">No</option>
          <option value="Spelling">Spelling</option>
          <option value="Grammer">Grammer</option>
          <option value="Pronunciation">Pronunciation</option>
        </select>
        <br/><br/>
            <label>Category Point : </label>
            <input value={questionCategoryPointsFromUser} type="number" style={{width:'50px'}} min="0" placeholder={"Category Point"} onChange={(event) => { setquestionCategoryPointsFromUser(event.target.value); }} />
            <br /><br />
           {QuestionType == 4?  <>
        <label>Text for TTS : </label>
        <textarea  value={qus4tts } cols="30" rows="10"  placeholder='Text For TTS' onChange={e => setqus4tts(e.target.value)}></textarea>
        <br /><br />
        <label>Answer  : </label>
        <textarea value={qus4answer}  cols="30" rows="10"  placeholder='Answer' onChange={(event) => { setqus4answer(event.target.value) }}></textarea>
      </> : " "}
       
      {QuestionType == 10?   <>
          <label>Text for TTS : </label>
          <input type="text" value={qus10textForTTS} placeholder='Text For TTS' onChange={(event) => { setqus10textForTTS(event.target.value)}} />
          <br /><br />
          <label>Text For Display  : </label> 
          <input type="text" value={qus10textForDisplay}  placeholder='Text For Display' onChange={(event) => { setqus10textForDisplay(event.target.value)}} />
          <br /><br />
          <label>Passing Score  : </label> 
          <input type="number" value={qus10passingScore} style={{width:'50px'}}  min="0" placeholder='Passing Score' onChange={(event) => { setqus10passingScore( event.target.value) }} />
          <br /><br />
          <label>No Of Tries  : </label> 
          <input type="number" value={qus10noOfTries} style={{width:'50px'}}  min="0" placeholder='No Of Tries ' onChange={(event) => { setqus10noOfTries(event.target.value)}} />
      </> : " "}

      {QuestionType== 1?   <>
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <input type="text" value={qus1options} placeholder='Ex - you;where;going;are' onChange={(event) => {setqus1options(event.target.value)}} />
          <br /><br />
          <label>Correct Sequence  : </label> 
          <input type="text" value={qus1answer} placeholder='Correct Sequence' onChange={(event) => {setqus1answer(event.target.value)}} />
      </>: " "}
      
      {QuestionType== 11?   <>
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <textarea type="text" cols="30" rows="10" value={qus11options} placeholder='Ex - you;where;going;are' onChange={(event) => {setqus11options(event.target.value)}} />
          <br /><br />
          <label>Text  : </label> 
          <textarea cols="30" rows="10" type="text" value={qus11answer} placeholder='Text For Display' onChange={(event) => {setqus11answer(event.target.value)}} />
      </>: " "}
     
      {QuestionType== 8?   <>
          <label>Optios (Enter the otion seperated by semicolion) : </label>
          <textarea type="text" cols="30" rows="10" value={qus8options} placeholder='Ex - you;where;going;are'  onChange={(event) => {setqus8options(event.target.value)}} />
          <br /><br />
          <label>Text : </label> 
          <textarea type="text" cols="30" rows="10"  value={qus8answer} placeholder='Text For Display'onChange={(event) => {setqus8answer(event.target.value)}} />
      </>: " "}
    
      {QuestionType== 5?   <> 
        <label>Option 1 : </label>
        <input name="text" placeholder="Enter Option" value={qus5option1} onChange={(event) => {setqus5option1(event.target.value)}}/> 
        <br /><br />
        <label>Option 2 : </label>
        <input name="text" placeholder="Enter Option" value={qus5option2} onChange={(event) => {setqus5option2(event.target.value)}}/>
        <br /><br />
        <label>Option 3 : </label>
        <input name="text" placeholder="Enter Option" value={qus5option3} onChange={(event) => {setqus5option3(event.target.value)}}/>
        <br /><br />
        <label>Option 4 : </label>
        <input type="text" value={qus5option4} placeholder="Enter Option" onChange={(event) => {setqus5option4(event.target.value)}}/> 
        <br /><br />
        <label> Choose the correct Option : </label>
          <select value={qus5Answer} onChange={(event) => { setqus5Answer(event.target.value) }}>
          <option value="Option 1">Option 1</option>
          <option value="Option 2">Option 2</option>
          <option value="Option 3">Option 3</option>
          <option value="Option 4">Option 4</option>
        </select>
      </>: " "}
         
         {QuestionType== 3?   <> 
        <label>listen Text : </label>
        <input type="text"  value={qus3Listentext} placeholder='Listen Text' onChange={(event) => {setqus3Listentext(event.target.value)}} />
        <br /> <br />
        <label>Option 1 : </label>
        <input name="text"  value={qus3option1} placeholder="Enter Option" onChange={(event) => {setqus3option1(event.target.value)}}/> 
        <br /><br />
        <label>Option 2 : </label>
        <input name="text"  value={qus3option2} placeholder="Enter Option" onChange={(event) => {setqus3option2(event.target.value)}}/>
        <br /><br />
        <label>Option 3 : </label>
        <input name="text"  value={qus3option3} placeholder="Enter Option" onChange={(event) => {setqus3option3(event.target.value)}}/>
        <br /><br />
        <label>Option 4 : </label>
        <input type="text" value={qus3option4} placeholder="Enter Option" onChange={(event) => {setqus3option4(event.target.value)}}/> 
        <br /><br />
        <label> Choose the correct Option : </label>
          <select value={qus3Answer} onChange={(event) => {setqus3Answer(event.target.value)}}>
          <option value="Option 1">Option 1</option>
          <option value="Option 2">Option 2</option>
          <option value="Option 3">Option 3</option>
          <option value="Option 4">Option 4</option>
        </select>

      </>: " "}
        
      {QuestionType===7?   <>
        <label>LHS 1 : </label>
        <input name="text" value={qus7lhsOption1} placeholder="Enter Option" onChange={(event) => {setqus7lhsOption1(event.target.value)}}/>&nbsp; 
        <label>RHS 1 : </label>
        <input name="text" value={qus7rhsOption1} placeholder="Enter Option" onChange={(event) => {setqus7rhsOption1(event.target.value)}}/> 
     <br /><br />
     <label>LHS 2 : </label>
    <input name="text" value={qus7lhsOption2} placeholder="Enter Option" onChange={(event) => {setqus7lhsOption2(event.target.value)}}/>&nbsp; 
    <label>RHS 2 : </label>
   <input name="text" value={qus7rhsOption2} placeholder="Enter Option" onChange={(event) => {setqus7rhsOption2(event.target.value)}}/>
     <br /><br />
     <label>LHS 3 : </label>
    <input name="text" value={qus7lhsOption3} placeholder="Enter Option" onChange={(event) => {setqus7lhsOption3(event.target.value)}}/>&nbsp; 
     <label>RHS 3 : </label>
    <input name="text" value={qus7rhsOption3} placeholder="Enter Option" onChange={(event) => {setqus7rhsOption3(event.target.value)}}/>
    <br /><br />
     <label> Choose the correct Pair : </label>
     <Select isMulti options = {lhstype} onChange={addedlhs} ></Select><br/><br/><br/>
    <Select isMulti  options = {rhstype} onChange={added1rhs} ></Select><br/><br/><br/>
    <button onClick={createPairLhsRhs}>Add Pair</button>
    <button onClick={clearPair}>Reset Pair</button>
    <h3>Pair 1 - { ` ${pair1}`}  </h3> 
  <h3>Pair 2  - { ` ${pair2}`}</h3>    
   <h3> Pair 3 - {` ${pair3} `}</h3>  
    </>: " "}
    
    {QuestionType==0?   <> 
    <label>Image 1 :</label>
        <input type="file"  onChange={onImageChange1} /><br/><br/>
        <img src={url1} alt=""width="193" height="130" /><br/><br/>
        <Button startIcon={<Upload/>} onClick={()=> uploadFiles1(file1)}>upload</Button>
        <Button startIcon={<Delete/>} onClick={()=> deleteImage1(url1)}>Delete</Button>
        <br/><br/>
        <label>   Option 1 : </label>
        <input name="text" value={qus0option1} placeholder="Enter Option" onChange={(event) => {setqus0option1(event.target.value)}}/> 
     <h3>uploaded {progress1}%</h3>
     <br /><br />
     <label>Image 2 :</label>
     <input type="file"  onChange={onImageChange2} /><br/><br/>
     <img src={url2} alt=""width="193" height="130" /><br/><br/>
        <Button startIcon={<Upload/>} onClick={()=> uploadFiles2(file2)}>upload</Button>
        <Button startIcon={<Delete/>} onClick={()=> deleteImage2(url2)}>Delete</Button>
        <br/>
        <label> Option 2 : </label>
        <input name="text" value={qus0option2} placeholder="Enter Option" onChange={(event) => {setqus0option2(event.target.value)}}/> 
     <h3>uploaded {progress2}%</h3>
     <br /><br />
     <label>Image 3 :</label>
     <input type="file"  onChange={onImageChange3} /><br/><br/>
     <img src={url3} alt=""width="193" height="130" /><br/><br/>
        <Button startIcon={<Upload/>} onClick={()=> uploadFiles3(file3)}>upload</Button>
        <Button  startIcon={<Delete/>} onClick={()=> deleteImage3(url3)}>Delete</Button>
        <br/><br/>
        <label> Option 3 : </label>
        <input name="text" value={qus0option3} placeholder="Enter Option" onChange={(event) => {setqus0option3(event.target.value)}}/> 
     <h3>uploaded {progress3}%</h3>
     <label> Choose the correct Pair : </label>
     <Select isMulti options = {Imagetype} onChange={added} ></Select>
    <Select isMulti options = {optiontype} onChange={added1} ></Select>
    <br/><br/>
    <Button onClick={createPair}>Add Pair</Button>
    <Button onClick={clearPair}>Reset Pair</Button>
    <h3>Pair 1 - { ` ${pair1}`}  </h3> 
   <h3>Pair 2  - { ` ${pair2}`}</h3>    
   <h3> Pair 3 - {` ${pair3} `}</h3>  
    </>: " "}
    {QuestionType==6?   <> 
         <label>Text for tts : </label>
          <input type="text" value={qus6textForTts} placeholder='Ex - you;where;going;are' onChange={(event) => {setqus6textForTts(event.target.value)}} />
          <br /><br />
          <label>Minmium Score : </label>
          <input type="number" value={qus6minScore} style={{width:'50px'}} min="0" onChange={(event) => {setqus6minScore(event.target.value)}} />
          <br /><br />
          <label>No Of Tries : </label>
          <input type="number" value={qus6minScoreNoOfTries} style={{width:'50px'}} min="0" onChange={(event) => {setqus6minScoreNoOfTries(event.target.value)}} />
          <br /><br />
          <label>Image 1 :</label>
        <input type="file"  onChange={onImageChange1} />
        <button onClick={()=> uploadFiles1(file1)}>upload</button>
        <button onClick={()=> deleteImage1(url1)}>Delete</button>
        <img src={url1} alt=""width="193" height="130" />
     <h3>uploaded {progress1}%</h3>
    </>: " "}
    {QuestionType==2? <> 
         <label>Words: </label>
          <input type="text" value={qus2word} placeholder='Ex - you;where;going;are' onChange={(event) => {setqus2word( event.target.value)}}/>
          <br /><br />
          <label> Choose the correct Image : </label>
          <select value={qus2correctImage} onChange={(event) => { setqus2correctImage(event.target.value) }}>
          <option value="0">Image1</option>
          <option value="1">Image2</option>
          <option value="2">Image3</option>
          <option value="3">Image4</option>
        </select>
          <br /><br />
          <label>Image 1 :</label>
        <input type="file"  onChange={onImageChange1} />
        <button onClick={()=> uploadFiles1(file1)}>upload</button>
        <button onClick={()=> deleteImage1(url1)}>Delete</button>
        <img src={url1} alt=""width="193" height="130" />
     <h3>uploaded {progress1}%</h3>
     <br /><br />
     <label>Image 2 :</label>
     <input type="file"  onChange={onImageChange2} />
        <button onClick={()=> uploadFiles2(file2)}>upload</button>
        <button onClick={()=> deleteImage2(url2)}>Delete</button>
        <img src={url2} alt=""width="193" height="130" />
     <h3>uploaded {progress2}%</h3>
     <br /><br />
     <label>Image 3 :</label>
     <input type="file"  onChange={onImageChange3} />
        <button onClick={()=> uploadFiles3(file3)}>upload</button>
        <button onClick={()=> deleteImage3(url3)}>Delete</button>
        <img src={url3} alt=""width="193" height="130" />
     <h3>uploaded {progress3}%</h3>
     <br /><br />
     <label>Image 4 :</label>
     <input type="file"  onChange={onImageChange4} />
        <button onClick={()=> uploadFiles4(file4)}>upload</button>
        <button onClick={()=> deleteImage4(url4)}>Delete</button>
        <img src={url4} alt=""width="193" height="130" />
        <h3>uploaded {progress4}%</h3>
    </>: " "}
      <br /> <br /> 
      <br /><br />
        <label>Question : </label>
        <textarea value={qus} cols="30" rows="10" placeholder='Question' onChange={(event) => { setqus(event.target.value); }}></textarea>
        <br /><br />
        <label>Instruction : </label>
        <textarea value={qusInstruction}  cols="30" rows="10"  placeholder='Instruction' onChange={(event) => { setqusInstruction(event.target.value); }} ></textarea>
          </form>
        </div>
        </DialogContent>
        <DialogActions>
        <Button type="submit" onClick={updateDataOnFirebase} >Submit</Button>
            <Button sx={{color:"red"}}onClick={handleClose}  >Cancel</Button>
        </DialogActions>
        </Dialog>
    </div>
  );
}
export default QuestionTable;
