import React, { useState } from 'react'
import TopicForm from './TopicForm';
import TopicTable from './TopicTable';
import './topic.css'

function Topic() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Topic");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Topic");
    }else{
     
      setvalue("Add new Topic");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<TopicTable/>:<TopicForm/>}
      </>
  )
}

export default Topic;