import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc , arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
import './topic.css'
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Button, TextField } from '@mui/material';
function TopicTable() {
  const [TopicDetails, setTopicDetails] = useState([]);
  const [TopicDetailsCopy, setTopicDetailsCopy] = useState([]);
  const [Users1,setUsers1] = useState('');
  const [lesson , setLesson]= useState([]);
  const [lessonUser , setLessonUser] = useState('');
  const [Reset, setReset] = useState(false);
  const [Open, setOpen] = useState(false);
  const [TopicId , setTopicId] = useState('');
  const [TopicShortDesc , setTopicShortDesc] = useState('');
  const [TopicCompletionText    , setTopicCompletionText] = useState('');
  const [TopicHeartTxn    , setTopicHeartTxn  ] = useState(0);
  const [Type , setType] = useState('');
  const [TextAfterCompletionOfTopicRoundDisable , setTextAfterCompletionOfTopicRoundDisable] = useState(false);
  const [TopicShortDescDisable , setTopicShortDescDisable] = useState(false);
  const [rootvalue , setrootvalue] = useState('');
  const [rootName  , setrootName] = useState([]);
  const [userrootname , setuserrootname] = useState('');
  const usercollectionRef = collection(db, "Topics")
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setTopicDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setTopicDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset,Open]);
  const handleClose = () => {
    setOpen(false);
  };
  var root = [
    {
      value : 'Courses',
      label : "General Course"
    },
    {
        value : 'Grammar',
        label : 'Grammar'
    },
    {
     value : 'Practice',
     label : "Practice"
   }
  ];
  const handleOpen = async(i) => {
    setOpen(true);
    console.log(TopicDetails[i])
    setTopicId(TopicDetails[i].topicId);
    setTopicShortDesc(TopicDetails[i].topicShortDesc);
    setrootvalue(TopicDetails[i].topicAssociatedCourseType);
    const TopicInfo = doc(db, `Topics/${TopicDetails[i].topicId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(TopicInfo);
    if(docSnap1.exists()){
        setTopicCompletionText(docSnap1.data().topicCompletionText);
        setTopicHeartTxn(docSnap1.data().topicHeartTxn);
    }  else {
        console.log("No such document! user 5");
      }
  };
  function submit(e) {
    e.preventDefault();
  }
  useEffect(()=>{
    if(rootvalue=='Courses' ||rootvalue=='Grammar'){
      setTopicShortDescDisable(false);
  setTextAfterCompletionOfTopicRoundDisable(false);
   }else if(rootvalue=='Practice'){
  setTopicShortDescDisable(true);
  setTextAfterCompletionOfTopicRoundDisable(true);
   }
},[rootvalue])
  const updateDataOnFirebase=async()=>{
    let TopicHeartTxnAsInt =  Math.abs(parseInt(TopicHeartTxn, 10));
    const TopicRef = doc(db, "Topics", TopicId);
   
    await updateDoc(TopicRef, {topicShortDesc:TopicShortDesc});
    const TopicInfo = doc(db, `Topics/${TopicId}/extraInfo/infoDoc`);
    await updateDoc(TopicInfo, {
      topicCompletionText:TopicCompletionText,
      topicHeartTxn: TopicHeartTxnAsInt ,

    });
      alert("data updated succefully");
      setOpen(false);
  }
  async function deleteImageOfQuestion(QuestionType, imageLink) {
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    if (QuestionType == 6) {
      imageUrl1 = imageLink;
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 2) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      imageUrl4 = imageLink[3];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 0) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
    }
  }
  async function deleteQuestionArray(Question) {
    const data = doc(db, `Questions/${Question}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setUsers1(docSnap.data());
      console.log("Document data:", docSnap.data().questionType);
      if (docSnap.data().questionType == 6) {
        deleteImageOfQuestion(docSnap.data().questionType, docSnap.data().questionData.imageLink);
      } else if (docSnap.data().questionType == 2) {
        let imageArray = [docSnap.data().questionData.imageOptions[0], docSnap.data().questionData.imageOptions[1], docSnap.data().questionData.imageOptions[2], docSnap.data().questionData.imageOptions[3]];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      } else if (docSnap.data().questionType == 0) {
        let imageArray = [docSnap.data().questionData.imageoptions[0].imageLink, docSnap.data().questionData.imageoptions[1].imageLink, docSnap.data().questionData.imageoptions[2].imageLink];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      }
      await deleteDoc(doc(db, "Questions", Question));
    } else {
      console.log("No such document!");
    }

  }
  async function deleteQuestionPool(quizQuestionPoolForDelete) {
    const data = doc(db, `QuestionPool/${quizQuestionPoolForDelete}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      setUsers1(docSnap1.data());
      let QuestionArray =docSnap1.data().questionsId;
      QuestionArray.map((Question) => {
        deleteQuestionArray(Question);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, "QuestionPool", quizQuestionPoolForDelete));
  }
  async function deleteQuiz(Quiz){
      const data = doc(db, `Quizs/${Quiz}`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
        deleteQuestionPool(docSnap1.data().quizQuestionPool);
      } else {
        console.log("No such document!");
      }
      await deleteDoc(doc(db, "Quizs", `${Quiz}/extraInfo/infoDoc`));
     await deleteDoc(doc(db, "Quizs", Quiz));
    
    }
  async function updateLesson(lessonId,topicId){
      const QuestionPoolRef =   doc(db,   `Lessons/${lessonId}`);
      await updateDoc(QuestionPoolRef, { lessonTopics: arrayRemove(topicId) });
  }
  const deleteItem = async(item)=>{
    const data = doc(db, `Items/${item}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      let type =docSnap1.data().category;
      if(type=="PDF" || type=="audio"){
        deleteImageOfQuestion(6,docSnap1.data().url)
      }
    } else {
      console.log("No such document!");
    }
      await deleteDoc(doc(db, "Items", item));
  }
  async function deleteTopic(i){
    if (window.confirm("Are you sure")) {    
    const data = doc(db, `Topics/${TopicDetails[i].topicId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      let QuizArray =docSnap1.data().topicQuiz;
      QuizArray.map((Quiz) => {
        deleteQuiz(Quiz);
      });
      let topicChildItems =docSnap1.data().topicChildItems;
      topicChildItems.map((Quiz) => {
        deleteItem(Quiz);
      });
    } else {
      console.log("No such document!");
    }
    updateLesson(TopicDetails[i].topicAssociatedLesson,TopicDetails[i].topicId);
    await deleteDoc(doc(db, "Topics", TopicDetails[i].topicId));
    setReset(!Reset);
  }
  }
  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setTopicDetails([]);
      for(var i =0;i<TopicDetailsCopy.length;i++){
          if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=='Courses') {
            const query = TopicDetailsCopy[i];
            setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
      if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=='Practice'  ) {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) => TopicDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
      if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=='Grammar' ) {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
      if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=='video') {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
      if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=="Audio") {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
    if (TopicDetailsCopy[i]["topicAssociatedCourseType"]=='Story') {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const getlessonType = async (value) => {
  setLesson([]);  
  const data =   doc(db,   `${rootvalue}/${value}/extraInfo/infoDoc`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
   if(docSnap.data().courseFinalArcade!=undefined){
    if(docSnap.data().courseFinalArcade!=""){
    setLesson([{value : docSnap.data().courseFinalArcade, label:docSnap.data().courseFinalArcade}]);
    }
  }
     console.log(docSnap.data().courseLessons.map((user) => { 
        const query = {value : user, label:user}
     setLesson(lesson=> lesson.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){
    if (TopicDetailsCopy[i]["topicAssociatedCourse"]==value) {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }
  getlessonType(e.value);
};
const lessonnamefromuser =(e)=>{
  setLessonUser(e.value);
  let lessonValue = e.value;
  setTopicDetails([]);
  for(var i =0;i<TopicDetailsCopy.length;i++){   
    if (TopicDetailsCopy[i]["topicAssociatedLesson"]==lessonValue) {
        const query = TopicDetailsCopy[i];
        setTopicDetails((TopicDetails) =>  TopicDetails.concat(query));
    }
  }
}
const change=()=>{
     setTopicDetails(TopicDetailsCopy);
     setrootvalue('');
     setuserrootname('');
     setLessonUser('');
}
  return (
    <div className="App">
       <label className='label'> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <br /><br />
     <label className='label'> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Lesson Name : </label>
    <Select value={lesson.filter(function(option) {return option.value === lessonUser;})} options={lesson} onChange={lessonnamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={change}>Reset Filter</button>
      <h2>Topic Data</h2>
      <table className='content-table' style={{marginRight:"20px"}}>
        <thead>
          <tr style={{width: "10%" }}>
            <th style={{width: "5%"}}>S No.</th>
            <th style={{ width: "50%"}}>QuestionPool Name</th>
            <th style={{ width: "100%" }}>Type</th>
            <th style={{ width: "100%"}}>Associated Parent</th>
            <th style={{width: "100%"}}>Associated Lesson</th>
          </tr>
        </thead>
        <tbody>
        {TopicDetails.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {TopicDetails.map((Topic, i) => {
          /*   getTypeOfTopicFromFirebase(i).then((value) => {
            console.log(value);
          });
          // console.log(Topic.topicName) ; */
          if (Topic.topicTypeEnum == 0) {
            Topic.topicTypeEnum ="Topic";
          } else if (Topic.topicTypeEnum == 1) {
            Topic.topicTypeEnum = "Round";
          }
            return (
              <tr key={i} >
                <td  >{i + 1}</td>
                <td style={{maxWidth:"250px",wordWrap:"break-word",marginLeft:"10px"}} >{Topic.topicId}
                  <br /><br />
                  <button className="edit" onClick={()=>handleOpen(i)}>Edit</button>
                  <button className="delete" onClick={()=>deleteTopic(i)}>Delete</button>
                </td>
                <td style={{maxWidth:"250px",wordWrap:"break-word",marginLeft:"10px"}} >{Topic.topicTypeEnum}
                </td>
                <td style={{maxWidth:"250px",wordWrap:"break-word",marginLeft:"10px"}} >{Topic.topicAssociatedCourse}
                </td>
                <td style={{maxWidth:"250px",wordWrap:"break-word",marginLeft:"10px"}} >{Topic.topicAssociatedLesson}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog onClose={handleClose} open={Open} PaperProps={{style:{
        borderRadius:"20px",padding:"10px"
      }}}>
        <DialogTitle  sx={{fontSize:24,color:"#308efe",fontWeight:'500'}}><text style={{color:"#308efe"}}>Update Topic Form</text></DialogTitle>
        <DialogContent>
          <form onSubmit={submit} style={{fontFamily:"sans-serif"}} >
            <span>Topic Id -   {TopicId}</span><br /><br />
            <label hidden={TopicShortDescDisable} >Short Description (maximum 45 characters) : </label><br/><br/>
            <textarea cols="30" rows="5" maxLength='45' hidden={TopicShortDescDisable} value={TopicShortDesc} type="text" placeholder={"Topic Short Description"} onChange={(event) => { setTopicShortDesc(event.target.value); }} />
            <br /><br />
            <label hidden={TextAfterCompletionOfTopicRoundDisable} >Text After Completion of Topic/Round (maximum 30 characters) : </label><br/><br/>
            <textarea cols="30" rows="5" maxLength='30' hidden={TextAfterCompletionOfTopicRoundDisable} value={TopicCompletionText} type="text" placeholder={"Text After Completion Topic"} onChange={(event) => { setTopicCompletionText(event.target.value); }} />
            <br /><br />
            <label>No of hearts that are to be credited to after completion ? : </label>
            <input value={TopicHeartTxn} type="number"  min="0" placeholder={"No of hearts that are to be credited to after completion"} onChange={(event) => { setTopicHeartTxn(event.target.value); }} />
            <br /><br />

          </form>
          </DialogContent>
          <DialogActions>
         < Button type="submit" onClick={updateDataOnFirebase} >Submit</Button>
            <Button onClick={handleClose} sx={{color:"red"}} >Cancel</Button>
         </DialogActions>
      </Dialog>
    </div>
  );
}
export default TopicTable;