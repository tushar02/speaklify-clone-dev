import { db } from "../../firebase/Firebase-config";
import { useState } from "react";
import {
  collection,
  getDocs,
  setDoc,
  updateDoc,
  doc,
  getDoc,
  arrayUnion,
} from "firebase/firestore";
import Select from "react-select";
import { useEffect } from "react";
import { isDisabled } from "@testing-library/user-event/dist/utils";
const TopicForm = () => {
  const [users, setusers] = useState([]);
  const [users1, setusers1] = useState([]);
  const [rootvalue, setrootvalue] = useState("");
  const [rootName, setrootName] = useState([]);
  const [lessonName, setlessonName] = useState([]);
  const [lessonName1, setlessonName1] = useState([]);
  const [userrootname, setuserrootname] = useState("");
  const [userlessonname, setuserlessonname] = useState("");
  const [userLessonType, setuserLessonType] = useState("");
  const [TopicId, setTopicId] = useState("");
  const [TopicName, setTopicName] = useState("");
  const [TextAfterCompletionOfTopicRound, setTextAfterCompletionOfTopicRound] =
    useState("");
  const [topicTypeEnumFromUser, settopicTypeEnumFromUser] = useState("");
  const [ShortDescription, setShortDescription] = useState("");
  const [
    NoOfHeartsThatAreToBeCreditedToAfterCompletion,
    setNoOfHeartsThatAreToBeCreditedToAfterCompletion,
  ] = useState(0);
  const [hasQuiz, sethasQuiz] = useState(true);
  const [hasQuizDisable, sethasQuizDisable] = useState(false);
  const [check, setcheck] = useState(false);
  const [ShortDescriptionDisable, setShortDescriptionDisable] = useState(false);
  const [
    TextAfterCompletionOfTopicRoundDisable,
    setTextAfterCompletionOfTopicRoundDisable,
  ] = useState(false);
  const [TopicChildItemDisable, setTopicChildItemDisable] = useState(false);

  const [root, setroot] = useState([
    {
      value: "Courses",
      label: "General Course",
      isDisabled: false,
    },
    {
      value: "Grammar",
      label: "Grammar",
      isDisabled: false,
    },
    {
      value: "Practice",
      label: "Practice",
      isDisabled: false,
    },
  ]);
  var AnimationType = [
    {
      value: 0,
      label: "1",
    },
    {
      value: 1,
      label: "2",
    },
    {
      value: 2,
      label: "3",
    },
  ];
  const [topicTypeEnum, settopicTypeEnu] = useState([
    {
      value: 0,
      label: "Topic",
      isDisabled: false,
    },
    {
      value: 1,
      label: "Round",
      isDisabled: false,
    },
  ]);

  var [lesson, setlesson] = useState([
    {
      value: "Courses",
      label: "Lesson",
      isDisabled: false,
    },
    {
      value: "Objective",
      label: "Objective",
      isDisabled: false,
    },
    {
      value: "Arcade",
      label: "Arcade",
      isDisabled: false,
    },
  ]);

  const getlessonType = async () => {
    const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setusers1(docSnap.data());
      console.log("Document data:", docSnap.data());
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
    }
  };
  const confirm = () => {
    setrootName([]);
    console.log(
      users.map((user) => {
        const query = { value: user.courseId, label: user.courseId };
        setrootName((rootName) => rootName.concat(query));
      })
    );
  };
  useEffect(() => {
    if (rootvalue == "Courses" || rootvalue == "Grammar") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopicTypeEnu((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setShortDescriptionDisable(false);
      setTextAfterCompletionOfTopicRoundDisable(false);
      setTopicChildItemDisable(false);
    } else if (rootvalue == "Practice") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopicTypeEnu((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setShortDescriptionDisable(true);
      setTextAfterCompletionOfTopicRoundDisable(true);
      setTopicChildItemDisable(true);
    } else if (rootvalue == "Test" || rootvalue == "video") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    }
  }, [rootvalue]);
  useEffect(() => {
    confirm();
  }, [users]);
  useEffect(() => {
    getlessonType();
  }, [userLessonType]);
  useEffect(() => {
    getlessonName();
  }, [users1]);
  const getlessonName = () => {
    if (userLessonType == "Arcade") {
      if (users1.courseFinalArcade != undefined) {
        setlessonName1([]);
        setlessonName1([
          { value: users1.courseFinalArcade, label: users1.courseFinalArcade },
        ]);
      }
    } else {
      setlessonName(users1.courseLessons);
      if (lessonName != undefined) {
        setlessonName1([]);
        console.log(
          lessonName.map((user) => {
            const query = { value: user, label: user };
            setlessonName1((lessonName1) => lessonName1.concat(query));
          })
        );
      }
    }
  };
  const getrootName = async () => {
    const usercollectionRef = collection(db, rootvalue);
    const data = await getDocs(usercollectionRef);
    console.log(data);
    setusers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    setuserrootname();
    setuserLessonType("");
    setuserlessonname("");
  };
  const rootnamefromuser = (e) => {
    setuserrootname(e.value);
    setuserLessonType("");
    setuserlessonname("");
  };
  const setuserlesson = (e) => {
    setuserlessonname(e.value);
  };
  const createTopicId = () => {
    // let date = new Date();
    // let ModifiedDate = (date.toString().replaceAll(" ","")).substring(0,20);
    // setTopicId(TopicName+"_"+ModifiedDate);

    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "")
      .toLowerCase();

    let final = TopicName.toLowerCase().replace(/[^a-z0-9 -]/gi, "");
    let newfinaldata = final.replace(/\s+/, "");
    setTopicId(newfinaldata + ModifiedDate);

    setcheck(true);
  };
  const setlessonType = (e) => {
    setuserLessonType(e.value);
    setuserlessonname("");
  };
  const settopicTypeEnum = (e) => {
    settopicTypeEnumFromUser(e.value);
    if (e.value == 1) {
      sethasQuizDisable(true);
      sethasQuiz(true);
    } else {
      sethasQuizDisable(false);
    }
  };
  const submitDataOnFirebase = async () => {
    if (check == false) {
      alert("please confirm");
    } else {
      let TopicChildItem = [];
      let TopicQuizItem = [];
      let NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt = Math.abs(
        parseInt(NoOfHeartsThatAreToBeCreditedToAfterCompletion, 10)
      );
      const docData = {
        topicAssociatedCourse: userrootname,
        topicAssociatedLesson: userlessonname,
        topicAssociatedCourseType: rootvalue,
        topicAssociatedLessonType: userLessonType,
        topicId: TopicId,
        topicName: TopicName,
        topicShortDesc: ShortDescription,
        topicTypeEnum: topicTypeEnumFromUser,
      };
      const infoData = {
        topicChildItems: TopicChildItem,
        topicCompletionText: TextAfterCompletionOfTopicRound,
        topicHeartTxn: NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt,
        topicQuiz: TopicQuizItem,
        topicTypeEnum: topicTypeEnumFromUser,
        topichasQuiz: hasQuiz,
      };
      const data = doc(db, `Lessons/${userlessonname}`);
      await updateDoc(data, { lessonTopics: arrayUnion(TopicId) });
      await setDoc(doc(db, "Topics", TopicId), docData).then(async () => {
        await setDoc(
          doc(db, `Topics/${TopicId}/extraInfo`, "infoDoc"),
          infoData
        );
      });
      alert("data updated successfully");
      setlessonName1([]);
      setrootName([]);
      setrootvalue("");
      setuserLessonType("");
      setuserlessonname("");
      settopicTypeEnumFromUser("");
      setuserrootname("");
      setTopicId("");
      document.getElementById("Topic-form").reset();
    }
  };
  function submit(e) {
    e.preventDefault();
  }
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Topic Form</h1>
      <form id="Topic-form" onSubmit={submit}>
        <label> Choose the Root : </label>
        <Select
          value={root.filter(function (option) {
            return option.value === rootvalue;
          })}
          options={root}
          onChange={rootvaluefromuser}
        ></Select>
        <button type="button" onClick={getrootName}>
          {" "}
          Get Data
        </button>
        <br />
        <br />
        <label> Choose the Root Name : </label>
        <Select
          value={rootName.filter(function (option) {
            return option.value === userrootname;
          })}
          options={rootName}
          onChange={rootnamefromuser}
        ></Select>
        <br />
        <br />
        <label> Choose the lesson Type : </label>
        <Select
          value={lesson.filter(function (option) {
            return option.value === userLessonType;
          })}
          options={lesson}
          onChange={setlessonType}
        ></Select>
        <button type="button" onClick={getlessonName}>
          {" "}
          Get Data
        </button>
        <br />
        <br />
        <label> Choose the Lesson Name : </label>
        <Select
          value={lessonName1.filter(function (option) {
            return option.value === userlessonname;
          })}
          options={lessonName1}
          onChange={setuserlesson}
        ></Select>
        <br />
        <br />
        <label>Create Topic/Round : </label>
        <Select
          value={topicTypeEnum.filter(function (option) {
            return option.value === topicTypeEnumFromUser;
          })}
          options={topicTypeEnum}
          onChange={settopicTypeEnum}
        ></Select>
        <br />
        <br />
        <label>Topic Name : </label>
        <input
          required
          type="text"
          placeholder="Topic Name"
          onChange={(event) => {
            setTopicName(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={TopicChildItemDisable}>Topic Child Item : </label>
        <br />
        <br />
        <label>Will this topic have any quiz ? : </label>
        <input
          type="checkbox"
          // disabled={hasQuizDisable}
          disabled
          // checked={hasQuiz}
          checked
          // defaultChecked={hasQuiz}
          defaultChecked={true}
          // onChange={(event) => {
          //   sethasQuiz(!hasQuiz);
          // }}
        />
        <br />
        <br />
        {/* <label>
          No of hearts that are to be credited to after completion ? :{" "}
        </label>
        <input
          required
          type="number"
          style={{ width: "50px" }}
          min="0"
          max={10}
          placeholder="No of hearts that are to be credited to after completion"
          onChange={(event) => {
            setNoOfHeartsThatAreToBeCreditedToAfterCompletion(
              event.target.value
            );
          }}
        />
        <br />
        <br /> */}
        <label>Topic Id : </label>
        <span>{TopicId}</span>
        <br />
        <br />
        <label hidden={ShortDescriptionDisable}>
          {" "}
          Short Description (maximum 45 characters) :{" "}
        </label>
        <textarea
          required
          maxLength="45"
          hidden={ShortDescriptionDisable}
          cols="30"
          rows="10"
          type="text"
          placeholder="Short Description "
          onChange={(event) => {
            setShortDescription(event.target.value);
          }}
        />
        <br />
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={createTopicId}>Confirm</button>{" "}
        </div>
        <br />
        <br />
        <label hidden={TextAfterCompletionOfTopicRoundDisable}>
          Text After Completion of Topic/Round (maximum 30 characters) :{" "}
        </label>
        <textarea
          required
          maxLength="30"
          hidden={TextAfterCompletionOfTopicRoundDisable}
          cols="30"
          rows="10"
          type="text"
          placeholder="Text After Completion of Topic/Round"
          onChange={(event) => {
            setTextAfterCompletionOfTopicRound(event.target.value);
          }}
        />
        <br />
        <br />
        <label>Quizes under Topic/Round : </label>
        <br />
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={submitDataOnFirebase}>Submit</button>{" "}
        </div>
        <br />
        <br />
      </form>
    </>
  );
};

export default TopicForm;
