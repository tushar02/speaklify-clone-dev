import React, { useState } from 'react'
import CourseForm from './CourseForm';
import CourseTable from './CourseTable';
import './course.css'

function Course() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Root");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to course");
    }else{
     
      setvalue("Add new Root");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<CourseTable/>:<CourseForm/>}
      </>
  )
}

export default Course;