import { db, storage } from "../../firebase/Firebase-config";
import { useState } from "react";
import {
  collection,
  getDocs,
  setDoc,
  doc,
  getDoc,
  Timestamp,
} from "firebase/firestore";
import Select from "react-select";
import { useEffect } from "react";
import { getDownloadURL, uploadBytesResumable } from "@firebase/storage";
import { ref, deleteObject } from "firebase/storage";
import { isDisabled } from "@testing-library/user-event/dist/utils";
const CourseForm = () => {
  const [rootvalue, setrootvalue] = useState("");
  const [Id, setId] = useState("");
  const [Name, setName] = useState("");
  const [imageDate, setImageDate] = useState("");
  const [Author, setAuthor] = useState("");
  const [Slug, setSlug] = useState("");
  const [date, setdate] = useState("");
  const [subscriptionId, setsubscriptionId] = useState("");
  const [TextAfterCompletionOfTopicRound, setTextAfterCompletionOfTopicRound] =
    useState("");
  const [LifeNeededToAccess, setLifeNeededToAccess] = useState(0);
  const [AnimationTypeFromUser, setAnimationTypeFromUser] = useState("");
  const [FlowTypeFromUser, setFlowTypeFromUser] = useState(0);
  const [AccessTypeFromUser, setAccessTypeFromUser] = useState(0);
  const [ShortDescription, setShortDescription] = useState("");
  const [
    NoOfHeartsThatAreToBeCreditedToAfterCompletion,
    setNoOfHeartsThatAreToBeCreditedToAfterCompletion,
  ] = useState(0);
  const [WillThereBeAnyFinalQuiz, setWillThereBeAnyFinalQuiz] = useState(false);
  const [WillThereBeAnyArcade, setWillThereBeAnyArcade] = useState(false);
  const [isDraftUSer, setisDraftUSer] = useState(false);
  const [isDraftUSerDisable, setisDraftUSerDisable] = useState(false);
  const [flow, setflow] = useState("");
  const [check, setcheck] = useState(false);
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [practiceNode, setpracticeNode] = useState("");
  const [PracticeId, setPracticeId] = useState([]);
  const [DepIdFromUser, setDepIdFromUser] = useState("");
  const [WillThereBeAnyArcadeDisable, setWillThereBeAnyArcadeDisable] =
    useState(false);
  const [originalPrice, setOriginalPrice] = useState("");
  const [discountPrice, setDiscountPrice] = useState("");
  const [priceDisable, setPriceDisable] = useState([]);
  const [DependencyIDDisable, setDependencyIDDisable] = useState(false);
  const [FeaturedImageDisable, setFeaturedImageDisable] = useState(false);
  const [AuthorDisable, setAuthorDisable] = useState(false);
  const [WillThereBeAnyFinalQuizDisable, setWillThereBeAnyFinalQuizDisable] =
    useState(false);
  const [
    AnimationScreenAfterCompletionDisable,
    setAnimationScreenAfterCompletionDisable,
  ] = useState(false);
  const [
    TextAfterCompletionofLessonObjectiveArcadeDisabled,
    setTextAfterCompletionofLessonObjectiveArcadeDisabled,
  ] = useState(false);
  const [ShortDescriptionDisable, setShortDescriptionDisable] = useState(false);
  const [FlowDisable, setFlowDisable] = useState(false);
  const [LifeNeededToAccessDisable, setLifeNeededToAccessDisable] =
    useState(false);
  const [AccessTypeDisable, setAccessTypeDisable] = useState(false);
  const [LanguageCategoryDisable, setLanguageCategoryDisable] = useState(false);
  const [LessonObjectiveUnderNodeDisable, setLessonObjectiveUnderNodeDisable] =
    useState(false);
  const [FinalArcadeundercourseDisable, setFinalArcadeundercourseDisable] =
    useState(false);
  const [FinalQuizundercourseDisable, setFinalQuizundercourseDisable] =
    useState(false);
  const [subscriptionIdDisable, setsubscriptionIdDisable] = useState(false);
  const [contentTypeFromUser, setcontentTypeFromUser] = useState(0);
  const [content, setcontent] = useState([]);
  var root = [
    {
      value: "Courses",
      label: "General Course",
    },
    {
      value: "Grammar",
      label: "Grammar",
    },
    {
      value: "Practice",
      label: "Practice",
    },
    {
      value: "video",
      label: "Video",
    },
    {
      value: "Test",
      label: "Test",
    },
    {
      value: "Story",
      label: "Story",
    },
    {
      value: "Audio",
      label: "Audio",
    },
  ];

  var FlowType = [
    {
      value: 0,
      label: "Free Flow",
    },
    {
      value: 1,
      label: "Linear Flow",
    },
  ];
  const [AccessType, setaccessType] = useState([
    {
      value: 0,
      label: "Free",
      isDisabled: false,
    },
    {
      value: 1,
      label: "Paid",
      isDisabled: false,
    },
  ]);
  var GrammerContentType = [
    {
      value: 0,
      label: "Verb",
    },
    {
      value: 1,
      label: "Tense",
    },
    {
      value: 2,
      label: "Passive Voice",
    },
    {
      value: 3,
      label: "Sentence",
    },
    {
      value: 4,
      label: "Noun",
    },
  ];
  var StoryAudioContentType = [
    {
      value: 0,
      label: "Meet and greet",
    },
    {
      value: 1,
      label: "Daily life Conversation",
    },
    {
      value: 2,
      label: "Travelling",
    },
    {
      value: 3,
      label: "Dining",
    },
    {
      value: 4,
      label: "Shopping",
    },
  ];
  useEffect(() => {
    getPracticeNode();
  }, [Id]);
  const IfWeChooseCourse = () => {
    setWillThereBeAnyArcadeDisable(true);
    setsubscriptionIdDisable(false);
    setDependencyIDDisable("none");
    setFeaturedImageDisable(false);
    setAuthorDisable(false);
    setWillThereBeAnyFinalQuizDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setShortDescriptionDisable(false);
    setFlowDisable(false);
    setLifeNeededToAccessDisable(false);
    setAccessTypeDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(false);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const IfWeChoosePractice = () => {
    setFeaturedImageDisable(true);
    setAuthorDisable(true);
    setsubscriptionIdDisable(true);
    setWillThereBeAnyFinalQuizDisable(true);
    setAnimationScreenAfterCompletionDisable(false);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(true);
    setShortDescriptionDisable(true);
    setFlowDisable("none");
    setLifeNeededToAccessDisable(true);
    setAccessTypeDisable("none");
    setWillThereBeAnyArcadeDisable(true);
    setDependencyIDDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(true);
    setFinalArcadeundercourseDisable(false);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(true);
    setisDraftUSerDisable(true);
    setisDraftUSer(false);
  };
  const IfWeChooseTest = () => {
    setAuthorDisable(true);
    setWillThereBeAnyArcadeDisable(true);
    setDependencyIDDisable("none");
    setsubscriptionIdDisable(true);
    setShortDescriptionDisable(true);
    setFlowDisable("none");
    setAccessTypeDisable(false);
    setFeaturedImageDisable(false);
    setWillThereBeAnyFinalQuizDisable(true);
    setWillThereBeAnyFinalQuiz(true);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(false);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(true);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const IfWeChooseGrammar = () => {
    setWillThereBeAnyArcadeDisable(true);
    setDependencyIDDisable("none");
    setFeaturedImageDisable(false);
    setsubscriptionIdDisable(false);
    setAuthorDisable(false);
    setWillThereBeAnyFinalQuizDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setShortDescriptionDisable(false);
    setFlowDisable(false);
    setLifeNeededToAccessDisable(false);
    setAccessTypeDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(false);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const IfWeChooseVideo = () => {
    setWillThereBeAnyArcadeDisable(true);
    setWillThereBeAnyFinalQuizDisable(true);
    setsubscriptionIdDisable(true);
    setDependencyIDDisable("none");
    setShortDescriptionDisable(true);
    setFlowDisable("none");
    setAccessTypeDisable("none");
    setFeaturedImageDisable(false);
    setAuthorDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(true);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(true);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const IfWeChooseAudio = () => {
    setWillThereBeAnyArcadeDisable(true);
    setWillThereBeAnyFinalQuizDisable(true);
    setDependencyIDDisable("none");
    setsubscriptionIdDisable(true);
    setShortDescriptionDisable(true);
    setFlowDisable("none");
    setAccessTypeDisable("none");
    setFeaturedImageDisable(false);
    setAuthorDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(true);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const IfWeChooseStroy = () => {
    setAccessTypeDisable("none");
    setWillThereBeAnyFinalQuizDisable(true);
    setsubscriptionIdDisable(true);
    setWillThereBeAnyArcadeDisable(true);
    setDependencyIDDisable("none");
    setShortDescriptionDisable(true);
    setFlowDisable("none");
    setFeaturedImageDisable(false);
    setAuthorDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(false);
    setLanguageCategoryDisable(true);
    setFinalQuizundercourseDisable(true);
    setFinalArcadeundercourseDisable(true);
    setLessonObjectiveUnderNodeDisable(false);
    setWillThereBeAnyArcade(false);
    setisDraftUSerDisable(false);
  };
  const SETCATEGORY = async (type) => {
    let data1 = "";
    if (type == "Courses") {
      data1 = doc(db, `Enums/CourseCategoryEnum`);
    } else if (type == "Grammar") {
      data1 = doc(db, `Enums/GrammarCategoryEnum`);
    } else if (type == "Practice") {
      data1 = doc(db, `Enums/PracticeCategoryEnum`);
    } else if (type == "video") {
      data1 = doc(db, `Enums/VideoCategoryEnum`);
    } else if (type == "Test") {
      data1 = doc(db, `Enums/TestCategoryEnum`);
    } else if (type == "Story") {
      data1 = doc(db, `Enums/StoryCategoryEnum`);
    } else if (type == "Audio") {
      data1 = doc(db, `Enums/audioCategoryEnum`);
    }
    const docSnap = await getDoc(data1);
    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      let data = docSnap.data();
      setcontent([]);
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          console.log(key + " -> " + data[key]);
          const query = { value: key, label: data[key] };
          setcontent((rootName) => rootName.concat(query));
        }
      }
    } else {
      console.log("No such document!");
    }
  };
  useEffect(() => {
    if (rootvalue == "Courses") {
      IfWeChooseCourse();
    } else if (rootvalue == "Practice") {
      IfWeChoosePractice();
    } else if (rootvalue == "Test") {
      IfWeChooseTest();
    } else if (rootvalue == "Grammar") {
      IfWeChooseGrammar();
    } else if (rootvalue == "video") {
      IfWeChooseVideo();
    } else if (rootvalue == "Audio") {
      IfWeChooseAudio();
    } else if (rootvalue == "Story") {
      IfWeChooseStroy();
    }
    SETCATEGORY(rootvalue);
  }, [rootvalue]);
  const getPracticeNode = async () => {
    const usercollectionRef = collection(db, `Practice`);
    const data = await getDocs(usercollectionRef);
    setpracticeNode(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const SETPRACTICENODE = () => {
    setPracticeId([]);
    console.log(
      practiceNode.map((user) => {
        const query = { value: user.courseId, label: user.courseId };
        setPracticeId((PracticeId) => PracticeId.concat(query));
      })
    );
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    if (e.value == "Courses" || e.value == "Test") {
      setaccessType((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else {
      setaccessType((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = true;
          }
          return curvalue;
        });
      });
    }
  };
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl1("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let flag = true;
    var reader = new FileReader();

    //Read the contents of Image File.
    reader.readAsDataURL(file);
    reader.onload = function (e) {
      //Initiate the JavaScript Image object.
      var image = new Image();

      //Set the Base64 string return from FileReader as source.
      image.src = e.target.result;

      //Validate the File Height and Width.
      image.onload = function () {
        var height = this.height;
        var width = this.width;
        if (height != 512 && width != 512) {
          alert("Height and Width must not equal 512*512.");
          flag = false;
          return false;
        } else {
          let date = new Date();
          let ModifiedDate = date
            .toString()
            .replaceAll(" ", "")
            .substring(0, 20)
            .replace(/[^a-z0-9 -]/gi, "");
          const storageref = ref(
            storage,
            `/files/${file.name}_${ModifiedDate}`
          );
          const uploadTask = uploadBytesResumable(storageref, file);
          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const prog = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              );
              setprogress1(prog);
            },
            (err) => console.log(err),
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((url) =>
                console.log(seturl1(url))
              );
            }
          );
          return true;
        }
      };
    };
    /*   if(flag){
      let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
      const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
      const  uploadTask = uploadBytesResumable(storageref,file);
      uploadTask.on("state_changed",(snapshot)=>{
         const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
         setprogress1(prog);
      },(err)=>console.log(err),()=>{
         getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
      });
    } */
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  };
  useEffect(() => {
    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "")
      .toLowerCase();
    setImageDate(date.toString().substring(0, 21));
  }, []);
  const createTopicId = () => {
    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "")
      .toLowerCase();
    setdate(date.toString().substring(0, 21));
    let final = Name.toLowerCase().replace(/[^a-z0-9 -]/gi, "");
    setId(final + "_" + ModifiedDate);
    setcheck(true);
    SETPRACTICENODE();
  };
  const setcontentType = (e) => {
    setcontentTypeFromUser(e.value);
  };
  const setFlowType = (e) => {
    setFlowTypeFromUser(e.value);
  };
  const setAccessType = (e) => {
    setAccessTypeFromUser(e.value);
    if (e.value == 1 && rootvalue == "Courses") {
      setPriceDisable(false);
    } else {
      setPriceDisable(true);
    }
  };
  const SetDepId = (e) => {
    setDepIdFromUser(e.value);
  };

  const submitDataOnFirebase = async () => {
    if (check == false) {
      alert("please confirm");
    } else {
      let depId = DepIdFromUser;
      let type = 0;
      if (rootvalue == "Courses") {
        type = 0;
        depId = "Null";
      } else if (rootvalue == "Grammar") {
        type = 3;
      } else if (rootvalue == "Practice") {
        type = 1;
      } else if (rootvalue == "video") {
        type = 5;
      } else if (rootvalue == "Test") {
        type = 2;
      } else if (rootvalue == "Story") {
        type = 4;
      } else if (rootvalue == "Audio") {
        type = 6;
      }
      let LessonArray = [];
      let NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt = Math.abs(
        parseInt(NoOfHeartsThatAreToBeCreditedToAfterCompletion, 10)
      );
      let LifeNeededToAccessAsInt = Math.abs(parseInt(LifeNeededToAccess, 10));
      let contentTypeFromUserAsInt = Math.abs(
        parseInt(contentTypeFromUser, 10)
      );
      let link = "";
      let FeaturedLink = "";
      if (type == 1) {
        link = url1;
      } else {
        FeaturedLink = url1;
      }
      const docData = {
        courseAuthor: Author,
        courseFeaturedImageUrl: FeaturedLink,
        courseGroupId: "",
        courseId: Id,
        courseName: Name,
        levelImage: link,
        subscriptionId: subscriptionId,
        isDraft: isDraftUSer,
      };
      const infoData = {
        courseAccessCost: LifeNeededToAccessAsInt,
        courseAccessTypeEnum: AccessTypeFromUser,
        courseCategoryTypeEnum: type,
        courseCompletionAnimTemplate: "",
        courseCompletionText: TextAfterCompletionOfTopicRound,
        courseCreationDate: Timestamp.fromDate(new Date()),
        courseDependencyId: depId,
        courseFinalArcade: "",
        discountPrice: discountPrice,
        originalPrice: originalPrice,
        courseFinalQuiz: "",
        courseFlow: FlowTypeFromUser,
        courseHeartTxn: NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt,
        courseLanguageCategoryEnum: 0,
        courseLessons: LessonArray,
        courseShortDesc: ShortDescription,
        hasFinalArcade: WillThereBeAnyArcade,
        hasFinalQuiz: WillThereBeAnyFinalQuiz,
        contentCategory: contentTypeFromUserAsInt,
      };
      await setDoc(doc(db, `${rootvalue}`, Id), docData)
        .then(async () => {
          await setDoc(
            doc(db, `${rootvalue}/${Id}/extraInfo`, "infoDoc"),
            infoData
          );
        })
        .then(() => {
          alert("data updated successfully");
          document.getElementById("Course-form").reset();
          setrootvalue("");
          setWillThereBeAnyArcade(false);
          setWillThereBeAnyFinalQuiz(false);
          setDepIdFromUser("");
          setcontentTypeFromUser(0);
          setFlowTypeFromUser(0);
          setAccessTypeFromUser(0);
          setsubscriptionId("");
          setId("");
          setdate("");
          seturl1("");
          setprogress1(0);
        });
    }
  };
  function submit(e) {
    e.preventDefault();
  }
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Course Form</h1>
      <form id="Course-form" onSubmit={submit}>
        <label>Name : </label>
        <input
          type="text"
          required
          placeholder="Root Name"
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <br />
        <br />
        <label>System Category Type : </label>
        <Select
          required
          value={root.filter(function (option) {
            return option.value === rootvalue;
          })}
          options={root}
          onChange={rootvaluefromuser}
        ></Select>
        <br />
        <br />
        <label hidden={AuthorDisable}>Author : </label>
        <input
          hidden={AuthorDisable}
          type="text"
          placeholder="Author"
          required
          onChange={(event) => {
            setAuthor(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={FeaturedImageDisable}>Featured Image : </label>
        <input
          hidden={FeaturedImageDisable}
          required
          type="file"
          onChange={onImageChange1}
        />
        <span hidden={FeaturedImageDisable}>uploaded {progress1}%</span>
        <button
          hidden={FeaturedImageDisable}
          type="button"
          onClick={() => uploadFiles1(file1)}
        >
          upload
        </button>
        <button
          hidden={FeaturedImageDisable}
          type="button"
          onClick={() => deleteImage1(url1)}
        >
          Delete
        </button>
        <img
          hidden={FeaturedImageDisable}
          src={url1}
          alt=""
          width="193"
          height="130"
        />
        <br />
        <br />
        {/* <label>
          No of hearts that are to be credited to after
          completion ? :{" "}
        </label>
        <input
          type="number"
          style={{ width: "50px" }}
          required
          min="0"
          max={50}
          oninput="javascript: if (this.value > this.min) this.value = this.value.slice(0, this.min);"
          placeholder="No of hearts that are to be credited to after completion"
          onChange={(event) => {
            setNoOfHeartsThatAreToBeCreditedToAfterCompletion(
              event.target.value
            );
          }}
        /> */}
        <label hidden={WillThereBeAnyArcadeDisable}>
          Will there be any arcade ? :{" "}
        </label>
        <input
          hidden={WillThereBeAnyArcadeDisable}
          type="checkbox"
          defaultChecked={WillThereBeAnyArcade}
          onChange={(event) => {
            setWillThereBeAnyArcade(!WillThereBeAnyArcade);
          }}
        />
        <br />
        <br />
        <label hidden={WillThereBeAnyFinalQuizDisable}>
          Will there be any final quiz? :{" "}
        </label>
        <input
          hidden={WillThereBeAnyFinalQuizDisable}
          require
          type="checkbox"
          defaultChecked={WillThereBeAnyFinalQuiz}
          onChange={(event) => {
            setWillThereBeAnyFinalQuiz(!WillThereBeAnyFinalQuiz);
          }}
        />
        <br />
        <br />
        <label>isDraft : </label>
        <input
          type="checkbox"
          defaultChecked={isDraftUSer}
          onChange={(event) => {
            setisDraftUSer(!isDraftUSer);
          }}
        />
        <br />
        <br />
        <label hidden={AnimationScreenAfterCompletionDisable}>
          Profile Background Image for Level :{" "}
        </label>
        <input
          hidden={AnimationScreenAfterCompletionDisable}
          required
          type="file"
          onChange={onImageChange1}
        />
        <span hidden={AnimationScreenAfterCompletionDisable}>
          uploaded {progress1}%
        </span>
        <button
          hidden={AnimationScreenAfterCompletionDisable}
          onClick={() => uploadFiles1(file1)}
          type="button"
        >
          upload
        </button>
        <button
          hidden={AnimationScreenAfterCompletionDisable}
          onClick={() => deleteImage1(url1)}
          type="button"
        >
          Delete
        </button>
        <img
          hidden={AnimationScreenAfterCompletionDisable}
          src={url1}
          alt=""
          width="193"
          height="130"
        />
        <br />
        <br />
        <label hidden={TextAfterCompletionofLessonObjectiveArcadeDisabled}>
          Text After Completion of this Node :{" "}
        </label>
        <textarea
          required
          hidden={TextAfterCompletionofLessonObjectiveArcadeDisabled}
          type="text"
          cols="30"
          rows="5"
          placeholder="Text After Completion of Course"
          onChange={(event) => {
            setTextAfterCompletionOfTopicRound(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={ShortDescriptionDisable}> Short Description : </label>
        <textarea
          cols="30"
          rows="10"
          required
          hidden={ShortDescriptionDisable}
          type="text"
          placeholder="Short Description"
          onChange={(event) => {
            setShortDescription(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={subscriptionIdDisable}>Subscription Id : </label>
        <textarea
          required
          maxlength="45"
          cols="30"
          rows="5"
          hidden={subscriptionIdDisable}
          type="text"
          placeholder="Subscription Id"
          onChange={(event) => {
            setsubscriptionId(event.target.value);
          }}
        />
        <br />
        <br />
        <label>Content Category</label>
        <Select
          // value={content.filter(function (option) {
          //   return option.value === contentTypeFromUser;
          // })}
          value={content[0]}
          options={content}
          isDisabled={true}
          // onChange={setcontentType}
        ></Select>
        <br />
        <br />
        <div style={{ display: FlowDisable }}>
          <label>Flow : </label>
          <Select
            value={FlowType.filter(function (option) {
              return option.value === FlowTypeFromUser;
            })}
            options={FlowType}
            onChange={setFlowType}
          ></Select>
          <br />
        </div>
        <div style={{ display: AccessTypeDisable }}>
          <label>Access Type : </label>
          <Select
            value={AccessType.filter(function (option) {
              return option.value === AccessTypeFromUser;
            })}
            options={AccessType}
            onChange={setAccessType}
          ></Select>
          <br />
          <br />
        </div>
        <div style={{ display: priceDisable ? "none" : "" }}>
          <label>Original Price : </label>
          <input
            required
            min={0}
            type="number"
            placeholder="Original Price"
            onChange={(event) => {
              setOriginalPrice(event.target.value);
            }}
          />
          <br />
          <br />
          <label> Discounted Price : </label>
          <input
            min={0}
            required
            type="number"
            placeholder="Discounted Price"
            onChange={(event) => {
              setDiscountPrice(event.target.value);
            }}
          />
        </div>
        <br />
        <label hidden={LifeNeededToAccessDisable}>
          Life Needed To Access :{" "}
        </label>
        <input
          required
          hidden={LifeNeededToAccessDisable}
          type="number"
          min="0"
          max={10}
          style={{ width: "50px" }}
          placeholder="Life Needed To Access"
          onChange={(event) => {
            setLifeNeededToAccess(event.target.value);
          }}
        />
        <br />
        <br />
        <label> Unique System Generated Id : </label>
        <span>{Id}</span>
        <br />
        <br />
        <label>Date of Creation : </label>
        <span>{date}</span>
        <br />
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={createTopicId}>Confirm</button>{" "}
        </div>
        <br />
        <br />
        <div style={{ display: DependencyIDDisable }}>
          <label>Dependency ID : </label>
          <Select
            value={PracticeId.filter(function (option) {
              return option.value === DepIdFromUser;
            })}
            options={PracticeId}
            onChange={SetDepId}
          ></Select>
          <br />
          <br />
        </div>
        <label hidden={LanguageCategoryDisable}>
          Language Category : Beng-Eng
        </label>
        <br />
        <br />
        <label hidden={LessonObjectiveUnderNodeDisable}>
          Lesson/Objective Under Node
        </label>
        <br />
        <br />
        <label hidden={FinalArcadeundercourseDisable}>
          Final Arcade under course :{" "}
        </label>
        <br />
        <br />
        <label hidden={FinalQuizundercourseDisable}>
          Final Quiz under course :{" "}
        </label>
        <br />
        <br />
        <br />
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={submitDataOnFirebase} type="submit">
            Submit
          </button>{" "}
        </div>
        <br />
        <br />
      </form>
    </>
  );
};

export default CourseForm;
/*  const data = doc(db, `Quizs/${userquizname}`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let quizQuestionPool =docSnap1.data().quizQuestionPool;
          if(quizQuestionPool==""){
              let questionArray = [];
            const docData = {   
                associatedLesson:userlessonname,
                associatedQuiz:userquizname,
                associatedRoot:userrootname,
                associatedTopic:usertopicname,     
                poolName:QuestionPoolName,
                questionPoolId: QuestionPoolId,
                questionsId:questionArray,
            };
            await setDoc(doc(db, "QuestionPool", QuestionPoolId),docData);
            const washingtonRef = doc(db, "Quizs", userquizname);
            await updateDoc(washingtonRef, {quizQuestionPool: QuestionPoolId});
            alert("QuestionPool Created successsfully");
          }else{
            alert("Please Choose another Quiz");
          }
        } else {
          console.log("No such document!");
        } */
