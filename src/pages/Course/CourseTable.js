import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc , arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable} from '@firebase/storage';
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { style, width } from '@mui/system';
import { Delete, TextFields, Upload } from '@mui/icons-material';
import { TextField } from '@material-ui/core';
function CourseTable() {
  const [Details, setDetails] = useState([]);
  const [Users1,setUsers1] = useState('');
  const [priceDisable , setPriceDisable] = useState(true);
  const [discountPrice , setdiscountPrice] = useState('')
  const [originalPrice, setoriginalPrice] = useState('');
    const [WillThereBeAnyFinalQuiz, setWillThereBeAnyFinalQuiz] =
      useState(false);
  const [Reset, setReset] = useState(false);
  const [rootvalue  , setrootvalue] = useState('');
  const [Open  , setOpen] = useState(false);
  const [Id , setId] = useState('');
  const [CourseAuthor , setCourseAuthor] = useState('');
  const [CourseAccessCost , setCourseAccessCost] = useState(0);
  const [CourseCompletionText , setCourseCompletionText] = useState('');
  const [CourseHeartTxn , setCourseHeartTxn] = useState(0);
  const [CourseShortDesc , setCourseShortDesc] = useState('');
  const [image1, setImage1] = useState(null);
  const [file1 , setfile1] = useState(null);
  const[url1 , seturl1] = useState("");
  const [progress1 , setprogress1] = useState(0);
  const [AnimationTypeFromUser , setAnimationTypeFromUser] = useState('');  
  const [FlowTypeFromUser , setFlowTypeFromUser] = useState(0);       
  const [AccessTypeFromUser , setAccessTypeFromUser] = useState(0); 
  const [practiceNode , setpracticeNode] = useState('');
  const [PracticeId , setPracticeId] = useState([]);
  const [DepIdFromUser , setDepIdFromUser] = useState('');
    const [DependencyIDDisable , setDependencyIDDisable] = useState(false);
    const [FeaturedImageDisable, setFeaturedImageDisable] = useState(false);
    const [AnimationScreenAfterCompletionDisable , setAnimationScreenAfterCompletionDisable] = useState(false);
    const [TextAfterCompletionofLessonObjectiveArcadeDisabled , setTextAfterCompletionofLessonObjectiveArcadeDisabled] = useState(false);
  const [ShortDescriptionDisable , setShortDescriptionDisable] = useState(false);
  const [FlowDisable , setFlowDisable] = useState(false);
  const [IsDraftUser , setIsDraftUser] = useState(false);
  const [LifeNeededToAccessDisable , setLifeNeededToAccessDisable] = useState(false);
  const [AccessTypeDisable , setAccessTypeDisable] = useState(false);
  const [ShortDescription , setShortDescription] = useState('');    
    const [NoOfHeartsThatAreToBeCreditedToAfterCompletion , setNoOfHeartsThatAreToBeCreditedToAfterCompletion] = useState(0); 
    const [TextAfterCompletionOfTopicRound , setTextAfterCompletionOfTopicRound] = useState('');    
    const [LifeNeededToAccess , setLifeNeededToAccess] = useState(0);    
  var AnimationType = [
    {
      value : 0,
      label : "1"
    },
    {
        value : 1,
        label : "2"
    },
    {
     value : 2,
     label : "3"
   }
  ];
  useEffect(()=>{
    getPracticeNode();
  },[Open])

  const getPracticeNode = async()=>{
    const usercollectionRef = collection(db, `Practice`)
    const data = await getDocs(usercollectionRef)
      setpracticeNode(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));  
  }
  const SETPRACTICENODE =()=>{
  setPracticeId([]);
  console.log(practiceNode.map((user) => { 
      const query = {value : user.courseId, label:user.courseName}
   setPracticeId(PracticeId => PracticeId.concat(query))}));
  }
  const SetDepId = (e)=>{
    setDepIdFromUser(e.value);
  }
  useEffect(() => { 
    const getUsers = async () => {
        const usercollectionRef = collection(db, `${rootvalue}`)
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [rootvalue,Reset,Open]); 
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async(i) => {
    setOpen(true);
    setId(Details[i].courseId);
    setCourseAuthor(Details[i].courseAuthor);
    setIsDraftUser(Details[i].isDraft);
    if(rootvalue=='Practice'){
      seturl1(Details[i].levelImage)
    }else{
    seturl1(Details[i].courseFeaturedImageUrl);
  }
    const TopicInfo = doc(db, `${rootvalue}/${Details[i].courseId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(TopicInfo);
  
    if(docSnap1.exists()){
        setCourseAccessCost(docSnap1.data().courseAccessCost);
        setCourseCompletionText(docSnap1.data().courseCompletionText);
        setCourseHeartTxn(docSnap1.data().courseHeartTxn);
        setCourseShortDesc(docSnap1.data().courseShortDesc);
        setFlowTypeFromUser(docSnap1.data().courseFlow);
      setAccessTypeFromUser(docSnap1.data().courseAccessTypeEnum);
      setWillThereBeAnyFinalQuiz(docSnap1.data().hasFinalQuiz);
        setDepIdFromUser(docSnap1.data().courseDependencyId);
        setoriginalPrice(docSnap1.data().originalPrice);
        setdiscountPrice(docSnap1.data().discountPrice);
        if(docSnap1.data().courseAccessTypeEnum==1 && rootvalue=="Courses"){
          setPriceDisable(false);
        }else{
          setPriceDisable(true);
        }
    }  else {
        console.log("No such document! user 5");
      }
  };
  const IfWeChooseCourse=()=>{
        setDependencyIDDisable('none');
        setFeaturedImageDisable(false);
        setAnimationScreenAfterCompletionDisable(true);
        setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
        setShortDescriptionDisable(false);
        setFlowDisable(false);
        setLifeNeededToAccessDisable(false);
        setAccessTypeDisable(false);
  }
  const IfWeChoosePractice =()=>{
    setFeaturedImageDisable(true);
    setAnimationScreenAfterCompletionDisable(false);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(true);
    setShortDescriptionDisable(true);
    setFlowDisable('none');
    setLifeNeededToAccessDisable(true);
    setAccessTypeDisable('none');
    setDependencyIDDisable(false);
  }
  const IfWeChooseTest=()=>{
    setDependencyIDDisable('none');
    setShortDescriptionDisable(true);
    setFlowDisable('none');
    setAccessTypeDisable('none');
    setFeaturedImageDisable(false);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
  }
  const IfWeChooseGrammar=()=>{
      setDependencyIDDisable('none');
      setFeaturedImageDisable(false);
      setAnimationScreenAfterCompletionDisable(true);
      setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
      setShortDescriptionDisable(false);
      setFlowDisable(false);
      setLifeNeededToAccessDisable(false);
      setAccessTypeDisable(false);
  }
  const IfWeChooseVideo =()=>{
             setDependencyIDDisable('none');
             setShortDescriptionDisable(true);
             setFlowDisable(true);
             setAccessTypeDisable(true);
             setFeaturedImageDisable(false);
             setAnimationScreenAfterCompletionDisable(true);
             setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
             setLifeNeededToAccessDisable(false);
  }
  const IfWeChooseStroy=()=>{
    setAccessTypeDisable('none');
    setDependencyIDDisable('none');
    setShortDescriptionDisable(true);
    setFlowDisable('none');
    setFeaturedImageDisable(false);
    setAnimationScreenAfterCompletionDisable(true);
    setTextAfterCompletionofLessonObjectiveArcadeDisabled(false);
    setLifeNeededToAccessDisable(false);
  }
useEffect(()=>{
      if(rootvalue=='Courses'){
        IfWeChooseCourse();
      }else if(rootvalue=='Practice'){
          IfWeChoosePractice();
      }else if(rootvalue=='Test'){
         IfWeChooseTest();
      }else if(rootvalue=='Grammar'){
        IfWeChooseGrammar();
      }else if(rootvalue=='video'){
        IfWeChooseVideo();
      }else if(rootvalue=="Audio"){
        IfWeChooseVideo();
      }else if(rootvalue=='Story'){
        IfWeChooseStroy();
      }
},[rootvalue])
  const deleteImage1 = (file) =>{
    const desertRef = ref(storage, file);
   deleteObject(desertRef).then(() => {
     seturl1("");
     alert("deleted succefully")
   }).catch((error) => {
     console.log(error);
   }); 
   };
   const uploadFiles1 = (file)=>{
    if(!file) return;
      let flag = true;
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(file);
      reader.onload = function (e) {
      
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        //Validate the File Height and Width.
        image.onload = function () {
          var height = this.height;
          var width = this.width;
          if (height !=512 && width != 512) {
            alert("Height and Width must not equal 512*512.");
            flag = false;
            return false;
          }else{
    let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
      const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const  uploadTask = uploadBytesResumable(storageref,file);
    uploadTask.on("state_changed",(snapshot)=>{
       const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
       setprogress1(prog);
    },(err)=>console.log(err),()=>{
       getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
    });
  }
};
};
 };
 const onImageChange1 = (event) => {
  if (event.target.files && event.target.files[0]) {
    setImage1(URL.createObjectURL(event.target.files[0]));
  }
   setfile1( event.target.files[0]);
 }
  function submit(e) {
    e.preventDefault();
  }
  const updateDataOnFirebase=async()=>{
    let HeartTxnAsInt;
    let CourseAccessCostAsInt;
    if(CourseHeartTxn.length==0){
      HeartTxnAsInt =0;
    }else{
   HeartTxnAsInt = Math.abs(parseInt(CourseHeartTxn, 10));
    }
    if(CourseAccessCost.length==0){
      CourseAccessCostAsInt = 0;
    }else{
      CourseAccessCostAsInt = Math.abs(parseInt(CourseAccessCost , 10));
    }
     let link = "";
     let FeaturedLink = "";       
     if(rootvalue=="Practice"){
       link = url1;
     }else{
       FeaturedLink = url1;
     }
    const Ref = doc(db, rootvalue, Id);
    await updateDoc(Ref, {
      courseFeaturedImageUrl:FeaturedLink,
      levelImage:link,
      isDraft:IsDraftUser
    });
    const Info = doc(db, `${rootvalue}/${Id}/extraInfo/infoDoc`);
    await updateDoc(Info, {
      courseAccessCost: CourseAccessCostAsInt,
      courseCompletionText: CourseCompletionText,
      courseHeartTxn: HeartTxnAsInt,
      courseShortDesc: CourseShortDesc,
      courseFlow: FlowTypeFromUser,
      hasFinalQuiz:WillThereBeAnyFinalQuiz,
      courseAccessTypeEnum: AccessTypeFromUser,
      courseDependencyId: DepIdFromUser,
      courseCompletionAnimTemplate: "",
      discountPrice: discountPrice,
      originalPrice: originalPrice,
    }); 
      alert("data updated succefully");
      setOpen(false);
  }

  async function deleteImageOfQuestion(QuestionType, imageLink) {
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    if (QuestionType == 6) {
      imageUrl1 = imageLink;
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 2) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      imageUrl4 = imageLink[3];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 0) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
    }
  }
  async function deleteQuestionArray(Question) {
    const data = doc(db, `Questions/${Question}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setUsers1(docSnap.data());
      console.log("Document data:", docSnap.data().questionType);
      if (docSnap.data().questionType == 6) {
        deleteImageOfQuestion(docSnap.data().questionType, docSnap.data().questionData.imageLink);
      } else if (docSnap.data().questionType == 2) {
        let imageArray = [docSnap.data().questionData.imageOptions[0], docSnap.data().questionData.imageOptions[1], docSnap.data().questionData.imageOptions[2], docSnap.data().questionData.imageOptions[3]];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      } else if (docSnap.data().questionType == 0) {
        let imageArray = [docSnap.data().questionData.imageoptions[0].imageLink, docSnap.data().questionData.imageoptions[1].imageLink, docSnap.data().questionData.imageoptions[2].imageLink];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      }
      await deleteDoc(doc(db, "Questions", Question));
    } else {
      console.log("No such document!");
    }

  }
  async function deleteQuestionPool(quizQuestionPoolForDelete) {
    const data = doc(db, `QuestionPool/${quizQuestionPoolForDelete}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      setUsers1(docSnap1.data());
      let QuestionArray =docSnap1.data().questionsId;
      QuestionArray.map((Question) => {
        deleteQuestionArray(Question);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, "QuestionPool", quizQuestionPoolForDelete));
  }
  async function deleteQuiz(Quiz){
      const data = doc(db, `Quizs/${Quiz}`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
        deleteQuestionPool(docSnap1.data().quizQuestionPool);
      } else {
        console.log("No such document!");
      }
      await deleteDoc(doc(db, "Quizs", `${Quiz}/extraInfo/infoDoc`)).then(async()=>{
        await deleteDoc(doc(db, "Quizs", Quiz));
      })
 
    
    }
  async function deleteTopic(Topic){
     const data = doc(db, `Topics/${Topic}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      let QuizArray =docSnap1.data().topicQuiz;
      QuizArray.map((Quiz) => {
        deleteQuiz(Quiz);
      });
      let topicChildItems =docSnap1.data().topicChildItems;
      topicChildItems.map((Quiz) => {
        deleteItem(Quiz);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, `Topics/${Topic}/extraInfo/infoDoc`)).then(async()=>{
      await deleteDoc(doc(db, "Topics", Topic));
    })
  }
  const deleteItem = async(item)=>{
    const data = doc(db, `Items/${item}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      let type =docSnap1.data().category;
      if(type=="PDF" || type=="audio"){
        deleteImageOfQuestion(6,docSnap1.data().url)
      }
    } else {
      console.log("No such document!");
    }
      await deleteDoc(doc(db, "Items", item));
  }
  async function deleteLesson(Lesson){
    const data = doc(db, `Lessons/${Lesson}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
  let LessonArray =docSnap1.data().lessonTopics;
  LessonArray.map((topic) => {
    deleteTopic(topic);
  });
    } else {
      console.log("No such document!");
    } 
    const data1 = doc(db, `Lessons/${Lesson}/extraInfo/infoDoc`);
      const docSnap2 = await getDoc(data1);
      if (docSnap2.exists()) {
    let LessonQuizArray =docSnap2.data().lessonQuiz;
    let lessonChildItems =docSnap2.data().lessonChildItems;
    LessonQuizArray.map((Quiz) => {
          deleteQuiz(Quiz);
        });
        lessonChildItems.map((Quiz) => {
          deleteItem(Quiz);
        });
      } else {
        console.log("No such document!");
      }
      await deleteDoc(doc(db, `Lessons/${Lesson}/extraInfo/infoDoc`)).then(async()=>{
        await deleteDoc(doc(db, `Lessons/${Lesson}`))
    }); 
  }
  async function deleteDetails(i){
    if (window.confirm("Are you sure")) {      
    const data = doc(db, `${rootvalue}/${Details[i].courseId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
  let LessonArray =docSnap1.data().courseLessons;
  LessonArray.map((Lesson) => {
        deleteLesson(Lesson);
      });
      let Arcade = docSnap1.data().courseFinalArcade;
      let Quiza = docSnap1.data().courseFinalQuiz;
      if(Arcade!=""){
        deleteLesson(Arcade);
      }
      if(Quiza!=""){
        deleteQuiz(Quiza);
      }
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, `${rootvalue}/${Details[i].courseId}/extraInfo/infoDoc`)).then(async()=>{
        await deleteDoc(doc(db, `${rootvalue}/${Details[i].courseId}`))
    }); 
    setReset(!Reset);
    alert("Course Deleted Successfully");  
    } 
  }
  var root = [
    {
      value : 'Courses',
      label : "General Course"
    },
    {
        value : 'Grammar',
        label : 'Grammar'
    },
    {
     value : 'Practice',
     label : "Practice"
   },
   {
     value : 'video',
     label : "Video"
   },
  {
   value : 'Test',
   label : "Test"
  },
  {
   value : 'Story',
   label : "Story"
  },
  {
   value : "Audio",
   label : "Audio"
  }
  ];
  var FlowType = [
    {
      value : 0,
      label : "Free Flow"
    },
    {
        value : 1,
        label : "Linear Flow"
    }
  ];
  var AccessType = [
    {
      value : 0,
      label : "Free"
    },
    {
        value : 1,
        label : "Paid"
    },
   /*  {
     value : 2,
     label : "Course"
   } */
  ];
  const setFlowType = (e)=>{
    setFlowTypeFromUser(e.value);
     }
 const setAccessType = (e)=>{
    setAccessTypeFromUser(e.value);
    if(e.value==1  && rootvalue=="Courses"){
      setPriceDisable(false);
    }else{
      setPriceDisable(true);
    }
}
  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
}
// const update = (link)=>{
//   const desertRef = ref(storage, link);
//   deleteObject(desertRef).then(() => {
//     console.log("image deleted");
//   }).catch((error) => {
//     console.log(error);
//   });
// }
  return (
    <div className="App">
      {/* <button onClick={()=>{update("https://firebasestorage.googleapis.com/v0/b/prod-speaklifi-gcp.appspot.com/o/files%2F%5BMP3FY%5D%20Owl%20City%20-%20Fireflies%20(Official%20Music%20Video).mp3_MonSep052022174302?alt=media&token=66bbbadc-b359-49e0-8d42-4717943a4eab")}}>Checking</button> */}
      <label className="label"> Choose the Root : </label>
      <Select
        value={root.filter(function (option) {
          return option.value === rootvalue;
        })}
        options={root}
        onChange={rootvaluefromuser}
      ></Select>
      <br />
      <br />
      <h2>{rootvalue} Data</h2>
      <table style={{ width: "90%" }} className="content-table">
        <thead>
          <tr style={{ borderCollapse: "collapse", width: "10%" }}>
            <th style={{ borderCollapse: "collapse", width: "5%" }}>S No.</th>
            <th style={{ borderCollapse: "collapse", width: "30%" }}>Name</th>
            <th style={{ borderCollapse: "collapse", width: "50%" }}>
              Edit/Delete
            </th>
          </tr>
        </thead>
        <tbody>
          {Details.length === 0 ? (
            <tr>
              <td colspan="6">No Items.</td>
            </tr>
          ) : (
            <></>
          )}
          {Details.map((details, i) => {
            return (
              <tr
                key={i}
                style={{
                  borderCollapse: "collapse",
                  textAlign: "left",
                  padding: "8px",
                }}
              >
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    height: "5px",
                    padding: "8px",
                  }}
                >
                  {i + 1}
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    wordWrap: "break-word",
                    maxWidth: "100px",
                  }}
                >
                  {details.courseId}
                  <br />
                  <br />
                </td>
                <td align="center">
                  <button className="edit" onClick={() => handleOpen(i)}>
                    Edit
                  </button>
                  <button className="delete" onClick={() => deleteDetails(i)}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog
        onClose={handleClose}
        open={Open}
        style={{ padding: "30px" }}
        PaperProps={{
          style: {
            borderRadius: "20px",
            height: "779px",
            padding: "10px",
          },
        }}
      >
        {/* <Dialog onClose={handleClose} open={Open} style={{overflow: 'scroll' ,  color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '90%', width: '70%' }/* {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */}
        <DialogTitle
          sx={{ fontSize: 24, color: "#308efe", fontWeight: "bold" }}
        >
          Update Form
        </DialogTitle>
        <DialogContent>
          <form onSubmit={submit} style={{ fontFamily: "sans-serif" }}>
            <span> Id - {Id}</span>
            <br />
            <br />
            <label style={{ fontSize: "16px" }} hidden={FeaturedImageDisable}>
              Featured Image :{" "}
            </label>
            <input
              hidden={FeaturedImageDisable}
              type="file"
              onChange={onImageChange1}
            />
            <br />
            <span hidden={FeaturedImageDisable}>uploaded {progress1}%</span>
            <br />
            <br />
            <img
              hidden={FeaturedImageDisable}
              src={url1}
              alt=""
              width="193"
              height="130"
            />
            <br />
            <Button
              hidden={FeaturedImageDisable}
              onClick={() => uploadFiles1(file1)}
              variant="contained"
              sx={{ height: "25px", padding: "5px", marginRight: "5px" }}
              startIcon={<Upload />}
            >
              upload
            </Button>
            <Button
              hidden={FeaturedImageDisable}
              onClick={() => deleteImage1(url1)}
              variant="contained"
              sx={{ height: "25px", padding: "5px", marginRight: "85px" }}
              startIcon={<Delete />}
            >
              Delete
            </Button>
            <br />
            <br />
            <label style={{ fontSize: "16px" }}>
              No of hearts that are to be credited to after completion ? :{" "}
            </label>
            <input
              type="number"
              value={CourseHeartTxn}
              style={{ width: "50px" }}
              min="0"
              placeholder="No of hearts that are to be credited to after completion"
              onChange={(event) => {
                setCourseHeartTxn(event.target.value);
              }}
            />

            <label
              style={{ fontSize: "16px" }}
              hidden={AnimationScreenAfterCompletionDisable}
            >
              Profile Background Image for Level :{" "}
            </label>
            <input
              hidden={AnimationScreenAfterCompletionDisable}
              type="file"
              onChange={onImageChange1}
            />
            <span hidden={AnimationScreenAfterCompletionDisable}>
              uploaded {progress1}%
            </span>
            <button
              hidden={AnimationScreenAfterCompletionDisable}
              onClick={() => uploadFiles1(file1)}
            >
              upload
            </button>
            <button
              hidden={AnimationScreenAfterCompletionDisable}
              onClick={() => deleteImage1(url1)}
            >
              Delete
            </button>
            <img
              hidden={AnimationScreenAfterCompletionDisable}
              src={url1}
              alt=""
              width="193"
              height="130"
            />
            <br />
            <br />
            <label
              style={{ fontSize: "16px" }}
              hidden={TextAfterCompletionofLessonObjectiveArcadeDisabled}
            >
              Text After Completion of Course :{" "}
            </label>
            <br />
            <br />
            <TextField
              variant="outlined"
              maxlength="50"
              hidden={TextAfterCompletionofLessonObjectiveArcadeDisabled}
              type="text"
              value={CourseCompletionText}
              placeholder="Text After Completion of Course"
              onChange={(event) => {
                setCourseCompletionText(event.target.value);
              }}
            />
            <br />
            <br />
            <label>isDraft : </label>
            <input
              type="checkbox"
              defaultChecked={IsDraftUser}
              onChange={(event) => {
                setIsDraftUser(!IsDraftUser);
              }}
            />
            <br />
            <br />
            <label
              style={{ fontSize: "16px" }}
              hidden={ShortDescriptionDisable}
            >
              {" "}
              Short Description :{" "}
            </label>
            <textarea
              maxlength="45"
              cols="30"
              rows="10"
              hidden={ShortDescriptionDisable}
              value={CourseShortDesc}
              type="text"
              placeholder="Short Description"
              onChange={(event) => {
                setCourseShortDesc(event.target.value);
              }}
            />
            <br />
            <br />
            <div style={{ display: FlowDisable }}>
              <label style={{ fontSize: "16px" }}>Flow : </label>
              <Select
                value={FlowType.filter(function (option) {
                  return option.value === FlowTypeFromUser;
                })}
                options={FlowType}
                onChange={setFlowType}
              ></Select>
              <br />
            </div>
            <div style={{ display: AccessTypeDisable }}>
              <label style={{ fontSize: "16px" }}>Access Type : </label>
              <Select
                value={AccessType.filter(function (option) {
                  return option.value === AccessTypeFromUser;
                })}
                options={AccessType}
                onChange={setAccessType}
              ></Select>
              <br />
            </div>
            <br></br>
            <label>Will there be any final quiz? : </label>
            <input
              require
              type="checkbox"
              defaultChecked={WillThereBeAnyFinalQuiz}
              onChange={(event) => {
                setWillThereBeAnyFinalQuiz(!WillThereBeAnyFinalQuiz);
              }}
            />
            <br></br>
            <br></br>
            <div style={{ display: priceDisable ? "none" : "" }}>
              <label>Original Price : </label>
              <input
                min={0}
                value={originalPrice}
                type="number"
                placeholder="Original Price"
                onChange={(event) => {
                  setoriginalPrice(event.target.value);
                }}
              />
              <br />
              <br />
              <label> Discounted Price : </label>
              <input
                min={0}
                type="number"
                value={discountPrice}
                placeholder="Discounted Price"
                onChange={(event) => {
                  setdiscountPrice(event.target.value);
                }}
              />
            </div>
            <label
              style={{ fontSize: "16px" }}
              hidden={LifeNeededToAccessDisable}
            >
              Life Needed To Access :{" "}
            </label>
            <input
              hidden={LifeNeededToAccessDisable}
              type="number"
              value={CourseAccessCost}
              min="0"
              style={{ width: "50px" }}
              placeholder="Life Needed To Access"
              onChange={(event) => {
                setCourseAccessCost(event.target.value);
              }}
            />
            <div style={{ display: DependencyIDDisable }}>
              <label>Dependency ID : </label>
              <Select
                value={PracticeId.filter(function (option) {
                  return option.value === DepIdFromUser;
                })}
                options={PracticeId}
                onChange={SetDepId}
              ></Select>
              <button onClick={SETPRACTICENODE}>Show</button>
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={updateDataOnFirebase}>
            Submit
          </Button>
          <Button sx={{ color: "red" }} onClick={handleClose}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default CourseTable;