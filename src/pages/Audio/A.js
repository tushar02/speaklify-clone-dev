import React, { useState } from 'react';
import Papa from 'papaparse';
function A() {
    const [csvfile , setcsvfile] = useState('');
    const [parsedCsvData, setParsedCsvData] = useState([]);
  const  handleChange = (event) => {
        setcsvfile( event.target.files[0]);
        let aya =  event.target.files[0];
        console.log(aya);
      };
      const parseFile = (file) => {
          let aman = ([]) ;
          setParsedCsvData([]);
        Papa.parse(file, {
          header: false,
          complete: results => {
            setParsedCsvData(results.data)
          },
        });
      };
  return (
    <div className="App">
    <h2>Import CSV File!</h2>
    <input
      className="csv-input"
      type="file"
      onChange={handleChange}
      />
    <p />
    {parsedCsvData[0]}
    <button onClick={()=>parseFile(csvfile)}> Upload now!</button>
  </div>
  )
}

export default A;