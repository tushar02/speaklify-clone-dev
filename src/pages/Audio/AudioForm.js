import React, { useState, useEffect } from "react";
import Papa from "papaparse";
class AudioForm extends React.Component {
  constructor() {
    super();
    this.state = {
      csvfile: undefined,
    };
    this.updateData = this.updateData.bind(this);
  }

  handleChange = (event) => {
    this.setState({
      csvfile: event.target.files[0],
    });
  };

  importCSV = () => {
    const { csvfile } = this.state;
    Papa.parse(csvfile, {
      complete: this.updateData,
      header: true,
    });
  };

  updateData(result) {
    var data = result.data;
    console.log(data);
  }

  render() {
    console.log(this.state.csvfile);
    return (
      <div className="App">
        <h2>Import CSV File!</h2>
        <input
          className="csv-input"
          type="file"
          ref={(input) => {
            this.filesInput = input;
          }}
          name="file"
          placeholder={null}
          onChange={this.handleChange}
        />
        <p />
        <button onClick={this.importCSV}> Upload now!</button>
      </div>
    );
  }
}

export default AudioForm;

/* import React, { useState } from 'react'
import { getDownloadURL, uploadBytesResumable} from '@firebase/storage';
import {  ref, deleteObject } from "firebase/storage";
import {db,storage} from '../../firebase/Firebase-config';
import {setDoc,doc} from 'firebase/firestore';
function AudioForm() {
  const [AudioName , setAudioName] = useState('');
    const [image1, setImage1] = useState(null);
    const [file1 , setfile1] = useState(null);
    const[url1 , seturl1] = useState("");
    const [progress1 , setprogress1] = useState(0);
    const [ShortDescription , setShortDescription] = useState('');
    const [audioCategory , setaudioCategory] = useState('');
    const [audioDuration , setaudioDuration] = useState('');
    const [TextFile , setTextFile] = useState('');
    function submit(e){
        e.preventDefault();
      }
         const deleteImage1 = (file) =>{
            const desertRef = ref(storage, file);
           deleteObject(desertRef).then(() => {
             seturl1("");
             alert("deleted succefully")
           }).catch((error) => {
             console.log(error);
           }); 
           };
           const uploadFiles1 = (file)=>{
            if(!file) return;
            const storageref = ref(storage, `/Audio/${file.name}`);
            const  uploadTask = uploadBytesResumable(storageref,file);
            uploadTask.on("state_changed",(snapshot)=>{
              const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
               setprogress1(prog);
            },(err)=>console.log(err),()=>{
               getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
            });
         };
         const onImageChange1 = (event) => {
          if (event.target.files && event.target.files[0]) {
            setImage1(URL.createObjectURL(event.target.files[0]));
          }
           setfile1( event.target.files[0]);
         }
         const UploadOnFirebase =async()=>{
           let FinalText = TextFile;
      if(FinalText.includes("\n")){
          FinalText = FinalText.replace("\n", "");
      }
      const FinalTextArray = FinalText.split(";");
      let date = new Date();
      let ModifiedDate = date.toString().replaceAll(" ","");
      let AudioId = AudioName+ModifiedDate;
      const docData = {
        audioCategory:audioCategory,
        audioDuration:audioDuration,
        audioLink:url1,
        audioLyrics:FinalTextArray,
        courseName:AudioName,
        courseShortDesc:ShortDescription,
        courseId:AudioId
    }
    await setDoc(doc(db, "Audio", AudioId),docData).then(()=>{
      alert("Data updated Successfully");
    })
         }
         
         const showFile = (event) => {
           event.preventDefault();
          const reader = new FileReader();
          reader.onload = (event) => {
            const text = event.target.result;
            console.log(text);
            setTextFile(text);
          };
          reader.readAsText(event.target.files[0]);
        };
        const upload=(e)=> {
          var data = null;
          var file = e.target.files[0];
      
          var reader = new FileReader();
          reader.readAsText(file);
          reader.onload = function (event) {
            var csvData = event.target.result;
            
            var parsedCSV = d3.csv.parseRows(csvData);
            
            parsedCSV.forEach(function (d, i) {
              if (i == 0) return true; // skip the header
                  document.getElementById(d[0]).value = d[1];
                });
              }
            }
            return (
              <>
              <h1 style={{ textAlign: 'center' }}>Audio Form</h1>
              <form id="Audio-form" onSubmit={submit}>
    <label>Name : </label>
    <input type="text" placeholder='Audio Name' onChange={(event) => { setAudioName(event.target.value); }} />
    <br /><br />
    <label>Short Description : </label>
    <textarea type="text" placeholder='Short Description' onChange={(event) => { setShortDescription(event.target.value); }} />
    <br /><br />
    <label>Audio Category : </label>
    <input type="text" placeholder='Audio Category' onChange={(event) => { setaudioCategory(event.target.value); }} />
    <br /><br />
    <label>Audio Duration : </label>
    <input type="text" placeholder='Audio Duration' onChange={(event) => { setaudioDuration(event.target.value); }} />
    <br /><br /><br />
   <label>Audio File : </label>
   <input  type="file"  onChange={onImageChange1} />
   <span>uploaded {progress1}%</span>
   <button  onClick={()=> uploadFiles1(file1)}>upload</button>
   <button  onClick={()=> deleteImage1(url1)}>Delete</button>
   {url1==""?<></> :<audio controls><source src={url1} type="audio/mpeg"/></audio>}
   <br /><br /><br />
   <label>Text File : </label>
   <input type="file"   onChange={showFile}></input>
   <br /><br />
   <button onClick={UploadOnFirebase}>Upload</button>
   </form>
   </>
   )
  }
  
  export default AudioForm */
/* import React, { useState } from 'react'
  import ReactFileReader from 'react-file-reader';
  
  function AudioForm() {
    const [ data  , setdata] = useState('')
    const handleFiles = files => {
      var reader = new FileReader();
      reader.onload = function(e) {
          // Use reader.result
          setdata(reader.result);
      }
      reader.readAsText(files[0]);
  }
    return (
      <>
      <ReactFileReader handleFiles={handleFiles} fileTypes={'.csv'}>
      <button className='btn'>Upload</button>
  </ReactFileReader>
  {}
  <button>fsdfdsfds</button>
      </>
    )
  }
  
  export default AudioForm */
