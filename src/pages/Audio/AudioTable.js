import React, { useState, useEffect } from "react";
import { db, storage } from "../../firebase/Firebase-config";
import {
  collection,
  getDocs,
  getDoc,
  doc,
  deleteDoc,
  updateDoc,
  arrayRemove,
} from "firebase/firestore";
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable } from "@firebase/storage";
import Select from "react-select";
import Modal from "@material-ui/core/Modal";
function AudioTable() {
  const [Details, setDetails] = useState([]);
  const [Reset, setReset] = useState(false);
  const [Open, setOpen] = useState(false);
  const [AudioName, setAudioName] = useState("");
  const [AudioId, setAudioId] = useState("");
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [ShortDescription, setShortDescription] = useState("");
  const [audioCategory, setaudioCategory] = useState("");
  const [audioDuration, setaudioDuration] = useState("");
  const [TextFile, setTextFile] = useState("");
  const [AudioLyrics, setAudioLyrics] = useState([]);
  useEffect(() => {
    const getUsers = async () => {
      const usercollectionRef = collection(db, "Audio");
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset, Open]);
  const handleClose = () => {
    setOpen(false);
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "");
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const prog = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setprogress1(prog);
      },
      (err) => console.log(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) =>
          console.log(seturl1(url))
        );
      }
    );
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  };
  const handleOpen = async (i) => {
    setOpen(true);
    setAudioId(Details[i].courseId);
    seturl1(Details[i].audioLink);
    setAudioName(Details[i].courseName);
    setShortDescription(Details[i].courseShortDesc);
    setaudioDuration(Details[i].audioDuration);
    setaudioCategory(Details[i].audioCategory);
    let Array = Details[i].audioLyrics;
    let data = "";
    for (let i = 0; i < Array.length; i++) {
      if (i < Array.length - 1) {
        data += Array[i] + ";";
      } else {
        data += Array[i];
      }
    }
    setAudioLyrics(data);
    setTextFile(data);
  };
  function submit(e) {
    e.preventDefault();
  }
  const UploadOnFirebase = async () => {
    let FinalText = TextFile;
    if (FinalText.includes("\n")) {
      FinalText = FinalText.replace("\n", "");
    }
    const FinalTextArray = FinalText.split(";");
    const Info = doc(db, `Audio/${AudioId}`);
    await updateDoc(Info, {
      audioCategory: audioCategory,
      audioDuration: audioDuration,
      audioLink: url1,
      audioLyrics: FinalTextArray,
      courseName: AudioName,
      courseShortDesc: ShortDescription,
    });
    setOpen(false);
  };
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        /*  seturl1(""); */
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  async function deleteDetails(i) {
    if (window.confirm("Are you sure")) {
      deleteImage1(Details[i].audioLink);
      await deleteDoc(doc(db, `Audio/${Details[i].courseId}`));
      setReset(!Reset);
      alert("Audio Deleted Successfully");
    }
  }
  const showFile = (event) => {
    event.preventDefault();
    const reader = new FileReader();
    reader.onload = (event) => {
      const text = event.target.result;
      console.log(text);
      setTextFile(text);
    };
    reader.readAsText(event.target.files[0]);
  };

  return (
    <div className="App">
   
      <h2>Audio Table Data</h2>
      <table
        style={{
          borderCollapse: "collapse",
          width: "100%",
          border: "1px solid",
        }}
      >
        <thead>
          <tr
            style={{
              borderCollapse: "collapse",
              width: "10%",
              border: "1px solid",
            }}
          >
            <th
              style={{
                borderCollapse: "collapse",
                width: "5%",
                border: "1px solid",
              }}
            >
              S No.
            </th>
            <th
              style={{
                borderCollapse: "collapse",
                width: "20%",
                border: "1px solid",
              }}
            >
              Course Name
            </th>
            <th
              style={{
                borderCollapse: "collapse",
                width: "30%",
                border: "1px solid",
              }}
            >
              Audio
            </th>
            <th
              style={{
                borderCollapse: "collapse",
                width: "20%",
                border: "1px solid",
              }}
            >
              Category
            </th>
            <th
              style={{
                borderCollapse: "collapse",
                width: "30%",
                border: "1px solid",
              }}
            >
              Short Description
            </th>
            <th
              style={{
                borderCollapse: "collapse",
                width: "50%",
                border: "1px solid",
              }}
            >
              Edit/Delete
            </th>
          </tr>
        </thead>
        <tbody>
          {Details.map((details, i) => {
            return (
              <tr
                key={i}
                style={{
                  borderCollapse: "collapse",
                  textAlign: "left",
                  padding: "8px",
                  border: "1px solid",
                }}
              >
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    height: "5px",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  {i + 1}
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  {details.courseName}
                  <br />
                  <br />
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  <audio controls>
                    <source src={details.audioLink} type="audio/mpeg" />
                  </audio>
                  <br />
                  <br />
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  {details.audioCategory}
                  <br />
                  <br />
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  {details.courseShortDesc}
                  <br />
                  <br />
                </td>
                <td>
                  <button onClick={() => handleOpen(i)}>Edit</button>
                  <button onClick={() => deleteDetails(i)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Modal
        onClose={handleClose}
        open={Open}
        style={
          {
            overflow: "scroll",
            color: "#ff6666",
            background: "#643EE1",
            top: "50%",
            left: "60%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            height: "90%",
            width: "70%",
          } /* {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */
        }
      >
        <div>
          <h2 style={{ textAlign: "center" }}>Update Form</h2>
          <form style={{ marginLeft: "10%" }} id="Audio-form" onSubmit={submit}>
            <label>Name : {AudioName} </label>
            <br />
            <br />
            <label>Short Description : </label>
            <textarea
              value={ShortDescription}
              type="text"
              placeholder="Short Description"
              onChange={(event) => {
                setShortDescription(event.target.value);
              }}
            />
            <br />
            <br />
            <label>Audio Category : </label>
            <input
              value={audioCategory}
              type="text"
              placeholder="Audio Category"
              onChange={(event) => {
                setaudioCategory(event.target.value);
              }}
            />
            <br />
            <br />
            <label>Audio Duration : </label>
            <input
              type="text"
              value={audioDuration}
              placeholder="Audio Duration"
              onChange={(event) => {
                setaudioDuration(event.target.value);
              }}
            />
            <br />
            <br />
            <br />
            <label>Audio File : </label>
            <input type="file" onChange={onImageChange1} />
            <span>uploaded {progress1}%</span>
            <button onClick={() => uploadFiles1(file1)}>upload</button>
            <button onClick={() => deleteImage1(url1)}>Delete</button>
            {url1 == "" ? (
              <></>
            ) : (
              <audio controls>
                <source src={url1} type="audio/mpeg" />
              </audio>
            )}
            <br />
            <br />
            <br />
            <label>Text File : </label>
            <input type="file" onChange={showFile}></input>
            <textarea
              disabled={true}
              value={AudioLyrics}
              cols="70"
              rows="10"
            ></textarea>
            <br />
            <br />
            <button onClick={UploadOnFirebase}>Upload</button>
            <button onClick={handleClose}>Close Form</button>
          </form>
        </div>
      </Modal>
    </div>
  );
}
export default AudioTable;
