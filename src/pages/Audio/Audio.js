import React, { useState } from 'react'
import AudioForm from './AudioForm';
import AudioTable from './AudioTable';

function Audio() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Audio");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("back to Audio");
    }else{
      setvalue("Add new Audio");
    }
}
  return (
      <>
      <button onClick={change}>{value}</button>
        {Reset==false?<AudioTable/>:<AudioForm/>}
      </>
  )
}

export default Audio;