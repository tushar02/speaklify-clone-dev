import { db, storage } from '../../firebase/Firebase-config';
import React, { useState } from 'react'
import { collection, getDocs, setDoc, updateDoc, doc, getDoc, arrayUnion, } from 'firebase/firestore';
import Select from "react-select";
import { useEffect } from 'react';
import { getDownloadURL, uploadBytesResumable } from '@firebase/storage';
import { ref, deleteObject } from "firebase/storage";
import Papa from 'papaparse';
const ItemForm = () => {
  const [type, settype] = useState('');
  const [AudioFileDisable, setAudioFileDisable] = useState(false);
  const [users, setusers] = useState([]);
  const [users1, setusers1] = useState([]);
  const [users2, setusers2] = useState([]);
  const [rootvalue, setrootvalue] = useState('');
  const [rootName, setrootName] = useState([]);
  const [lessonName, setlessonName] = useState([]);
  const [topicName, settopicName] = useState([]);
  const [lessonName1, setlessonName1] = useState([]);
  const [topicName1, settopicName1] = useState([]);
  const [userrootname, setuserrootname] = useState('');
  const [userItemCategory, setuserItemCategory] = useState('');
  const [userlessonname, setuserlessonname] = useState('');
  const [usertopicname, setusertopicname] = useState('');
  const [userquizname, setuserquizname] = useState('');
  const [userLessonType, setuserLessonType] = useState('');
  const [userTopicType, setuserTopicType] = useState('');
  const [ItemId, setItemId] = useState('');
  const [ItemName, setItemName] = useState('');
  const [link, setlink] = useState('');
  const [check, setcheck] = useState(false);
  const [TopicDisable, setTopicDisable] = useState(false);
  const [ShortDescriptionDisable, setShortDescriptionDisable] = useState(false);
  const [audioCategoryDisable, setaudioCategoryDisable] = useState(false);
  const [audioDurationDisable, setaudioDurationDisable] = useState(false);
  const [TextFileDisable, setTextFileDisable] = useState(false);
  const [URLDisable, setURLDisable] = useState(false);
  const [ShortDescription, setShortDescription] = useState('');
  const [audioCategory, setaudioCategory] = useState('');
  const [audioDuration, setaudioDuration] = useState('');
  const [TextFile, setTextFile] = useState('');
  const [csvfile, setcsvfile] = useState('');
  const [parsedCsvData, setParsedCsvData] = useState([]);
  var root = [
    {
      value: 'Courses',
      label: "General Course"
    },
    {
      value: 'Grammar',
      label: 'Grammar'
    },
    {
      value: 'video',
      label: "Video"
    },
    {
      value: 'Story',
      label: "Story"
    },
    {
      value: 'Audio',
      label: 'Audio'
    }
  ];
  var [ItemCategory, setItemCategory] = useState([
    {
      value: "PDF",
      label: "PDF",
      isDisabled: false
    },
    {
      value: "VIDEO",
      label: "VIDEO",
      isDisabled: true
    },
    {
      value: "audio",
      label: "Audio",
      isDisabled: true
    },
    {
      value: "HTML",
      label: "HTML",
      isDisabled: true
    }
  ]);

  var [lesson, setlesson] = useState([
    {
      value: 'Courses',
      label: "Lesson",
      isDisabled: false
    },
    {
      value: "Objective",
      label: "Objective",
      isDisabled: false
    },
    {
      value: "Arcade",
      label: "Arcade",
      isDisabled: false
    }
  ]);
  var [topic, settopic] = useState([
    {
      value: 'Lessons',
      label: "Topic",
      isDisabled: false
    },
    {
      value: 'Round',
      label: "Round",
      isDisabled: false
    }
  ]);
  const SETTEXTFILE = () => {
    let FinalText = TextFile;
    if (FinalText.includes("\n")) {
      FinalText = FinalText.replace("\n", "");
    }
  }
  const showFile = (event) => {
    event.preventDefault();
    const reader = new FileReader();
    reader.onload = (event) => {
      const text = event.target.result;
      console.log(text);
      setTextFile(text);
    };
    reader.readAsText(event.target.files[0]);
  };

  const getlessonType = async () => {
    const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setusers1(docSnap.data());
      console.log("Document data:", docSnap.data());
    } else {
      //    doc.data() will be undefined in this case
      console.log("No such document!");
    }
  }
  const getTopicType = async () => {
    const data = doc(db, `Lessons/${userlessonname}`);
    const docSnap = await getDoc(data);

    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      setusers2(docSnap.data());
    } else {
      console.log("No such document!");
    }
  }
  const confirm = () => {
    setrootName([]);
    console.log(users.map((user) => {
      const query = { value: user.courseId, label: user.courseId }
      setrootName(rootName => rootName.concat(query))
    }));

  };
  useEffect(() => {
    if (userItemCategory == "PDF") {
      setShortDescriptionDisable(true);
      setaudioCategoryDisable(true);
      setaudioDurationDisable(true);
      setAudioFileDisable(false);
      setTextFileDisable(true);
      setURLDisable(true);
      settype("PDF");
    }
    else if (userItemCategory == "VIDEO") {
      setShortDescriptionDisable(true);
      setaudioCategoryDisable(true);
      setaudioDurationDisable(true);
      setAudioFileDisable(true);
      setTextFileDisable(true);
      setURLDisable(false);
    } else if (userItemCategory == "audio") {
      setShortDescriptionDisable(false);
      setaudioCategoryDisable(false);
      setaudioDurationDisable(false);
      setAudioFileDisable(false);
      setTextFileDisable(false);
      setURLDisable(true);
      settype("Audio");
    } else if (userItemCategory == "HTML") {
      setShortDescriptionDisable(true);
      setaudioCategoryDisable(true);
      setaudioDurationDisable(true);
      setAudioFileDisable(true);
      setTextFileDisable(true);
      setURLDisable(false);
    }
  }, [userItemCategory])
  useEffect(() => {
    if (rootvalue == 'Courses' || rootvalue == 'Grammar' || rootvalue == 'Story') {
      setuserItemCategory('');
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
      setItemCategory((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 12) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
    } else if (rootvalue == 'Practice') {
      setuserItemCategory('');
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
      setItemCategory((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 12) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
    } else if (rootvalue == 'Test' || rootvalue == 'video' || rootvalue == 'Audio') {
      setuserItemCategory('');
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })

    }
    if (rootvalue == 'video') {
      setuserItemCategory('');
      setTopicDisable(true);
      setItemCategory((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx != 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
    } else if (rootvalue == 'Story') {
      setuserItemCategory('');
      setTopicDisable(true);
      setItemCategory((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx != 3) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        })
      })
    }
    else if (rootvalue == 'Audio') {
      setuserItemCategory('');
      setTopicDisable(true);
      setItemCategory((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 2) {
            curvalue.isDisabled = false;
          } else {
            curvalue.isDisabled = true;
          }
          return curvalue;
        })
      })
    } else {
      setTopicDisable(false);
    }
  }, [rootvalue])
  useEffect(() => {
    confirm();
  }, [users]);
  useEffect(() => {
    getlessonType();
  }, [userLessonType]);
  useEffect(() => {
    getlessonName();
  }, [users1]);
  useEffect(() => {
    getTopicType();
  }, [userTopicType]);
  useEffect(() => {
    getTopicName();
  }, [users2]);
  const getlessonName = () => {
    if (userLessonType == "Arcade") {
      if (users1.courseFinalArcade != undefined) {
        setlessonName1([]);
        setlessonName1([{ value: users1.courseFinalArcade, label: users1.courseFinalArcade }]);
      }
    } else {
      setlessonName(users1.courseLessons);
      if (lessonName != undefined) {
        setlessonName1([]);
        console.log(lessonName.map((user) => {
          const query = { value: user, label: user }
          setlessonName1(lessonName1 => lessonName1.concat(query))
        }));
      }
    }
  };
  const getTopicName = () => {
    settopicName(users2.lessonTopics);
    if (topicName != undefined) {
      settopicName1([]);
      console.log(topicName.map((user) => {
        const query = { value: user, label: user }
        settopicName1(topicName1 => topicName1.concat(query))
      }));
    }
  }
  const getrootName = async () => {
    const usercollectionRef = collection(db, rootvalue);
    const data = await getDocs(usercollectionRef);
    console.log(data);
    setusers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    setuserrootname();
    setuserLessonType('');
    setuserlessonname('');
    setusertopicname('');
    setuserquizname('');
    setuserLessonType('');
    setuserTopicType('');

  }
  const rootnamefromuser = (e) => {
    setuserrootname(e.value);
    setuserLessonType('');
    setuserlessonname('');
    setusertopicname('');
    setuserquizname('');
    setuserLessonType('');
    setuserTopicType('');

  };
  const setuserlesson = (e) => {
    setuserlessonname(e.value);
    setusertopicname('');
    setuserquizname('');
    setuserTopicType('');
  };
  const setTopicValue = (e) => {
    setusertopicname(e.value);
    setuserquizname('');
  }
  const setquizValue = () => {
    let date = new Date();
    let ModifiedDate = (date.toString().replaceAll(" ", "")).substring(0, 20).replace(/[^a-z0-9 -]/gi, '').toLowerCase();

    let final = (ItemName.toLowerCase()).replace(/[^a-z0-9 -]/gi, '');
    let newfinaldata = final.replace(/\s+/, "")
    setItemId(newfinaldata + ModifiedDate);
    setcheck(true);
  }
  const setlessonType = (e) => {
    setuserLessonType(e.value);
    setuserlessonname('');
    setusertopicname('');
    setuserquizname('');
    setuserTopicType('');
  }
  const setTopicType = (e) => {
    setuserTopicType(e.value);
    setusertopicname('');
    setuserquizname('');
  }
  const setItemCategoryType = (e) => {
    setuserItemCategory(e.value);
  }
  useEffect(() => {
    parseFile(csvfile);
  }, [csvfile])
  const [hello, sethello] = useState();
  const submitDataOnFirebase = async () => {
    let dataa;
    if (check == false) {
      alert("please confirm");
    } else {
      if (userItemCategory == "PDF") {
        if (url1.length == 0) {
          alert("please upload file");
          return;
        }
        const docData = {
          associatedRoot: userrootname,
          associatedLesson: userlessonname,
          associatedTopic: usertopicname,
          associatedRootType: rootvalue,
          associatedLessonType: userLessonType,
          associatedTopicType: userTopicType,
          category: userItemCategory,
          itemId: ItemId,
          title: ItemName,
          url: url1
        }
        dataa = docData;
      } else if (userItemCategory == "VIDEO") {
        if (link.length == 0) {
          alert("please update file");
          return;
        }

        const docData = {
          associatedRoot: userrootname,
          associatedLesson: userlessonname,
          associatedTopic: usertopicname,
          associatedRootType: rootvalue,
          associatedLessonType: userLessonType,
          associatedTopicType: userTopicType,
          category: userItemCategory,
          itemId: ItemId,
          title: ItemName,
          url: link
        }
        dataa = docData;
      } else if (userItemCategory == "audio") {
        const FinalTextArray = [];
        for (var i = 0; i < parsedCsvData.length - 1; i++) {
          FinalTextArray.push(parsedCsvData[i][0] + "" + parsedCsvData[i][1]);
        }

        if (url1.length == 0) {
          alert("please upload file");
          return;
        }
        const docData = {
          associatedRoot: userrootname,
          associatedLesson: userlessonname,
          associatedTopic: usertopicname,
          associatedRootType: rootvalue,
          associatedLessonType: userLessonType,
          associatedTopicType: userTopicType,
          audioCategory: 0,
          audioDuration: audioDuration,
          title: ItemName,
          audioLink: url1,
          url: url1,
          audioLink: url2,
          lrcFileLink: url2,
          courseShortDesc: ShortDescription,
          itemId: ItemId,
          category: userItemCategory
        }
        dataa = docData;
        console.log(dataa)
      } else if (userItemCategory == "HTML") {
        if (link.length == 0) {
          alert("please upload file");
          return;
        }
        const docData = {
          associatedRoot: userrootname,
          associatedLesson: userlessonname,
          associatedTopic: usertopicname,
          associatedRootType: rootvalue,
          associatedLessonType: userLessonType,
          associatedTopicType: userTopicType,
          category: userItemCategory,
          itemId: ItemId,
          title: ItemName,
          url: link
        }
        dataa = docData;
      }
      const data = doc(db, `Lessons/${userlessonname}/extraInfo/infoDoc`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
        let courseLessons = docSnap1.data().lessonChildItems;
        if (courseLessons.length >= 1 && (rootvalue == "Audio" || rootvalue == "Story" || rootvalue == "video")) {
          alert("already have one item");
          return
        }
        else {
          if (usertopicname != '') {
            const data = doc(db, `Topics/${usertopicname}/extraInfo/infoDoc`);
            await updateDoc(data, { topicChildItems: arrayUnion(ItemId) });
            await setDoc(doc(db, "Items", ItemId), dataa).then(async () => {
              alert("Item Updated Successfully");
            })
          } else if (userlessonname != '') {
            const data = doc(db, `Lessons/${userlessonname}/extraInfo/infoDoc`);
            await updateDoc(data, { lessonChildItems: arrayUnion(ItemId) });
            await setDoc(doc(db, "Items", ItemId), dataa).then(async () => {
              alert("Item Updated Successfully");
            })
          }
          setrootName([]);
          setuserItemCategory('');
          setItemName('');
          setItemId('');
          setlessonName1([]);
          settopicName1([]);
          setlink('');
          seturl1('')
        }
      }
    }
  }
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [file2, setfile2] = useState(null);
  const [url2, seturl2] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [progress2, setprogress2] = useState(0);
  function submit(e) {
    e.preventDefault();
  }
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef).then(() => {
      seturl1("");
      alert("deleted succefully")
    }).catch((error) => {
      console.log(error);
    });
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = (date.toString().replaceAll(" ", "")).substring(0, 20).replace(/[^a-z0-9 -]/gi, '');
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on("state_changed", (snapshot) => {
      const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes * 100));
      setprogress1(prog);
    }, (err) => console.log(err), () => {
      getDownloadURL(uploadTask.snapshot.ref).then(url => console.log(seturl1(url)));
    });
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  }

  const deleteImage2 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef).then(() => {
      seturl2("");
      alert("deleted succefully")
    }).catch((error) => {
      console.log(error);
    });
  };
  const uploadFiles2 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = (date.toString().replaceAll(" ", "")).substring(0, 20).replace(/[^a-z0-9 -]/gi, '');
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on("state_changed", (snapshot) => {
      const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes * 100));
      setprogress2(prog);
    }, (err) => console.log(err), () => {
      getDownloadURL(uploadTask.snapshot.ref).then(url => console.log(seturl2(url)));
    });
  };
  const onImageChange2 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile2(event.target.files[0]);
  }
  const handleChange = (event) => {
    setcsvfile(event.target.files[0]);
    let aya = event.target.files[0];
    console.log(aya);
  };
  const parseFile = (file) => {
    setParsedCsvData([]);
    Papa.parse(file, {
      header: false,
      complete: results => {
        setParsedCsvData(results.data)
      },
    });

  };
  return (<>
    <h1 style={{ textAlign: 'center' }}>Item Form</h1>
    <form id="Item-form" onSubmit={submit}>
      <label>Item Name : </label>
      <input required value={ItemName} type="text" placeholder='Item Name' onChange={(event) => { setItemName(event.target.value); }} />
      <br /><br />
      <label> Choose the Root : </label>
      <Select value={root.filter(function (option) { return option.value === rootvalue; })} options={root} onChange={rootvaluefromuser} ></Select>
      <button type="button" onClick={getrootName}> Get Data</button>
      <br /><br />
      <label> Choose the Root Name : </label>
      <Select value={rootName.filter(function (option) { return option.value === userrootname; })} options={rootName} onChange={rootnamefromuser} ></Select>
      <br /><br />
      <label> Choose the lesson Type : </label>
      <Select value={lesson.filter(function (option) { return option.value === userLessonType; })} options={lesson} onChange={setlessonType} ></Select>
      <button type="button" onClick={getlessonName}> Get Data</button>
      <br /><br />
      <label> Choose the Lesson Name : </label>
      <Select value={lessonName1.filter(function (option) { return option.value === userlessonname; })} options={lessonName1} onChange={setuserlesson} ></Select>
      <br /><br />
      <label> Choose the Topic Type : </label>
      <Select isDisabled={TopicDisable} value={topic.filter(function (option) { return option.value === userTopicType; })} options={topic} onChange={setTopicType} ></Select>
      <button type="button" disabled={TopicDisable} onClick={getTopicName}> Get Data</button>
      <br /><br />
      <label>  Choose the Topic Name : </label>
      <Select isDisabled={TopicDisable} value={topicName1.filter(function (option) { return option.value === usertopicname; })} options={topicName1} onChange={setTopicValue} ></Select>
      <br /><br /><br /><hr /><br />
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button onClick={setquizValue} >Confirm</button> </div>
      <label>Item Category : </label>
      <Select value={ItemCategory.filter(function (option) { return option.value === userItemCategory; })} options={ItemCategory} onChange={setItemCategoryType} ></Select>
      <br /><br />
      <label>Item Id : </label><span>{ItemId}</span>
      <br /><br />
      <label hidden={ShortDescriptionDisable}>Short Description : </label>
      <textarea required hidden={ShortDescriptionDisable} type="text" placeholder='Short Description' onChange={(event) => { setShortDescription(event.target.value); }} />
      <br /><br />
      {/* <label hidden={audioCategoryDisable}>Audio Category : </label>
    <input hidden={audioCategoryDisable} type="text" placeholder='Audio Category' onChange={(event) => { setaudioCategory(event.target.value); }} />
    <br /><br /> */}
      {/* <label hidden={audioDurationDisable}>Audio Duration : </label>
    <input type="text" hidden={audioDurationDisable} placeholder='Audio Duration' onChange={(event) => { setaudioDuration(event.target.value); }} />
    <br /><br /><br /> */}
      <label hidden={AudioFileDisable}>{type} File : </label>
      <input required hidden={AudioFileDisable} type="file" onChange={onImageChange1} />
      <span hidden={AudioFileDisable}>uploaded {progress1}%</span>
      <button type="button" hidden={AudioFileDisable} onClick={() => uploadFiles1(file1)}>upload</button>
      <button type="button" hidden={AudioFileDisable} onClick={() => deleteImage1(url1)}>Delete</button>
      {type == "Audio" ? <>{url1 == "" ? <></> : <audio controls><source src={url1} type="audio/mpeg" /></audio>}</> : <></>}
      {type == "PDF" ? <>{url1 == "" ? <></> : <embed src={url1} type="application/pdf" frameBorder="0" scrolling="auto" height="100%" width="50%"  ></embed>}</> : <></>}
      <br /><br /><br />
      <label hidden={TextFileDisable}>Audio LRC File : </label>
      <input required hidden={TextFileDisable} type="file" onChange={onImageChange2}></input>
      <span hidden={TextFileDisable}>uploaded {progress2}%</span>
      <button type="button" hidden={TextFileDisable} onClick={() => uploadFiles2(file2)}>upload</button>
      <button type="button" hidden={TextFileDisable} onClick={() => deleteImage2(url2)}>Delete</button>
      <br /><br />
      <label hidden={URLDisable}>URL : </label>
      <input required hidden={URLDisable} value={link} type="url" placeholder='URL' onChange={(event) => { setlink(event.target.value); }} />
      <br /><br />
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button type='submit' onClick={submitDataOnFirebase}>Submit</button> </div>
    </form>
    <br /><br />
  </>
  );
}

export default ItemForm;