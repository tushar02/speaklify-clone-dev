import React, { useState } from 'react'
import ItemTable from './ItemTable';
import ItemForm from './ItemForm';
import './item.css'
function Course() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Item");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Item");
    }else{
     
      setvalue("Add new Item");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<ItemTable/>:<ItemForm/>}
      </>
  )
}

export default Course;