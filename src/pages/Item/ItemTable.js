import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc, arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable } from '@firebase/storage';
import Select from "react-select";
import Papa from 'papaparse';
import './item.css'
import { Button, Dialog, TextField } from '@mui/material';
import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Delete, Upload } from '@mui/icons-material';
function ItemTable() {
  const [ItemDetails, setItemDetails] = useState([]);
  const [ItemDetailsCopy, setItemDetailsCopy] = useState([]);
  const [Reset, setReset] = useState(false);
  const [Open, setOpen] = useState(false);
  const [ItemId, setItemId] = useState('');
  const [lesson , setLesson]= useState([]);
  const [topic , setTopic]= useState([]);
  const [lessonUser , setLessonUser] = useState('');
  const [topicUser , setTopicUser] = useState('');
  const usercollectionRef = collection(db, "Items")
  const [link, setlink] = useState('');
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [ShortDescription, setShortDescription] = useState('');
  const [audioCategory, setaudioCategory] = useState('');
  const [audioDuration, setaudioDuration] = useState('');
  const [TextFile, setTextFile] = useState('');
  const [AudioLyrics, setAudioLyrics] = useState([]);
  const [type, settype] = useState('');
  const [csvfile , setcsvfile] = useState('');
  const [rootvalue , setrootvalue] = useState('');
  const [rootName  , setrootName] = useState([]);
  const [userrootname , setuserrootname] = useState('');
  const [parsedCsvData, setParsedCsvData] = useState([]);
  var root = [
    {
      value: 'Courses',
      label: "General Course"
    },
      {
      value: 'Grammar',
      label: 'Grammar'
    },
    {
    value: 'video',
      label: "Video"
    },
    {
      value: 'Story',
      label: "Story"
    },
    {
      value: 'Audio',
      label: 'Audio'
    }
  ];
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setItemDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setItemDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset, Open]);
  const handleClose = () => {
    setOpen(false);
  };
  const deleteImage1 = (file) =>{
    const desertRef = ref(storage, file);
   deleteObject(desertRef).then(() => {
     seturl1("");
     alert("deleted succefully")
   }).catch((error) => {
     console.log(error);
   }); 
   };
   const uploadFiles1 = (file)=>{
    if(!file) return;
    let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
      const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const  uploadTask = uploadBytesResumable(storageref,file);
    uploadTask.on("state_changed",(snapshot)=>{
       const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
       setprogress1(prog);
    },(err)=>console.log(err),()=>{
       getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
    });
 };
 const onImageChange1 = (event) => {
  if (event.target.files && event.target.files[0]) {
    setImage1(URL.createObjectURL(event.target.files[0]));
  }
   setfile1( event.target.files[0]);
 }
  const handleOpen = async (i) => {
    setOpen(true);
    let itemType = ItemDetails[i].category;
    settype(itemType);
    if (itemType == "PDF") {
      setItemId(ItemDetails[i].itemId);
      seturl1(ItemDetails[i].url);
    } else if (itemType == "VIDEO") {
      setItemId(ItemDetails[i].itemId);
      setlink(ItemDetails[i].url);
    } else if (itemType == "Audio") {
      setItemId(ItemDetails[i].itemId);
      seturl1(ItemDetails[i].audioLink);
      setShortDescription(ItemDetails[i].courseShortDesc);
      setaudioDuration(ItemDetails[i].audioDuration);
      setaudioCategory(ItemDetails[i].audioCategory);
      let Array = ItemDetails[i].audioLyrics;
      let data = "";
      for (let i = 0; i < Array.length; i++) {
        if (i < Array.length - 1) {
          data += Array[i] + ";";
        } else {
          data += Array[i];
        }
      }
      setAudioLyrics(data);
      setTextFile(data);
    }
  };
  function submit(e) {
    e.preventDefault();
  }
  const showFile = (event) => {
    event.preventDefault();
    const reader = new FileReader();
    reader.onload = (event) => {
      const text = event.target.result;
      console.log(text);
      setTextFile(text);
    };
    reader.readAsText(event.target.files[0]);
  };
  const updateDataOnFirebase = async () => {
    alert(ItemId);
    const ItemRef = doc(db, "Items", ItemId);
    if (type== "PDF") {
      await updateDoc(ItemRef, {
        url: url1
      });
    }else if (type == "VIDEO") {
      await updateDoc(ItemRef, {
          url: link
        });
      }
        else if (type == "Audio") {
          const FinalTextArray =[];
        for(var i =0;i<parsedCsvData.length-1;i++){
             FinalTextArray.push(parsedCsvData[i][0]+""+parsedCsvData[i][1]);
        }
       await updateDoc(ItemRef, {
            audioDuration: audioDuration,
            audioLink: url1,
            url:url1,
            audioLyrics:FinalTextArray,
            courseShortDesc:ShortDescription,
          });
        }
    alert("data updated succefully");
    setOpen(false);
  }
  async function deleteItem(i) {
    if (window.confirm("Are you sure")) {    
    let TopicItem = ItemDetails[i].associatedTopic;
    let LessonItem = ItemDetails[i].associatedLesson;
    let ItemId = ItemDetails[i].itemId;
    if (TopicItem != "") {
      const TopicRef = doc(db, `Topics/${TopicItem}/extraInfo/infoDoc`);
      await updateDoc(TopicRef, { topicChildItems: arrayRemove(ItemId) });
    } else {
      const LessonRef = doc(db, `Lessons/${LessonItem}/extraInfo/infoDoc`);
      await updateDoc(LessonRef, { lessonChildItems: arrayRemove(ItemId) });
    }
    await deleteDoc(doc(db, "Items", ItemDetails[i].itemId));
    setReset(!Reset);
    alert("Deleted data successfully");
  }
  }
  const  handleChange = (event) => {
    setcsvfile( event.target.files[0]);
    let aya =  event.target.files[0];
    console.log(aya);
  };
  const parseFile = (file) => {
      setParsedCsvData([]);
    Papa.parse(file, {
      header: false,
      complete: results => {
        setParsedCsvData(results.data)
      },
    });
    
  };
  useEffect(()=>{
    parseFile(csvfile);
   },[csvfile]);
   const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setItemDetails([]);
      for(var i =0;i<ItemDetailsCopy.length;i++){
          if (ItemDetailsCopy[i]["associatedRootType"]=='Courses') {
            const query = ItemDetailsCopy[i];
            setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
      if (ItemDetails[i]["associatedRootType"]=='Practice'  ) {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) => ItemDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
      if (ItemDetailsCopy[i]["associatedRootType"]=='Grammar' ) {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
      if (ItemDetailsCopy[i]["associatedRootType"]=='video') {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
      if (ItemDetailsCopy[i]["associatedRootType"]=="Audio") {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
    if (ItemDetailsCopy[i]["associatedRootType"]=='Story') {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const getlessonType = async (value) => {
  setLesson([]);  
  const data =   doc(db,   `${rootvalue}/${value}/extraInfo/infoDoc`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
   if(docSnap.data().courseFinalArcade!=undefined){
    if(docSnap.data().courseFinalArcade!=""){
    setLesson([{value : docSnap.data().courseFinalArcade, label:docSnap.data().courseFinalArcade}]);
    }
  }
     console.log(docSnap.data().courseLessons.map((user) => { 
        const query = {value : user, label:user}
     setLesson(lesson=> lesson.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const getTopicType = async (value) => {
  setTopic([]);
  const data = doc(db, `Lessons/${value}`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
     console.log(docSnap.data().lessonTopics.map((user) => { 
        const query = {value : user, label:user}
     setTopic(topic=> topic.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){
    if (ItemDetailsCopy[i]["associatedRoot"]==value) {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }
  getlessonType(e.value);
};
const lessonnamefromuser =(e)=>{
  setLessonUser(e.value);
  let lessonValue = e.value;
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){   
    if (ItemDetailsCopy[i]["associatedLesson"]==lessonValue) {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }
  getTopicType(lessonValue);
}
const topicnamefromuser =(e)=>{
  setTopicUser(e.value);
  let lessonValue = e.value;
  setItemDetails([]);
  for(var i =0;i<ItemDetailsCopy.length;i++){   
    if (ItemDetailsCopy[i]["associatedTopic"]==lessonValue) {
        const query = ItemDetailsCopy[i];
        setItemDetails((ItemDetails) =>  ItemDetails.concat(query));
    }
  }
}
const change=()=>{
     setItemDetails(ItemDetailsCopy);
     setrootvalue('');
     setuserrootname('');
     setLessonUser('');
     setTopicUser('');
}
  return (
    <div className="App">
      <div>
        <label className="label"> Choose the Root : </label>
        <Select
          value={root.filter(function (option) {
            return option.value === rootvalue;
          })}
          options={root}
          onChange={rootvaluefromuser}
        ></Select>
        <br />
        <br />
        <label className="label"> Choose the Root Name : </label>
        <Select
          value={rootName.filter(function (option) {
            return option.value === userrootname;
          })}
          options={rootName}
          onChange={rootnamefromuser}
        ></Select>
        <br />
        <br />
        <label className="label"> Choose the Lesson Name : </label>
        <Select
          value={lesson.filter(function (option) {
            return option.value === lessonUser;
          })}
          options={lesson}
          onChange={lessonnamefromuser}
        ></Select>
        <br />
        <br />
        <label className="label"> Choose the Topic Name : </label>
        <Select
          value={topic.filter(function (option) {
            return option.value === topicUser;
          })}
          options={topic}
          onChange={topicnamefromuser}
        ></Select>
        <br />
        <br />
        <button
          style={{ marginLeft: "51vw" }}
          className="addButton"
          onClick={change}
        >
          Reset Filter
        </button>
      </div>
      <h2>Item Data</h2>
      {`${JSON.stringify(type)}`}
      <table className="content-table">
        <thead>
          <tr style={{ width: "10%" }}>
            <th style={{ width: "5%" }}>S No.</th>
            <th style={{ width: "50%" }}>Item Name</th>
            <th style={{ width: "100%" }}>Type</th>
            <th style={{ width: "100%" }}>Associated Parent</th>
            <th style={{ width: "100%" }}>Associated Lesson</th>
            <th style={{ width: "100%" }}>Associated Topic </th>
          </tr>
        </thead>
        <tbody>
          {ItemDetails.length === 0 ? (
            <tr>
              <td colspan="6">No Items.</td>
            </tr>
          ) : (
            <></>
          )}
          {ItemDetails.map((Item, i) => {
            return (
              <tr key={i}>
                <td>{i + 1}</td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Item.itemId}
                  <br />
                  <br />
                  <button className="edit" onClick={() => handleOpen(i)}>
                    Edit
                  </button>
                  <button className="delete" onClick={() => deleteItem(i)}>
                    Delete
                  </button>
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Item.category}
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Item.associatedRoot}
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Item.associatedLesson}
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Item.associatedTopic}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog open={Open} onClose={handleClose}>
        <DialogTitle sx={{ color: "#308efe", fontWeight: "500" }}>
          <text style={{ color: "#308efe" }}>Update Item Form</text>
        </DialogTitle>
        <DialogContent>
          <div>
            <form
              onSubmit={submit}
              style={{
                fontFamily: "sans-serif",
                fontSize: "16px",
                fontWeight: "bolds",
              }}
            >
              <span>Item Id - {ItemId}</span>
              {type == "audio" ? (
                <>
                  <label>Short Description : </label>
                  <textarea
                    type="text"
                    placeholder="Short Description"
                    onChange={(event) => {
                      setShortDescription(event.target.value);
                    }}
                  />
                  <br />
                  <br />
                  {/* <label>Audio Category : </label>
              <input type="text" placeholder='Audio Category' onChange={(event) => { setaudioCategory(event.target.value); }} />
              <br /><br /> */}
                  <label>Audio Duration : </label>
                  <input
                    type="text"
                    placeholder="Audio Duration"
                    onChange={(event) => {
                      setaudioDuration(event.target.value);
                    }}
                  />
                  <br />
                  <br />
                  <br />
                  <label>{type} File : </label>
                  <input type="file" onChange={onImageChange1} />
                  <span>Uploaded {progress1}%</span>
                  <br />
                  <br />
                  {url1 == "" ? (
                    <></>
                  ) : (
                    <audio controls>
                      <source src={url1} type="audio/mpeg" />
                    </audio>
                  )}
                  <br />
                  <br />
                  <Button
                    startIcon={<Upload />}
                    onClick={() => uploadFiles1(file1)}
                  >
                    upload
                  </Button>
                  <Button
                    startIcon={<Delete />}
                    onClick={() => deleteImage1(url1)}
                  >
                    Delete
                  </Button>

                  <br />
                  <br />
                  <br />
                  <label>Text File : </label>
                  <input type="file" onChange={handleChange}></input>
                  <textarea
                    disabled={true}
                    value={AudioLyrics}
                    cols="70"
                    rows="10"
                  ></textarea>
                  <br />
                  <br />
                  <br />
                  <br />
                </>
              ) : (
                <></>
              )}
              {type == "PDF" ? (
                <>
                  {" "}
                  <label>{type} File : </label>
                  <br />
                  <br />
                  <input type="file" onChange={onImageChange1} />
                  <br />
                  {url1 == "" ? (
                    <></>
                  ) : (
                    <embed
                      src={url1}
                      type="application/pdf"
                      frameBorder="0"
                      scrolling="auto"
                      height="100%"
                      width="50%"
                    ></embed>
                  )}
                  <br />
                  <br />
                  <Button
                    startIcon={<Upload />}
                    onClick={() => uploadFiles1(file1)}
                  >
                    upload
                  </Button>
                  <Button
                    startIcon={<Delete />}
                    onClick={() => deleteImage1(url1)}
                  >
                    Delete
                  </Button>
                  <br />
                  <br />
                  <span>uploaded {progress1}%</span>
                  <br />
                  <br />
                </>
              ) : (
                <></>
              )}
              {type == "VIDEO" ? (
                <>
                  <label>URL : </label>
                  <br />
                  <br />
                  <TextField
                    value={link}
                    type="url"
                    placeholder="URL"
                    onChange={(event) => {
                      setlink(event.target.value);
                    }}
                  />
                  <br />
                  <br />
                </>
              ) : (
                <></>
              )}
              {type == "HTML" ? (
                <>
                  <label>URL : </label>
                  <br />
                  <br />
                  <TextField
                    value={link}
                    type="url"
                    placeholder="URL"
                    onChange={(event) => {
                      setlink(event.target.value);
                    }}
                  />
                  <br />
                  <br />
                </>
              ) : (
                <></>
              )}
            </form>
          </div>
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={updateDataOnFirebase}>
            Submit
          </Button>
          <Button sx={{ color: "red" }} onClick={handleClose}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default ItemTable;