import React, { useState, useEffect } from "react";
import { collection, addDoc } from "firebase/firestore";
import { getAuth, sendSignInLinkToEmail } from "firebase/auth";
import {
  isSignInWithEmailLink,
  createUserWithEmailAndPassword,
  signInWithEmailLink,
} from "firebase/auth";
import { sendPasswordResetEmail } from "firebase/auth";
import $ from "jquery";
import { useNavigate } from "react-router";
import "./AddNewUser.css";
import { db } from "../../firebase/Firebase-config";
import { Link } from "react-router-dom";

const AddNewUser = ({ handleLogout }) => {
  const navigate = useNavigate();
  const goToHome = () => navigate("/");
  const [isUser, setIsUser] = useState(false);
  const [isInstructor, setIsInstructor] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const goToUser = () => navigate("/admin/users");
  const [fullname, setFullName] = useState("");
  const [useremail, setEmail] = useState([]);

  const [errorMessage , seterrorMessage] = useState("");

  const sendmail = () => {
    const auth = getAuth();

    var actionCodeSettings = {
      url: "http://localhost:3000",
      handleCodeInApp: false,
    };
    /*  if (compVal == "default") {
      alert("Select Company");
      return;
    } */
    createUserWithEmailAndPassword(auth, useremail, "password")
      .then(async (userCredential) => {
        // Signed in
        const user = userCredential.user;
        const role = "";
        sendPasswordResetEmail(auth, useremail, actionCodeSettings)
          .then(() => {
            alert("Reset link sent");
            seterrorMessage("");
          })
          .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            seterrorMessage(errorMessage);
            // ..
            console.log(errorCode + errorMessage);
          });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        seterrorMessage(errorMessage);
        // ..
      });
  };
  return (
    <div className="section" style={{textAlign:"center"}}>
      <h1>Add New User</h1>
      <label>Full Name : </label>
      <input
        type="text"
        id="name"
        value={fullname}
        className="admin-course-main-top-input"
        onChange={(event) => {
          setFullName(event.target.value);
        }}
      />
<br /><br />
      <label>Email : </label>
      <input
        type="text"
        id="email"
        className="admin-course-main-top-input"
        value={useremail}
        onChange={(event) => {
          setEmail(event.target.value);
        }}
      />
<br /><br />
      <button className="btn" onClick={sendmail}>Save</button>
      <br />{errorMessage}
    </div>
  );
};

export default AddNewUser;
