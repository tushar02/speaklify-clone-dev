import { Button, Checkbox, Container, FormControlLabel, ThemeProvider, Typography } from "@mui/material";
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { event } from "jquery";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./LoginPage.css";
const { createTheme } = require("@mui/material");

function LoginPage() {
  const navigate = useNavigate();
  const goToHome = () => navigate("/");
  const [user, setUser] = useState('');
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [userNameError, setUserNameError] = useState('');

  const clearInputs = () => {
    setUserNameError("");
    setPassword("");
  };

  const clearErrors = () => {
    setUserNameError("");
    setPasswordError("");
  };

  const handleLogin = async() => {

    clearErrors();
    const auth = getAuth();
    signInWithEmailAndPassword(auth, userName, password)
      .then(async(userCredential) => {

        const user = userCredential.user;
        await userCredential.user.getIdToken().then((Res)=>{
          console.log(Res)
        })
      /*   setCookie("UID", user.uid); */
        alert("successfully login");
       goToHome();
      })
      .catch(err => {
        switch (err.code) {
          case "auth/invalid-username":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setUserNameError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
        }
      });
  };
/*   const authListener = () => {
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      if (user) {
        clearInputs();
        const uid = user.uid;
        console.log(uid);
        setUser(user);
      } else {
        setUser("");
      }
    });
  };

  useEffect(() => {
    authListener();
  }, []); */
 const submit=(e)=>{
    e.preventDefault();
  }
  return (
    <>  
    <h2 id ='heading'>Login Page</h2> 
     <div style={{marginLeft: "35vw"}} className="login">    
         <form onSubmit={submit} id="login">    
        <label className="lab"><b>User Name     
        </b>    
        </label>    
        <input type="text" value={userName} name="Uname" id="Uname" placeholder="Username"onChange={(event) => { setUserName(event.target.value); }} />    
        <br /> {userNameError}
        <b></b>    <br /><br />
        <label className="lab"><b>Password     
        </b>    
        </label>    <br />
        <input type="Password" value={password} name="Pass" id="Pass" placeholder="Password" onChange={(event) => { setPassword(event.target.value); }} />    
        <br />{passwordError}
        <br/>   <br />
        <button onClick={handleLogin} name="log" id="log">Log In</button>       
        <br></br>    
    
        </form>     
      </div>     
   </>
  )
}

export default LoginPage;