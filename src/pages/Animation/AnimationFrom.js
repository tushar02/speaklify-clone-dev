import React, { useState } from "react";
import Select from "react-select";
import { getDownloadURL, uploadBytesResumable } from "@firebase/storage";
import { ref, deleteObject } from "firebase/storage";
import { db, storage } from "../../firebase/Firebase-config";
import { doc, updateDoc, setDoc, arrayUnion } from "firebase/firestore";
function AnimationFrom() {
  const [AnimationTypeFromUser, setAnimationTypeFromUser] = useState("");
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  var AnimationType = [
    {
      value: "Happy",
      label: "Happy Animation",
    },
    {
      value: "Neutral",
      label: "Netural Animation",
    },
    {
      value: "Sad",
      label: "Sad Animation",
    },
  ];
  function submit(e) {
    e.preventDefault();
  }
  const setAnimationType = (e) => {
    setAnimationTypeFromUser(e.value);
  };
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        seturl1("");
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "");
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const prog = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setprogress1(prog);
      },
      (err) => console.log(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) =>
          console.log(seturl1(url))
        );
      }
    );
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  };
  const UploadOnFirebase = async () => {
    if (AnimationTypeFromUser == "Happy") {
      alert(url1);
      const Ref = doc(db, "Animation", "animationDoc");
      alert(url1);
      await updateDoc(Ref, { happyAnimation: arrayUnion(url1) }).then(
        (error) => {
          console.log(error);
        }
      );
    } else if (AnimationTypeFromUser == "Neutral") {
      const Ref = doc(db, "Animation", "animationDoc");
      await updateDoc(Ref, { neutralAnimation: arrayUnion(url1) });
    } else if (AnimationTypeFromUser == "Sad") {
      const Ref = doc(db, "Animation", "animationDoc");
      await updateDoc(Ref, { sadAnimation: arrayUnion(url1) });
    }
    alert("data updated succefully");
    document.getElementById("Animation-form").reset();
    seturl1("");
    setprogress1(0);
  };
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Animation Form</h1>
      <form id="Animation-form" onSubmit={submit}>
        <label>Animation Mode : </label>
        <Select
          value={AnimationType.filter(function (option) {
            return option.value === AnimationTypeFromUser;
          })}
          options={AnimationType}
          onChange={setAnimationType}
        ></Select>
        <br />
        <br />
        <label>Gif : </label>
        <input type="file" onChange={onImageChange1} />
        <span>uploaded {progress1}%</span>
        <button onClick={() => uploadFiles1(file1)}>upload</button>
        <button onClick={() => deleteImage1(url1)}>Delete</button>
        <img src={url1} alt="" width="193" height="130" />
        <br />
        <br />
        <button onClick={UploadOnFirebase}>Upload</button>
      </form>
    </>
  );
}

export default AnimationFrom;
