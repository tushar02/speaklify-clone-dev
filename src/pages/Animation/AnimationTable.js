import React, { useState, useEffect } from "react";
import { db, storage } from "../../firebase/Firebase-config";
import {
  collection,
  getDocs,
  getDoc,
  doc,
  deleteDoc,
  updateDoc,
  arrayRemove,
  arrayUnion,
} from "firebase/firestore";
import { getStorage, ref, deleteObject } from "firebase/storage";
import { getDownloadURL, uploadBytesResumable } from "@firebase/storage";
import Select from "react-select";
import Modal from "@material-ui/core/Modal";
import { Delete, SettingsEthernet, Upload } from "@mui/icons-material";
import { Button, Dialog } from "@mui/material";
import { DialogActions, DialogContent, DialogTitle } from "@material-ui/core";
function AnimationTable() {
  const [Reset, setReset] = useState(false);
  const [HappyAnimation, setHappyAnimation] = useState([]);
  const [SadAnimation, setSadAnimation] = useState([]);
  const [NeatrulAnimation, setNeatrulAnimation] = useState([]);
  const [image1, setImage1] = useState(null);
  const [file1, setfile1] = useState(null);
  const [url1, seturl1] = useState("");
  const [progress1, setprogress1] = useState(0);
  const [Open, setOpen] = useState(false);
  const [AnimationType, setAnimationType] = useState("");
  const [OldUrl, setOldUrl] = useState("");
  const handleOpen = async (Type, url) => {
    setOpen(true);
    setAnimationType(Type);
    seturl1(url);
    setOldUrl(url);
  };
  useEffect(() => {
    const getUsers = async () => {
      const data = doc(db, `Animation/animationDoc`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
        setHappyAnimation(docSnap1.data().happyAnimation);
        setNeatrulAnimation(docSnap1.data().neutralAnimation);
        setSadAnimation(docSnap1.data().sadAnimation);
      } else {
        console.log("No such document!");
      }
    };
    getUsers();
  }, [Reset]);
  const deleteImage1 = (file) => {
    const desertRef = ref(storage, file);
    deleteObject(desertRef)
      .then(() => {
        alert("deleted succefully");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  async function deleteDetails(Type, url) {
    if (window.confirm("Are you sure")) {
      deleteImage1(url);

      if (Type == "Happy") {
        const AnimationRef = doc(db, "Animation", "animationDoc");
        await updateDoc(AnimationRef, { happyAnimation: arrayRemove(url) });
      } else if (Type == "Sad") {
        const AnimationRef = doc(db, "Animation", "animationDoc");
        await updateDoc(AnimationRef, { sadAnimation: arrayRemove(url) });
      } else if (Type == "Neatrul") {
        const AnimationRef = doc(db, "Animation", "animationDoc");
        await updateDoc(AnimationRef, { neutralAnimation: arrayRemove(url) });
      }
      alert("Animation Deleted Successfully");
      setReset(!Reset);
    }
  }
  function submit(e) {
    e.preventDefault();
  }
  const handleClose = () => {
    setOpen(false);
  };
  const uploadFiles1 = (file) => {
    if (!file) return;
    let date = new Date();
    let ModifiedDate = date
      .toString()
      .replaceAll(" ", "")
      .substring(0, 20)
      .replace(/[^a-z0-9 -]/gi, "");
    const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
    const uploadTask = uploadBytesResumable(storageref, file);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const prog = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setprogress1(prog);
      },
      (err) => console.log(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) =>
          console.log(seturl1(url))
        );
      }
    );
  };
  const onImageChange1 = (event) => {
    if (event.target.files && event.target.files[0]) {
      setImage1(URL.createObjectURL(event.target.files[0]));
    }
    setfile1(event.target.files[0]);
  };
  const updateDataOnFirebase = async () => {
    if (AnimationType == "Happy") {
      const Ref = doc(db, "Animation", "animationDoc");
      await updateDoc(Ref, { happyAnimation: arrayUnion(url1) }).then(
        async () => {
          await updateDoc(Ref, { happyAnimation: arrayRemove(OldUrl) });
        }
      );
    } else if (AnimationType == "Neutral") {
      const Ref = doc(db, "Animation", "animationDoc");
      await updateDoc(Ref, { neutralAnimation: arrayUnion(url1) }).then(
        async () => {
          await updateDoc(Ref, { neutralAnimation: arrayRemove(OldUrl) });
        }
      );
    } else if (AnimationType == "Sad") {
      const Ref = doc(db, "Animation", "animationDoc");
      await updateDoc(Ref, { sadAnimation: arrayUnion(url1) }).then(
        async () => {
          await updateDoc(Ref, { sadAnimation: arrayRemove(OldUrl) });
        }
      );
    }
    deleteImage1(OldUrl);
    setOpen(false);
    setReset(!Reset);
    alert("Animation Updated");
  };
  return (
    <div className="App">
      <table className="content-table">
        <thead>
          <tr style={{ width: "10%" }}>
            <th style={{ width: "50%" }}>GIF</th>
            <th style={{ width: "30%" }}>Type</th>
            <th style={{ width: "50%" }}>Edit/Delete</th>
          </tr>
        </thead>
        <tbody>
          {HappyAnimation.length === 0 &&
          NeatrulAnimation.length === 0 &&
          SadAnimation.length === 0 ? (
            <tr>
              <td colspan="6">No Items.</td>
            </tr>
          ) : (
            <></>
          )}
          {HappyAnimation.map((details, i) => {
            return (
              <tr key={i} style={{ textAlign: "left", padding: "8px" }}>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  {" "}
                  <img src={details} alt="" width="193" height="130" />
                </td>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  Happy Animation
                </td>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  <button
                    className="edit"
                    onClick={() => handleOpen("Happy", details)}
                  >
                    Edit
                  </button>
                  <button
                    className="delete"
                    style={{ marginLeft: "5px" }}
                    onClick={() => deleteDetails("Happy", details)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
          {NeatrulAnimation.map((details, i) => {
            return (
              <tr key={i} style={{ textAlign: "left", padding: "8px" }}>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  {" "}
                  <img src={details} alt="" width="193" height="130" />
                  <br />
                  <br />
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  Neutral Animation
                </td>
                <td
                  style={{
                    borderCollapse: "collapse",
                    textAlign: "left",
                    padding: "8px",
                    border: "1px solid",
                  }}
                >
                  <button
                    className="edit"
                    onClick={() => handleOpen("Neatrul", details)}
                  >
                    Edit
                  </button>
                  <button
                    className="delete"
                    onClick={() => deleteDetails("Neatrul", details)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
          {SadAnimation.map((details, i) => {
            return (
              <tr key={i} style={{ textAlign: "left", padding: "8px" }}>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  {" "}
                  <img src={details} alt="" width="193" height="130" />
                  <br />
                  <br />
                </td>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  Sad Animation
                </td>
                <td style={{ textAlign: "left", padding: "8px" }}>
                  <button
                    className="edit"
                    onClick={() => handleOpen("Sad", details)}
                  >
                    Edit
                  </button>
                  <button
                    className="delete"
                    style={{ marginLeft: "5px" }}
                    onClick={() => deleteDetails("Sad", details)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog onClose={handleClose} open={Open}>
        <DialogTitle
          sx={{ fontSize: 24, color: "#308efe", fontWeight: "bold" }}
        >
          <text style={{ color: "#308efe" }}>Update Animation</text>
        </DialogTitle>
        <DialogContent>
          <div>
            <form onSubmit={submit} style={{ fontFamily: "sans-serif" }}>
              <span
                style={{
                  fontSize: "18px",
                  fontWeight: "bold",
                  fontFamily: "sans-serif",
                }}
              >
                {" "}
                Type - {AnimationType}
              </span>
              <br />
              <br />
              <label style={{ fontSize: "18px", fontWeight: "bold" }}>
                Featured Animation :{" "}
              </label>
              <input type="file" onChange={onImageChange1} />
              <br />
              <br />
              <img src={url1} alt="" width="193" height="130" />
              <br />
              <br />
              <Button
                onClick={() => uploadFiles1(file1)}
                startIcon={<Upload />}
              >
                upload
              </Button>
              <Button onClick={() => deleteImage1(url1)} startIcon={<Delete />}>
                Delete
              </Button>
              <h3>Uploaded {progress1}%</h3>
            </form>
          </div>
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={updateDataOnFirebase}>
            Submit
          </Button>
          <Button sx={{ color: "red" }} onClick={handleClose}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default AnimationTable;
