import React, { useState } from "react";
import AnimationForm from "./AnimationFrom";
import AnimationTable from "./AnimationTable";
import "./animation.css";

function Animation() {
  const [Reset, setReset] = useState(false);
  const [value, setvalue] = useState("Add new Animation");
  const change = () => {
    setReset(!Reset);
    if (Reset == false) {
      setvalue("Back to Animation");
    } else {
      setvalue("Add new Animation");
    }
  };
  return (
    <>
      <button className="addButton" onClick={change}>
        {value}
      </button>
      {Reset == false ? <AnimationTable /> : <AnimationForm />}
    </>
  );
}

export default Animation;
