import React, { useState } from 'react';
import WhatsappChangeForm from './whatsappchange';
import {useLocation} from 'react-router-dom';


function Whatsapp (){

    const [choice,setChoice] = useState("");  
    const location = useLocation();
    const [passedValues,setPassedValues] = useState(location.state);
      //(event)=>setChoice(event.target.value);
      console.log("loaction"+location.state)
      return(
        <div className="whatsapp">
            <center>
                <h2>Select Service Provider</h2>
                <div className='Radio-btn-coll' onChange={(event)=>setChoice(event.target.value)}>
                    <input type="radio" value="Provider1" name="provider" /> Twilio
                    <br></br>
                    {/* <input type="radio" value="Provider2" name="provider" disabled/> Provider 2 */}
                    <br></br>
                </div>

                <WhatsappChangeForm choice={choice} values={passedValues}/>
             
            </center>
        </div>
      );
    };

export default Whatsapp;
  