import React, { useState } from 'react';
import WhatsappChangeForm from './whatsappchange';
import {useLocation} from 'react-router-dom';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { collection, query, where, getDocs, doc,setDoc } from "firebase/firestore";
import MailSettings from './mailsettingschange';

const firebaseConfig = {
  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

function SettingsChange (props){
    const [choice,setChoice] = useState("");  
    const location = useLocation();
    const [passedValues,setPassedValues] = useState(location.state);
    const [GetPhone,setGetPhone] = useState('');
    const [AccountSID,setAccountSID] = useState('');
    const [AuthToken,setAuthToken] = useState('');

    if(GetPhone=='' && AccountSID=='' && AuthToken==''){
      getDocs(collection(db, "WhatsAppMessaging")).then(
        (querySnapshot)=>{
          querySnapshot.forEach((doc) => {
            // console.log(doc.id);
            if(doc.id=="twilio"){
              // console.log(doc.data()['Get Phone From']);
              setGetPhone(doc.data()['Get_Phone_From']);
              setAuthToken(doc.data()['Auth_Token']);
              setAccountSID(doc.data()['Account_SID']);
            }
          });
    });
    }

    const whatsappUpdate = async(e)=>{
      e.preventDefault();
        await setDoc(doc(db, "WhatsAppMessaging", "twilio"), {
            Account_SID: AccountSID,
            Auth_Token: AuthToken,
            Get_Phone_From: GetPhone,
          }).then(alert("Updated Successfully!!"));
    }
    const whatsappReset = async(e)=>{
      e.preventDefault();
        await setDoc(doc(db, "WhatsAppMessaging", "twilio"), {
            Account_SID: AccountSID,
            Auth_Token: AuthToken,
            Get_Phone_From: GetPhone,
          }).then(alert("Twilio Reset Successfully!!"));
    }
      //(event)=>setChoice(event.target.value);
    if(props.choice==""){
        return(
         <div></div>
        );
    }
    else if(props.choice=="Provider1"){
      return(
        <div className="Settings">
            <center>
              <br></br>
              <div className='Radio-btn-coll' onChange={(event)=>{setChoice(event.target.value)}}>
                  <input type="radio" value="Provider1" name="provider" /> Gmail
                  <input type="radio" value="Provider2" name="provider" /> Outlook
                  {/* <input type="radio" value="Provider3" name="provider" /> SendGrid */}
              </div>
              <MailSettings choice={choice}/>
            </center>
        </div>
      );
    }
    else if(props.choice=="Provider2"){
        return(
            <div>
              <br></br>
              <label>
      Get Message From: 
      <input type={'text'} value={GetPhone} onChange={(e)=>setGetPhone(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Account SID: 
      <input type={'text'} value={AccountSID} onChange={(e)=>setAccountSID(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Auth Token: 
      <input type={'text'} value={AuthToken} onChange={(e)=>setAuthToken(e.target.value)}/>
    </label>
    <br></br>

    <br></br>
      
      <input type={'submit'} value={"Update"} onClick={whatsappUpdate}/>
      <input type={'submit'} value={"Reset"} onClick={whatsappReset}/>
            </div>
           );
    }
    
    };

export default SettingsChange;