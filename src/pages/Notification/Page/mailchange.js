import React, { useState } from 'react';
import axios from 'axios';

import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { collection, query, where, getDocs, doc } from "firebase/firestore";
import {getStorage, ref, uploadBytes} from "firebase/storage";


const firebaseConfig = {
  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage();


function MailChange(props){
    const [senderMail,setSenderMail] = useState('');
    const [senderpassword,setSenderPassword ] = useState('');
    const [subject,setSubject] = useState('');
    
    const [host,setHost] = useState('');
    const [port,setPort] = useState('');
    const [html,setHtml] = useState('');
    const [secure,setSecure] = useState(false);
    const [passedValues,setPassedValues] = useState(props.values);
    const [attachment,setAttachment] = useState([]);
    const [attachmentFileName, setAttachmentFileName] = useState([]);

    if(props.choice == "Provider1"){
      // setAttachment([]);
      // setAttachmentFileName([]);
      getDocs(collection(db, "MailSettings")).then(
        (querySnapshot)=>{
          querySnapshot.forEach((doc) => {
            // console.log(doc.id);
            if(doc.id=="Gmail"){
              // console.log(doc.data()['Get Phone From']);
              setHost(doc.data()['Host']);
              setPort(doc.data()['Port']);
              setSecure(doc.data()['Secure']);
              setSenderMail(doc.data()['Email']);
              setSenderPassword(doc.data()['Password']);
            }
          });
      });
    }
    else if(props.choice=="Provider2"){
      // setAttachment([]);
      // setAttachmentFileName([]);
      getDocs(collection(db, "MailSettings")).then(
        (querySnapshot)=>{
          querySnapshot.forEach((doc) => {
            // console.log(doc.id);
            if(doc.id=="Outlook"){
              // console.log(doc.data()['Get Phone From']);
              setHost(doc.data()['Host']);
              setPort(doc.data()['Port']);
              setSecure(doc.data()['Secure']);
              setSenderMail(doc.data()['Email']);
              setSenderPassword(doc.data()['Password']);
            }
          });
      });
    }
    else if(props.choice=="Provider3"){
      // setAttachment([]);
      // setAttachmentFileName([]);
    }

    const handleAttachment = (e)=>{
      e.preventDefault();
      setAttachment([]);
      setAttachmentFileName([]);
      for(let i=0;i<e.target.files.length;i++){
        const newFile = e.target.files[i];
        newFile['filename'] = e.target.files[i].name;
        console.log(newFile);
        setAttachment((previousState)=>[...previousState,newFile]);
        setAttachmentFileName((previousState)=>[...previousState,newFile['filename']]);
      }
    }

    const handleSubmit = (event) => {
      event.preventDefault();
      passedValues.forEach(element => {
        
        axios.post('https://us-central1-ispeak-5e601.cloudfunctions.net/sendMail', {
        userMail:senderMail,
        pass:senderpassword,
        to:element['Email'],
        subject:subject,
        html:html,
        host:host,
        port:port,
        secure:secure,
        attachment:attachment,
        //token: element['token'],//"dV9ujcQZRx2XeCi1qgUVUt:APA91bGg3h2uNFlPCHO0IS5sztnnqHP9mM0Zl07pW9m6Ap3_4HPD4nzL4ePP6030rAsBJ7ZlSWFUJ34Dj_T-D09eSwHAKEkkI6ckS2pQFf2MwxATdwM3d98XEFf163Snz2IyLs1oreuw",
        //title: title,
        
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
      });
      }

      const handleSubmitSendGrid = (event) =>{
        event.preventDefault();
        //sgMail.setApiKey(sendGridApiKey);
        passedValues.forEach(element => {
        
          axios.post('https://us-central1-ispeak-5e601.cloudfunctions.net/sendGridMail', {
          userMail:senderMail,
          to:element['Email'],
          subject:subject,
          html:html,
          attachment:attachment,
        })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
        });
      }



    if(props.choice == ""){
        return <div></div>;
    }
    else if(props.choice == "Provider1"){
      // if(host=='') setHost("smtp.gmail.com");
      //   if(port=='') setPort(465);
      //   if(secure!= true) setSecure(true);
        return (
        <div>
          <br></br>

        <h3>Gmail form will appear here!! </h3>
            
            <form onSubmit={handleSubmit}>
            <br></br>
            {/* <br></br>
            <label>
              Sender's Mail: 
              <input type={'text'} value={senderMail} onChange={(e)=>setSenderMail(e.target.value)}/>
            </label>
            <br></br>
            <br></br>
            <label>
              Sender's App Password: 
              <input type={'text'} value={senderpassword} onChange={(e)=>setSenderPassword(e.target.value)}/><br></br>
              ( sign in with an App Password. If you don't use 2-Step Verification, you might need to allow it first.)
            </label>
    <br></br>
    <br></br> */}
    <label>
      Subject 
      <input type={'text'} value={subject} onChange={(e)=>setSubject(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    
    <label>
      HTML: 
      <input type={'text'} value={html} onChange={(e)=>setHtml(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    Attachment:
    <input type={"file"} multiple onChange={handleAttachment}/>
    <br></br>
    <br></br>

    {/* <h4>Settings Section</h4>
    <br></br>
    <br></br>
    <label>
      Host: 
      <input type={'text'} value={host} onChange={(e)=>setHost(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Port: 
      <input type={'text'} value={port} onChange={(e)=>setPort(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Secure: 
      <input type={'text'} value={secure} onChange={(e)=>setSecure(e.target.value)}/>
    </label>
    <br></br>
    <br></br> */}
    
    <input type="submit" />
  </form>
  </div>
        )
    }
    else if(props.choice == "Provider2"){
        if(host=='') setHost("smtp.office365.com");
        if(port=='') setPort(587);
        if(secure!= false) setSecure(false);
        return (
        <div>
          <br></br>

        <h3>Outlook form will appear here!! </h3>
            
            <form onSubmit={handleSubmit}>
            <br></br>
            <br></br>
            {/* <label>
              Sender's Mail: 
              <input type={'text'} value={senderMail} onChange={(e)=>setSenderMail(e.target.value)}/>
            </label>
            <br></br>
            <br></br>
            <label>
              Sender's App Password: 
              <input type={'text'} value={senderpassword} onChange={(e)=>setSenderPassword(e.target.value)}/><br></br>
              (Enable two-step verification and create app password in microsoft account!!)
            </label>
    <br></br>
    <br></br> */}
    <label>
      Subject 
      <input type={'text'} value={subject} onChange={(e)=>setSubject(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    
    <label>
      HTML: 
      <input type={'text'} value={html} onChange={(e)=>setHtml(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    Attachment:
    <input type={"file"} multiple onChange={handleAttachment}/>
    <br></br>
    <br></br>
    {/* <h4>Settings Section</h4>
    <br></br>
    <br></br>
    <label>
      Host: 
      <input type={'text'} value={host} onChange={(e)=>setHost(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Port: 
      <input type={'text'} value={port} onChange={(e)=>setPort(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Secure: 
      <input type={'text'} value={secure} onChange={(e)=>setSecure(e.target.value)}/>
    </label>
    <br></br> */}
    <br></br>
    <input type="submit" />
  </form>
  </div> 
  )}
    else if(props.choice == "Provider3"){
      
      return (
      <div>
        <br></br>

      <h3>SendGrid form will appear here!! </h3>
          
          <form onSubmit={handleSubmitSendGrid}>
          <br></br>
  <br></br>
  <label>
    Subject 
    <input type={'text'} value={subject} onChange={(e)=>setSubject(e.target.value)}/>
  </label>
  <br></br>
  <br></br>
  
  <label>
    HTML: 
    <input type={'text'} value={html} onChange={(e)=>setHtml(e.target.value)}/>
  </label>
  <br></br>
  <br></br>

  <input type="submit" />
</form>
</div> 
      )}
    
}
export default MailChange;