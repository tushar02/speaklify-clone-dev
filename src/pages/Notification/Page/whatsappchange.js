import React, { useState } from 'react';
import axios from 'axios';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { collection, query, where, getDocs, doc } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

function WhatsappChangeForm(props){
    const [Phone,setPhone] = useState('');
    const [GetPhone,setGetPhone] = useState('');
    const [AccountSID,setAccountSID] = useState('');
    const [AuthToken,setAuthToken] = useState('');
    const [Message,setMessage] = useState('');
    const [passedValues,setPassedValues] = useState(props.values);
    const [successCount,setSuccessCount] = useState(0);
    const [failureCount,setFailureCount] = useState(0);

  if(GetPhone=='' && AccountSID=='' && AuthToken==''){
    getDocs(collection(db, "WhatsAppMessaging")).then(
      (querySnapshot)=>{
        querySnapshot.forEach((doc) => {
          // console.log(doc.id);
          if(doc.id=="twilio"){
            // console.log(doc.data()['Get Phone From']);
            setGetPhone(doc.data()['Get_Phone_From']);
            setAuthToken(doc.data()['Auth_Token']);
            setAccountSID(doc.data()['Account_SID']);
          }
        });
  });
  }
  console.log("check2", props.values);

    const handleSubmit = (event) => {
        event.preventDefault();

        console.log("check", passedValues);
        
        
        passedValues.forEach(ele => {
            axios.post('https://us-central1-ispeak-5e601.cloudfunctions.net/whatsappSend',{
            token: ele['token'],
            phone: "+91"+ele['contact'],
            getPhoneFrom: GetPhone,
            accountSid: AccountSID,
            authToken: AuthToken,
            message: Message,
        }).then(function (response){
            
            setSuccessCount(successCount+1);
            console.log(response,successCount);
        }).catch(function (error){
            console.log(error);
            setFailureCount(failureCount+1);
        })
        });
        
        
      }
    if(props.choice == ""){
        return <div></div>;
    }
    else if(props.choice == "Provider1"){

        return (
        <div>
        <h3>Twilio form will appear here!! </h3>
        <h6>Note: the current Twilio account is not for production!!</h6>
            <form onSubmit={handleSubmit}>
    
    <br></br>
    <br></br>
    {/* <label>
      Get Phone From: 
      <input type={'text'} value={GetPhone} onChange={(e)=>setGetPhone(e.target.value)}/>
    </label>
    <br></br>
    <label>
      Account SID: 
      <input type={'text'} value={AccountSID} onChange={(e)=>setAccountSID(e.target.value)}/>
    </label>
    <br></br>
    <label>
      Auth Token: 
      <input type={'text'} value={AuthToken} onChange={(e)=>setAuthToken(e.target.value)}/>
    </label> */}
    <br></br>
    <label>
      Message: 
      <input type={'text'} value={Message} onChange={(e)=>setMessage(e.target.value)}/>
    </label>
    <br></br>
    <input type="submit" />
  </form>
  </div> )
    }
    else if(props.choice == "Provider2"){
        return (<div><h3>Provider 2 form will appear here!! </h3>
        
            <form onSubmit={handleSubmit}>
    
    
    <br></br>
    <label>
      Get Message From: 
      <input type={'text'} value={GetPhone} onChange={(e)=>setGetPhone(e.target.value)}/>
    </label>
    <label>
      Account SID: 
      <input type={'text'} value={AccountSID} onChange={(e)=>setAccountSID(e.target.value)}/>
    </label>
    <label>
      Auth Token: 
      <input type={'text'} value={AuthToken} onChange={(e)=>setAuthToken(e.target.value)}/>
    </label>
    <label>
      Message: 
      <input type={'text'} value={Message} onChange={(e)=>setMessage(e.target.value)}/>
    </label>
    <input type="submit" />
  </form>
  </div>
        )
    }
    
}
export default WhatsappChangeForm;