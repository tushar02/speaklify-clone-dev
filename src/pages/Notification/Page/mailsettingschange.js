import React, { useState } from 'react';
import WhatsappChangeForm from './whatsappchange';
import {useLocation} from 'react-router-dom';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { collection, query, where, getDocs, doc, setDoc } from "firebase/firestore";


const firebaseConfig = {
  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);


function MailSettings (props){
    const [choice,setChoice] = useState("");  
    const location = useLocation();
    const [passedValues,setPassedValues] = useState(location.state);
    const [host,setHost] = useState('');
    const [port,setPort] = useState('');
    const [secure,setSecure] = useState('');
    const [senderMail,setSenderMail] = useState('');
    const [senderPassword,setSenderPassword] = useState('');
    const [gmailRender, setgmailRender] = useState(false);
    const [outlookRender, setoutlookRender] = useState(false);

    const gmailUpdate = async (e) =>{
        e.preventDefault();
        await setDoc(doc(db, "MailSettings", "Gmail"), {
            Email: senderMail,
            Host: host,
            Password: senderPassword,
            Port:port,
            Secure: secure,
          }).then(alert("Updated Successfully!!"));
      
    }
    const gmailReset = async(e) =>{
        e.preventDefault();
        await setDoc(doc(db, "MailSettings", "Gmail"), {
            Email: "udio.data@gmail.com",
            Host: "smtp.gmail.com",
            Password: "hzybaorzwsesspfj",
            Port:465,
            Secure: true,
          }).then(alert("Gmail Settings Reset Successfully!!"));
          
    }
    const outlookUpdate = async(e) =>{
        e.preventDefault();
        await setDoc(doc(db, "MailSettings", "Outlook"), {
            Email: senderMail,
            Host: host,
            Password: senderPassword,
            Port:port,
            Secure: secure,
          }).then(alert("Updated Successfully!!"));
          
    }
    const outlookReset = async(e) =>{
        e.preventDefault();
        await setDoc(doc(db, "MailSettings", "Outlook"), {
            Email: "udio.data@gmail.com",
            Host: "smtp.office365.com",
            Password: "surnrdcgmhttfvdo",
            Port:587,
            Secure: false,
          }).then(alert("Updated Successfully!!"));
          
    }

    if(props.choice == "Provider1" && !gmailRender){
        // setAttachment([]);
        // setAttachmentFileName([]);
        setgmailRender(true);
        setoutlookRender(false);
        getDocs(collection(db, "MailSettings")).then(
          (querySnapshot)=>{
            querySnapshot.forEach((doc) => {
              // console.log(doc.id);
              if(doc.id=="Gmail"){
                // console.log(doc.data()['Get Phone From']);
                setHost(doc.data()['Host']);
                setPort(doc.data()['Port']);
                setSecure(doc.data()['Secure']);
                setSenderMail(doc.data()['Email']);
                setSenderPassword(doc.data()['Password']);
              }
            });
        });
      }
      else if(props.choice=="Provider2" && !outlookRender){
        setgmailRender(false);
        setoutlookRender(true);
        getDocs(collection(db, "MailSettings")).then(
          (querySnapshot)=>{
            querySnapshot.forEach((doc) => {
              // console.log(doc.id);
              if(doc.id=="Outlook"){
                // console.log(doc.data()['Get Phone From']);
                setHost(doc.data()['Host']);
                setPort(doc.data()['Port']);
                setSecure(doc.data()['Secure']);
                setSenderMail(doc.data()['Email']);
                setSenderPassword(doc.data()['Password']);
              }
            });
        });
      }
      


    if(props.choice==""){
        return(
         <div></div>
        );
    }
    else if(props.choice=="Provider1"){
      return(
        <div>
            <center>
                <form>
                <br></br>
    <br></br>
    <label>
      Sender Mail: 
      <input type={'text'} value={senderMail} onChange={(e)=>setSenderMail(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Sender/App Password: 
      <input type={'text'} value={senderPassword} onChange={(e)=>setSenderPassword(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
            
    <label>
      Host: 
      <input type={'text'} value={host} onChange={(e)=>setHost(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Port: 
      <input type={'text'} value={port} onChange={(e)=>setPort(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Secure: 
      <input type={'text'} value={secure} onChange={(e)=>setSecure(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    
    <input type="submit" value={"Update"} onClick={gmailUpdate} />
    <input type="submit" value={"Reset"} onClick={gmailReset}/>
  </form>
              
            </center>
        </div>
      );
    }
    else if(props.choice=="Provider2"){
        return(
            <div>
            <center>
                <form>
                <br></br>
    <br></br>
    <label>
      Sender Mail: 
      <input type={'text'} value={senderMail} onChange={(e)=>setSenderMail(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Sender/App Password: 
      <input type={'text'} value={senderPassword} onChange={(e)=>setSenderPassword(e.target.value)}/>
    </label>

            <br></br>
    <br></br>
    <label>
      Host: 
      <input type={'text'} value={host} onChange={(e)=>setHost(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Port: 
      <input type={'text'} value={port} onChange={(e)=>setPort(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    <label>
      Secure: 
      <input type={'text'} value={secure} onChange={(e)=>setSecure(e.target.value)}/>
    </label>
    <br></br>
    <br></br>
    
    <input type="submit" value={"Update"} onClick={outlookUpdate}/>
    <input type="submit" value={"Reset"} onClick={outlookReset}/>
  </form>
  </center>
  </div>
        );
    }

  
            
    
    };

export default MailSettings;
  
  