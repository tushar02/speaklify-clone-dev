import React from 'react';
import { DataGrid, selectedIdsLookupSelector } from '@mui/x-data-grid';
import { IconButton } from '@mui/material';
import { initializeApp } from 'firebase/app';
import { getDoc, getFirestore } from 'firebase/firestore';
import { collection, query, where, getDocs, doc } from "firebase/firestore";
import Cookies from 'universal-cookie';
import {Link,useHistory} from 'react-router-dom';
import {Button} from 'react-bootstrap';
//Calling Bootstrap 4.5 css
import 'bootstrap/dist/css/bootstrap.min.css';
//Calling Firebase config setting to call the data
const firebaseConfig = {
    apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
    authDomain: "ispeak-5e601.firebaseapp.com",
    databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
    projectId: "ispeak-5e601",
    storageBucket: "ispeak-5e601.appspot.com",
    messagingSenderId: "286746647766",
    appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
    measurementId: "G-1MSP3H1FKM"
  };
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);


class AdminPortal extends React.Component {

constructor(props) {
    super(props);
    this.state = {
        users : [],
        userName: [],
        contact: [],
        email: [],
        rows:[],
        rows_copy:[],
        selectedRows:[],
        token:[],
        download:[],
        secLevel:[],
        columns:[{ field: 'id', headerName: 'ID', width: 100},
        { field: 'username', headerName: 'Username', width: 230 },
        { field: 'contact', headerName: 'Contact', width: 230 },
        {
          field: 'Email',
          headerName: 'Email',
          sortable:false,
          width: 300,
        },
        {
          field: "Download",
          renderCell: (cellValues) => {
            return (
              <Button
                variant="contained"
                color="primary"
                onClick={(event) => {
                  // handleClick(event, cellValues);
                  this.downloadTxtFile(cellValues);
                }}
              >
                Download
              </Button>
            );
          }
        }
      ]
    }
  } 
 save =()=>{
  const cookies = new Cookies();
  cookies.set('data', this.state.selectedRows, { path: '/' });
  console.log(cookies.get('vendorToken'));
 }
  //history = useHistory();
   downloadTxtFile = async (cellValues) => {
     console.log("in download fn");
    const element = document.createElement("a");
    
    //add all values;
    //console.log(this.state.users[cellValues['id']-1]);// got the 1st level document id
    const docRef = doc(db, "Users", this.state.users[cellValues['id']-1]);
    const docSnap = await getDoc(docRef);
    console.log("docSnap",docSnap.data());
    const temp = [];
    temp.push({first_Level: docSnap.data()});
    
    //trying to add second level data 
    getDocs(collection(db,"Users",this.state.users[cellValues['id']-1],"groupInfo")).then(
      (querySnapshot)=>{
        querySnapshot.forEach((doci) => {
          const rr = doci.id;
          const obj1 = {rr: doci.data()};
          console.log(obj1);
          temp.push(obj1);
        });
      }
    );
   
    // const docref1 = doc(docRef,"activeStreakInfo","activeStreakDoc");
    // const docSnap1 = await getDoc(docref1);
    // console.log(docSnap1.data());
    //const sfRef = db.collection('Users').doc('14iIcufsiBgXF6UqpiyWUufsd2o2');
    // console.log(db.collection("Users").document(cellValues['id']-1).collection("enrolledCourse").document("enrollInfo"));

    const file = new Blob([JSON.stringify(temp)], {type: 'application/json'});
    element.href = URL.createObjectURL(file);
    element.download = "myFile.json";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }
  
  

  componentDidMount(){
    getDocs(collection(db, "Users")).then(
        (querySnapshot)=>{
          let usersList = [];
          let userNameList = [];
          let contactList = [];
          let emailList = [];
          let deviceToken = [];

          querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            usersList.push(doc.id);
            userNameList.push(doc.data()['userName']);
            contactList.push(doc.data()['phoneNumber']);
            emailList.push(doc.data()['email']);
            deviceToken.push(doc.data()['deviceToken']);
          });

          this.setState({users: usersList});
          this.setState({userName: userNameList});
          this.setState({contact: contactList});
          this.setState({email: emailList});
          this.setState({token:deviceToken});

          let r = [];
          for(let i=0;i<this.state.users.length;i++){
            let obj = {};
            obj.id = i+1;
            obj.username = this.state.userName[i];
            obj.contact = this.state.contact[i];
            obj.Email = this.state.email[i];
            
            r.push(obj);
          }
          this.setState({rows:r});
          this.setState({rows_copy:r});
        }
      );
 }


//  mailHandler (e){
//     e.preventDefault();
//     this.history.push({pathname:'/mail',
//       state: { studId: 'abc', studFirstName: 'Vikas' },
//  });
//  }


  render(){
    
    return (
        <div style={{ height: 600, width: '100%' }}>
            <center>
                <h2>Admin Portal</h2>
                <br></br>
                
            </center>
          <DataGrid
            rows={this.state.rows}
            columns={this.state.columns}
            pageSize={15}
            rowsPerPageOptions={[5]}
            checkboxSelection
            // onRowSelected={(x) => console.log("x.api.current.getSelectedRows()")}
            onSelectionModelChange={(ids) => {
              //console.log(itm)
              const selectedIDs = new Set(ids);
              // const selectedRows1=(this.state.rows.filter((row)=>{
              //   selectedIDs.has(row.id);
              // }))
              const selectedRows1 = [];
              selectedIDs.forEach((x)=>{
                const ss1 = this.state.rows_copy[x-1];
                
                ss1.token = this.state.token[x-1];
                console.log("Rows_Copy=>",ss1);
                selectedRows1.push(ss1);
              })
              this.setState({selectedRows:selectedRows1});
              // console.log(this.state.selectedRows.length);
              // console.log(this.state.rows[0]);
              console.log("popps",JSON.stringify(this.state.selectedRows));
              console.log("Rows"+this.state.selectedRows);
            }}
          />
        
          <div className='links'>
          <Link
            to={{
                pathname: "/Notification/mail",
                state: this.state.selectedRows // your data array of objects
            }}
            > <button onClick={this.save}> Send Mail</button></Link>
          
            <br></br>
            <Link
            to={{
                pathname: "/Notification/whatsapp",
                state: this.state.selectedRows // your data array of objects
            }}
            
            >Send WhatsApp</Link>
            <br></br>
            <Link
            to={{
                pathname: "/Notification/PushNotification",
                state: this.state.selectedRows // your data array of objects
            }}
            >Send Push Notification</Link>
            <br></br>
            <Link
            to={{
                pathname: "/Notification/Settings",
                state: this.state.selectedRows // your data array of objects
            }}
            >Settings</Link>
            </div>
        
        </div>
      );
  }
}
export default AdminPortal;