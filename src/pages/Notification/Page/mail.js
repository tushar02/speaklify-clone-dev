import { render } from '@testing-library/react';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import MailChange from './mailchange';
import Cookies from 'universal-cookie';
function Mail (){
  const [choice,setChoice] = useState("");  
  const location = useLocation();
  const [passedValues,setPassedValues] = useState(location.state);
  console.log("location",location.state)
    //(event)=>setChoice(event.target.value);
    useEffect(()=>{
      const cookies = new Cookies();
      console.log(cookies.get('data'));
      const data = cookies.get('data');
      setPassedValues(data);
    },[])
    return(
      <div className="Mail">
          <center>
              <h2>Select Service Provider</h2>
              <div className='Radio-btn-coll' onChange={(event)=>{setChoice(event.target.value)}}>
                  <input type="radio" value="Provider1" name="provider" /> Gmail
                  <input type="radio" value="Provider2" name="provider" /> Outlook
                  <input type="radio" value="Provider3" name="provider" /> SendGrid
              </div>
              <MailChange choice={choice} values={passedValues}/>
                <h1>{location.state}</h1>
          </center>
      </div>
    );
  };

export default Mail;
  
  