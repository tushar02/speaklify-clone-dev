import React, { useState } from 'react';
import WhatsappChangeForm from './whatsappchange';
import {useLocation} from 'react-router-dom';
import SettingsChange from './settingschange';


function Settings (){
    const [choice,setChoice] = useState("");  
    const location = useLocation();
    const [passedValues,setPassedValues] = useState(location.state);
      //(event)=>setChoice(event.target.value);
      
      return(
        <div className="Settings">
            <center>
              <h3>Settings Page!!</h3>
              <br></br>
            <div className='Radio-btn-coll' onChange={(event)=>{setChoice(event.target.value)}}>
                  <input type="radio" value="Provider1" name="provider" /> Email
                  <input type="radio" value="Provider2" name="provider" /> WhatsApp
                  
              </div>
              <SettingsChange choice={choice}/>
            </center>
        </div>
      );
    };

export default Settings;