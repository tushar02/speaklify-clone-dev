import React, { useState } from 'react';
import {useLocation} from 'react-router-dom';
import { initializeApp } from 'firebase/app';
import { getFunctions,httpsCallable } from 'firebase/functions';
import { getDocs, getFirestore } from 'firebase/firestore';
import { collection, query, where, doc, setDoc } from "firebase/firestore";
import axios from 'axios';
import { async } from '@firebase/util';

const app = initializeApp({
  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM"
});
 const functions = getFunctions(app);
 const db = getFirestore(app);

function Pushnoti  (props){
   const location = useLocation();
   const [title, setTitle] = useState("");
   const [body, setBody] = useState("");
   const [passedValues,setPassedValues] = useState(location.state); 

   const handleSubmit = (event) => {
    event.preventDefault();
    passedValues.forEach(element => {
      axios.post('https://us-central1-ispeak-5e601.cloudfunctions.net/pushNotification', {
      token: element['token'],//"dV9ujcQZRx2XeCi1qgUVUt:APA91bGg3h2uNFlPCHO0IS5sztnnqHP9mM0Zl07pW9m6Ap3_4HPD4nzL4ePP6030rAsBJ7ZlSWFUJ34Dj_T-D09eSwHAKEkkI6ckS2pQFf2MwxATdwM3d98XEFf163Snz2IyLs1oreuw",
      title: title,
      body: body
    })
    .then(async function (response) {
      console.log(response);
      //adding in firebase log
      const UsersRef = collection(db, "Users");
      const q = query(UsersRef, where("deviceToken", "==", element['token']));
      const querysnap = await getDocs(q);
      querysnap.forEach(async(doci)=> {
        //write 
        var currentTimeInSeconds=Date().toLocaleString();
        await setDoc(doc(db, "Users", doci.id,"notificationLog",currentTimeInSeconds), {
          title: title,
          body: body,
          status: "Sent",
        });
      })
    })
    .catch(async function (error) {
      console.log(error);
      const UsersRef = collection(db, "Users");
      const q = query(UsersRef, where("deviceToken", "==", element['token']));
      const querysnap = await getDocs(q);
      querysnap.forEach(async(doci)=> {
        //write 
        var currentTimeInSeconds=Date().toLocaleString();
        await setDoc(doc(db, "Users", doci.id,"notificationLog",currentTimeInSeconds), {
          title: title,
          body: body,
          status: "Failed!",
        });
      })
    });
    });
    
  //   const addMessage = httpsCallable(functions, 'pushNotification');
  //   addMessage({ token: "dV9ujcQZRx2XeCi1qgUVUt:APA91bGg3h2uNFlPCHO0IS5sztnnqHP9mM0Zl07pW9m6Ap3_4HPD4nzL4ePP6030rAsBJ7ZlSWFUJ34Dj_T-D09eSwHAKEkkI6ckS2pQFf2MwxATdwM3d98XEFf163Snz2IyLs1oreuw",
  //   title: "From App",
  //   body: "I said HEELLOOOOOOOOOO!!", })
  // .then((result) => {
  //   // Read result of the Cloud Function.
  //   /** @type {any} */
  //   const data = result.data;
  //   alert("you are ", data);
  // });
  }
      return(
        <div className="Push">
            <center>
                <h2>Send Push Notification!!</h2>
                {console.log("location.state",passedValues)}
                <br></br>
                <form onSubmit={handleSubmit}>
                  <label>
                    Enter Title: 
                    <input type={'text'} value={title} onChange={(e)=>setTitle(e.target.value)}/>
                  </label>
                  <br></br>
                  <br></br>
                  <label>
                    Enter body: 
                    <input type={'text'} value={body} onChange={(e)=>setBody(e.target.value)}/>
                  </label>
                  <input type="submit" />
                </form>
            </center>
        </div>
      );
    };

export default Pushnoti;
  