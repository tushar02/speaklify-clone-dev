import React, { useState, useEffect } from 'react';
import { db } from "../../firebase/Firebase-config";
import { getDoc, doc, updateDoc , arrayRemove ,arrayUnion} from 'firebase/firestore';
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
function SubscriptionTable() {
  const [PlanTypeFromUser , setPlanTypeFromUser] = useState('');
  const [OLDPlanTypeFromUser , setOLDPlanTypeFromUser] = useState('');
  const [Reset, setReset] = useState(false);
  const [Open  , setOpen] = useState(false);
  const [subsArray , setSubsArray] = useState([]);
  const [subsArrayReal , setSubsArrayReal] = useState([]);
  const [PlanDiscount , setPlanDiscount] = useState('');
  const [PlanPrize , setPlanPrize] = useState('');
  const [Plan , setPlan] = useState('')
  const [OLDPlanDiscount , setOLDPlanDiscount] = useState('');
  const [OLDPlanPrize , setOLDPlanPrize] = useState('');
  const [OLDPlan , setOLDPlan] = useState('')
  const [position , setposition] = useState(0);
  var PlanType = [
    {
      value : 0,
      label : "1 month"
    },
    {
      value : 1,
      label : "3 month"
    },
    {
      value : 2,
      label : "6 month"
    },
    {
        value : 3,
        label : "1 year"
    },
    {
     value : 4,
     label : "LifeTime"
   }
  ];
  useEffect(() => { 
    const getUsers = async () => {
      const data = doc(db, `Subscription/subscriptionInfo`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
    setSubsArray(docSnap1.data().subscription);
    setSubsArrayReal(docSnap1.data().subscription);
      } else {
        console.log("No such document!");
      }
    };
    getUsers();
  }, [Reset,Open]); 
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = (i) => {
        setPlanDiscount(subsArray[i].planDiscount);
        setPlanPrize(subsArray[i].planPrize);
        let PLAN = "";
        if(subsArray[i].planDurationEnum=="1 month"){
          PLAN =0;
        }else if(subsArray[i].planDurationEnum=="3 month"){
          PLAN = 1;
        }else if(subsArray[i].planDurationEnum=="6 month"){
          PLAN =2;
        }else if(subsArray[i].planDurationEnum=="1 year"){
          PLAN =3;
        } else if(subsArray[i].planDurationEnum=="LifeTime"){
          PLAN =4;
        }
        setPlanTypeFromUser(PLAN);
        setPlan(subsArray[i].planType);
        setOpen(true);
        setOLDPlan(subsArray[i].planType);
        setOLDPlanTypeFromUser(PLAN);
        setOLDPlanDiscount(subsArray[i].planDiscount);
        setOLDPlanPrize(subsArray[i].planPrize);
        setposition(i);
  };
  const setPlanType = (e)=>{
    setPlanTypeFromUser(e.value);
     }
     function submit(e){
        e.preventDefault();
      }
  const updateDataOnFirebase=async()=>{
     let PlanDiscountAsInt = parseInt(PlanDiscount, 10);
    let PlanPrizeAsInt = parseInt(PlanPrize, 10);
      const docData ={
        planDiscount:PlanDiscountAsInt,
        planDurationEnum:PlanTypeFromUser,
        planPrize:PlanPrizeAsInt,
        planType:Plan
      }
      let Array =  subsArrayReal;
      Array[position] = docData
    const Ref = doc(db, 'Subscription', 'subscriptionInfo');
    await updateDoc(Ref, {subscription: Array});
      alert("SubsCription updated succefully");
      setOpen(false);
  }
  const deleteDetails=async(i)=>{
    if (window.confirm("Are you sure")) {    
    let PlanDiscountAsInt = parseInt(subsArray[i].planDiscount, 10);
    let PlanPrizeAsInt = parseInt(subsArray[i].planPrize, 10);
    let PLAN = "";
    if(subsArray[i].planDurationEnum=="1 month"){
      PLAN =0;
    }else if(subsArray[i].planDurationEnum=="3 month"){
      PLAN = 1;
    }else if(subsArray[i].planDurationEnum=="6 month"){
      PLAN =2;
    }else if(subsArray[i].planDurationEnum=="1 year"){
      PLAN =3;
    } else if(subsArray[i].planDurationEnum=="LifeTime"){
      PLAN =4;
    }
      const docData ={
        planDiscount:PlanDiscountAsInt,
        planDurationEnum:PLAN,
        planPrize:PlanPrizeAsInt,
        planType:subsArray[i].planType
      }
    const Ref = doc(db, 'Subscription', 'subscriptionInfo');
    await updateDoc(Ref, {subscription: arrayRemove(docData)})
    setReset(!Reset);
    alert("SubsCription Removed Succesfully");
    }
  }
  
  return (
    <div className="App">
      <h2>Subscription Table Data</h2>
      <table className="content-table" style={{ borderCollapse: "collapse", width: "100%", border: "1px solid" }}>
        <thead>
          <tr style={{  width: "10%" }}>
            <th style={{  width: "5%" }}>S No.</th>
            <th style={{  width: "15%" }}>Plan Discount</th>
            <th style={{  width: "20%" }}>Plan Prize</th>
            <th style={{  width: "30%" }}>Plan Type</th>
            <th style={{  width: "20%" }}>Plan Duration</th>
            <th style={{  width: "80%" }}>Edit/Delete</th>
          </tr>
        </thead>
        <tbody>
        {subsArray.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {subsArray.map((details, i) => {
            if(details.planDurationEnum==0){
              details.planDurationEnum="1 month";
            }else if(details.planDurationEnum==1){
              details.planDurationEnum= "3 month";
            }else if(details.planDurationEnum==2){
              details.planDurationEnum="6 month";
            }else if(details.planDurationEnum==3){
              details.planDurationEnum="1 year";
            } else if(details.planDurationEnum==4){
              details.planDurationEnum="LifeTime";
            }
            return (
              <tr key={i} style={{  textAlign: "left", padding: "8px" }}>
                <td style={{  textAlign: "left", height: "5px", padding: "8px" }} >{i + 1}</td>
                <td style={{  textAlign: "left", padding: "8px" }} >{details.planDiscount}</td>
                <td style={{  textAlign: "left", padding: "8px" }} >{details.planPrize}</td>
                <td style={{  textAlign: "left", padding: "8px" }} >{details.planType}</td>
                <td style={{  textAlign: "left", padding: "8px" }} >{details.planDurationEnum}</td>
               <td>
               <button className='edit' onClick={()=>handleOpen(i)}>Edit</button>
               <button className='delete' onClick={()=>deleteDetails(i)}>Delete</button>
               </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Modal onClose={handleClose} open={Open} style={{overflow: 'scroll' ,  color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '90%', width: '70%' }/* {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */} >
        <div>
          <h3 style={{ textAlign: 'center' }}>Update Form</h3>
          <form onSubmit={submit} style={{ marginLeft: '10%' }}>
          <label>Plan Duration : </label>
    <Select value={PlanType.filter(function(option) {return option.value === PlanTypeFromUser;})} options={PlanType} onChange={setPlanType} ></Select>
   <br /><br />
   <label>Plan Discount : </label>
   <input value={PlanDiscount} type="number" placeholder='Plan Discount' onChange={(event) => { setPlanDiscount(event.target.value); }} />
      <br /><br />
   <label>Plan Prize : </label>
   <input type="number" value={PlanPrize} placeholder='Plan Prize' onChange={(event) => { setPlanPrize(event.target.value); }} />
      <br /><br />
   <label>Plan Type : </label>
   <input type="text" value={Plan} placeholder='Plan Type' onChange={(event) => { setPlan(event.target.value); }} />
      <br /><br />
            <button type="submit" onClick={updateDataOnFirebase} >Submit</button>&emsp;&emsp;
            <button onClick={handleClose}  >Close Form</button>
          </form>
        </div>
      </Modal>
    </div>
  );
}
export default SubscriptionTable;