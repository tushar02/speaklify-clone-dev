import React, { useState } from 'react'
import SubscriptionTable from './SubscriptionTable';
import SubsCriptionFrom from './SubsCriptionForm';
import './subscription.css'

function Animation() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add New SubsCription");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Subscription");
    }else{
      setvalue("Add new Subscription");
    }
}
  return (
      <>
      <button className="addButton" onClick={change}>{value}</button>
        {Reset==false?<SubscriptionTable/>:<SubsCriptionFrom/>}
      </>
  )
}

export default Animation;