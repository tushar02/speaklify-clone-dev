import React, { useState } from 'react'
import Select from "react-select";
import {db } from '../../firebase/Firebase-config';
import { doc,updateDoc,arrayUnion } from 'firebase/firestore';
function SubsCriptionFrom() {
    const [PlanTypeFromUser , setPlanTypeFromUser] = useState('');
   const [PlanDiscount , setPlanDiscount] = useState('');
   const [PlanPrize , setPlanPrize] = useState('');
   const [Plan , setPlan] = useState('')

    var PlanType = [
        {
          value : 0,
          label : "1 month"
        },
        {
          value : 1,
          label : "3 month"
        },
        {
          value : 2,
          label : "6 month"
        },
        {
            value : 3,
            label : "1 year"
        },
        {
         value : 4,
         label : "LifeTime"
       }
      ];
          const UploadOnFirebase =async()=>{
            let PlanDiscountAsInt = parseInt(PlanDiscount, 10);
            let PlanPrizeAsInt = parseInt(PlanPrize, 10);
              const docData ={
                planDiscount:PlanDiscountAsInt,
                planDurationEnum:PlanTypeFromUser,
                planPrize:PlanPrizeAsInt,
                planType:Plan
              }
            const Ref = doc(db, 'Subscription', 'subscriptionInfo');
            await updateDoc(Ref, {subscription: arrayUnion(docData)});
            alert("SubsCription Added Succesfully");
            document.getElementById("Course-form").reset();
            setPlanTypeFromUser('');
           } 
      const setPlanType = (e)=>{
        setPlanTypeFromUser(e.value);
         }
         function submit(e){
            e.preventDefault();
          }
  return (
    <>
      <h1 style={{ textAlign: 'center' }}>Add Subscription</h1>
      <form id="Course-form" onSubmit={submit}>
    <label>Plan Duration : </label>
    <Select value={PlanType.filter(function(option) {return option.value === PlanTypeFromUser;})} options={PlanType} onChange={setPlanType} ></Select>
   <br /><br />
   <label>Plan Discount : </label>
   <input type="number" placeholder='Plan Discount' onChange={(event) => { setPlanDiscount(event.target.value); }} />
      <br /><br />
   <label>Plan Prize : </label>
   <input type="number" placeholder='Plan Prize' onChange={(event) => { setPlanPrize(event.target.value); }} />
      <br /><br />
   <label>Plan Type : </label>
   <input type="text" placeholder='Plan Type' onChange={(event) => { setPlan(event.target.value); }} />
      <br /><br />
    <button onClick={UploadOnFirebase}>Upload</button>
</form>
    </>
  )
}

export default SubsCriptionFrom