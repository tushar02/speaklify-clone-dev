import React, { useState } from 'react'
import QuestionPoolForm from './QuestionPoolForm';
import QuestionPoolTable from './QuestionPoolTable';
import './pool.css'

function QuestionPool() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new QuestionPool");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to QuestionPool");
    }else{
     
      setvalue("Add new QuestionPool");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<QuestionPoolTable/>:<QuestionPoolForm/>}
      </>
  )
}

export default QuestionPool;