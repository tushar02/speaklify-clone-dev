import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import './quiz.css'
import { Button, Dialog, DialogTitle, TextField } from '@mui/material';
import { DialogActions } from '@material-ui/core';
import './pool.css'
import Select from "react-select";
import { async } from '@firebase/util';
function QuestionPoolTable() {
  const [QuestionPoolDetails, setQuestionPoolDetails] = useState([]);
  const [QuestionPoolDetailsCopy, setQuestionPoolDetailsCopy] = useState([]);
  const [rootvalue , setrootvalue] = useState('');
  const [rootName  , setrootName] = useState([]);
  const [lesson , setLesson]= useState([]);
  const [topic , setTopic]= useState([]);
  const [quiz , setQuiz]= useState([]);
  const [userrootname , setuserrootname] = useState('');
  const [lessonUser , setLessonUser] = useState('');
  const [quizUser , setQuizUser] = useState('');
  const [topicUser , setTopicUser] = useState('');
  const [Users1, setUsers1] = useState('');
  const [Reset, setReset] = useState(false);
  const [Open, setOpen] = useState(false);
  const [NoofquestionsTypeWhatYouHear, setNoofquestionsTypeWhatYouHear] = useState(0);
  const [NoofquestionsChooseTheCorrectOption, setNoofquestionsChooseTheCorrectOption] = useState(0);
  const [NoofquestionsReadListenSpeak, setNoofquestionsReadListenSpeak] = useState(0);
  const [NoofquestionsFillInTheSequence, setNoofquestionsFillInTheSequence] = useState(0);
  const [NoofquestionsIdentifyTheImage, setNoofquestionsIdentifyTheImage] = useState(0);
  const [NoofquestionsChooseTheCorrectImageOfTheGivenWord, setNoofquestionsChooseTheCorrectImageOfTheGivenWord] = useState(0);
  const [NoofquestionsFillInTheBlankType, setNoofquestionsFillInTheBlankType] = useState(0);
  const [NoofquestionsPairingWordsToWords, setNoofquestionsPairingWordsToWords] = useState(0);
  const [NoofquestionsPairingWordToImages, setNoofquestionsPairingWordToImages] = useState(0);
  const [NoofquestionsTapWhatYouHear, setNoofquestionsTapWhatYouHear] = useState(0);
  const [index, setindex] = useState('');
  const [validateReport, setvalidateReport] = useState(true);
  const [expectedNumberOfChallenge, setexpectedNumberOfChallenge] = useState(0);
  const [ExpectedNoofquestionsTypeWhatYouHear, setExpectedNoofquestionsTypeWhatYouHear] = useState(0);
  const [ExpectedNoofquestionsChooseTheCorrectOption, setExpectedNoofquestionsChooseTheCorrectOption] = useState(0);
  const [ExpectedNoofquestionsReadListenSpeak, setExpectedNoofquestionsReadListenSpeak] = useState(0);
  const [ExpectedNoofquestionsFillInTheSequence, setExpectedNoofquestionsFillInTheSequence] = useState(0);
  const [ExpectedNoofquestionsIdentifyTheImage, setExpectedNoofquestionsIdentifyTheImage] = useState(0);
  const [ExpectedNoofquestionsChooseTheCorrectImageOfTheGivenWord, setExpectedNoofquestionsChooseTheCorrectImageOfTheGivenWord] = useState(0);
  const [ExpectedNoofquestionsFillInTheBlankType, setExpectedNoofquestionsFillInTheBlankType] = useState(0);
  const [ExpectedNoofquestionsPairingWordsToWords, setExpectedNoofquestionsPairingWordsToWords] = useState(0);
  const [ExpectedNoofquestionsPairingWordToImages, setExpectedNoofquestionsPairingWordToImages] = useState(0);
  const [ExpectedNoofquestionsTapWhatYouHear, setExpectedNoofquestionsTapWhatYouHear] = useState(0);
  const usercollectionRef = collection(db, "QuestionPool")
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setQuestionPoolDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setQuestionPoolDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset]);
  const handleClose = () => {
    setOpen(false);
    setvalidateReport(true);
  };
  function submit(e) {
    e.preventDefault();
  }
  var root = [
    {
      value : 'Courses',
      label : "General Course"
    },
    {
        value : 'Grammar',
        label : 'Grammar'
    },
    {
     value : 'Practice',
     label : "Practice"
   },
  {
   value : 'Test',
   label : "Test"
  },
  {
   value : 'Story',
   label : "Story"
  }
  ];
  const ValidateDataOnFirebase = async () => {
    console.log(QuestionPoolDetails[index]);
    const data = doc(db, `Quizs/${QuestionPoolDetails[index].associatedQuizId}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      let expectedQuestion = (QuestionPoolDetails[index].questionsId.length) / docSnap.data().quizQuestionCount;
      let floorexpectedQuestion = Math.ceil(expectedQuestion);
      console.log(docSnap.data().questionsForTypeWhatYouHear!=undefined)
      setvalidateReport(false);
      setexpectedNumberOfChallenge(expectedQuestion);
    /*   if(docSnap.data().questionsForTypeWhatYouHear!=undefined){
      setExpectedNoofquestionsTypeWhatYouHear(docSnap.data().questionsForTypeWhatYouHear*floorexpectedQuestion);
      setExpectedNoofquestionsChooseTheCorrectOption(docSnap.data().questionsForMcq*floorexpectedQuestion);
      setExpectedNoofquestionsReadListenSpeak(docSnap.data().questionsForReadListenSpeak*floorexpectedQuestion);
      setExpectedNoofquestionsFillInTheSequence(docSnap.data().questionsForFillTheSequence*floorexpectedQuestion);
      setExpectedNoofquestionsIdentifyTheImage(docSnap.data().questionsForIdentifyTheImage*floorexpectedQuestion);
      setExpectedNoofquestionsChooseTheCorrectImageOfTheGivenWord(docSnap.data().questionsForCorrectImageFromWord*floorexpectedQuestion);
      setExpectedNoofquestionsFillInTheBlankType(docSnap.data().questionsForFillTheBlanksType*floorexpectedQuestion);
      setExpectedNoofquestionsPairingWordsToWords(docSnap.data().questionsForWordsToWords*floorexpectedQuestion);
      setExpectedNoofquestionsPairingWordToImages(docSnap.data().questionsForWordsToImage*floorexpectedQuestion);
      setExpectedNoofquestionsTapWhatYouHear(docSnap.data().questionsForTapWhatYouHear*floorexpectedQuestion);
      } */
    } else {
      console.log("No such document!");
    }
  }
  const handleOpen = async (i) => {
    setOpen(true);
    setNoofquestionsTypeWhatYouHear(QuestionPoolDetails[i].availQuestionsForTypeWhatYouHear);
    setNoofquestionsChooseTheCorrectOption(QuestionPoolDetails[i].availQuestionsForMcq);
    setNoofquestionsReadListenSpeak(QuestionPoolDetails[i].availQuestionsForReadListenSpeak);
    setNoofquestionsFillInTheSequence(QuestionPoolDetails[i].availQuestionsForFillTheSequence);
    setNoofquestionsIdentifyTheImage(QuestionPoolDetails[i].availQuestionsForIdentifyTheImage);
    setNoofquestionsChooseTheCorrectImageOfTheGivenWord(QuestionPoolDetails[i].availQuestionsForCorrectImageFromWord);
    setNoofquestionsFillInTheBlankType(QuestionPoolDetails[i].availQuestionsForFillTheBlanksType);
    setNoofquestionsPairingWordsToWords(QuestionPoolDetails[i].availQuestionsForWordsToWords);
    setNoofquestionsPairingWordToImages(QuestionPoolDetails[i].availQuestionsForWordsToImage);
    setNoofquestionsTapWhatYouHear(QuestionPoolDetails[i].availQuestionsForTapWhatYouHear);
    setindex(i);
    const data = doc(db, `Quizs/${QuestionPoolDetails[i].associatedQuizId}`);
    const docSnap = await getDoc(data);






    if (docSnap.exists()) {

    
       var expectedQuestion = (QuestionPoolDetails[i].questionsId.length) / docSnap.data().quizQuestionCount;
    console.log("Rahul"+ expectedQuestion)

     
      if(docSnap.data().quizQuestionCount==0){
        expectedQuestion=0;
      }else{
        expectedQuestion = (QuestionPoolDetails[i].questionsId.length) / docSnap.data().quizQuestionCount;
      }


      let floorexpectedQuestion = Math.ceil(expectedQuestion);
      console.log(floorexpectedQuestion+"->"+expectedQuestion)
      if(docSnap.data().questionsForTypeWhatYouHear!=undefined){
      setExpectedNoofquestionsTypeWhatYouHear(docSnap.data().questionsForTypeWhatYouHear*floorexpectedQuestion);
      setExpectedNoofquestionsChooseTheCorrectOption(docSnap.data().questionsForMcq*floorexpectedQuestion);
      setExpectedNoofquestionsReadListenSpeak(docSnap.data().questionsForReadListenSpeak*floorexpectedQuestion);
      setExpectedNoofquestionsFillInTheSequence(docSnap.data().questionsForFillTheSequence*floorexpectedQuestion);
      setExpectedNoofquestionsIdentifyTheImage(docSnap.data().questionsForIdentifyTheImage*floorexpectedQuestion);
      setExpectedNoofquestionsChooseTheCorrectImageOfTheGivenWord(docSnap.data().questionsForCorrectImageFromWord*floorexpectedQuestion);
      setExpectedNoofquestionsFillInTheBlankType(docSnap.data().questionsForFillTheBlanksType*floorexpectedQuestion);
      setExpectedNoofquestionsPairingWordsToWords(docSnap.data().questionsForWordsToWords*floorexpectedQuestion);
      setExpectedNoofquestionsPairingWordToImages(docSnap.data().questionsForWordsToImage*floorexpectedQuestion);
      setExpectedNoofquestionsTapWhatYouHear(docSnap.data().questionsForTapWhatYouHear*floorexpectedQuestion);
      }
    } else {
      console.log("No such document!");
    }
  };
  async function deleteImageOfQuestion(QuestionType, imageLink) {
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    if (QuestionType == 6) {
      imageUrl1 = imageLink;
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 2) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      imageUrl4 = imageLink[3];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 0) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
    }
  }
  async function deleteQuestionArray(Question) {
    const data = doc(db, `Questions/${Question}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setUsers1(docSnap.data());
      console.log("Document data:", docSnap.data().questionType);
      if (docSnap.data().questionType == 6) {
        deleteImageOfQuestion(docSnap.data().questionType, docSnap.data().questionData.imageLink);
      } else if (docSnap.data().questionType == 2) {
        let imageArray = [docSnap.data().questionData.imageOptions[0], docSnap.data().questionData.imageOptions[1], docSnap.data().questionData.imageOptions[2], docSnap.data().questionData.imageOptions[3]];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      } else if (docSnap.data().questionType == 0) {
        let imageArray = [docSnap.data().questionData.imageoptions[0].imageLink, docSnap.data().questionData.imageoptions[1].imageLink, docSnap.data().questionData.imageoptions[2].imageLink];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      }
      await deleteDoc(doc(db, "Questions", Question));
    } else {
      console.log("No such document!");
    }

  }
  async function deleteQuestionPool(i) {
    if (window.confirm("Are you sure")) {    
    let QuestionQuizid = QuestionPoolDetails[i].associatedQuiz;
    let QuestionArray = QuestionPoolDetails[i].questionsId;
    QuestionArray.map((Question) => {
      deleteQuestionArray(Question);
    });
    await deleteDoc(doc(db, "QuestionPool", QuestionPoolDetails[i].questionPoolId));
    const QuizsRef = doc(db, "Quizs", QuestionQuizid);
    await updateDoc(QuizsRef, { quizQuestionPool: "" });
    setReset(!Reset);
    alert("Deleted data successfully");
  }
  }

  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setQuestionPoolDetails([]);
      for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
          if (QuestionPoolDetailsCopy[i]["associatedRootType"]=='Courses') {
            const query = QuestionPoolDetailsCopy[i];
            setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
      if (QuestionPoolDetailsCopy[i]["associatedRootType"]=='Practice'  ) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) => QuestionPoolDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
      if (QuestionPoolDetailsCopy[i]["associatedRootType"]=='Grammar' ) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
      if (QuestionPoolDetailsCopy[i]["associatedRootType"]=='video') {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
      if (QuestionPoolDetailsCopy[i]["associatedRootType"]=="Audio") {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
    if (QuestionPoolDetailsCopy[i]["associatedRootType"]=='Story') {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const getlessonType = async (value) => {
  setLesson([]);  
  const data =   doc(db,   `${rootvalue}/${value}/extraInfo/infoDoc`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
   if(docSnap.data().courseFinalArcade!=undefined){
    if(docSnap.data().courseFinalArcade!=""){
    setLesson([{value : docSnap.data().courseFinalArcade, label:docSnap.data().courseFinalArcade}]);
    }
  }
     console.log(docSnap.data().courseLessons.map((user) => { 
        const query = {value : user, label:user}
     setLesson(lesson=> lesson.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const getTopicType = async (value) => {
  setTopic([]);
  const data = doc(db, `Lessons/${value}`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
     console.log(docSnap.data().lessonTopics.map((user) => { 
        const query = {value : user, label:user}
     setTopic(topic=> topic.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){
    if (QuestionPoolDetailsCopy[i]["associatedRoot"]==value) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
  getlessonType(e.value);
};
const lessonnamefromuser =(e)=>{
  setLessonUser(e.value);
  let lessonValue = e.value;
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){   
    if (QuestionPoolDetailsCopy[i]["associatedLesson"]==lessonValue) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
  getTopicType(lessonValue);
}
const topicnamefromuser =(e)=>{
  setTopicUser(e.value);
  let lessonValue = e.value;
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){   
    if (QuestionPoolDetailsCopy[i]["associatedTopic"]==lessonValue) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
}
const quiznamefromuser =(e)=>{
  setQuizUser(e.value);
  let quizValue = e.value;
  setQuestionPoolDetails([]);
  for(var i =0;i<QuestionPoolDetailsCopy.length;i++){   
    if (QuestionPoolDetailsCopy[i]["associatedQuiz"]==quizValue) {
        const query = QuestionPoolDetailsCopy[i];
        setQuestionPoolDetails((QuestionPoolDetails) =>  QuestionPoolDetails.concat(query));
    }
  }
}
const change=()=>{
     setQuestionPoolDetails(QuestionPoolDetailsCopy);
     setrootvalue('');
     setuserrootname('');
     setLessonUser('');
     setTopicUser('');
}
const getQuizdata= async ()=>{
  setQuiz([]);
  if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test' ||rootvalue=='Practice'){
      if(topicUser!=''||rootvalue=='Practice'){
          const data =   doc(db,   `Topics/${topicUser}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);

          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().topicQuiz);
            console.log(docSnap.data().topicQuiz.map((user) => { 
              const query = {value : user, label:user}
           setQuiz(quiz => quiz.concat(query))}));
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
      }else if(topicUser==''&& lessonUser==''){
          const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().courseFinalQuiz);
         //   setquizName(users3.lessonQuiz);
         //setquizName(users3.courseFinalQuiz);
         const query = {value : docSnap.data().courseFinalQuiz, label:docSnap.data().courseFinalQuiz}
      setQuiz(quiz => quiz.concat(query));   
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }       
       }else if(topicUser=='' && lessonUser!=''){
          const data =   doc(db,   `Lessons/${lessonUser}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            console.log("Document data:", docSnap.data().lessonQuiz);
           // setquizName(users3.lessonQuiz);
           console.log(docSnap.data().lessonQuiz.map((user) => { 
              const query = {value : user, label:user}
           setQuiz(quiz => quiz.concat(query))})); 
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
       }
}
}
/* const quizsoln = ()=>{
   setquizName1([])
   if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test'||rootvalue=='Practice'){
      if(usertopicname!=''||rootvalue=='Practice'){
          setquizName(users3.topicQuiz);
          console.log(quizName.map((user) => { 
             const query = {value : user, label:user}
          setquizName1(quizName1 => quizName1.concat(query))}));
      }else if(usertopicname=='' && userlessonname!=''){
             setquizName(users3.lessonQuiz);
             console.log(quizName.map((user) => { 
                const query = {value : user, label:user}
             setquizName1(quizName1 => quizName1.concat(query))})); 
      }else if(usertopicname==''&& userlessonname==''){
          setquizName(users3.courseFinalQuiz);
             const query = {value : quizName, label:quizName}
          setquizName1(quizName1 => quizName1.concat(query));        
}
}
} */
const QuestionCountUpdate = async()=>{
  console.log(QuestionPoolDetails[index])
  const data = doc(db, `QuestionPool/${QuestionPoolDetails[index].id}`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data().questionsId);
    var questionArrayId = docSnap.data().questionsId;
    var ques0 = 0;
    var ques1 = 0;
    var ques2 = 0;
    var ques3 = 0;
    var ques4 = 0;
    var ques5 = 0;
    var ques6 = 0;
    var ques7 = 0;
    var ques8 = 0;
    var ques9 = 0;
    var ques10 = 0;
    let promise = []
    for(var i =0;i<questionArrayId.length;i++){
      const data = doc(db, `Questions/${questionArrayId[i]}`);
   promise.push(await getDoc(data).then((response)=>{
          console.log(response.data());
          if(response.data().questionType==0){
         
            ques0++;
           }else if(response.data().questionType==1){
          
            ques1++;
           }else if(response.data().questionType==2){
          
            ques2++;
           }else if(response.data().questionType==3){
         
            ques3++;
           }else if(response.data().questionType==4){
           
            ques4++;
           }else if(response.data().questionType==5){
          
            ques5++;
           }else if(response.data().questionType==6){
          
            ques6++;
           }else if(response.data().questionType==7){
       
            ques7++;
           }else if(response.data().questionType==8){
         
            ques8++;
           }else if(response.data().questionType==10){
           
            ques10++;
           }
      }).catch((e)=>{
        console.log(e)
      })
   );
    }

Promise.allSettled(promise).
then(async(results) =>{
  console.log(results);
  const Ref = doc(db, "QuestionPool", QuestionPoolDetails[index].id);
    await updateDoc(Ref, {
           availQuestionsForCorrectImageFromWord:ques2,
            availQuestionsForFillTheBlanksOptions:0, 
             availQuestionsForFillTheBlanksType:ques8,
              availQuestionsForFillTheBlanksVoice:0,
                availQuestionsForFillTheSequence:ques1,
                  availQuestionsForIdentifyTheImage:ques6,
                    availQuestionsForMcq:ques5,
                      availQuestionsForReadListenSpeak:ques10,
                        availQuestionsForTapWhatYouHear:ques3,
                          availQuestionsForTypeWhatYouHear:ques4,
                            availQuestionsForWordsToImage:ques0,
                              availQuestionsForWordsToWords:ques7,
                               availQuestionsInPool:ques0+ques1+ques2+ques3+ques4+ques5+ques6+ques7+ques8+ques10
    });
} );
  } else {
    console.log("No such document!");
  }
}
  return (
    <div className="App">
        <label className='label'> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <br /><br />
     <label className='label'> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Lesson Name : </label>
    <Select value={lesson.filter(function(option) {return option.value === lessonUser;})} options={lesson} onChange={lessonnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Topic Name : </label>
    <Select value={topic.filter(function(option) {return option.value === topicUser;})} options={topic} onChange={topicnamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={getQuizdata}>Confirm</button>
    <br />
     <label className='label'> Choose the Quiz Name : </label>
    <Select value={quiz.filter(function(option) {return option.value === quizUser;})} options={quiz} onChange={quiznamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={change}>Reset Filter</button>
      <h2>QuestionPool Data</h2>
      <table className='content-table'>
        <thead>
          <tr style={{ width: "10%" }}>
            <th style={{ width: "5%" }}>S No.</th>
            <th style={{ width: "20%" }}>QuestionPool Name</th>
            <th style={{ width: "20%" }}>Associated Parent</th>
            <th style={{ width: "20%" }}>Associated Lesson objective Arcade</th>
            <th style={{ width: "20%" }}>Associated Topic Round</th>
            <th style={{ width: "20%" }}>Associated Question Pool</th>
          </tr>
        </thead>
        <tbody>
        {QuestionPoolDetails.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {QuestionPoolDetails.map((QuestionPool, i) => {
            return (
              <tr key={i} >
                <td >{i + 1}</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{QuestionPool.questionPoolId}
                  <br /><br />
                  <button className='delete' onClick={() => deleteQuestionPool(i)}>Delete</button>
                  <button className='edit' onClick={() => handleOpen(i)}>View</button>
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{QuestionPool.associatedRoot}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{QuestionPool.associatedLesson}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{QuestionPool.associatedTopic}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{QuestionPool.associatedQuiz}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog onClose={handleClose} open={Open} style={{ padding: "30px" }} PaperProps={{
        style: {
          borderRadius: "20px", padding: "10px"
        }
      }}>
        <DialogTitle sx={{ fontSize: "24", color: "#308efe", fontWeight: '500' }}>QuestionPool view</DialogTitle>
        {/*<Modal onClose={handleClose} open={Open} style={{ color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '90%', width: '60%' } {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */}
        <div>
         {/*  <form onSubmit={submit} style={{ marginLeft: '5%', fontFamily: "sans-serif" }}>
            <label >Actual of questions for Type what you hear : {NoofquestionsTypeWhatYouHear} </label>
            <br /><br /> <label>Actual No of questions for Choose the correct option : {NoofquestionsChooseTheCorrectOption}</label>
            <br /><br /> <label>Actual No of questions for Read Listen Speak : {NoofquestionsReadListenSpeak}</label>
            <br /><br /> <label>Actual No of questions for Fill in the sequence : {NoofquestionsFillInTheSequence}</label>
            <br /><br /> <label>Actual No of questions for Identify the image : {NoofquestionsIdentifyTheImage}</label>
            <br /><br /> <label>Actual No of questions for Choose the correct image of the given word : {NoofquestionsChooseTheCorrectImageOfTheGivenWord}</label>
            <br /><br /> <label>Actual No of questions for Fill in the blank (type) : {NoofquestionsFillInTheBlankType}</label>
            <br /><br /> <label>Actual No of questions for Pairing (words to words) : {NoofquestionsPairingWordsToWords}</label>
            <br /><br /> <label>Actual No of questions for Pairing (word to images) : {NoofquestionsPairingWordToImages}</label>
            <br /><br /> <label>Actual No of questions for Tap what you hear : {NoofquestionsTapWhatYouHear}</label>
          </form> */}
          <table className='content-table'>
        <thead>
          <tr style={{ width: "10%" }}>
            <th style={{ width: "10%" }}>S No.</th>
            <th style={{ width: "90%" }}>Question Type</th>
            <th style={{ width: "100%" }}>Actual No of Questions</th>
            <th style={{ width: "100%" }}>Expected No of Questions</th>
          </tr>
        </thead>
        <tbody>
              <tr >
                <td >1</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Type what you hear
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{NoofquestionsTypeWhatYouHear}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsTypeWhatYouHear}
                </td>
              </tr>
              <tr >
                <td >2</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Choose the correct option
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsChooseTheCorrectOption}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsChooseTheCorrectOption}
                </td>
              </tr>
              <tr >
                <td >3</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Read Listen Speak
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsReadListenSpeak}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}> {ExpectedNoofquestionsReadListenSpeak}
                </td>
              </tr>
              <tr >
                <td >4</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Fill in the sequence
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsFillInTheSequence}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsFillInTheSequence}
                </td>
              </tr>
              <tr >
                <td >5</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Identify the image
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{NoofquestionsIdentifyTheImage}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsIdentifyTheImage}
                </td>
              </tr>
              <tr >
                <td >6</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Choose the correct image of the given word
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{NoofquestionsChooseTheCorrectImageOfTheGivenWord}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsChooseTheCorrectImageOfTheGivenWord}
                </td>
              </tr>
              <tr >
                <td >7</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Fill in the blank (type)
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsFillInTheBlankType}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}> {ExpectedNoofquestionsFillInTheBlankType}
                </td>
              </tr>
              <tr >
                <td >8</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Pairing (words to words)
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsPairingWordsToWords}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}> {ExpectedNoofquestionsPairingWordsToWords}
                </td>
              </tr>
              <tr >
                <td >9</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Pairing (word to images)
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} > {NoofquestionsPairingWordToImages}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsPairingWordToImages}
                </td>
              </tr>
              <tr >
                <td >10</td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>Tap what you hear
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }} >{NoofquestionsTapWhatYouHear}
                </td>
                <td style={{ maxWidth: "190px", wordWrap: "break-word", marginLeft: "10px" }}>{ExpectedNoofquestionsTapWhatYouHear}
                </td>
              </tr>
              
        </tbody>
      </table>
        </div>
        <div style={{ marginLeft: '5%', fontFamily: "sans-serif" }} hidden={validateReport} >
          <br />
          <label>Expected Number Of Challenge : {expectedNumberOfChallenge}</label> <br />
          {expectedNumberOfChallenge % 1 == 0 ? <></> : <>Your App Might be Carshed Because It is not integer</>}
        </div>
        <DialogActions><Button type="submit" onClick={QuestionCountUpdate} >Question Count Update</Button>
       <Button type="submit" onClick={ValidateDataOnFirebase} >Validate</Button>
          <Button onClick={handleClose} sx={{ color: "red" }} >Close</Button>
          
          
          
          </DialogActions>
      </Dialog>
    </div>
  );
}
export default QuestionPoolTable;