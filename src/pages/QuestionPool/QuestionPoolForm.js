import {db} from '../../firebase/Firebase-config';
import { useState  } from 'react';
import {collection, getDocs,setDoc,updateDoc ,doc,getDoc} from 'firebase/firestore';
import Select from "react-select";
import { useEffect } from 'react';
const QuestionPoolForm = () => {
    const [users , setusers] = useState([]);
    const [users1 , setusers1] = useState([]);
    const [users2 , setusers2] = useState([]);
    const [users3 , setusers3] = useState([]);
    const [users4 , setusers4] = useState([]);
    const [hideLessonTopic , sethideLessonTopic] = useState("");
    const [users5 , setusers5] = useState([]);
    const [hideTopic , sethideTopic] = useState("");
    const [rootvalue  , setrootvalue] = useState('');
    const [rootName  , setrootName] = useState([]);
    const [lessonName , setlessonName] = useState([]);
    const [topicName , settopicName] = useState([]);
    const [quizName , setquizName] = useState([]);
    const [poolName , setpoolName] = useState([]);
    const [lessonName1 , setlessonName1] = useState([]);
    const [topicName1 , settopicName1] = useState([]);
    const [quizName1 , setquizName1] = useState([]);
    const [poolName1 , setpoolName1] = useState([]);
    const [userrootname , setuserrootname] = useState('');
    const [userlessonname , setuserlessonname] = useState('');
    const [usertopicname , setusertopicname] = useState('');
    const [userquizname , setuserquizname] = useState('');
    const [quizvalue , setquizvalue] = useState('');
    const [quizvaluecolumn , setquizvaluecolumn] = useState('');
    const [userLessonType , setuserLessonType] = useState('');
    const [userTopicType , setuserTopicType] = useState('');
    const [userquestionPool , setuserquestionPool] = useState('');    
    const [QuestionPoolId , setQuestionPoolId] = useState('');    
    const [QuestionPoolName , setQuestionPoolName] = useState(''); 
       
    var root = [
        {
          value : 'Courses',
          label : "General Course"
        },
        {
            value : 'Grammar',
            label : 'Grammar'
        },
        {
         value : 'Practice',
         label : "Practice"
       },
      {
       value : 'Test',
       label : "Test"
      },
      {
       value : 'Story',
       label : "Story"
      }
      ];
      
      var[ lesson ,setlesson] = useState([
        {
          value : 'Courses',
          label : "Lesson",
         isDisabled : false
        },
        {
         value : "Objective",
         label : "Objective",
         isDisabled : false
       },
       {
         value : "Arcade",
         label : "Arcade",
         isDisabled : false
       }
      ]);
      var [ topic, settopic]= useState([
        {
          value : 'Lessons',
          label : "Topic",
          isDisabled : false
        },
        {
         value : 'Round',
         label : "Round",
         isDisabled : false
       }
      ]);
        
        const getlessonType = async () => {
          const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
          const docSnap = await getDoc(data);
          if (docSnap.exists()) {
            setusers1(docSnap.data());
            console.log("Document data:", docSnap.data());         
          } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
          }
     }
          const getTopicType = async () => {
            const data =   doc(db,   `Lessons/${userlessonname}`);
            const docSnap = await getDoc(data);
  
            if (docSnap.exists()) {
              console.log("Document data:", docSnap.data());
              setusers2(docSnap.data());
            } else {
              console.log("No such document!");
            }
            }
        const confirm = ()=>{
            setrootName([]);
            console.log(users.map((user) => { 
                const query = {value : user.courseId, label:user.courseId}
             setrootName(rootName => rootName.concat(query))}));
             
        };
        useEffect(()=>{
            if(rootvalue=='Courses' ||rootvalue=='Grammar'){
                setlesson((prev)=>{
                    return prev.filter((curvalue , idx)=>{
                        if(idx==1 ||idx==2){
                            curvalue.isDisabled=true;
                        }else{
                            curvalue.isDisabled=false;
                           }
                        return curvalue;
                    })
                }) 
                settopic((prev)=>{
                   return prev.filter((curvalue , idx)=>{
                       if(idx==1){
                           curvalue.isDisabled=true;
                       }else{
                        curvalue.isDisabled=false;
                       }
                       return curvalue;
                   })
               })
           }else if(rootvalue=='Practice'){
            setlesson((prev)=>{
                return prev.filter((curvalue , idx)=>{
                    if(idx==0){
                        curvalue.isDisabled=true;
                    }else{
                        curvalue.isDisabled=false;
                       }
                    return curvalue;
                })
            }) 
            settopic((prev)=>{
               return prev.filter((curvalue , idx)=>{
                   if(idx==0){
                       curvalue.isDisabled=true;
                   }else{
                    curvalue.isDisabled=false;
                   }
                   return curvalue;
               })
           })
           }else if(rootvalue=='Test'||rootvalue=='video'){
            setlesson((prev)=>{
                return prev.filter((curvalue , idx)=>{
                    if(idx==0||idx==1||idx==2){
                        curvalue.isDisabled=true;
                    }else{
                        curvalue.isDisabled=false;
                       }
                    return curvalue;
                })
            }) 
            settopic((prev)=>{
               return prev.filter((curvalue , idx)=>{
                   if(idx==0||idx==1){
                       curvalue.isDisabled=true;
                   }else{
                    curvalue.isDisabled=false;
                   }
                   return curvalue;
               })
           })
           }
           else if(rootvalue=='Story'){
            setlesson((prev)=>{
                return prev.filter((curvalue , idx)=>{
                    if(idx==2||idx==1){
                        curvalue.isDisabled=true;
                    }else{
                        curvalue.isDisabled=false;
                       }
                    return curvalue;
                })
            })
           }
        },[rootvalue])
        useEffect(() => {
            confirm();
          },[users]);
            useEffect(() => {
           getlessonType();
          },[userLessonType]);
          useEffect(() => {
            getlessonName();
           },[users1]);
           useEffect(() => {
            getTopicType();
           },[userTopicType]);
           useEffect(() => {
             getTopicName();
            },[users2]);
            useEffect(() => {
                quizsoln();
               },[users3]);
        const getlessonName = ()=>{
          if(userLessonType=="Arcade"){
            if(users1.courseFinalArcade!=undefined){
            setlessonName1([]);
             setlessonName1([{value : users1.courseFinalArcade, label:users1.courseFinalArcade}]);
            }
          }else{
            setlessonName(users1.courseLessons);
            if(lessonName!=undefined){
            setlessonName1([]);
             console.log(lessonName.map((user) => { 
                const query = {value : user, label:user}
             setlessonName1(lessonName1 => lessonName1.concat(query))}));
            }
          }
        };
        const getTopicName = ()=>{
            settopicName(users2.lessonTopics);
            if(topicName!=undefined){
               settopicName1([]);
               console.log(topicName.map((user) => { 
                  const query = {value : user, label:user}
               settopicName1(topicName1 => topicName1.concat(query))}));
          }
        }
          const getrootName = async() =>{
            const usercollectionRef = collection( db , rootvalue);
            const data = await getDocs(usercollectionRef);
            console.log(data);
            setusers(data.docs.map((doc) => ({...doc.data(),id: doc.id})));
        };
      const rootvaluefromuser = (e)=>{
         setrootvalue(e.value);
         setuserrootname();
         setuserLessonType('');
         setuserlessonname('');
         setusertopicname('');
         setuserquizname('');
         setuserLessonType('');
         setuserTopicType('');
         if(e.value=="Test"){
          sethideLessonTopic("none");
          sethideTopic("none")
        }else if(e.value=='Story'){
         
          sethideLessonTopic("none");
          sethideTopic("none");
        }else{
           sethideLessonTopic(false);
           sethideTopic(false);
        }
        
    }
      const rootnamefromuser = (e)=>{
        setuserrootname(e.value);
        setuserLessonType('');
        setuserlessonname('');
        setusertopicname('');
        setuserquizname('');
        setuserLessonType('');
        setuserTopicType('');

     };
     const setuserlesson = (e)=>{
        setuserlessonname(e.value);
        setusertopicname('');
        setuserquizname('');
        setuserTopicType('');
     };
     const setTopicValue = (e)=>{
         setusertopicname(e.value);
         setuserquizname('');

     }
     const setquizValue = (e)=>{
        setuserquizname(e.value);
        // let date = new Date();
        // let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20);
        // setQuestionPoolId(QuestionPoolName+"_"+ModifiedDate);

        let date = new Date();
        let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '').toLowerCase();
         
        let final = (QuestionPoolName.toLowerCase()).replace(/[^a-z0-9 -]/gi, '');
        let newfinaldata=final.replace(/\s+/, "") 
        setQuestionPoolId(newfinaldata+ModifiedDate)
        //console.log(newfinaldata+ModifiedDate)
    }
     const getQuizdata= async ()=>{
        if(rootvalue=='Courses' || rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test' ||rootvalue=='Practice'|| rootvalue=="Story" ){
            if(usertopicname!=''||rootvalue=='Practice'){
                const data =   doc(db,   `Topics/${usertopicname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
      
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }
            }else if(usertopicname==''&& userlessonname==''){
                const data =   doc(db,   `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }       
             }else if(usertopicname=='' && userlessonname!=''){
                const data =   doc(db,   `Lessons/${userlessonname}/extraInfo/infoDoc`);
                const docSnap = await getDoc(data);
                if (docSnap.exists()) {
                  console.log("Document data:", docSnap.data());
                  setusers3(docSnap.data());
                } else {
                  // doc.data() will be undefined in this case
                  console.log("No such document!");
                }
             }
     }
     }
     const quizsoln = ()=>{
         setquizName1([])
         if(rootvalue=='Courses' || rootvalue=="Story"|| rootvalue=='Grammar' || rootvalue=='video' || rootvalue=='Test'||rootvalue=='Practice'){
            if(usertopicname!=''||rootvalue=='Practice'){
                setquizName(users3.topicQuiz);
                console.log(quizName.map((user) => { 
                   const query = {value : user, label:user}
                setquizName1(quizName1 => quizName1.concat(query))}));
            }else if(usertopicname=='' && userlessonname!=''){
                   setquizName(users3.lessonQuiz);
                   console.log(quizName.map((user) => { 
                      const query = {value : user, label:user}
                   setquizName1(quizName1 => quizName1.concat(query))})); 
            }else if(usertopicname==''&& userlessonname==''){
                setquizName(users3.courseFinalQuiz);
                   const query = {value : quizName, label:quizName}
                setquizName1(quizName1 => quizName1.concat(query));        
     }
}
     }
     const setlessonType = (e)=>{
         setuserLessonType(e.value);
         setuserlessonname('');
        setusertopicname('');
        setuserquizname('');
        setuserTopicType('');
     }
     const setTopicType = (e)=>{
    setuserTopicType(e.value);
    setusertopicname('');
    setuserquizname('');
     }
     const submitDataOnFirebase= async()=>{
        const data = doc(db, `Quizs/${userquizname}`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let quizQuestionPool =docSnap1.data().quizQuestionPool;
          if(quizQuestionPool==""){
              let questionArray = [];
            const docData = {   
                associatedLesson:userlessonname,
                associatedQuiz:userquizname,
                associatedQuizId:userquizname,
                associatedRoot:userrootname,
                associatedTopic:usertopicname, 
                associatedRootType: rootvalue,
                associatedLessonType: userLessonType,
                associatedTopicType: userTopicType,    
                poolName:QuestionPoolName,
                questionPoolId: QuestionPoolId,
                questionsId:questionArray,
                availQuestionsForCorrectImageFromWord:0,
                availQuestionsForFillTheBlanksOptions:0,
                availQuestionsForFillTheBlanksType:0,
                availQuestionsForFillTheBlanksVoice:0,
                availQuestionsForFillTheSequence:0,
                availQuestionsForIdentifyTheImage:0,
                availQuestionsForMcq:0,
                availQuestionsForReadListenSpeak:0,
                availQuestionsForTapWhatYouHear:0,
                availQuestionsForTypeWhatYouHear:0,
                availQuestionsForWordsToImage:0,
                availQuestionsInPool:0,
            };
            await setDoc(doc(db, "QuestionPool", QuestionPoolId),docData).then(async()=>{
              const washingtonRef = doc(db, "Quizs", userquizname);
             await updateDoc(washingtonRef, {quizQuestionPool: QuestionPoolId}).then(()=>{
              alert("QuestionPool Created successsfully");
              setrootvalue('');
              setuserLessonType('');
              setuserTopicType('');
              setuserlessonname('');
              setuserrootname('');
              setQuestionPoolName('');
              setQuestionPoolId('');
              setusertopicname('');
              setuserquizname('');
            })
            
            })
            
          }else{
            alert("Please Choose another Quiz");
          }
        } else {
          console.log("No such document!");
        }
     }
     function submit(e) {
      e.preventDefault();
    }
  
    return (<>
      <form id="Quiz-form" onSubmit={submit}>
    <h1 style={{ textAlign: 'center' }}>QuestionPool Form</h1>
    <label>QuestionPool Name : </label>
    <input required type="text" placeholder='QuestionPool Name' onChange={(event) => { setQuestionPoolName(event.target.value); }} />
    <br /><br />
     <label> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <button type='button' onClick={ getrootName }> Get Data</button>
     <br /><br />
     <label> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
    <div style={{display: hideTopic }}>
    <label> Choose the lesson Type : </label>
    <Select value={lesson.filter(function(option) {return option.value === userLessonType;})}  options={lesson} onChange={setlessonType} ></Select>
    <button type="button" onClick={getlessonName}> Get Data</button>
    <br /><br />
    <label> Choose the Lesson Name : </label>
    <Select value={lessonName1.filter(function(option) {return option.value === userlessonname;})} options={lessonName1} onChange={setuserlesson} ></Select>
    <br /><br />
    </div>
    <div  style={{display:hideLessonTopic}}>
    <label> Choose the Topic Type : </label>
    <Select value={topic.filter(function(option) {return option.value === userTopicType;})} options={topic} onChange={setTopicType} ></Select>
    <button type='button' onClick={ getTopicName }> Get Data</button>
    <br /><br />
    <label> Choose the Topic Name : </label>
    <Select  value={topicName1.filter(function(option) {return option.value === usertopicname;})} options={topicName1} onChange ={setTopicValue} ></Select>
    </div>
    <br /><br /><br /><hr /><br />
    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button type='submit' onClick={getQuizdata}>Confirm</button> </div>
    <br />
    <label> Choose the Quiz : </label>
    <Select value={quizName1.filter(function(option) {return option.value === userquizname;})} options={quizName1} onChange={setquizValue}></Select>
    <button type='button' onClick={quizsoln}>Show Data</button>
    <br /><br />
    <label>Question Pool Id : </label><span>{QuestionPoolId}</span>
    <br /><br /><br />
    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button type='submit' onClick={submitDataOnFirebase}>Submit</button> </div>
    </form>
    <br /><br />
        </>
    );
}
  
export default QuestionPoolForm;