import React ,{useState} from 'react';
function Checkbox() {
    const [checked, setChecked] = useState(true);
  
    return (
      <label>
        <input type="checkbox"
          defaultChecked={checked}
          onChange={() => setChecked(!checked)}
        />
        Check Me!
      </label>
    );
  }
export default Checkbox;  