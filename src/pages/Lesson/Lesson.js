import React, { useState } from 'react'
import LessonForm from './LessonForm';
import LessonTable from './LessonTable';
import './lesson.css'

function Lesson() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Lesson");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Lesson");
    }else{
     
      setvalue("Add new Lesson");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<LessonTable/>:<LessonForm/>}
      </>
  )
}

export default Lesson;