import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc , arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
import './lesson.css'
import { Button, Dialog, DialogActions, TextField } from '@mui/material';
import { DialogContent, DialogTitle } from '@material-ui/core';

function LessonTable() {
  const [LessonDetails, setLessonDetails] = useState([]);
  const [LessonDetailsCopy, setLessonDetailsCopy] = useState([]);
  const [rootName  , setrootName] = useState([]);
  const [Users1,setUsers1] = useState('');
  const [Reset, setReset] = useState(false);
  const [isDraftUSer, setisDraftUSer] = useState(false);
  const [userrootname , setuserrootname] = useState('');
  const [Open, setOpen] = useState(false);
  const [LessonId, setLessonId] = useState('');
  const [LessonName, setLessonName] = useState("");    
  const [users , setusers] = useState([]);
  const [LessonShortDesc, setLessonShortDesc] = useState('');
  const [LessonCompletiontext, setLessonCompletiontext] = useState('');
  const [LessonHeartTxn, setLessonHeartTxn] = useState(0);
  const [SampleLesson , setSampleLesson] = useState(false);
  const [SampleLessonDisable , setSampleLessonDisable] = useState(false);
  const [LessonShortDescDisable , setLessonShortDescDisable] = useState(false);
  const [TextAfterCompletionOfTopicRoundDisable , setTextAfterCompletionOfTopicRoundDisable] = useState(false);
  const [rootvalue , setrootvalue] = useState(''); 
  const [hasQuiz , sethasQuiz] = useState(false);    
  const [rootValueName , setRootValueName] = useState('');
  const usercollectionRef = collection(db, "Lessons")
  var root = [
    {
      value : 'Courses',
      label : "General Course"
    },
    {
        value : 'Grammar',
        label : 'Grammar'
    },
    {
     value : 'Practice',
     label : "Practice"
   },
   {
     value : 'video',
     label : "Video"
   },
  {
   value : 'Story',
   label : "Story"
  },
  {
   value : "Audio",
   label : "Audio"
  }
  ];
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setLessonDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setLessonDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset,Open]);
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async(i) => {
    setOpen(true);
    setLessonId(LessonDetails[i].lessonId);
    setLessonName(LessonDetails[i].lessonName);
    setLessonShortDesc(LessonDetails[i].lessonShortDesc);
    setrootvalue(LessonDetails[i].lessonAssociatedCourseType);
    setisDraftUSer(LessonDetails[i].isDraft);
    const TopicInfo = doc(db, `Lessons/${LessonDetails[i].lessonId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(TopicInfo);
    if(docSnap1.exists()){
        setLessonCompletiontext(docSnap1.data().lessonCompletiontext);
        setLessonHeartTxn(docSnap1.data().lessonHeartTxn);
        sethasQuiz(docSnap1.data().lessonHasQuiz)
    }  else {
        console.log("No such document! user 5");
      }
  };
  function submit(e) {
    e.preventDefault();
  }
  const IfWeChooseCourse=()=>{
    setSampleLessonDisable(false);
    setLessonShortDescDisable(false);
  
    setTextAfterCompletionOfTopicRoundDisable(false);
}
const IfWeChoosePractice =()=>{
  setSampleLessonDisable(true);
  setLessonShortDescDisable(true);

    setTextAfterCompletionOfTopicRoundDisable(true);
}
const IfWeChooseVideo =()=>{
          setSampleLessonDisable(true);
          setLessonShortDescDisable(true);  
    
          setTextAfterCompletionOfTopicRoundDisable(false);
}
useEffect(()=>{
    if(rootvalue=='Courses'){
      IfWeChooseCourse();
    }else if(rootvalue=='Practice'){
        IfWeChoosePractice();
    }else if(rootvalue=='Grammar'){
      IfWeChooseCourse();
    }else if(rootvalue=='video'){
      IfWeChooseVideo();
    }else if(rootvalue=="Audio"){
      IfWeChooseVideo();
    }else if(rootvalue=='Story'){
      IfWeChooseVideo();
    }
},[rootvalue])
  const updateDataOnFirebase=async()=>{
    let LessonHeartTxnAsInt
    if(LessonHeartTxn.length==0){
       LessonHeartTxnAsInt = 0;
    }else{
       LessonHeartTxnAsInt = Math.abs(parseInt(LessonHeartTxn, 10));
    }
    const TopicRef = doc(db, "Lessons", LessonId); 
    await updateDoc(TopicRef, {
      lessonShortDesc: LessonShortDesc,
      lessonName:LessonName,
      isSampleLesson:SampleLesson,
      isDraft: isDraftUSer,
    });
    const TopicInfo = doc(db, `Lessons/${LessonId}/extraInfo/infoDoc`);
    await updateDoc(TopicInfo, {
      topicCompletionText: LessonCompletiontext ,
      topicHeartTxn: LessonHeartTxnAsInt ,
      lessonHasQuiz:hasQuiz,
    });
      alert("data updated succefully");
      setOpen(false);
  }
  async function deleteImageOfQuestion(QuestionType, imageLink) {
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    if (QuestionType == 6) {
      imageUrl1 = imageLink;
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 2) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      imageUrl4 = imageLink[3];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 0) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
    }
  }
  async function deleteQuestionArray(Question) {
    const data = doc(db, `Questions/${Question}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setUsers1(docSnap.data());
      console.log("Document data:", docSnap.data().questionType);
      if (docSnap.data().questionType == 6) {
        deleteImageOfQuestion(docSnap.data().questionType, docSnap.data().questionData.imageLink);
      } else if (docSnap.data().questionType == 2) {
        let imageArray = [docSnap.data().questionData.imageOptions[0], docSnap.data().questionData.imageOptions[1], docSnap.data().questionData.imageOptions[2], docSnap.data().questionData.imageOptions[3]];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      } else if (docSnap.data().questionType == 0) {
        let imageArray = [docSnap.data().questionData.imageoptions[0].imageLink, docSnap.data().questionData.imageoptions[1].imageLink, docSnap.data().questionData.imageoptions[2].imageLink];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      }
      await deleteDoc(doc(db, "Questions", Question));
    } else {
      console.log("No such document!");
    }

  }
  async function deleteQuestionPool(quizQuestionPoolForDelete) {
    const data = doc(db, `QuestionPool/${quizQuestionPoolForDelete}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      setUsers1(docSnap1.data());
      let QuestionArray =docSnap1.data().questionsId;
      QuestionArray.map((Question) => {
        deleteQuestionArray(Question);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, "QuestionPool", quizQuestionPoolForDelete));
  }
  async function deleteQuiz(Quiz){
      const data = doc(db, `Quizs/${Quiz}`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
        deleteQuestionPool(docSnap1.data().quizQuestionPool);
      } else {
        console.log("No such document!");
      }
      await deleteDoc(doc(db, "Quizs", `${Quiz}/extraInfo/infoDoc`));
     await deleteDoc(doc(db, "Quizs", Quiz));
    
    }
  async function deleteTopic(Topic){
     const data = doc(db, `Topics/${Topic}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      let QuizArray =docSnap1.data().topicQuiz;
      QuizArray.map((Quiz) => {
        deleteQuiz(Quiz);
      });
      let topicChildItems =docSnap1.data().topicChildItems;
      topicChildItems.map((Quiz) => {
        deleteItem(Quiz);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, `Topics/${Topic}/extraInfo/infoDoc`)).then(async()=>{
      await deleteDoc(doc(db, "Topics", Topic));
    })
  
  }
  async function updateCourse(checkType,id,deltedItem,type){
  
   if(checkType=='Courses'){
      const QuestionPoolRef =   doc(db,   `Courses/${id}/extraInfo/infoDoc`);
      await updateDoc(QuestionPoolRef, { courseLessons: arrayRemove(deltedItem) });
   }else if(checkType=='Grammar'){
    const QuestionPoolRef =   doc(db,   `Grammar/${id}/extraInfo/infoDoc`);
    await updateDoc(QuestionPoolRef, { courseLessons: arrayRemove(deltedItem) });
   } else if (checkType == 'Practice') {
     if (type == "Objective") {
       const QuestionPoolRef = doc(db, `Practice/${id}/extraInfo/infoDoc`);
       await updateDoc(QuestionPoolRef, {
         courseLessons: arrayRemove(deltedItem),
       });
     } else if (type == "Arcade") {
       const QuestionPoolRef = doc(db, `Practice/${id}/extraInfo/infoDoc`);
       await updateDoc(QuestionPoolRef, {
         courseFinalArcade: "",
       });
     }
   
   }else if(checkType=='video'){
    const QuestionPoolRef =   doc(db,   `video/${id}/extraInfo/infoDoc`);
    await updateDoc(QuestionPoolRef, { courseLessons: arrayRemove(deltedItem) });
   }else if(checkType=='Story'){
    const QuestionPoolRef =   doc(db,   `Story/${id}/extraInfo/infoDoc`);
    await updateDoc(QuestionPoolRef, { courseLessons: arrayRemove(deltedItem) });
   }else if(checkType=='Audio'){
    const QuestionPoolRef =   doc(db,   `Audio/${id}/extraInfo/infoDoc`);
    await updateDoc(QuestionPoolRef, { courseLessons: arrayRemove(deltedItem) });
   }
}
const deleteItem = async(item)=>{
  const data = doc(db, `Items/${item}`);
  const docSnap1 = await getDoc(data);
  if (docSnap1.exists()) {
    let type =docSnap1.data().category;
    if(type=="PDF" || type=="audio"){
      deleteImageOfQuestion(6,docSnap1.data().url)
    }
  } else {
    console.log("No such document!");
  }
    await deleteDoc(doc(db, "Items", item));
}
  async function deleteLesson(i){
    if (window.confirm("Are you sure")) {    
    let LessonTopics = LessonDetails[i].lessonTopics;
    LessonTopics.map((topic) => {
        deleteTopic(topic);
      });
      const data = doc(db, `Lessons/${LessonDetails[i].lessonId}/extraInfo/infoDoc`);
      const docSnap1 = await getDoc(data);
      if (docSnap1.exists()) {
    let LessonQuizArray =docSnap1.data().lessonQuiz;
    LessonQuizArray.map((Quiz) => {
          deleteQuiz(Quiz);
        });
    let lessonChildItems =docSnap1.data().lessonChildItems;
    lessonChildItems.map((Quiz) => {
          deleteItem(Quiz);
        });
      } else {
        console.log("No such document!");
      }
         updateCourse(LessonDetails[i].lessonAssociatedCourseType,LessonDetails[i].lessonAssociatedCourse,LessonDetails[i].lessonId,LessonDetails[i].lessonTypeEnum);
         await deleteDoc(doc(db, `Lessons/${LessonDetails[i].lessonId}/extraInfo/infoDoc`)).then(async()=>{
          await deleteDoc(doc(db, "Lessons", LessonDetails[i].lessonId)); 
         })
     
      setReset(!Reset);
      alert("Lesson Deleted Successfully");
        }
  }
  
  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setLessonDetails([]);
      for(var i =0;i<LessonDetailsCopy.length;i++){
          if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=='Courses') {
            const query = LessonDetailsCopy[i];
            setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
      if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=='Practice'  ) {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) => LessonDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
      if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=='Grammar' ) {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
      if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=='video') {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
      if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=="Audio") {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
    if (LessonDetailsCopy[i]["lessonAssociatedCourseType"]=='Story') {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setLessonDetails([]);
  for(var i =0;i<LessonDetailsCopy.length;i++){
    console.log(LessonDetailsCopy[i]["lessonAssociatedCourse"]);
    console.log(value);
    if (LessonDetailsCopy[i]["lessonAssociatedCourse"]==value) {
        const query = LessonDetailsCopy[i];
        setLessonDetails((LessonDetails) =>  LessonDetails.concat(query));
    }
  }
};
const change=()=>{
     setLessonDetails(LessonDetailsCopy);
     setrootvalue('');
     setuserrootname('');
}
  return (
    <div className="App">
      <label className="label"> Choose the Root : </label>
      <Select
        value={root.filter(function (option) {
          return option.value === rootvalue;
        })}
        options={root}
        onChange={rootvaluefromuser}
      ></Select>
      <br />
      <br />
      <label className="label"> Choose the Root Name : </label>
      <Select
        value={rootName.filter(function (option) {
          return option.value === userrootname;
        })}
        options={rootName}
        onChange={rootnamefromuser}
      ></Select>
      <br />
      <br />
      <button
        style={{ marginLeft: "51vw" }}
        className="addButton"
        onClick={change}
      >
        Reset Filter
      </button>
      <h2>Lesson Data</h2>
      <table style={{ width: "90%" }} className="content-table">
        <thead>
          <tr>
            <th style={{ width: "30%" }}>S No.</th>
            <th style={{ width: "30%", wordWrap: "break-word" }}>
              Lesson Name
            </th>
            <th style={{ width: "20%", wordWrap: "break-word" }}>Type</th>
            <th style={{ width: "30%", wordWrap: "break-word" }}>
              Associated Parent
            </th>
          </tr>
        </thead>
        <tbody>
          {LessonDetails.length === 0 ? (
            <tr>
              <td colspan="6">No Items.</td>
            </tr>
          ) : (
            <></>
          )}
          {LessonDetails.map((Lesson, i) => {
            if (Lesson.lessonTypeEnum == 0) {
              Lesson.lessonTypeEnum = "Lesson";
            } else if (Lesson.lessonTypeEnum == 1) {
              Lesson.lessonTypeEnum = "Objective";
            } else if (Lesson.lessonTypeEnum == 2) {
              Lesson.lessonTypeEnum = "Arcade";
            }
            return (
              <tr key={i}>
                <td>{i + 1}</td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Lesson.lessonId}
                  <br />
                  <br />
                  <button className="edit" onClick={() => handleOpen(i)}>
                    Edit
                  </button>
                  <button className="delete" onClick={() => deleteLesson(i)}>
                    Delete
                  </button>
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Lesson.lessonTypeEnum}
                </td>
                <td
                  style={{
                    maxWidth: "250px",
                    wordWrap: "break-word",
                    marginLeft: "10px",
                  }}
                >
                  {Lesson.lessonAssociatedCourse}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog
        open={Open}
        onClose={handleClose}
        PaperProps={{
          style: {
            borderRadius: "20px",
            padding: "10px",
          },
        }}
      >
        <DialogTitle
          sx={{ fontSize: "24", color: "#308efe", fontWeight: "500" }}
        >
          <text style={{ color: "#308efe" }}>Update Lesson Data</text>
        </DialogTitle>
        {/* <Modal onClose={handleClose} open={Open} style={{ color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '65%', width: '50%' } {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */}
        <DialogContent>
          <form onSubmit={submit}>
            <span>Lesson Id - {LessonId}</span>
            <br />
            <label>Lesson Name : </label>
            <input
              required
              value={LessonName}
              type="text"
              placeholder="Lesson Name"
              onChange={(event) => {
                setLessonName(event.target.value);
              }}
            />{" "}
            <br />
            <br/>
            <label hidden={LessonShortDescDisable}>
              Short Description (maximum 45 characters) :{" "}
            </label>
            <br />
            <br />
            <textarea
              maxlength="45"
              cols="30"
              rows="10"
              hidden={LessonShortDescDisable}
              value={LessonShortDesc}
              type="text"
              placeholder={"Lesson Short Description"}
              onChange={(event) => {
                setLessonShortDesc(event.target.value);
              }}
            />
            <br />
            <br />
            <label hidden={TextAfterCompletionOfTopicRoundDisable}>
              {" "}
              Text After Completion of Lesson/Objective/Arcade (maximum 30
              characters) :{" "}
            </label>
            <br />
            <br />
            <textarea
              cols="30"
              rows="10"
              maxlength="30"
              hidden={TextAfterCompletionOfTopicRoundDisable}
              value={LessonCompletiontext}
              type="text"
              placeholder={"Text After Completion Topic"}
              onChange={(event) => {
                setLessonCompletiontext(event.target.value);
              }}
            />
            <br />
            <br />
            <label>
              No of hearts that are to be credited to after completion ? :{" "}
            </label>
            <br />
            <br />
            <input
              value={LessonHeartTxn}
              type="number"
              style={{ width: "50px" }}
              min="0"
              placeholder={
                "No of hearts that are to be credited to after completion"
              }
              onChange={(event) => {
                setLessonHeartTxn(event.target.value);
              }}
            />
            <br />
            <br />
            <label hidden={SampleLessonDisable}>
              Will this Lesson be a Sample Lesson ? :{" "}
            </label>
            <input
              hidden={SampleLessonDisable}
              type="checkbox"
              defaultChecked={SampleLesson}
              onChange={(event) => {
                setSampleLesson(!SampleLesson);
              }}
            />
            <br />
            <br />
            <label>isDraft : </label>
            <input
              type="checkbox"
              defaultChecked={isDraftUSer}
              onChange={(event) => {
                setisDraftUSer(!isDraftUSer);
              }}
            />
            <br />
            <br />
            <br />
            <br />
            <label>Will this lesson have any quiz ? : </label>
            <input
              type="checkbox"
              defaultChecked={hasQuiz}
              onChange={(event) => {
                sethasQuiz(!hasQuiz);
              }}
            />
            <br />
            <br />
          </form>
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={updateDataOnFirebase}>
            Submit
          </Button>
          <Button onClick={handleClose} sx={{ color: "red" }}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default LessonTable;