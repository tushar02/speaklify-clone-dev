import { db } from "../../firebase/Firebase-config";
import { useState } from "react";
import {
  collection,
  getDocs,
  setDoc,
  updateDoc,
  doc,
  getDoc,
  arrayUnion,
} from "firebase/firestore";
import Select from "react-select";
import { useEffect } from "react";
import { async } from "@firebase/util";
const LessonForm = () => {
  const [users, setusers] = useState([]);
  const [users1, setusers1] = useState([]);
  const [arc, setarc] = useState(true);
  const [rootvalue, setrootvalue] = useState("");
  const [objCon, setobjCon] = useState(true);
  const [isDraftUSer, setisDraftUSer] = useState(false);
  const [rootName, setrootName] = useState([]);
  const [lessonName, setlessonName] = useState([]);
  const [lessonName1, setlessonName1] = useState([]);
  const [userrootname, setuserrootname] = useState("");
  const [userlessonname, setuserlessonname] = useState("");
  const [userLessonType, setuserLessonType] = useState("");
  const [LessonId, setLessonId] = useState("");
  const [LessonName, setLessonName] = useState("");
  const [TextAfterCompletionOfTopicRound, setTextAfterCompletionOfTopicRound] =
    useState("");
  const [ShortDescription, setShortDescription] = useState("");
  const [
    NoOfHeartsThatAreToBeCreditedToAfterCompletion,
    setNoOfHeartsThatAreToBeCreditedToAfterCompletion,
  ] = useState(0);
  const [hasQuiz, sethasQuiz] = useState(false);
  const [SampleLesson, setSampleLesson] = useState(false);
  const [check, setcheck] = useState(false);
  const [SampleLessonDisable, setSampleLessonDisable] = useState(false);
  const [hasQuizDisable, sethasQuizDisable] = useState(false);
  const [ShortDescriptionDisable, setShortDescriptionDisable] = useState(false);
  const [
    TextAfterCompletionOfTopicRoundDisable,
    setTextAfterCompletionOfTopicRoundDisable,
  ] = useState(false);
  var root = [
    {
      value: "Courses",
      label: "General Course",
    },
    {
      value: "Grammar",
      label: "Grammar",
    },
    {
      value: "Practice",
      label: "Practice",
    },
    {
      value: "video",
      label: "Video",
    },
    {
      value: "Story",
      label: "Story",
    },
    {
      value: "Audio",
      label: "Audio",
    },
  ];

  var [lesson, setlesson] = useState([
    {
      value: 0,
      label: "Lesson",
      isDisabled: false,
    },
    {
      value: 1,
      label: "Objective",
      isDisabled: false,
    },
    {
      value: 2,
      label: "Arcade",
      isDisabled: false,
    },
  ]);
  const IfWeChooseCourse = () => {
    setSampleLessonDisable(false);
    setShortDescriptionDisable(false);
    setTextAfterCompletionOfTopicRoundDisable(false);
    sethasQuizDisable(false);
  };
  const IfWeChoosePractice = () => {
    setSampleLessonDisable(true);
    setShortDescriptionDisable(true);
    sethasQuizDisable(true);
    sethasQuiz(false);
    setTextAfterCompletionOfTopicRoundDisable(true);
  };
  const IfWeChooseVideo = () => {
    setSampleLessonDisable(true);
    setShortDescriptionDisable(true);
    sethasQuizDisable(true);
    sethasQuiz(false);
    setTextAfterCompletionOfTopicRoundDisable(false);
  };
  const IfWeChooseAudio = () => {
    setSampleLessonDisable(true);
    setShortDescriptionDisable(true);
    sethasQuizDisable(true);
    sethasQuiz(false);
    setTextAfterCompletionOfTopicRoundDisable(false);
  };
  const IfWeChooseStory = () => {
    setSampleLessonDisable(true);
    setShortDescriptionDisable(true);
    sethasQuizDisable(false);
    setTextAfterCompletionOfTopicRoundDisable(false);
  };
  useEffect(() => {
    if (rootvalue == "Courses") {
      IfWeChooseCourse();
    } else if (rootvalue == "Practice") {
      IfWeChoosePractice();
    } else if (rootvalue == "Grammar") {
      IfWeChooseCourse();
    } else if (rootvalue == "video") {
      IfWeChooseVideo();
    } else if (rootvalue == "Audio") {
      IfWeChooseAudio();
    } else if (rootvalue == "Story") {
      IfWeChooseStory();
    }
  }, [rootvalue]);
  const confirm = () => {
    setrootName([]);
    console.log(
      users.map((user) => {
        const query = { value: user.courseId, label: user.courseId };
        setrootName((rootName) => rootName.concat(query));
      })
    );
  };
  useEffect(() => {
    if (rootvalue == "Courses" || rootvalue == "Grammar") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else if (rootvalue == "Practice") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else if (
      rootvalue == "Test" ||
      rootvalue == "video" ||
      rootvalue == "Story" ||
      rootvalue == "Audio"
    ) {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    }
  }, [rootvalue]);
  useEffect(() => {
    confirm();
  }, [users]);
  const getrootName = async () => {
    const usercollectionRef = collection(db, rootvalue);
    const data = await getDocs(usercollectionRef);
    console.log(data);
    setusers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    setuserrootname();
    setuserLessonType("");
    setuserlessonname("");
  };
  const rootnamefromuser = (e) => {
    setuserrootname(e.value);
    setuserLessonType("");
    setuserlessonname("");
  };
  const createTopicId = () => {
    let date = new Date();
    let ModifiedDate = date.toString().replaceAll(" ", "").substring(0, 20);
    setLessonId(LessonName + "_" + ModifiedDate);
    setcheck(true);
  };
  const setlessonType = (e) => {
    setuserLessonType(e.value);
    if (e.value == 1) {
      setobjCon(false);
    } else {
      setobjCon(true);
    }
  };
  const submitDataOnFirebase = async () => {
    let name = LessonName;
    const spaces1 = name.length - name.replaceAll(" ", "").length;
    console.log(spaces1);
    if (check == false) {
      alert("Please confirm");
    } else {
      if (userLessonType == 1 && (LessonName.length < 10 || spaces1 != 1)) {
        alert("Pleae check Arcade Name condition");
      } else {
        let LessonChildItems = [];
        let LessonQuiz = [];
        let LessonTopics = [];
        let NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt = Math.abs(
          parseInt(NoOfHeartsThatAreToBeCreditedToAfterCompletion, 10)
        );
        const docData = {
          isSampleLesson: SampleLesson,
          lessonAssociatedCourseType: rootvalue,
          lessonAssociatedCourse: userrootname,
          isDraft: isDraftUSer,
          lessonId: LessonId,
          lessonName: LessonName,
          lessonShortDesc: ShortDescription,
          lessonTopics: LessonTopics,
          lessonTypeEnum: userLessonType,
        };
        const infoData = {
          lessonChildItems: LessonChildItems,
          lessonCompletiontext: TextAfterCompletionOfTopicRound,
          lessonHasQuiz: hasQuiz,
          lessonHeartTxn: NoOfHeartsThatAreToBeCreditedToAfterCompletionAsInt,
          lessonQuiz: LessonQuiz,
        };
        if (rootvalue == "Courses") {
          const data = doc(db, `Courses/${userrootname}/extraInfo/infoDoc`);
          await updateDoc(data, { courseLessons: arrayUnion(LessonId) }).then(
            async () => {
              await setDoc(doc(db, "Lessons", LessonId), docData)
                .then(async () => {
                  await setDoc(
                    doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                    infoData
                  );
                })
                .then(() => {
                  alert("data updated successfully");
                  document.getElementById("Lesson-form").reset();
                  setrootName([]);
                  setLessonId("");
                  setTextAfterCompletionOfTopicRound("");
                  sethasQuiz(false);
                  setrootvalue("");
                  setuserrootname("");
                  setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                  setShortDescription("");
                });
            }
          );
        } else if (rootvalue == "Grammar") {
          const data = doc(db, `Grammar/${userrootname}/extraInfo/infoDoc`);
          await updateDoc(data, { courseLessons: arrayUnion(LessonId) }).then(
            async () => {
              await setDoc(doc(db, "Lessons", LessonId), docData)
                .then(async () => {
                  await setDoc(
                    doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                    infoData
                  );
                })
                .then(() => {
                  alert("data updated successfully");
                  document.getElementById("Lesson-form").reset();
                  setrootName([]);
                  setLessonId("");
                  setTextAfterCompletionOfTopicRound("");
                  sethasQuiz(false);
                  setrootvalue("");
                  setuserrootname("");
                  setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                  setShortDescription("");
                });
            }
          );
        } else if (rootvalue == "Practice") {
          if (userLessonType == 1) {
            const data = doc(db, `Practice/${userrootname}/extraInfo/infoDoc`);
            await updateDoc(data, { courseLessons: arrayUnion(LessonId) }).then(
              async () => {
                await setDoc(doc(db, "Lessons", LessonId), docData)
                  .then(async () => {
                    await setDoc(
                      doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                      infoData
                    );
                  })
                  .then(() => {
                    alert("data updated successfully");
                    document.getElementById("Lesson-form").reset();
                    setrootName([]);
                    setLessonId("");
                    setTextAfterCompletionOfTopicRound("");
                    sethasQuiz(false);
                    setrootvalue("");
                    setuserrootname("");
                    setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                    setShortDescription("");
                  });
              }
            );
          } else if (userLessonType == 2) {
            const data = doc(db, `Practice/${userrootname}/extraInfo/infoDoc`);
            const docSnap1 = await getDoc(data);
            if (docSnap1.exists()) {
              let courseFinalArcade = docSnap1.data().courseFinalArcade;
              if (courseFinalArcade == "") {
                const data = doc(
                  db,
                  `Practice/${userrootname}/extraInfo/infoDoc`
                );
                await updateDoc(data, { courseFinalArcade: LessonId }).then(
                  async () => {
                    await setDoc(doc(db, "Lessons", LessonId), docData)
                      .then(async () => {
                        await setDoc(
                          doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                          infoData
                        );
                      })
                      .then(() => {
                        alert("data updated successfully");
                        document.getElementById("Lesson-form").reset();
                        setrootName([]);
                        setLessonId("");
                        setTextAfterCompletionOfTopicRound("");
                        sethasQuiz(false);
                        setrootvalue("");
                        setuserrootname("");
                        setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                        setShortDescription("");
                      });
                  }
                );
              } else {
                alert("Please Choose Another Arcade");
              }
            }
          }
        } else if (rootvalue == "video") {
          const data = doc(db, `video/${userrootname}/extraInfo/infoDoc`);
          const docSnap1 = await getDoc(data);
          if (docSnap1.exists()) {
            let courseLessons = docSnap1.data().courseLessons;
            if (courseLessons.length == 0) {
              const data = doc(db, `video/${userrootname}/extraInfo/infoDoc`);
              await updateDoc(data, {
                courseLessons: arrayUnion(LessonId),
              }).then(async () => {
                await setDoc(doc(db, "Lessons", LessonId), docData)
                  .then(async () => {
                    await setDoc(
                      doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                      infoData
                    );
                  })
                  .then(() => {
                    alert("data updated successfully");
                    document.getElementById("Lesson-form").reset();
                    setrootName([]);
                    setLessonId("");
                    setTextAfterCompletionOfTopicRound("");
                    sethasQuiz(false);
                    setrootvalue("");
                    setuserrootname("");
                    setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                    setShortDescription("");
                  });
              });
            } else {
              alert("Please Choose Another");
            }
          }
        } else if (rootvalue == "Test") {
          const data = doc(db, `Test/${userrootname}/extraInfo/infoDoc`);
          await updateDoc(data, { courseLessons: arrayUnion(LessonId) }).then(
            async () => {
              await setDoc(doc(db, "Lessons", LessonId), docData)
                .then(async () => {
                  await setDoc(
                    doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                    infoData
                  );
                })
                .then(() => {
                  alert("data updated successfully");
                  document.getElementById("Lesson-form").reset();
                  setrootName([]);
                  setLessonId("");
                  setTextAfterCompletionOfTopicRound("");
                  sethasQuiz(false);
                  setrootvalue("");
                  setuserrootname("");
                  setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                  setShortDescription("");
                });
            }
          );
        } else if (rootvalue == "Story") {
          const data = doc(db, `Story/${userrootname}/extraInfo/infoDoc`);
          const docSnap1 = await getDoc(data);
          if (docSnap1.exists()) {
            let courseLessons = docSnap1.data().courseLessons;
            if (courseLessons.length == 0) {
              const data = doc(db, `Story/${userrootname}/extraInfo/infoDoc`);
              await updateDoc(data, {
                courseLessons: arrayUnion(LessonId),
              }).then(async () => {
                await setDoc(doc(db, "Lessons", LessonId), docData)
                  .then(async () => {
                    await setDoc(
                      doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                      infoData
                    );
                  })
                  .then(() => {
                    alert("data updated successfully");
                    document.getElementById("Lesson-form").reset();
                    setrootName([]);
                    setLessonId("");
                    setTextAfterCompletionOfTopicRound("");
                    sethasQuiz(false);
                    setrootvalue("");
                    setuserrootname("");
                    setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                    setShortDescription("");
                  });
              });
            } else {
              alert("Please Choose Another");
            }
          }
        } else if (rootvalue == "Audio") {
          const data = doc(db, `Audio/${userrootname}/extraInfo/infoDoc`);
          const docSnap1 = await getDoc(data);
          if (docSnap1.exists()) {
            let courseLessons = docSnap1.data().courseLessons;
            if (courseLessons.length == 0) {
              const data = doc(db, `Audio/${userrootname}/extraInfo/infoDoc`);
              await updateDoc(data, {
                courseLessons: arrayUnion(LessonId),
              }).then(async () => {
                await setDoc(doc(db, "Lessons", LessonId), docData)
                  .then(async () => {
                    await setDoc(
                      doc(db, `Lessons/${LessonId}/extraInfo`, "infoDoc"),
                      infoData
                    );
                  })
                  .then(() => {
                    alert("data updated successfully");
                    document.getElementById("Lesson-form").reset();
                    setrootName([]);
                    setLessonId("");
                    setTextAfterCompletionOfTopicRound("");
                    sethasQuiz(false);
                    setrootvalue("");
                    setuserrootname("");
                    setNoOfHeartsThatAreToBeCreditedToAfterCompletion(0);
                    setShortDescription("");
                  });
              });
            } else {
              alert("Please Choose Another");
            }
          }
        }

        /*  await setDoc(doc(db, "Lessons", LessonId),docData).then(async()=>{
                await setDoc(doc(db, `Lessons/${LessonId}/extraInfo`, 'infoDoc'),infoData);
            }).then(()=>{ 
        alert("data updated successfully");
        document.getElementById("Lesson-form").reset();
            setrootName([]);
            setLessonId('');
            setrootvalue('');
            setuserrootname('');
            setarc(true);
        }) */
      }
    }
  };
  function submit(e) {
    e.preventDefault();
  }
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Lesson Form</h1>
      <form id="Lesson-form" onSubmit={submit}>
        <label> Choose the Root : </label>
        <Select
          value={root.filter(function (option) {
            return option.value === rootvalue;
          })}
          options={root}
          onChange={rootvaluefromuser}
        ></Select>
        <button required type="button" onClick={getrootName}>
          {" "}
          Get Data
        </button>
        <br />
        <br />
        <label> Choose the Root Name : </label>
        <Select
          value={rootName.filter(function (option) {
            return option.value === userrootname;
          })}
          options={rootName}
          onChange={rootnamefromuser}
        ></Select>
        <br />
        <br />
        <label> Choose the lesson Type : </label>
        <Select
          value={lesson.filter(function (option) {
            return option.value === userLessonType;
          })}
          options={lesson}
          onChange={setlessonType}
        ></Select>
        <br />
        <br />
        <label>Lesson Name : </label>
        <input
          required
          type="text"
          placeholder="Lesson Name"
          onChange={(event) => {
            setLessonName(event.target.value);
          }}
        />{" "}
        <br />
        <label hidden={objCon}>
          Objective names can be maximum 10 characters and must have one space
        </label>
        <br />
        <br />
        <label hidden={SampleLessonDisable}>
          Will this Lesson be a Sample Lesson ? :{" "}
        </label>
        <input
          type="checkbox"
          hidden={SampleLessonDisable}
          defaultChecked={SampleLesson}
          onChange={(event) => {
            setSampleLesson(!SampleLesson);
          }}
        />
        <br />
        <br />
        <label>Will this lesson have any quiz ? : </label>
        <input
          type="checkbox"
          value={hasQuiz}
          disabled={hasQuizDisable}
          defaultChecked={hasQuiz}
          onChange={(event) => {
            sethasQuiz(!hasQuiz);
          }}
        />
        <br />
        <br />
        <label>isDraft : </label>
        <input
          type="checkbox"
          defaultChecked={isDraftUSer}
          onChange={(event) => {
            setisDraftUSer(!isDraftUSer);
          }}
        />
        <br /> <br />
        {/* <label>No of hearts that are to be credited to after completion ? : </label>
    <input required type="number" style={{width:'50px'}} min="0" max={10} placeholder='No of hearts that are to be credited to after completion' onChange={(event) => { setNoOfHeartsThatAreToBeCreditedToAfterCompletion(event.target.value); }} />
    <br /><br /> */}
        <label>System Generated Unique Id : </label>
        <span>{LessonId}</span>
        <br />
        <br />
        <label hidden={ShortDescriptionDisable}>
          {" "}
          Short Description *(maximum 45 characters) :{" "}
        </label>
        <textarea
          required
          maxLength="45"
          hidden={ShortDescriptionDisable}
          cols="30"
          rows="10"
          type="text"
          placeholder="Short Description "
          onChange={(event) => {
            setShortDescription(event.target.value);
          }}
        />
        <br />
        <br />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={createTopicId}>Confirm</button>{" "}
        </div>
        <br />
        <br />
        <label hidden={TextAfterCompletionOfTopicRoundDisable}>
          Text After Completion of Lesson/Objective/Arcade (maximum 30
          characters) :{" "}
        </label>
        <textarea
          required
          maxLength="30"
          hidden={TextAfterCompletionOfTopicRoundDisable}
          cols="30"
          rows="10"
          type="text"
          placeholder="Text After Completion of Lesson/Objective/Arcade"
          onChange={(event) => {
            setTextAfterCompletionOfTopicRound(event.target.value);
          }}
        />
        <br />
        <br />
        {/*   <label>Topics under lesson : </label>
    <br /><br />
    <label>Quiz under Lesson : </label>
    <br /><br /> */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button type="submit" onClick={submitDataOnFirebase}>
            Submit
          </button>{" "}
        </div>
        <br />
        <br />
      </form>
    </>
  );
};

export default LessonForm;
/*  const data = doc(db, `Quizs/${userquizname}`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let quizQuestionPool =docSnap1.data().quizQuestionPool;
          if(quizQuestionPool==""){
              let questionArray = [];
            const docData = {   
                associatedLesson:userlessonname,
                associatedQuiz:userquizname,
                associatedRoot:userrootname,
                associatedTopic:usertopicname,     
                poolName:QuestionPoolName,
                questionPoolId: QuestionPoolId,
                questionsId:questionArray,
            };
            await setDoc(doc(db, "QuestionPool", QuestionPoolId),docData);
            const washingtonRef = doc(db, "Quizs", userquizname);
            await updateDoc(washingtonRef, {quizQuestionPool: QuestionPoolId});
            alert("QuestionPool Created successsfully");
          }else{
            alert("Please Choose another Quiz");
          }
        } else {
          console.log("No such document!");
        } */
