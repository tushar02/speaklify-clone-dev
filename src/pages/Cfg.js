import React, { useState, useEffect } from "react";
import Select from "react-select";
import {db} from '../firebase/Firebase-config';
  
function Cfg() {
  const [country, setCountry] = useState(null);
  const [lang, setLang] = useState(null);
  const [langList, setLangList] = useState([]);
  const [link, setLink] = useState("");
 
  // handle change event of the country dropdown
  const [info , setInfo] = useState([]);
  
    // Start the fetch operation as soon as
    // the page loads
    window.addEventListener('load', () => {
        Fetchdata();
      });
  
    // Fetch the required data using the get() method
    const Fetchdata = ()=>{
        db.collection("data").get().then((querySnapshot) => {
             
            // Loop through the data and store
            // it in array to display
            querySnapshot.forEach(element => {
                var data = element.data();
                setInfo(arr => [...arr , data]);
                  
            });
        })
    }
  const handleCountryChange = (obj) => {
    setCountry(obj);
    setLangList(obj.languages);
    setLang(null);
  };
 
  // handle change event of the language dropdown
  const handleLanguageChange = (obj) => {
    setLang(obj);
  };

 
 // generate the link when both dropdowns are selected
  /* useEffect(() => {
    if (country && lang) {
      setLink(`https://www.${country.url}/search?q=Clue+Mediator&gl=${country.country_code}&hl=${lang.code}`);
    }
  }, [country, lang]);  */
 
  return (
    <div className="App">
        <b>Country</b>
     
    </div>   
  );
}
 
export default Cfg;