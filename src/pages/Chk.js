import React from 'react';

function Chk() {
class Chk extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			Root : [],
			RootName : [],
			Lesson_Type : [],
			selectedCountry : '--Select Root Type--',
			selectedState : '--Choose root name--'
		};
		this.changeCountry = this.changeCountry.bind(this);
		this.changeState = this.changeState.bind(this);
	}
  
	componentDidMount() {
		this.setState({
			Root : [
				{ name: 'General Courses & Grammer', RootName: [{name: 'Gernral courses 1', Lesson_Type : ['Lesson1', 'Objective1', 'Arcade1']} ,{name: 'Gernral courses 2', Lesson_Type: ['Lesson', 'Objective', 'Arcade']} ] },
				{ name: 'Practice', RootName: [ {name: 'B', Lesson_Type: ['Barcelona']} ] },
				{ name: 'Test', RootName: [ {name: 'C', Lesson_Type: ['Downers Grove']} ] },
				{ name: 'Video', RootName: [ {name: '',  Lesson_Type: []} ] },
				{ name: 'Stroy', RootName: [ {name: 'E', Lesson_Type: ['Delhi', 'Kolkata', 'Mumbai', 'Bangalore']} ] },
			]
		});
	}
  
	changeCountry(event) {
		this.setState({selectedCountry: event.target.value});
		this.setState({RootName : this.state.Root.find(cntry => cntry.name === event.target.value).RootName});
	}

	changeState(event) {
		this.setState({selectedState: event.target.value});
		const stats = this.state.Root.find(cntry => cntry.name === this.state.selectedCountry).RootName;
		this.setState({Lesson_Type : stats.find(stat => stat.name === event.target.value).Lesson_Type});
	}
	
	render() {
		return (
			<div id="container">
	
				<div>
					<label>Root Type : </label>
					<select placeholder="Country" value={this.state.selectedCountry} onChange={this.changeCountry}>
						<option>--Choose Root--</option>
						{this.state.Root.map((e, key) => {
							return <option key={key}>{e.name}</option>;
						})}
					</select> &emsp; &emsp; &emsp;
                    <label> Root Name : </label>
					<select placeholder="State" value={this.state.selectedState} onChange={this.changeState}>
						<option>--Choose Root n--</option>
						{this.state.RootName.map((e, key) => {
							return <option key={key}>{e.name}</option>;
						})}
					</select>
				</div>
                <br />
				<div>
					<label> Lesson Type : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>&emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp;
                    <label> Lesson Name : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>
				</div>
                <br />
                <div>
					<label> Topic Type : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>&emsp; &emsp; &emsp;&emsp; &emsp; &emsp;&emsp;&emsp; 
                    <label> Topic Name : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>
				</div>
                <br />
                <div>
					<label> Quiz : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>&emsp; &emsp; &emsp;&emsp; &emsp; &emsp;&emsp; &emsp; &emsp;&emsp;
                    <label> Question Pool : </label>
					<select placeholder="City">
						<option>--Choose City--</option>
						{this.state.Lesson_Type.map((e, key) => {
							return <option key={key}>{e}</option>;
						})}
					</select>
				</div>
			</div>
		)
	}
}   
  return <div>
      <Chk/>
  </div>;
}

export default Chk;

