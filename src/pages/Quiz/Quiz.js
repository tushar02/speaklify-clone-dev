import React, { useState } from 'react';
import QuizForm from './QuizForm';
import QuizTable from './QuizTable';
import './quiz.css'

function Quiz() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Quiz");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("Back to Quiz");
    }else{
     
      setvalue("Add new Quiz");
    }
}
  return (
      <>
      <button className='addButton' onClick={change}>{value}</button>
        {Reset==false?<QuizTable/>:<QuizForm/>}
      </>
  )
}

export default Quiz;