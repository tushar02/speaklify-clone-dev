import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, getDoc, doc, deleteDoc, updateDoc , arrayRemove } from 'firebase/firestore';
import { getStorage, ref, deleteObject } from "firebase/storage";
import Select from "react-select";
import Modal from '@material-ui/core/Modal';
import './quiz.css'
import { Button, Dialog, DialogTitle, TextField } from '@mui/material';
import { DialogActions } from '@material-ui/core';
function QuizTable() {
  const [QuizDetails, setQuizDetails] = useState([]);
  const [QuizDetailsCopy, setQuizDetailsCopy] = useState([]);
  const [Users1,setUsers1] = useState('');
  const [Open, setOpen] = useState(false);
  const [rootvalue , setrootvalue] = useState('');
  const [rootName  , setrootName] = useState([]);
  const [userrootname , setuserrootname] = useState('');
  const [Reset, setReset] = useState(false);
  const [QuizId , setQuizId] = useState('');
  const [QuizName , setQuizName] = useState('');
  const [BannerAd , setBannerAd] = useState(true);
  const [lesson , setLesson]= useState([]);
  const [topic , setTopic]= useState([]);
  const [lessonUser , setLessonUser] = useState('');
  const [topicUser , setTopicUser] = useState('');
  const [MessageAfterQuiz , setMessageAfterQuiz] = useState('');
  const [PracticePoint , setPracticePoint] = useState('');
  const [ChallengeNameSeries, setChallengeNameSeries] = useState('');
  const [NoOfHeart, setNoOfHeart] = useState('');
  const [TimeLimit , setTimeLimit] = useState('');
  const [NoOfQuestion , setNoOfQuestion] = useState('');
  const [PassingScore , setPassingScore] = useState('');
  const [AllQuestionRequired , setAllQuestionRequired] = useState(false);
  const [ChallengeNameSeriesDisable , setChallengeNameSeriesDisable] = useState(false);
  const [NoofHeartsaftereverychallengeDisable , setNoofHeartsaftereverychallengeDisable] = useState(false);
  const [TimeLimitDisable , setTimeLimitDisable] = useState(false);
  const [NoofquestionsforeachchallengeDisable , setNoofquestionsforeachchallengeDisable] = useState(false);
  const [AreAllQuestionsRequiredDisable , setAreAllQuestionsRequiredDisable] = useState(true);
  const [NoofquestionsTypeWhatYouHear,setNoofquestionsTypeWhatYouHear] = useState(0);
  const [NoofquestionsChooseTheCorrectOption,setNoofquestionsChooseTheCorrectOption] = useState(0);
  const [NoofquestionsReadListenSpeak,setNoofquestionsReadListenSpeak] = useState(0);
  const [NoofquestionsFillInTheSequence,setNoofquestionsFillInTheSequence] = useState(0);
  const [NoofquestionsIdentifyTheImage,setNoofquestionsIdentifyTheImage] = useState(0);
  const [NoofquestionsChooseTheCorrectImageOfTheGivenWord,setNoofquestionsChooseTheCorrectImageOfTheGivenWord] = useState(0);
  const [NoofquestionsFillInTheBlankType,setNoofquestionsFillInTheBlankType] = useState(0);
  const [NoofquestionsPairingWordsToWords,setNoofquestionsPairingWordsToWords] = useState(0);
  const [NoofquestionsPairingWordToImages,setNoofquestionsPairingWordToImages] = useState(0);
  const [NoofquestionsTapWhatYouHear,setNoofquestionsTapWhatYouHear] = useState(0);
  const [NoofquestionsType , setNoofquestionsType] = useState(true);
  const usercollectionRef = collection(db, "Quizs");
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setQuizDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      setQuizDetailsCopy(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset,Open]);
 const handleClose = () => {
    setOpen(false);
  };
  var root = [
    {
      value : 'Courses',
      label : "General Course"
    },
    {
        value : 'Grammar',
        label : 'Grammar'
    },
    {
     value : 'Practice',
     label : "Practice"
   },
  {
   value : 'Test',
   label : "Test"
  },
  {
   value : 'Story',
   label : "Story"
  }
  ];
  const handleOpen = async(i) => {
    setOpen(true);
    setQuizId(QuizDetails[i].quizId);
    setQuizName(QuizDetails[i].quizName);
    setAllQuestionRequired(QuizDetails[i].quizShouldAnswerAllCorrect);
    setTimeLimit(QuizDetails[i].quizTimeLimit);
    setNoOfQuestion(QuizDetails[i].quizQuestionCount)
    /* setBannerAd(QuizDetails[i].quizHasBannerAd); */
    setrootvalue(QuizDetails[i].associatedRootType);
    setNoofquestionsTypeWhatYouHear(QuizDetails[i].questionsForTypeWhatYouHear);
  setNoofquestionsChooseTheCorrectOption(QuizDetails[i].questionsForMcq);
  setNoofquestionsReadListenSpeak(QuizDetails[i].questionsForReadListenSpeak);
    setNoofquestionsFillInTheSequence(QuizDetails[i].questionsForFillTheSequence);
    setNoofquestionsIdentifyTheImage(QuizDetails[i].questionsForIdentifyTheImage);
  setNoofquestionsChooseTheCorrectImageOfTheGivenWord(QuizDetails[i].questionsForCorrectImageFromWord);
  setNoofquestionsFillInTheBlankType(QuizDetails[i].questionsForFillTheBlanksType);
  setNoofquestionsPairingWordsToWords(QuizDetails[i].questionsForWordsToWords);
  setNoofquestionsPairingWordToImages(QuizDetails[i].questionsForWordsToImage);
  setNoofquestionsTapWhatYouHear(QuizDetails[i].questionsForTapWhatYouHear);
    const QuizInfo = doc(db, `Quizs/${QuizDetails[i].quizId}/extraInfo/infoDoc`);
    const docSnap1 = await getDoc(QuizInfo);
    if(docSnap1.exists()){
        setChallengeNameSeries(docSnap1.data().quizChallengeNameSeries);
        setMessageAfterQuiz(docSnap1.data().quizCompletionMessage);
        setNoOfHeart(docSnap1.data().quizHeartTxn);
        setPassingScore(docSnap1.data().quizPassingScore);
        setPracticePoint(docSnap1.data().quizChallengePracticePoint);
    }  else {
        console.log("No such document! user 5");
      }
  };
  function submit(e) {
    e.preventDefault();
  }
  const updateDataOnFirebase=async()=>{
    let  TimeLimitAsInt ;
    let NoOfQuestionAsInt;
    let PassingScoreAsInt;
    let NoOfHeartAsInt ;
    if(TimeLimit.length==0){
    TimeLimitAsInt = 0;
    }else{
      TimeLimitAsInt = parseInt(TimeLimit , 10);
    }

    if(NoOfQuestion.length==0){
      NoOfQuestionAsInt =0;
    }else{
      NoOfQuestionAsInt= parseInt(NoOfQuestion , 10);
    }


    if(PassingScore.length==0){
      PassingScoreAsInt =0;
    }else{
      PassingScoreAsInt = parseInt(PassingScore , 10);
    }
    
    if(NoOfHeart.length==0){
    NoOfHeartAsInt = 0;
    }else{
     NoOfHeartAsInt = parseInt(NoOfHeart , 10);
    }

   /*    let  TimeLimitAsInt = parseInt(TimeLimit , 10);
      let NoOfQuestionAsInt = parseInt(NoOfQuestion , 10);
      let PassingScoreAsInt = parseInt(PassingScore , 10);
      let NoOfHeartAsInt = parseInt(NoOfHeart , 10); */
      let  NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt = Math.abs(parseInt(NoofquestionsChooseTheCorrectImageOfTheGivenWord, 10));
      let  NoofquestionsFillInTheBlankTypeAsInt = Math.abs(parseInt(NoofquestionsFillInTheBlankType, 10));
      let  NoofquestionsFillInTheSequenceAsInt = Math.abs(parseInt(NoofquestionsFillInTheSequence, 10));
      let  NoofquestionsIdentifyTheImageAsInt = Math.abs(parseInt(NoofquestionsIdentifyTheImage, 10));
      let  NoofquestionsChooseTheCorrectOptionAsInt = Math.abs(parseInt(NoofquestionsChooseTheCorrectOption, 10));
      let  NoofquestionsReadListenSpeakAsInt = Math.abs(parseInt(NoofquestionsReadListenSpeak, 10));
      let  NoofquestionsTapWhatYouHearAsInt = Math.abs(parseInt(NoofquestionsTapWhatYouHear, 10));
      let  NoofquestionsTypeWhatYouHearAsInt = Math.abs(parseInt(NoofquestionsTypeWhatYouHear, 10));
      let  NoofquestionsPairingWordToImagesAsInt = Math.abs(parseInt(NoofquestionsPairingWordToImages, 10));
      let  NoofquestionsPairingWordsToWordsAsInt = Math.abs(parseInt(NoofquestionsPairingWordsToWords, 10));
      let total = NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt+NoofquestionsFillInTheBlankTypeAsInt+NoofquestionsFillInTheSequenceAsInt+NoofquestionsIdentifyTheImageAsInt+NoofquestionsChooseTheCorrectOptionAsInt+ NoofquestionsReadListenSpeakAsInt+NoofquestionsTapWhatYouHearAsInt+NoofquestionsTypeWhatYouHearAsInt+NoofquestionsPairingWordToImagesAsInt+ NoofquestionsPairingWordsToWordsAsInt;
      if(total!=NoOfQuestionAsInt){
        alert("Question for Each Challenge-> "+NoOfQuestionAsInt+"Total Question-> "+total)
      }else{
    const TopicRef = doc(db, "Quizs", QuizId);
    await updateDoc(TopicRef, {
      quizHasBannerAd:BannerAd,
      quizQuestionCount: NoOfQuestionAsInt ,
      quizShouldAnswerAllCorrect:AllQuestionRequired,
      quizTimeLimit: TimeLimitAsInt ,
      questionsForCorrectImageFromWord:NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt,
      questionsForFillTheBlanksOptions:0,
      questionsForFillTheBlanksType:NoofquestionsFillInTheBlankTypeAsInt,
      questionsForFillTheBlanksVoice:0,
      questionsForFillTheSequence:NoofquestionsFillInTheSequenceAsInt,
      questionsForIdentifyTheImage:NoofquestionsIdentifyTheImageAsInt,
      questionsForMcq:NoofquestionsChooseTheCorrectOptionAsInt,
      questionsForReadListenSpeak:NoofquestionsReadListenSpeakAsInt,
      questionsForTapWhatYouHear:NoofquestionsTapWhatYouHearAsInt,
      questionsForTypeWhatYouHear:NoofquestionsTypeWhatYouHearAsInt,
      questionsForWordsToImage:NoofquestionsPairingWordToImagesAsInt,
      questionsForWordsToWords:NoofquestionsPairingWordsToWordsAsInt,
    });
    const TopicInfo = doc(db, `Quizs/${QuizId}/extraInfo/infoDoc`);
    await updateDoc(TopicInfo, {
      quizHeartTxn:NoOfHeartAsInt,
      quizPassingScore: PassingScoreAsInt ,
      quizCompletionMessage: MessageAfterQuiz,
      quizChallengePracticePoint: PracticePoint,
      quizChallengeNameSeries: ChallengeNameSeries,
    }); 
      alert("data updated succefully");
      setOpen(false);
      
    }
  } 
  useEffect(()=>{
    if(rootvalue=='Courses' ||rootvalue=='Grammar'){
       setNoofquestionsforeachchallengeDisable(true);
       setChallengeNameSeriesDisable(true);
          setNoofHeartsaftereverychallengeDisable(true);
          setTimeLimitDisable(true);
          setNoofquestionsType(true);
   }else if(rootvalue=='Practice'){
   setTimeLimitDisable(true);
   setNoofquestionsforeachchallengeDisable(false);  
   setChallengeNameSeriesDisable(false);
      setNoofHeartsaftereverychallengeDisable(false);
      setNoofquestionsType(false);
   }else if(rootvalue=='Test'){
   setChallengeNameSeriesDisable(true);
   setNoofHeartsaftereverychallengeDisable(true);
   setNoofquestionsforeachchallengeDisable(true);
   setTimeLimitDisable(true);
   setNoofquestionsType(true);
   }else if(rootvalue=='Story'){
    setChallengeNameSeriesDisable(true);
    setNoofHeartsaftereverychallengeDisable(true);
    setNoofquestionsforeachchallengeDisable(true);
    setTimeLimitDisable(false);
    setNoofquestionsType(true);
   }
 
},[rootvalue])
  async function deleteImageOfQuestion(QuestionType, imageLink) {
    let imageUrl1 = '';
    let imageUrl2 = '';
    let imageUrl3 = '';
    let imageUrl4 = '';
    if (QuestionType == 6) {
      imageUrl1 = imageLink;
      const desertRef = ref(storage, imageUrl1);
      deleteObject(desertRef).then(() => {
        console.log("image deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 2) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      imageUrl4 = imageLink[3];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef4 = ref(storage, imageUrl4);
      deleteObject(desertRef4).then(() => {
        console.log("image4 deleted");
      }).catch((error) => {
        console.log(error);
      });
    } else if (QuestionType == 0) {
      imageUrl1 = imageLink[0];
      imageUrl2 = imageLink[1];
      imageUrl3 = imageLink[2];
      const desertRef1 = ref(storage, imageUrl1);
      deleteObject(desertRef1).then(() => {
        console.log("image1 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef2 = ref(storage, imageUrl2);
      deleteObject(desertRef2).then(() => {
        console.log("image2 deleted");
      }).catch((error) => {
        console.log(error);
      });
      const desertRef3 = ref(storage, imageUrl3);
      deleteObject(desertRef3).then(() => {
        console.log("image3 deleted");
      }).catch((error) => {
        console.log(error);
      });
    }
  }
  async function deleteQuestionArray(Question) {
    const data = doc(db, `Questions/${Question}`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setUsers1(docSnap.data());
      console.log("Document data:", docSnap.data().questionType);
      if (docSnap.data().questionType == 6) {
        deleteImageOfQuestion(docSnap.data().questionType, docSnap.data().questionData.imageLink);
      } else if (docSnap.data().questionType == 2) {
        let imageArray = [docSnap.data().questionData.imageOptions[0], docSnap.data().questionData.imageOptions[1], docSnap.data().questionData.imageOptions[2], docSnap.data().questionData.imageOptions[3]];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      } else if (docSnap.data().questionType == 0) {
        let imageArray = [docSnap.data().questionData.imageoptions[0].imageLink, docSnap.data().questionData.imageoptions[1].imageLink, docSnap.data().questionData.imageoptions[2].imageLink];
        deleteImageOfQuestion(docSnap.data().questionType, imageArray);
      }
      await deleteDoc(doc(db, "Questions", Question));
    } else {
      console.log("No such document!");
    }

  }
  async function deleteQuestionPool(quizQuestionPoolForDelete) {
    const data = doc(db, `QuestionPool/${quizQuestionPoolForDelete}`);
    const docSnap1 = await getDoc(data);
    if (docSnap1.exists()) {
      setUsers1(docSnap1.data());
      let QuestionArray =docSnap1.data().questionsId;
      QuestionArray.map((Question) => {
        deleteQuestionArray(Question);
      });
    } else {
      console.log("No such document!");
    }
    await deleteDoc(doc(db, "QuestionPool", quizQuestionPoolForDelete));
  }
  async function updateCourseLessonTopicQuiz(type,checkType,id,deleteID){
        if(type=='Course'){
       //   const QuestionPoolRef = doc(db, "Courses",id);
       if(checkType=='Courses'){
          const QuestionPoolRef =   doc(db,   `Courses/${id}/extraInfo/infoDoc`);
          await updateDoc(QuestionPoolRef, { courseFinalQuiz:""});
       }else if(checkType=='Grammar'){
        const QuestionPoolRef =   doc(db,   `Grammar/${id}/extraInfo/infoDoc`);
        await updateDoc(QuestionPoolRef, { courseFinalQuiz:""});
       }else if(checkType=='Practice'){
        const QuestionPoolRef =   doc(db,   `Practice/${id}/extraInfo/infoDoc`);
        await updateDoc(QuestionPoolRef, { courseFinalQuiz:""});
       }else if(checkType=='Test'){
        const QuestionPoolRef =   doc(db,   `Test/${id}/extraInfo/infoDoc`);
        await updateDoc(QuestionPoolRef, { courseFinalQuiz:""});
       }else if(checkType=='Story'){
        const QuestionPoolRef =   doc(db,   `Story/${id}/extraInfo/infoDoc`);
        await updateDoc(QuestionPoolRef, { courseFinalQuiz:""});
       }
        }else if(type=='Lesson'){
          //const QuestionPoolRef = doc(db, "Lessons", id);
          const QuestionPoolRef =   doc(db,   `Lessons/${id}/extraInfo/infoDoc`);
          await updateDoc(QuestionPoolRef, { lessonQuiz: arrayRemove(deleteID)});

        }else if(type=='Topic'){
        //  const QuestionPoolRef = doc(db, "Topics", id);
          const QuestionPoolRef =   doc(db,   `Topics/${id}/extraInfo/infoDoc`);
          await updateDoc(QuestionPoolRef, { topicQuiz: arrayRemove(deleteID) });
        }
  }
  async function deleteQuiz(i){
    if (window.confirm("Are you sure")) {    
      let quizQuestionPoolForDelete = QuizDetails[i].quizQuestionPool;
      let courseQuiz = QuizDetails[i].quizAssociatedCourse;
      let topicQuiz = QuizDetails[i].quizAssociatedTopic;
      let lessonQuiz = QuizDetails[i].quizAssociatedLesson;
      let roottpye = QuizDetails[i]. associatedRootType
          deleteQuestionPool(quizQuestionPoolForDelete);
          if(topicQuiz!=""){
                console.log(topicQuiz);
                updateCourseLessonTopicQuiz('Topic',roottpye,topicQuiz,QuizDetails[i].quizId);
          }else if(lessonQuiz!=""){
                console.log(lessonQuiz);
                updateCourseLessonTopicQuiz('Lesson',roottpye,lessonQuiz,QuizDetails[i].quizId);
          }else{
               console.log(courseQuiz);
               updateCourseLessonTopicQuiz('Course',roottpye,courseQuiz,QuizDetails[i].quizId);
          }
          await deleteDoc(doc(db, "Quizs", QuizDetails[i].quizId));
          setReset(!Reset);
          alert("Deleted data successfully");
        }
  }
  const rootvaluefromuser = (e)=>{
    setrootvalue(e.value);
    var filterValue = e.value;
if(filterValue=='Courses'){
  setQuizDetails([]);
      for(var i =0;i<QuizDetailsCopy.length;i++){
          if (QuizDetailsCopy[i]["associatedRootType"]=='Courses') {
            const query = QuizDetailsCopy[i];
            setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
        }
      }
}else if(filterValue=='Practice'){
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
      if (QuizDetails[i]["associatedRootType"]=='Practice'  ) {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) => QuizDetails.concat(query));
    }
  }

}else if(filterValue=='Grammar'){
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
      if (QuizDetailsCopy[i]["associatedRootType"]=='Grammar' ) {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }

}else if(filterValue=='video'){
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
      if (QuizDetailsCopy[i]["associatedRootType"]=='video') {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }

}else if(filterValue=="Audio"){

  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
      if (QuizDetailsCopy[i]["associatedRootType"]=="Audio") {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }
}else if(filterValue=='Story'){
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
    if (QuizDetailsCopy[i]["associatedRootType"]=='Story') {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }
}
getrootName(filterValue);
}
const getrootName = async(value) =>{
  setrootName([]);
  const usercollectionRef = collection( db , value);
  const data = await getDocs(usercollectionRef).then((response)=>{
    response.docs.map((doc) => { 
      console.log(doc.data().courseId);    
      const query = {value : doc.data().courseId, label:doc.data().courseId}
    setrootName(rootName => rootName.concat(query))});
  })
};
const getlessonType = async (value) => {
  setLesson([]);  
  const data =   doc(db,   `${rootvalue}/${value}/extraInfo/infoDoc`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
   if(docSnap.data().courseFinalArcade!=undefined){
    if(docSnap.data().courseFinalArcade!=""){
    setLesson([{value : docSnap.data().courseFinalArcade, label:docSnap.data().courseFinalArcade}]);
    }
  }
     console.log(docSnap.data().courseLessons.map((user) => { 
        const query = {value : user, label:user}
     setLesson(lesson=> lesson.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const getTopicType = async (value) => {
  setTopic([]);
  const data = doc(db, `Lessons/${value}`);
  const docSnap = await getDoc(data);
  if (docSnap.exists()) {
    console.log("Document data:", docSnap.data());   
     console.log(docSnap.data().lessonTopics.map((user) => { 
        const query = {value : user, label:user}
     setTopic(topic=> topic.concat(query))}));
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  }
}
const rootnamefromuser = (e)=>{
  setuserrootname(e.value);
  let value = e.value;
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){
    if (QuizDetailsCopy[i]["quizAssociatedCourse"]==value) {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }
  getlessonType(e.value);
};
const lessonnamefromuser =(e)=>{
  setLessonUser(e.value);
  let lessonValue = e.value;
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){   
    if (QuizDetailsCopy[i]["quizAssociatedLesson"]==lessonValue) {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }
  getTopicType(lessonValue);
}
const topicnamefromuser =(e)=>{
  setTopicUser(e.value);
  let lessonValue = e.value;
  setQuizDetails([]);
  for(var i =0;i<QuizDetailsCopy.length;i++){   
    if (QuizDetailsCopy[i]["quizAssociatedTopic"]==lessonValue) {
        const query = QuizDetailsCopy[i];
        setQuizDetails((QuizDetails) =>  QuizDetails.concat(query));
    }
  }
}
const change=()=>{
     setQuizDetails(QuizDetailsCopy);
     setrootvalue('');
     setuserrootname('');
     setLessonUser('');
     setTopicUser('');
}
  return (
    <div className="App">
      <label className='label'> Choose the Root : </label>
     <Select    value={root.filter(function(option) {return option.value === rootvalue;})} options = {root} onChange={ rootvaluefromuser  } ></Select>
     <br /><br />
     <label className='label'> Choose the Root Name : </label>
    <Select value={rootName.filter(function(option) {return option.value === userrootname;})} options={rootName} onChange={rootnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Lesson Name : </label>
    <Select value={lesson.filter(function(option) {return option.value === lessonUser;})} options={lesson} onChange={lessonnamefromuser} ></Select>
    <br /><br />
     <label className='label'> Choose the Topic Name : </label>
    <Select value={topic.filter(function(option) {return option.value === topicUser;})} options={topic} onChange={topicnamefromuser} ></Select>
    <br /><br />
    <button style={{marginLeft: "51vw"}} className='addButton' onClick={change}>Reset Filter</button>
      <h2>Quiz Data</h2>
      <table className='content-table'>
        <thead>
          <tr style={{ width: "10%"}}>
            <th style={{ width: "5%"}}>S No.</th>
            <th style={{ width: "100%"}}>QuestionPool Name</th>
            <th style={{ width: "100%"}}>Associated Parent</th>
            <th style={{ width: "100%"}}>Associated Course</th>
            <th style={{ width: "100%"}}>Associated Lesson</th>
            <th style={{ width: "100%"}}>Associated Topic</th>
          </tr>
        </thead>
        <tbody>
        {QuizDetails.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {QuizDetails.map((Quiz, i) => {
            if (Quiz.quizMode == 0) {
              Quiz.quizMode = "ObjectiveMode";
            } else if (Quiz.quizMode == 1) {
              Quiz.quizMode = "ArcadeMode";
            } else if (Quiz.quizMode == 2) {
              Quiz.quizMode = 'QuizMode';
            }
            return (
              <tr key={i} >
                <td  >{i + 1}</td>
                <td  style={{maxWidth:"200px",wordWrap:"break-word",marginLeft:"10px"}}>{Quiz.quizId}
                  <br /><br />
                 <button className='edit' onClick={()=> handleOpen(i)}>Edit</button> 
                  <button className='delete' onClick={()=>deleteQuiz(i)}>Delete</button>
                </td>
                <td style={{maxWidth:"200px",wordWrap:"break-word",marginLeft:"10px"}} >{Quiz.quizMode}
                </td>
                <td style={{maxWidth:"200px",wordWrap:"break-word",marginLeft:"10px"}} >{Quiz.quizAssociatedCourse}
                </td>
                <td style={{maxWidth:"200px",wordWrap:"break-word",marginLeft:"10px"}} >{Quiz.quizAssociatedLesson}
                </td>
                <td style={{maxWidth:"200px",wordWrap:"break-word",marginLeft:"10px"}} >{Quiz.quizAssociatedTopic}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog onClose={handleClose} open={Open} style={{padding:"30px"}} PaperProps={{style:{
        borderRadius: "20px", padding:"10px"
      }}}>
        <DialogTitle sx={{fontSize:"24",color:"#308efe",fontWeight:'500'}}>Update Quiz Form</DialogTitle>
      {/*<Modal onClose={handleClose} open={Open} style={{ color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '90%', width: '60%' } {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */} 
        <div>
          <form onSubmit={submit} style={{ marginLeft: '5%',fontFamily:"sans-serif" }}>
            <span>Quiz Name -   {QuizName}</span><br /><br />
           {/*  <label>Banner ad if any ? : </label>
            <input type="checkbox" defaultChecked={BannerAd} onChange={(event) => { setBannerAd(!BannerAd)}} />
            <br /><br /> */}
            <label>Message after quiz (maximum 90 characters) : </label>
            <textarea cols="30" rows="10" maxLength='90' value={MessageAfterQuiz} type="text" placeholder={"Message after quiz"} onChange={(event) => { setMessageAfterQuiz(event.target.value); }} />
            <br /><br />
            {/* <label hidden={ChallengeNameSeriesDisable} >Challenge Name Series : </label><br/><br/>
            <TextField hidden={ChallengeNameSeriesDisable} value={ChallengeNameSeries} type="text" placeholder={"Challenge Name Series"} onChange={(event) => { setChallengeNameSeries(event.target.value); }} />
            <br /><br /> */}
            <label hidden={NoofHeartsaftereverychallengeDisable}>No of Hearts after every challenge : </label>
            <input hidden={NoofHeartsaftereverychallengeDisable} value={NoOfHeart} type="number" style={{width:'50px'}}  min="0" placeholder='No of Hearts after every challenge' onChange={(event) => { setNoOfHeart(event.target.value); }} />
            <br /><br />
            <label hidden={TimeLimitDisable}>Time Limit : </label>
            <input hidden={TimeLimitDisable} value={TimeLimit} type="number" style={{width:'50px'}}  min="0" placeholder='Time Limit' onChange={(event) => { setTimeLimit(event.target.value); }} />
            <br /><br />
             <label hidden={NoofquestionsforeachchallengeDisable}>No of questions for each challenge</label>
             <input hidden={NoofquestionsforeachchallengeDisable}value={NoOfQuestion} type="number" style={{width:'50px'}}  min="0" placeholder='No of questions for each challenge' onChange={(event) => { setNoOfQuestion(event.target.value); }} />
             <br /><br />
             <div hidden={NoofquestionsType}>
    <label >No of questions for Type what you hear : </label>
    <input  type="number" value={NoofquestionsTypeWhatYouHear}  style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsTypeWhatYouHear(event.target.value); }} />
    <br /><br /> <label  >No of questions for Choose the correct option: </label>
    <input  type="number" value={NoofquestionsChooseTheCorrectOption} style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsChooseTheCorrectOption(event.target.value); }} />
    <br /><br /> <label  >No of questions for Read Listen Speak : </label>
    <input type="number" value={NoofquestionsReadListenSpeak}  style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsReadListenSpeak(event.target.value); }} />
    <br /><br /> <label >No of questions for Fill in the sequence : </label>
    <input type="number" value={NoofquestionsFillInTheSequence}  style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsFillInTheSequence(event.target.value); }} />
    <br /><br /> <label >No of questions for Identify the image : </label>
    <input type="number"  value = {NoofquestionsIdentifyTheImage} style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsIdentifyTheImage(event.target.value); }} />
    <br /><br /> <label >No of questions for Choose the correct image of the given word : </label>
    <input type="number" value={NoofquestionsChooseTheCorrectImageOfTheGivenWord} style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsChooseTheCorrectImageOfTheGivenWord(event.target.value); }} />
    <br /><br /> <label  >No of questions for Fill in the blank (type) : </label>
    <input  type="number" value={NoofquestionsFillInTheBlankType} style={{width:'50px'}} min="0"  onChange={(event) => { setNoofquestionsFillInTheBlankType(event.target.value); }} />
    <br /><br /> <label  >No of questions for Pairing (words to words) : </label>
    <input type="number" value={NoofquestionsPairingWordsToWords}  style={{width:'50px'}} min="0" onChange={(event) => { setNoofquestionsPairingWordsToWords(event.target.value); }} />
    <br /><br /> <label  >No of questions for Pairing (word to images) : </label>
    <input  type="number" value={NoofquestionsPairingWordToImages} style={{width:'50px'}} min="0" onChange={(event) => { setNoofquestionsPairingWordToImages(event.target.value); }} />
    <br /><br /> <label >No of questions for Tap what you hear : </label>
    <input type="number" value={NoofquestionsTapWhatYouHear} style={{width:'50px'}} min="0" onChange={(event) => { setNoofquestionsTapWhatYouHear(event.target.value); }} />
    <br /><br />
    </div>
             <label>Passing Score : </label>
             <input value={PassingScore} type="number" style={{width:'50px'}}  min="0" placeholder='Passing Score' onChange={(event) => { setPassingScore(event.target.value); }} />
             <br /><br />
             <label>Are All Questions Required : </label>
             <input disabled={AreAllQuestionsRequiredDisable} type="checkbox" defaultChecked={AllQuestionRequired} onChange={(event) => { setAllQuestionRequired(!AllQuestionRequired)}} />
             <br /><br />
          </form>
        </div>
        <DialogActions><Button type="submit" onClick={updateDataOnFirebase} >Submit</Button>
            <Button onClick={handleClose} sx={{color:"red"}} >Cancel</Button></DialogActions>
        </Dialog>
    </div>
  );
}
export default QuizTable;