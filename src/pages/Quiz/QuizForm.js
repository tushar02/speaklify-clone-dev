import { db } from "../../firebase/Firebase-config";
import { useState } from "react";
import {
  collection,
  getDocs,
  setDoc,
  updateDoc,
  doc,
  getDoc,
  arrayUnion,
} from "firebase/firestore";
import Select from "react-select";
import { useEffect } from "react";
import { isDocument } from "@testing-library/user-event/dist/utils";
const QuizForm = () => {
  const [users, setusers] = useState([]);
  const [users1, setusers1] = useState([]);
  const [users2, setusers2] = useState([]);
  const [lessonDisable , setlessonDisable] = useState("");
  const [users3, setusers3] = useState([]);
  const [users4, setusers4] = useState([]);
  const [users5, setusers5] = useState([]);
  const [rootvalue, setrootvalue] = useState("");
  const [rootName, setrootName] = useState([]);
  const [lessonName, setlessonName] = useState([]);
  const [topicName, settopicName] = useState([]);
  const [quizName, setquizName] = useState([]);
  const [poolName, setpoolName] = useState([]);
  const [lessonName1, setlessonName1] = useState([]);
  const [topicName1, settopicName1] = useState([]);
  const [quizName1, setquizName1] = useState([]);
  const [poolName1, setpoolName1] = useState([]);
  const [userrootname, setuserrootname] = useState("");
  const [usermodename, setusermodename] = useState("");
  const [userlessonname, setuserlessonname] = useState("");
  const [usertopicname, setusertopicname] = useState("");
  const [userquizname, setuserquizname] = useState("");
  const [quizvalue, setquizvalue] = useState("");
  const [quizvaluecolumn, setquizvaluecolumn] = useState("");
  const [userLessonType, setuserLessonType] = useState("");
  const [userTopicType, setuserTopicType] = useState("");
  const [userquestionPool, setuserquestionPool] = useState("");
  const [QuizlId, setQuizId] = useState("");
  const [QuizName, setQuizName] = useState("");
  const [MessageAfterQuiz, setMessageAfterQuiz] = useState("");
  const [BannerAd, setBannerAd] = useState(true);
  const [AnimationTypeFromUser, setAnimationTypeFromUser] = useState("");
  const [Slug, setSlug] = useState("");
  const [PracticePoint, setPracticePoint] = useState(0);
  const [challengeNameSeries, setchallengeNameSeries] = useState("");
  const [noFfHeartsAfterEveryChallenge, setnoFfHeartsAfterEveryChallenge] =
    useState(0);
  const [timeLimit, settimeLimit] = useState(0);
  const [noOfQuestionsForEachChallenge, setnoOfQuestionsForEachChallenge] =
    useState(0);
  const [passingScore, setpassingScore] = useState(0);
  const [AllQuestionsAreRequired, setAllQuestionsAreRequired] = useState(false);
  const [check, setcheck] = useState(false);
  const [SlugDisable, setSlugDisable] = useState(false);
  const [TimeLimitDisable, setTimeLimitDisable] = useState(false);
  const [ChallengeNameSeriesDisable, setChallengeNameSeriesDisable] =
    useState(false);
  const [
    NoofHeartsaftereverychallengeDisable,
    setNoofHeartsaftereverychallengeDisable,
  ] = useState(false);
  const [
    NoofquestionsforeachchallengeDisable,
    setNoofquestionsforeachchallengeDisable,
  ] = useState(false);
  const [
    AnimationScreenAfterCompletionDisable,
    setAnimationScreenAfterCompletionDisable,
  ] = useState(false);
  const [PassingScoreDisable, setPassingScoreDisable] = useState(false);
  const [NoofquestionsType, setNoofquestionsType] = useState(true);
  const [NoofquestionsTypeWhatYouHear, setNoofquestionsTypeWhatYouHear] =
    useState(0);
  const [
    NoofquestionsChooseTheCorrectOption,
    setNoofquestionsChooseTheCorrectOption,
  ] = useState(0);
  const [NoofquestionsReadListenSpeak, setNoofquestionsReadListenSpeak] =
    useState(0);
  const [NoofquestionsFillInTheSequence, setNoofquestionsFillInTheSequence] =
    useState(0);
  const [NoofquestionsIdentifyTheImage, setNoofquestionsIdentifyTheImage] =
    useState(0);
  const [
    NoofquestionsChooseTheCorrectImageOfTheGivenWord,
    setNoofquestionsChooseTheCorrectImageOfTheGivenWord,
  ] = useState(0);
  const [NoofquestionsFillInTheBlankType, setNoofquestionsFillInTheBlankType] =
    useState(0);
  const [
    NoofquestionsPairingWordsToWords,
    setNoofquestionsPairingWordsToWords,
  ] = useState(0);
  const [
    NoofquestionsPairingWordToImages,
    setNoofquestionsPairingWordToImages,
  ] = useState(0);
  const [NoofquestionsTapWhatYouHear, setNoofquestionsTapWhatYouHear] =
    useState(0);
  var root = [
    {
      value: "Courses",
      label: "General Course",
    },
    {
      value: "Grammar",
      label: "Grammar",
    },
    {
      value: "Practice",
      label: "Practice",
    },
    {
      value: "Test",
      label: "Test",
    },
    {
      value: "Story",
      label: "Story",
    },
  ];
  const [Mode, setMode] = useState([
    {
      value: 0,
      label: "ObjectiveMode",
      isDisabled: false,
    },
    {
      value: 1,
      label: "ArcadeMode",
      isDisabled: false,
    },
    {
      value: 2,
      label: "QuizMode",
      isDisabled: false,
    },
  ]);

  var [lesson, setlesson] = useState([
    {
      value: "Courses",
      label: "Lesson",
      isDisabled: false,
    },
    {
      value: "Objective",
      label: "Objective",
      isDisabled: false,
    },
    {
      value: "Arcade",
      label: "Arcade",
      isDisabled: false,
    },
  ]);
  var [topic, settopic] = useState([
    {
      value: "Lessons",
      label: "Topic",
      isDisabled: false,
    },
    {
      value: "Round",
      label: "Round",
      isDisabled: false,
    },
  ]);

  const getlessonType = async () => {
    const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
    const docSnap = await getDoc(data);
    if (docSnap.exists()) {
      setusers1(docSnap.data());
      console.log("Document data:", docSnap.data());
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
    }
  };
  const getTopicType = async () => {
    const data = doc(db, `Lessons/${userlessonname}`);
    const docSnap = await getDoc(data);

    if (docSnap.exists()) {
      console.log("Document data:", docSnap.data());
      setusers2(docSnap.data());
    } else {
      console.log("No such document!");
    }
  };
  const confirm = () => {
    setrootName([]);
    console.log(
      users.map((user) => {
        const query = { value: user.courseId, label: user.courseId };
        setrootName((rootName) => rootName.concat(query));
      })
    );
  };
  useEffect(() => {
    if (rootvalue == "Courses" || rootvalue == "Grammar") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setNoofquestionsforeachchallengeDisable(true);
      setSlugDisable(true);
      setlessonDisable(false);
      setChallengeNameSeriesDisable(true);
      setNoofHeartsaftereverychallengeDisable(true);
      setTimeLimitDisable(true);
      setAnimationScreenAfterCompletionDisable(false);
      setAllQuestionsAreRequired(false);
      setNoofquestionsType(true);
    } else if (rootvalue == "Practice") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setSlugDisable(true);
      setTimeLimitDisable(true);
      setNoofquestionsforeachchallengeDisable(false);
      setlessonDisable("none")
      setChallengeNameSeriesDisable(false);
      setNoofHeartsaftereverychallengeDisable(false);
      setlessonDisable(false);
      setAnimationScreenAfterCompletionDisable(false);
      setAllQuestionsAreRequired(true);
      setNoofquestionsType(false);
    } else if (rootvalue == "Test") {
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setMode((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setAnimationScreenAfterCompletionDisable("none");
      setlessonDisable("none");
      setSlugDisable(false);
      setChallengeNameSeriesDisable(true);
      setNoofHeartsaftereverychallengeDisable(true);
      setNoofquestionsforeachchallengeDisable(true);
      setTimeLimitDisable(false);
      setAllQuestionsAreRequired(false);
      setNoofquestionsType(true);
    } else if (rootvalue == "Story") {
      setMode((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setlesson((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2 ||idx==0) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      settopic((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
      setAnimationScreenAfterCompletionDisable("none");
      setlessonDisable("none");
      setSlugDisable(false);
      setChallengeNameSeriesDisable(true);
      setNoofHeartsaftereverychallengeDisable(true);
      setNoofquestionsforeachchallengeDisable(true);
      setTimeLimitDisable(false);
      setAllQuestionsAreRequired(false);
      setNoofquestionsType(true);
    }
  }, [rootvalue]);
  useEffect(() => {
    confirm();
  }, [users]);
  useEffect(() => {
    getlessonType();
    if (userLessonType == "Courses") {
      setMode((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 1) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else if (userLessonType == "Objective") {
      setMode((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 1 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    } else if (userLessonType == "Arcade") {
      setMode((prev) => {
        return prev.filter((curvalue, idx) => {
          if (idx == 0 || idx == 2) {
            curvalue.isDisabled = true;
          } else {
            curvalue.isDisabled = false;
          }
          return curvalue;
        });
      });
    }
  }, [userLessonType]);
  useEffect(() => {
    if (usermodename == 1 || usermodename == 2) {
      setPassingScoreDisable(false);
    } else {
      setPassingScoreDisable(true);
    }
  }, [usermodename]);
  useEffect(() => {
    getlessonName();
  }, [users1]);
  useEffect(() => {
    getTopicType();
  }, [userTopicType]);
  useEffect(() => {
    getTopicName();
  }, [users2]);
  const getlessonName = () => {
    if (userLessonType == "Arcade") {
      if (users1.courseFinalArcade != undefined) {
        setlessonName1([]);
        setlessonName1([
          { value: users1.courseFinalArcade, label: users1.courseFinalArcade },
        ]);
      }
    } else {
      setlessonName(users1.courseLessons);
      if (lessonName != undefined) {
        setlessonName1([]);
        console.log(
          lessonName.map((user) => {
            const query = { value: user, label: user };
            setlessonName1((lessonName1) => lessonName1.concat(query));
          })
        );
      }
    }
  };
  const getTopicName = () => {
    settopicName(users2.lessonTopics);
    if (topicName != undefined) {
      settopicName1([]);
      console.log(
        topicName.map((user) => {
          const query = { value: user, label: user };
          settopicName1((topicName1) => topicName1.concat(query));
        })
      );
    }
  };
  const getrootName = async () => {
    const usercollectionRef = collection(db, rootvalue);
    const data = await getDocs(usercollectionRef);
    console.log(data);
    setusers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };
  const rootvaluefromuser = (e) => {
    setrootvalue(e.value);
    setuserrootname();
    setuserLessonType("");
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserLessonType("");
    setuserTopicType("");
    setusermodename("");
  };
  const rootnamefromuser = (e) => {
    setuserrootname(e.value);
    setuserLessonType("");
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserLessonType("");
    setuserTopicType("");
    setusermodename("");
  };
  const setuserlesson = (e) => {
    setuserlessonname(e.value);
    setusertopicname("");
    setuserquizname("");
    setuserTopicType("");
    setusermodename("");
  };
  const setTopicValue = (e) => {
    setusertopicname(e.value);
    setuserquizname("");
    setusermodename("");
  };
  const setquizValue = () => {
    // let date = new Date();
    // let ModifiedDate = date.toString().replaceAll(" ", "").substring(0, 20);
    // setQuizId(QuizName + "_" + ModifiedDate);


    
    let date = new Date();
    let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '').toLowerCase();
     
    let final = (QuizName.toLowerCase()).replace(/[^a-z0-9 -]/gi, '');
    let newfinaldata=final.replace(/\s+/, "") 
    setQuizId(newfinaldata+ModifiedDate)
    //console.log(newfinaldata+ModifiedDate)
    setcheck(true);
  };
  const setlessonType = (e) => {
    setuserLessonType(e.value);
    setuserlessonname("");
    setusertopicname("");
    setuserquizname("");
    setuserTopicType("");
    setusermodename("");
  };
  const setTopicType = (e) => {
    setuserTopicType(e.value);
    setusertopicname("");
    setuserquizname("");
    setusermodename("");
    setusermodename("");
  };
  const setModeType = (e) => {
    setusermodename(e.value);
    if (e.value == 1 || e.value == 2) {
      setAllQuestionsAreRequired(false);
    }
  };
  const setAnimationType = (e) => {
    setAnimationTypeFromUser(e.value);
  };
  const submitDataOnFirebase = async () => {
    let noOfQuestionsForEachChallengeAsInt = Math.abs(
      parseInt(noOfQuestionsForEachChallenge, 10)
    );
    let timeLimitAsInt = Math.abs(parseInt(timeLimit, 10));
    let PracticePointAsInt = Math.abs(parseInt(PracticePoint, 10));
    let noFfHeartsAfterEveryChallengeAsInt = Math.abs(
      parseInt(noFfHeartsAfterEveryChallenge, 10)
    );
    let passingScoreAsInt = Math.abs(parseInt(passingScore, 10));
    let NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt = Math.abs(
      parseInt(NoofquestionsChooseTheCorrectImageOfTheGivenWord, 10)
    );
    let NoofquestionsFillInTheBlankTypeAsInt = Math.abs(
      parseInt(NoofquestionsFillInTheBlankType, 10)
    );
    let NoofquestionsFillInTheSequenceAsInt = Math.abs(
      parseInt(NoofquestionsFillInTheSequence, 10)
    );
    let NoofquestionsIdentifyTheImageAsInt = Math.abs(
      parseInt(NoofquestionsIdentifyTheImage, 10)
    );
    let NoofquestionsChooseTheCorrectOptionAsInt = Math.abs(
      parseInt(NoofquestionsChooseTheCorrectOption, 10)
    );
    let NoofquestionsReadListenSpeakAsInt = Math.abs(
      parseInt(NoofquestionsReadListenSpeak, 10)
    );
    let NoofquestionsTapWhatYouHearAsInt = Math.abs(
      parseInt(NoofquestionsTapWhatYouHear, 10)
    );
    let NoofquestionsTypeWhatYouHearAsInt = Math.abs(
      parseInt(NoofquestionsTypeWhatYouHear, 10)
    );
    let NoofquestionsPairingWordToImagesAsInt = Math.abs(
      parseInt(NoofquestionsPairingWordToImages, 10)
    );
    let NoofquestionsPairingWordsToWordsAsInt = Math.abs(
      parseInt(NoofquestionsPairingWordsToWords, 10)
    );
    let total =
      NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt +
      NoofquestionsFillInTheBlankTypeAsInt +
      NoofquestionsFillInTheSequenceAsInt +
      NoofquestionsIdentifyTheImageAsInt +
      NoofquestionsChooseTheCorrectOptionAsInt +
      NoofquestionsReadListenSpeakAsInt +
      NoofquestionsTapWhatYouHearAsInt +
      NoofquestionsTypeWhatYouHearAsInt +
      NoofquestionsPairingWordToImagesAsInt +
      NoofquestionsPairingWordsToWordsAsInt;
    if (total != noOfQuestionsForEachChallengeAsInt) {
      alert(
        "Question for Each Challenge-> " +
          noOfQuestionsForEachChallengeAsInt +
          "Total Question-> " +
          NoofquestionsPairingWordsToWordsAsInt
      );
    } else if (check == false) {
      alert("please confirm");
    } else {
      const docData = {
        quizAssociatedCourse: userrootname,
        quizAssociatedLesson: userlessonname,
        quizAssociatedTopic: usertopicname,
        associatedRootType: rootvalue,
        associatedLessonType: userLessonType,
        associatedTopicType: userTopicType,
        quizHasBannerAd: BannerAd,
        quizId: QuizlId,
        quizMode: usermodename,
        quizName: QuizName,
        quizQuestionCount: noOfQuestionsForEachChallengeAsInt,
        quizQuestionPool: "",
        quizShouldAnswerAllCorrect: AllQuestionsAreRequired,
        quizTimeLimit: timeLimitAsInt,
        questionsForCorrectImageFromWord:
          NoofquestionsChooseTheCorrectImageOfTheGivenWordAsInt,
        questionsForFillTheBlanksOptions: 0,
        questionsForFillTheBlanksType: NoofquestionsFillInTheBlankTypeAsInt,
        questionsForFillTheBlanksVoice: 0,
        questionsForFillTheSequence: NoofquestionsFillInTheSequenceAsInt,
        questionsForIdentifyTheImage: NoofquestionsIdentifyTheImageAsInt,
        questionsForMcq: NoofquestionsChooseTheCorrectOptionAsInt,
        questionsForReadListenSpeak: NoofquestionsReadListenSpeakAsInt,
        questionsForTapWhatYouHear: NoofquestionsTapWhatYouHearAsInt,
        questionsForTypeWhatYouHear: NoofquestionsTypeWhatYouHearAsInt,
        questionsForWordsToImage: NoofquestionsPairingWordToImagesAsInt,
        questionsForWordsToWords: NoofquestionsPairingWordsToWordsAsInt,
      };
      const infoData = {
        quizChallengeNameSeries: challengeNameSeries,
        quizChallengePracticePoint: PracticePointAsInt,
        quizCompletionMessage: MessageAfterQuiz,
        quizHeartTxn: noFfHeartsAfterEveryChallengeAsInt,
        quizPassingScore: passingScoreAsInt,
        quizSlug: Slug,
      };
      if (usertopicname != "") {
        const data = doc(db, `Topics/${usertopicname}/extraInfo/infoDoc`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let hasQuiz = docSnap1.data().topichasQuiz;
          if (hasQuiz) {
            const data = doc(db, `Topics/${usertopicname}/extraInfo/infoDoc`);
            await updateDoc(data, { topicQuiz: arrayUnion(QuizlId) });
            await setDoc(doc(db, "Quizs", QuizlId), docData).then(async () => {
              await setDoc(
                doc(db, `Quizs/${QuizlId}/extraInfo`, "infoDoc"),
                infoData
              );
            });
            alert("data updated");
            document.getElementById("Quiz-form").reset();
            setQuizId("");
            setlessonName1([]);
            setrootName([]);
            settopicName1([]);
          } else {
            alert("choose Another Topic");
          }
        } else {
          console.log("No such document!");
        }
      } else if (userlessonname != "") {
        const data = doc(db, `Lessons/${userlessonname}/extraInfo/infoDoc`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let hasQuiz = docSnap1.data().lessonHasQuiz;
          if (hasQuiz) {
            const data = doc(db, `Lessons/${userlessonname}/extraInfo/infoDoc`);
            await updateDoc(data, { lessonQuiz: arrayUnion(QuizlId) });
            await setDoc(doc(db, "Quizs", QuizlId), docData).then(async () => {
              await setDoc(
                doc(db, `Quizs/${QuizlId}/extraInfo`, "infoDoc"),
                infoData
              );
            });
            alert("data updated");
            document.getElementById("Quiz-form").reset();
            setQuizId("");
            setlessonName1([]);
            setrootName([]);
            settopicName1([]);
          } else {
            alert("choose Another Lesson");
          }
        } else {
          console.log("No such document!");
        }
      } else {
        const data = doc(db, `${rootvalue}/${userrootname}/extraInfo/infoDoc`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let CourseFinalQuiz = docSnap1.data().courseFinalQuiz;
          let hasQuiz = docSnap1.data().hasFinalQuiz;
          if (CourseFinalQuiz == "" && hasQuiz) {
            const washingtonRef = doc(
              db,
              `${rootvalue}/${userrootname}/extraInfo/infoDoc`
            );
            await updateDoc(washingtonRef, { courseFinalQuiz: QuizlId });
            await setDoc(doc(db, "Quizs", QuizlId), docData).then(async () => {
              await setDoc(
                doc(db, `Quizs/${QuizlId}/extraInfo`, "infoDoc"),
                infoData
              );
            });
            alert("data updated");
            document.getElementById("Quiz-form").reset();
            setQuizId("");
            setlessonName1([]);
            setrootName([]);
            settopicName1([]);
          } else {
            if (!hasQuiz) {
              alert("Please make quiz true in the root");
            } else {
            alert("Story already have quiz");  
            }
            
          }
        } else {
          console.log("No such document!");
        }
      }
    }
  };
  function submit(e) {
    e.preventDefault();
  }

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Quiz Form</h1>
      <form id="Quiz-form" onSubmit={submit}>
        <label>Quiz Name : </label>
        <input required
          type="text"
          placeholder="Quiz Name"
          onChange={(event) => {
            setQuizName(event.target.value);
          }}
        />
        <br />
        <br />
        <label> Choose the Root : </label>
        <Select
          value={root.filter(function (option) {
            return option.value === rootvalue;
          })}
          options={root}
          onChange={rootvaluefromuser}
        ></Select>
        <button type="button" onClick={getrootName}> Get Data</button>
        <br />
        <br />
        <label> Choose the Root Name : </label>
        <Select
          value={rootName.filter(function (option) {
            return option.value === userrootname;
          })}
          options={rootName}
          onChange={rootnamefromuser}
        ></Select>
        <br />
        <br />
        <div style={{ display: lessonDisable }}>      
            <label> Choose the lesson Type : </label>
        <Select
          value={lesson.filter(function (option) {
            return option.value === userLessonType;
          })}
          options={lesson}
          onChange={setlessonType}
        ></Select>
        <button type="button" onClick={getlessonName}> Get Data</button>
        <br />
        <br />
        <label> Choose the Lesson Name : </label>
        <Select
          value={lessonName1.filter(function (option) {
            return option.value === userlessonname;
          })}
          options={lessonName1}
          onChange={setuserlesson}
        ></Select>
        </div>
        <br />
        <br />
        <div style={{ display: AnimationScreenAfterCompletionDisable }}>
          <label> Choose the Topic Type : </label>
          <Select
            value={topic.filter(function (option) {
              return option.value === userTopicType;
            })}
            options={topic}
            onChange={setTopicType}
          ></Select>
          <button type="button" onClick={getTopicName}> Get Data</button>
          <br />
          <br />
          <label> Choose the Topic Name : </label>
          <Select
            value={topicName1.filter(function (option) {
              return option.value === usertopicname;
            })}
            options={topicName1}
            onChange={setTopicValue}
          ></Select>
          <br />
          <br />
          <br />
          <hr />
          <br />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button onClick={setquizValue}>Confirm</button>{" "}
        </div>
        <br />
        <label>Quiz Mode : </label>
        <Select
          value={Mode.filter(function (option) {
            return option.value === usermodename;
          })}
          options={Mode}
          onChange={setModeType}
        ></Select>
        <br />
        <br />
        <label>Quiz Id : </label>
        <span>{QuizlId}</span>
        <br />
        <br />
        {/*    <label> Will there be any Banner Ad ? : </label>
        <input type="checkbox" defaultChecked={BannerAd} onChange={(event) => { setBannerAd(!BannerAd) }} />
    <br /><br /> */}
        <label>Message after quiz (maximum 90 characters) : </label>
        <textarea required
          maxLength="90"
          cols="30"
          rows="10"
          type="text"
          placeholder="Message after Quiz"
          onChange={(event) => {
            setMessageAfterQuiz(event.target.value);
          }}
        />
        <br />
        <br />
        {/* <label hidden={SlugDisable}>Slug : </label>
        <textarea required
          hidden={SlugDisable}
          cols="30"
          rows="10"
          type="text"
          placeholder="Slug"
          onChange={(event) => {
            setSlug(event.target.value);
          }}
        />
        <br />
        <br /> */}
        <label>Practice Point after every challenge/quiz : </label>
        <input required
          type="number"
          style={{ width: "50px" }}
          min="0"
          placeholder="Practice Point after every challenge"
          onChange={(event) => {
            setPracticePoint(event.target.value);
          }}
        />
        {/* <br />
        <br />
        <label hidden={ChallengeNameSeriesDisable}>
          Challenge Name Series :{" "}
        </label>
        <input required
          hidden={ChallengeNameSeriesDisable}
          type="text"
          placeholder="Challenge Name Series"
          onChange={(event) => {
            setchallengeNameSeries(event.target.value);
          }}
        /> */}
        <br />
        <br />
        <label hidden={NoofHeartsaftereverychallengeDisable}>
          No of Hearts after every challenge :{" "}
        </label>
        <input required
          hidden={NoofHeartsaftereverychallengeDisable}
          type="number"
          style={{ width: "50px" }}
          min="0"
           max={50}
          placeholder="No of Hearts after every challenge"
          onChange={(event) => {
            setnoFfHeartsAfterEveryChallenge(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={TimeLimitDisable}>Time Limit : </label>
        <input required
          hidden={TimeLimitDisable}
          type="number"
          style={{ width: "50px" }}
          min="0"
          placeholder="Time Limit"
          onChange={(event) => {
            settimeLimit(event.target.value);
          }}
        />
        <br />
        <br />
        <label hidden={NoofquestionsforeachchallengeDisable}>
          No of questions for each challenge :{" "}
        </label>
        <input required
          hidden={NoofquestionsforeachchallengeDisable}
          type="number"
          style={{ width: "50px" }}
          min="0"
          placeholder="No of questions for each challenge"
          onChange={(event) => {
            setnoOfQuestionsForEachChallenge(event.target.value);
          }}
        />
        <br />
        <br />
        <div  hidden={NoofquestionsType}>
          <label>No of questions for Type what you hear : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsTypeWhatYouHear(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Choose the correct option: </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsChooseTheCorrectOption(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Read Listen Speak : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsReadListenSpeak(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Fill in the sequence : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsFillInTheSequence(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Identify the image : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsIdentifyTheImage(event.target.value);
            }}
          />
          <br />
          <br />{" "}
          <label>
            No of questions for Choose the correct image of the given word :{" "}
          </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsChooseTheCorrectImageOfTheGivenWord(
                event.target.value
              );
            }}
          />
          <br />
          <br /> <label>No of questions for Fill in the blank (type) : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsFillInTheBlankType(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Pairing (words to words) : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsPairingWordsToWords(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Pairing (word to images) : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsPairingWordToImages(event.target.value);
            }}
          />
          <br />
          <br /> <label>No of questions for Tap what you hear : </label>
          <input required
            type="number"
            style={{ width: "50px" }}
            min="0"
            onChange={(event) => {
              setNoofquestionsTapWhatYouHear(event.target.value);
            }}
          />
          <br />
          <br />
        </div>
        <label hidden={PassingScoreDisable}>Passing Score : </label>
        <input required
          hidden={PassingScoreDisable}
          type="text"
          placeholder="Passing Score"
          value={passingScore}
          onChange={(event) => {
            if (event.target.value >= 0 && event.target.value <= 100) {
              setpassingScore(event.target.value);
            } else {
              event.preventDefault();
            }
          }}
        />
        <br />
        <br />
        <label>
          Are All Questions Required : {String(AllQuestionsAreRequired)}{" "}
        </label>
        {/* <input disabled={false}   type="checkbox" value={AllQuestionsAreRequired} defaultChecked={true} onChange={(event) => { setAllQuestionsAreRequired(!AllQuestionsAreRequired) }} />
    <br /><br /> */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {" "}
          <button type="submit" onClick={submitDataOnFirebase}>Submit</button>{" "}
        </div>
        <br />
        <br />
      </form>
    </>
  );
};

export default QuizForm;
/*  const data = doc(db, `Quizs/${userquizname}`);
        const docSnap1 = await getDoc(data);
        if (docSnap1.exists()) {
          let quizQuestionPool =docSnap1.data().quizQuestionPool;
          if(quizQuestionPool==""){
              let questionArray = [];
            const docData = {   
                associatedLesson:userlessonname,
                associatedQuiz:userquizname,
                associatedRoot:userrootname,
                associatedTopic:usertopicname,     
                poolName:QuestionPoolName,
                questionPoolId: QuestionPoolId,
                questionsId:questionArray,
            };
            await setDoc(doc(db, "QuestionPool", QuestionPoolId),docData);
            const washingtonRef = doc(db, "Quizs", userquizname);
            await updateDoc(washingtonRef, {quizQuestionPool: QuestionPoolId});
            alert("QuestionPool Created successsfully");
          }else{
            alert("Please Choose another Quiz");
          }
        } else {
          console.log("No such document!");
        } */
