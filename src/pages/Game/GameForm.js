import React,{useState} from 'react'
import {db, storage} from '../../firebase/Firebase-config';
import { getDownloadURL, uploadBytesResumable} from '@firebase/storage';
import {  ref, deleteObject } from "firebase/storage";
import { doc, setDoc, Timestamp } from "firebase/firestore"; 
import './Game.css'
function GameForm() {
    const [Name , setName] = useState('');  
    const [isActiveFormUSer , setisActiveFormUSer] = useState(false);  
    const [image1, setImage1] = useState(null);
    const [file1 , setfile1] = useState(null);
    const[url1 , seturl1] = useState("");
    const [progress1 , setprogress1] = useState(0);
    const [image2, setImage2] = useState(null);
    const [file2 , setfile2] = useState(null);
    const[url2 , seturl2] = useState("");
    const [progress2 , setprogress2] = useState(0);
    const [StartDate , setStartDate] = useState('')
    const [EndDate , setEndDate] = useState('')
    const deleteImage1 = (file) =>{
        const desertRef = ref(storage, file);
       deleteObject(desertRef).then(() => {
         seturl1("");
         alert("deleted succefully")
       }).catch((error) => {
         console.log(error);
       }); 
       };
       const uploadFiles1 = (file)=>{
        if(!file) return;
        let date = new Date();
        let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
          const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
        const  uploadTask = uploadBytesResumable(storageref,file);
        uploadTask.on("state_changed",(snapshot)=>{
           const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
           setprogress1(prog);
        },(err)=>console.log(err),()=>{
           getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl1(url)));
        });
     };
    const deleteImage2 = (file) =>{
        const desertRef = ref(storage, file);
       deleteObject(desertRef).then(() => {
         seturl2("");
         alert("deleted succefully")
       }).catch((error) => {
         console.log(error);
       }); 
       };
       const uploadFiles2 = (file)=>{
        if(!file) return;
        let date = new Date();
        let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '');
          const storageref = ref(storage, `/files/${file.name}_${ModifiedDate}`);
        const  uploadTask = uploadBytesResumable(storageref,file);
        uploadTask.on("state_changed",(snapshot)=>{
           const prog = Math.round((snapshot.bytesTransferred/snapshot.totalBytes*100));
           setprogress2(prog);
        },(err)=>console.log(err),()=>{
           getDownloadURL(uploadTask.snapshot.ref).then(url =>console.log(seturl2(url)));
        });
     };
     const onImageChange1 = (event) => {
      if (event.target.files && event.target.files[0]) {
        setImage1(URL.createObjectURL(event.target.files[0]));
      }
       setfile1( event.target.files[0]);
     }
     const onImageChange2 = (event) => {
      if (event.target.files && event.target.files[0]) {
        setImage2(URL.createObjectURL(event.target.files[0]));
      }
       setfile2( event.target.files[0]);
     }
     function submit(e){
        e.preventDefault();
      }
      const submitDataOnFirebase= async()=>{
        let date = new Date();
        let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20);
        let userid = Name+"_"+ModifiedDate;  

      //   let date = new Date();
      //   let ModifiedDate =  (date.toString().replaceAll(" ","")).substring(0,20).replace(/[^a-z0-9 -]/gi, '').toLowerCase();
         
      //   let final = (Name.toLowerCase()).replace(/[^a-z0-9 -]/gi, '');
      //   let newfinaldata=final.replace(/\s+/, "") 
      //  let userid=newfinaldata+ModifiedDate
        console.log(userid)
       const docData = {   
        imageUrl:url1,
        isActive:isActiveFormUSer,
        startDate:Timestamp.fromDate(new Date(StartDate)),
        lastRefreshTime:Timestamp.fromDate(new Date()),
        endDate:Timestamp.fromDate(new Date(EndDate)),
        participants:[],
        top20:[],
        name:Name,
        id :userid
      };
      await setDoc(doc(db, "League", userid),docData).then(()=>{
      alert("data updated successfully");
      document.getElementById("League-form").reset();
      seturl1("");
    seturl2("");
    setprogress1(0);
    setprogress2(0);
    });
  }
  const SETNAMEID=(event)=>{
    setName(event.target.value);
  }
  return (
    <>
    <h1 className='heading'>League Form</h1>
    <form id="League-form" onSubmit={submit} >
        <label>Name : </label>
        <input type="text" placeholder=' Name' onChange={SETNAMEID} />
        <br /><br />
        <label>Start Date  :  </label> 
        <input type="date" onChange={(event) => { setStartDate(event.target.value); }} />
        <br /><br />
        <label>End Date  :  </label> 
        <input type="date" onChange={(event) => { setEndDate(event.target.value); }}  />
        <br /><br />
        <label >isActive  : </label>
    <input type="checkbox" defaultChecked={isActiveFormUSer} onChange={(event) => { setisActiveFormUSer(!isActiveFormUSer) }} />
    <br /><br />
    <label  >Image : </label>
    <input  type="file"  onChange={onImageChange1} />
    <span >uploaded {progress1}%</span>
        <button  onClick={()=> uploadFiles1(file1)}>upload</button>
        <button onClick={()=> deleteImage1(url1)}>Delete</button>
        <img src={url1} alt=""width="193" height="130" />
    <br /><br />
    <label  >League Badge : </label>
    <input  type="file"  onChange={onImageChange2} />
    <span >uploaded {progress2}%</span>
        <button  onClick={()=> uploadFiles2(file2)}>upload</button>
        <button onClick={()=> deleteImage2(url2)}>Delete</button>
        <img src={url2} alt=""width="193" height="130" />
    <br /><br />
    <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} > <button onClick={submitDataOnFirebase}>Submit</button> </div>
    </form>
    </>
  )
}

export default GameForm