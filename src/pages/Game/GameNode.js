import React, { useState } from 'react'
import GameForm from './GameForm';
import GameTable from './GameTable';
import './Game.css'

function GameNode() {
    const [Reset ,setReset] = useState(false);
    const [value ,setvalue] = useState("Add new Animation");
const change =()=>{
    setReset(!Reset);
    if(Reset==false){
      setvalue("back to Game Table");
    }else{
     
      setvalue("Add new Game");
    }
}
  return (
      <>
     {/*  <button className='addButton' onClick={change}>{value}</button> */}
        {Reset==false?<GameTable/>:<GameForm/>}
      </>
  )
}

export default GameNode;