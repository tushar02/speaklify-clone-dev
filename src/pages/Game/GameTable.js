import React, { useState, useEffect } from 'react';
import { db, storage } from "../../firebase/Firebase-config";
import { collection, getDocs, doc, updateDoc } from 'firebase/firestore';
import './Game.css'
import { Button, Dialog, DialogActions, TextField } from '@mui/material';
import { DialogContent, DialogTitle } from '@material-ui/core';

function GameTable() {
  const [GameDetails, setGameDetails] = useState([]);
  const [GameList, setGameList] = useState([]);
  const [Reset, setReset] = useState(false);
  const [Open, setOpen] = useState(false);
  const [GameId, setGameId] = useState('');
  const [GameLimit, setGameLimit] = useState(0);
  const usercollectionRef = collection(db, "Game");
  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usercollectionRef);
      console.log(data);
      setGameDetails(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getUsers();
  }, [Reset, Open]);
  const addForm = () => {
    const query = { wBlank:"",aWord:"" }
    setGameList(GameList => GameList.concat(query));
  }
  const deleteForm = () => {
    setGameList(GameList.splice(0, GameList.length - 1, 1))
  }
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async (i) => {
    setOpen(true);
    setGameId(GameDetails[i].gameId);
     setGameLimit(GameDetails[i].tLimit);
     setGameList(GameDetails[i].wordList)
  };
  function submit(e) {
    e.preventDefault();
  }
  const updateDataOnFirebase = async () => {
    let GameLimitAsInt = Math.abs(parseInt(GameLimit, 10));
    const TopicRef = doc(db, "Game", GameId);
    await updateDoc(TopicRef, {
      tLimit: GameLimitAsInt,
      wordList: GameList
    });
    alert("data updated succefully");
    setOpen(false);
  } 
  return (
    <div className="App">
      <h2>Game Data</h2>
      <table style={{ width: "90%" }} className='content-table' >
        <thead>
          <tr >
            <th style={{ width: "10%" }}>S No.</th>
            <th style={{ width: "30%", wordWrap: "break-word" }}>Game Name</th>
            <th style={{ width: "20%", wordWrap: "break-word" }}>Action</th>
          </tr>
        </thead>
        <tbody>
        {GameDetails.length === 0?<tr><td colspan="6">No Items.</td></tr>:<></>}
          {GameDetails.map((Game, i) => {
            return (
              <tr key={i} >
                <td  >{i + 1}</td>
                <td style={{ maxWidth: "250px", wordWrap: "break-word", marginLeft: "10px" }}>{Game.gameName}

                </td>
                <td style={{ maxWidth: "250px", wordWrap: "break-word", marginLeft: "10px" }}>

                  <button className='edit' onClick={() => handleOpen(i)}>Edit</button>
                 {/*  <button className='delete' onClick={()=>deleteLesson(i)} >Delete</button>  */}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Dialog open={Open} onClose={handleClose} PaperProps={{
        style: {
          borderRadius: "20px", padding: "10px" ,width: "25vw"
        }
      }}>
        <DialogTitle sx={{ fontSize: '24', color: "#308efe", fontWeight: '500' }}><text style={{ color: "#308efe" }}>Update Game Data</text></DialogTitle>
        {/* <Modal onClose={handleClose} open={Open} style={{ color: "#ff6666", background: '#643EE1', top: '50%', left: '60%', right: 'auto', bottom: 'auto', marginRight: '-50%', transform: 'translate(-50%, -50%)', height: '65%', width: '50%' } {  border: '2px solid #000', backgroundColor: 'gray', height:80,  width: 240,   margin: 'auto'} */}
        <DialogContent>
          <form onSubmit={submit}>
            <span>Game Id -   {GameId}</span><br /><br />
            <label >Time Limit in second : </label><br /><br />
            <TextField value={GameLimit} type="number" min ="0" placeholder={"Time Limit"} onChange={(event) => { setGameLimit(event.target.value); }} />
            <br /><br />
            {GameList.map((data, i) => {

              return (
                <>
                  <label>List  : {i+1}</label><br /><br />
                  <label>Word with Blanks : </label><br /><br />
                  <TextField maxlength="45" type="text" value={data.wBlank} placeholder={"Word with Blanks "} onChange={(event) => {/*  data.pointOfContactName = event.target.value  */
                             setGameList((prev) => {
                              return prev.filter((curvalue, idx) => {
                                if (idx == i) {
                                  curvalue.wBlank = event.target.value;
                                }
                                return curvalue;
                              })
                            })
                      }} />
                  <br /><br />
                  <label  >Actual Word : </label><br /><br />
                  <TextField maxlength="45" type="text" placeholder={"Actual Word"} value={data.aWord} onChange={(event) => {/*  data.pointOfContactName = event.target.value  */
                             setGameList((prev) => {
                              return prev.filter((curvalue, idx) => {
                                if (idx == i) {
                                  curvalue.aWord = event.target.value;
                                }
                                return curvalue;
                              })
                            })
                      }} />
                  <br /><br />
                  <DialogActions>
                    <Button onClick={deleteForm} sx={{ color: "red" }} >Delete</Button></DialogActions>
                </>
              );
            })}
            <DialogActions>
              <Button type="submit" onClick={addForm} >Add More</Button>
            </DialogActions>
          </form>
        </DialogContent>
        <DialogActions>
          <Button type="submit"  onClick={updateDataOnFirebase}  >Submit</Button>
          <Button onClick={handleClose} sx={{ color: "red" }} >Cancel</Button></DialogActions>

      </Dialog>
    </div>
  );
}
export default GameTable;