import React, { useState , useEffect} from 'react';
import {db} from "./Firebase-config";
import {collection, getDocs , addDoc} from 'firebase/firestore';
function FirebaseRW() {
    const [newName , setNewName] = useState("");
    const [newAge , setNewAge] = useState(0);
  const [users , setusers] = useState([]);
  const usercollectionRef = collection( db , "users")
  const createUser = async() =>{
           await  addDoc(usercollectionRef,{name :newName , age : newAge})
           alert("data added succefully");
  }
  useEffect(() =>{
    const getUsers = async() =>{
        const data = await getDocs(usercollectionRef);
        console.log(data);
        setusers (data.docs.map((doc) => ({...doc.data(),id: doc.id})));
    };
    getUsers();
  },[]);
  return (
    <div className="App">
        
        {users.map((user) => { 
          return( 
          <div> 
            <h2>Name : {user.name}  &emsp; Age :{user.age}</h2> 
          </div>
          );
          })}
        <input type="text"  placeholder='Name' onChange={(event)=>{setNewName(event.target.value)}}/> &emsp;
        <input type="number" placeholder='Age'  onChange={(event)=>{setNewAge(event.target.value)}} /> &emsp;
        <button onClick={createUser}>Create new user</button>   
    </div>
  );
}
export default FirebaseRW;
