import { getFirestore } from "@firebase/firestore";
import { initializeApp } from "firebase/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import { getStorage, ref } from "firebase/storage";
const firebaseConfig = {
  /*  apiKey: "AIzaSyDMrtS9WgidSDgsinbdbyrs7_96JOAoHVc",
  authDomain: "testproj-cef55.firebaseapp.com",
  databaseURL: "https://testproj-cef55-default-rtdb.firebaseio.com",
  projectId: "testproj-cef55",
  storageBucket: "testproj-cef55.appspot.com",
  messagingSenderId: "467354877034",
  appId: "1:467354877034:web:a0e1f92d82fa5ea4789e13",
  measurementId: "G-RBTDSYGHXD" */
  /*  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
    authDomain: "ispeak-5e601.firebaseapp.com",
    databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
    projectId: "ispeak-5e601",
    storageBucket: "ispeak-5e601.appspot.com",
    messagingSenderId: "286746647766",
    appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
    measurementId: "G-1MSP3H1FKM" */
  /*    apiKey: "AIzaSyBQjluGfcbsndJ6myfIixlRH6huMyKEQuo",
    authDomain: "speaklifitest.firebaseapp.com",
    databaseURL: "https://speaklifitest-default-rtdb.firebaseio.com",
    projectId: "speaklifitest",
    storageBucket: "speaklifitest.appspot.com",
    messagingSenderId: "935182218024",
    appId: "1:935182218024:web:c1de7f825490b574e3d61d",
    measurementId: "G-8L9JCD18NF" */
  /* apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
    authDomain: "ispeak-5e601.firebaseapp.com",
    databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
    projectId: "ispeak-5e601",
    storageBucket: "ispeak-5e601.appspot.com",
    messagingSenderId: "286746647766",
    appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
    measurementId: "G-1MSP3H1FKM" */

  apiKey: "AIzaSyCLNZ5WnCEHm2A71Jo9dq__XseezjsyV4A",
  authDomain: "ispeak-5e601.firebaseapp.com",
  databaseURL: "https://ispeak-5e601-default-rtdb.firebaseio.com",
  projectId: "ispeak-5e601",
  storageBucket: "ispeak-5e601.appspot.com",
  messagingSenderId: "286746647766",
  appId: "1:286746647766:web:2d0a28f9b9babdb6857d77",
  measurementId: "G-1MSP3H1FKM",

  //   apiKey: "AIzaSyAQNH9voGak2GsHFIocMZYl6PGx7YqLcFU",
  //   authDomain: "prod-speaklifi-gcp.firebaseapp.com",
  //  databaseURL: "https://prod-speaklifi-gcp-default-rtdb.firebaseio.com",
  //   projectId: "prod-speaklifi-gcp",
  //   storageBucket: "prod-speaklifi-gcp.appspot.com",
  //   messagingSenderId: "910918890270",
  //  appId: "1:910918890270:web:f60b7b2d8c1be52a2e4f0f",
  //   measurementId: "G-ZVPWG7RTKF"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
/* const auth = app.auth(); */
const storage = getStorage(app);
export { app, db, ref, storage };
