import { useEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import './sidebar.scss';
import GeneralSettings from './icons/General Settings@3x.svg';
import GeneralCourse from './icons/General Course@3x.svg';
import League from './icons/League@3x.svg';
import Lesson from './icons/Lesson@3x.svg';
import Topic from './icons/Topic@1.5x.svg';
import Item from './icons/Item.svg';
import Quiz from './icons/Quiz.svg';
import QuestionPool from './icons/Question Pool@2x.svg';
import Question from './icons/Questions@2x.svg';
import Practice from './icons/Practice@3x.svg';
import video from './icons/Video@2x.svg';
import test from './icons/Test.svg';
import Home from './icons/home@1.5x.svg'
const sidebarNavItems = [
  {
    display: "League",
    icon: <img src={League} alt="" srcset="" />,
    to: "/",
    section: "",
  },
  {
    display: "Root",
    icon: <img src={GeneralCourse} alt="" srcset="" />,
    to: "/Genral_course",
    section: "Genral_course",
  },
  {
    display: "Lesson",
    icon: <img src={Lesson} alt="" srcset="" />,
    to: "/Lesson",
    section: "Lesson",
  },
  {
    display: "Topic/Round",
    icon: <img src={Topic} alt="" srcset="" />,
    to: "/Topic",
    section: "Topic",
  },
  {
    display: "Item",
    icon: <img src={Item} alt="" srcset="" />,
    to: "/Item",
    section: "Item",
  },
  {
    display: "Quiz/Challenge",
    icon: <img src={Quiz} alt="" srcset="" />,
    to: "/Quiz",
    section: "Quiz",
  },
  {
    display: "Question Pool",
    icon: <img src={QuestionPool} alt="" srcset="" />,
    to: "/Question_pool",
    section: "Question_pool",
  },
  {
    display: "Questions",
    icon: <img src={Question} alt="" srcset="" />,
    to: "/Questions",
    section: "Questions",
  },
  {
    display: "Animation",
    icon: <img src={test} alt="" srcset="" />,
    to: "Animation",
    section: "Animation",
  },
  {
    display: "Game",
    icon: <img src={test} alt="" srcset="" />,
    to: "Game",
    section: "Game",
  },
  {
    display: "Subscription",
    icon: <img src={test} alt="" srcset="" />,
    to: "/Subscription",
    section: "Subscription",
  },
  {
    display: "Dynamic Link",
    icon: <img src={test} alt="" srcset="" />,
    to: "/dynamicLink",
    section: "dynamicLink",
  },
  {
    display: "Add New User",
    icon: <img src={test} alt="" srcset="" />,
    to: "/AddNewUser",
    section: "AddNewUser",
  } /* ,
    {
        display: 'Notification',
        icon: <img src={test} alt="" srcset="" /> ,
        to: '/Notification',
        section: 'Notification'
    }, */,
];

const Sidebar = () => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [stepHeight, setStepHeight] = useState(0);
    const sidebarRef = useRef();
    const indicatorRef = useRef();
    const location = useLocation();

    useEffect(() => {
        setTimeout(() => {
            const sidebarItem = sidebarRef.current.querySelector('.sidebar__menu__item');
            indicatorRef.current.style.height = `${sidebarItem.clientHeight}px`;
            setStepHeight(sidebarItem.clientHeight);
        }, 50);
    }, []);

    // change active index
    useEffect(() => {
        const curPath = window.location.pathname.split('/')[1];
        const activeItem = sidebarNavItems.findIndex(item => item.section === curPath);
        setActiveIndex(curPath.length === 0 ? 0 : activeItem);
    }, [location]);

    return <div className='sidebar'>
        <div className="sidebar__logo" >
      <p><img src={Home} alt="" srcset=""/> Dashboard</p>
        </div>
        <div ref={sidebarRef} className="sidebar__menu">
            <div
                ref={indicatorRef}
                className="sidebar__menu__indicator"
                style={{
                    transform: `translateX(-50%) translateY(${activeIndex * stepHeight}px)`
                }}
            ></div>
            {
                sidebarNavItems.map((item, index) => (
                    <Link to={item.to} key={index}>
                        <div className={`sidebar__menu__item ${activeIndex === index ? 'active' : ''}`}>
                            <div className="sidebar__menu__item__icon">
                                {item.icon}
                            </div>
                            <div className="sidebar__menu__item__text">
                                {item.display}
                            </div>
                        </div>
                    </Link>
                ))
            }
        </div>
    </div>;
};

export default Sidebar;
