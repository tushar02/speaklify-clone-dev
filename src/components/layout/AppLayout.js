import { Outlet } from "react-router-dom";
import Sidebar from "../sidebar/Sidebar";
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
const AppLayout = () => {
    const navigate = useNavigate();
    const goToLogin= () => navigate("/login");
    const checkAuth=()=>{
        const auth = getAuth();
        if(auth.currentUser==null){
            goToLogin();
        };
    }
  const checkLogin=()=>{
        const auth = getAuth();
        auth.onAuthStateChanged((user)=> {
        if (user) {
       
        } else {
         // No user is signed in.
         goToLogin();
        }
       });
       }
    useEffect(()=>{
        checkLogin();
    },[])
    const handleLogout = () => {
        const auth = getAuth();
    console.log(auth.currentUser);
       signOut(auth).then(() => {
            goToLogin();
        }).catch((error) => {
          // An error happened.
        }); 
        //   fire.auth().signOut();   
      };
    return <div style={{
        padding: '50px 0px 0px 370px'
    }}>
        <button  style={{border: "none",
    backgroundColor: "#308efe",
    color: "#ffffff",
    height: "35px",
    width: "180px",
    borderRadius: "3px",
    fontSize: "17px",
    lineHeight: "20px",
    cursor: "pointer",
    float: "right",
}} onClick={handleLogout} >Logout</button><br /> <br />
        <Sidebar />
        <Outlet />
    </div>;
};

export default AppLayout;