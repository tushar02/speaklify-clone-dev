
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AppLayout from './components/layout/AppLayout';
import Blank from './pages/Blank';
import Lesson from './pages/Lesson/Lesson';
import Question from './pages/Question/Question';
import Course from './pages/Course/Course';
import QuestionPool from './pages/QuestionPool/QuestionPool';
import Topic from './pages/Topic/Topic';
import Quiz from './pages/Quiz/Quiz';
import Item from './pages/Item/Item';
import SubsCription from './pages/SubsCription/SubsCription';
import Animation from './pages/Animation/Animation';
import Audio from './pages/Audio/Audio';
import A  from './pages/Audio/A';
import League from './pages/League/League';
import DynamicLink from './pages/dynamicLink/DynamicLink';
import GameNode from './pages/Game/GameNode';
import AddNewUser from './pages/AddUser/AddNewUser'
import LoginPage from './pages/AddUser/LoginPage';
// import AdminPortal from "./pages/Notification/Page/home"
// import Mail from "./pages/Notification/Page/mail";
// import Pushnoti from "./pages/Notification/Page/push";
// import Settings from "./pages/Notification/Page/settings";
// import Whatsapp from "./pages/Notification/Page/whatsapp";
import {useNavigate} from "react-router-dom"
function App() {
    return (
      /*  <FirebaseRW /> */
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<LoginPage />} />
          <Route path="/" element={<AppLayout />}>
            <Route index element={<League />} />
            <Route path='/League' element={<League />} />
            <Route path='/dynamicLink' element={<DynamicLink />} />
            <Route index element={<Course />} />
            <Route path="Genral_course" element={<Course />} />
            <Route path="/Game" element={<GameNode />} />
            <Route path="/Lesson" element={<Lesson />} />
            <Route path="/Topic" element={<Topic />} />
            <Route path="/Item" element={<Item />} />
            <Route path="/Quiz" element={<Quiz />} />
            <Route path="/Question_pool" element={<QuestionPool />} />
            <Route path="/Questions" element={<Question />} />
            <Route path="/Animation" element={<Animation />} />
            <Route path="/Subscription" element={<SubsCription />} />
            <Route path="/AddNewUser" element={<AddNewUser />} />
          </Route>
        </Routes>
      </BrowserRouter>
    );
}

export default App;